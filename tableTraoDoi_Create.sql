
CREATE TYPE [dbo].[tblMessageType] AS TABLE(
	[MessageID] [bigint] NOT NULL,
	[MessageKey] [nvarchar](250) NULL,
	[GroupID] [bigint] NOT NULL,
	[GroupKey] [nvarchar](250) NULL,
	[UserIDGui] [bigint] NULL,
	[UserNameGui] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[MessageType] [int] NULL,
	[ReplyData] [nvarchar](max) NULL,
	[TinhTrangID] [int] NULL,
	[TenTinhTrang] [nvarchar](50) NULL,
	[UserIDDaXem] [nvarchar](250) NULL,
	[FullNameDaXem] [nvarchar](4000) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[IsThuHoi] [bit] NULL,
	[UserDelMess] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](max) NULL
)
GO
/****** Object:  View [dbo].[App_TraoDoi_V2_SyncUserView]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[App_TraoDoi_V2_SyncUserView] 
	as 
	--select * from f_App_TraoDoi_V2_GetUser()
				select up.UserID,up.Username,
					up.DisplayName FullName,
					cn.Avatar,
				um.PhongBanID,dv.TenDonVi TenPhongBan,
				cv.ChucVuID,cv.TenChucVu,
				cn.SoDT,cn.Email
				from VI_PORTAL..[Users] up left join VI_MASTER..USER_MASTER um on up.UserID = um.User_PortalID
                  left join VI_MASTER..DM_DONVI dv on um.PhongBanID = dv.DonViID
                  left join VI_MASTER..CANBO_DONVI cbdv on cbdv.LaChucVuChinh = 1 and cbdv.CanBoID = um.CanBoID 
                  left join VI_MASTER..DM_CHUCVU cv on cbdv.ChucVuID = cv.ChucVuID 
                  left join VI_MASTER..CANBO cb on um.CanBoID = cb.CanBoID
                  left join VI_MASTER..CONNGUOI cn on cn.ConNguoiID = cb.ConNguoiID
                  left join VI_QLVB..DM_DONVI_NGUOIDUNG nd on nd.NguoiDungID=um.User_PortalID
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_ConnectManager]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_ConnectManager](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[UserName] [nvarchar](50) NULL,
	[DeviceID] [varchar](50) NULL,
	[Token] [nvarchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[IsOff] [bit] NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_ConnectManager] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_FileManager]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_FileManager](
	[FileID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupID] [bigint] NULL,
	[GroupKey] [nvarchar](50) NULL,
	[UserIDGui] [bigint] NULL,
	[MessageID] [bigint] NULL,
	[MessageKey] [nvarchar](250) NULL,
	[FileName] [nvarchar](250) NULL,
	[Thumbnail] [nvarchar](4000) NULL,
	[FileUrl] [nvarchar](max) NULL,
	[FileType] [int] NULL,
	[FileSize] [float] NULL,
	[MoTa] [nvarchar](4000) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_FileManager] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_GroupGhimMess]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_GroupGhimMess](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupID] [bigint] NULL,
	[GroupKey] [nvarchar](250) NULL,
	[GhimMessage] [nvarchar](max) NULL,
	[UserGhimID] [bigint] NULL,
	[Note] [nvarchar](max) NULL,
	[LoaiGhim] [int] NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_GroupGhimMess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_GroupInfo]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_GroupInfo](
	[GroupID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupKey] [nvarchar](250) NULL,
	[GroupName] [nvarchar](250) NULL,
	[GroupIcon] [nvarchar](250) NULL,
	[HinhNen] [nvarchar](250) NULL,
	[CreateUserID] [bigint] NULL,
	[CreateUserName] [nvarchar](50) NULL,
	[LastMessage] [nvarchar](max) NULL,
	[LastMessageType] [int] NULL,
	[LastMessageDate] [datetime] NULL,
	[GhimMessage] [nvarchar](max) NULL,
	[IsGroup] [bit] NULL,
	[IsDonVi] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_GroupInfo] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_MessageDetail]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_MessageDetail](
	[MessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[MessageKey] [nvarchar](250) NULL,
	[GroupID] [bigint] NULL,
	[GroupKey] [nvarchar](250) NULL,
	[UserIDGui] [bigint] NULL,
	[UserNameGui] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[MessageType] [int] NULL,
	[ReplyData] [nvarchar](max) NULL,
	[Reaction] [nvarchar](50) NULL,
	[TinhTrangID] [int] NULL,
	[TenTinhTrang] [nvarchar](50) NULL,
	[UserIDDaXem] [nvarchar](250) NULL,
	[FullNameDaXem] [nvarchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[IsThuHoi] [bit] NULL,
	[UserDelMess] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](max) NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_MessageDetail] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_TinhTrangView]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_TinhTrangView](
	[ID] [int] NOT NULL,
	[TenTinhTrang] [nvarchar](250) NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_TinhTrangView] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_UserInGroup]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_UserInGroup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupID] [bigint] NULL,
	[GroupKey] [nvarchar](250) NULL,
	[UserID] [bigint] NULL,
	[UserName] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[LastUpdate] [datetime] NULL,
	[IsOff] [bit] NULL,
	[IsDelete] [bit] NULL,
	[Deactivated] [bit] NULL,
	[CountNewMessage] [int] NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_UserInGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[App_TraoDoi_V2_UserOnline]    Script Date: 7/24/2021 10:51:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_TraoDoi_V2_UserOnline](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[UserName] [varchar](50) NULL,
	[DeviceID] [varchar](50) NULL,
	[ConnectionID] [nvarchar](250) NULL,
	[DateLogin] [datetime] NOT NULL,
 CONSTRAINT [PK_App_TraoDoi_V2_UserOnline] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_ConnectManager] ADD  CONSTRAINT [DF_App_TraoDoi_V2_ConnectManager_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_ConnectManager] ADD  CONSTRAINT [DF_App_TraoDoi_V2_ConnectManager_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_ConnectManager] ADD  CONSTRAINT [DF_App_TraoDoi_V2_ConnectManager_IsOff]  DEFAULT ((0)) FOR [IsOff]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_FileManager] ADD  CONSTRAINT [DF_App_TraoDoi_V2_FileManager_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupGhimMess] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupGhimMess_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupGhimMess] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupGhimMess_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupGhimMess] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupGhimMess_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupInfo] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupInfo_LastMessageType]  DEFAULT ((0)) FOR [LastMessageType]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupInfo] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupInfo_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupInfo] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupInfo_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_GroupInfo] ADD  CONSTRAINT [DF_App_TraoDoi_V2_GroupInfo_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_MessageDetail] ADD  CONSTRAINT [DF_App_TraoDoi_V2_MessageDetail_FullNameDaXem]  DEFAULT ('') FOR [FullNameDaXem]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_MessageDetail] ADD  CONSTRAINT [DF_App_TraoDoi_V2_MessageDetail_IsDelete]  DEFAULT ((0)) FOR [IsThuHoi]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_IsOff]  DEFAULT ((0)) FOR [IsOff]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_Deactivated]  DEFAULT ((0)) FOR [Deactivated]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserInGroup] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserInGroup_CountNewMessage]  DEFAULT ((0)) FOR [CountNewMessage]
GO
ALTER TABLE [dbo].[App_TraoDoi_V2_UserOnline] ADD  CONSTRAINT [DF_App_TraoDoi_V2_UserOnline_DateLogin]  DEFAULT (getdate()) FOR [DateLogin]
GO
