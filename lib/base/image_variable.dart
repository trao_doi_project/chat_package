class ImageVariable {
  static const String novideo = 'assets/images/novideo.jpg';
  static const String noimg = 'assets/images/noimg.png';

  // thong tin ca nhan
  static const String person = 'assets/images/person.png';
  static const String groupAvatar = 'assets/images/group_avatar.png';

  //chat
  static const String gallery = 'assets/images/gallery.png';
  static const String videoPlay = 'assets/images/play.png';
  static const String cameraPick = 'assets/images/dslr.png';
  static const String files = 'assets/images/files.png';
  static const String imageLuuTru = 'assets/images/gallery.png';
  static const String playVideo = 'assets/images/playvideo.png';
  static const String youtube = 'assets/images/youtube.png';
  static const String audio = 'assets/images/audio_icon.png';
  static const String location = 'assets/images/location_icon.png';

  //reaction
  static const String angry = 'assets/reaction/angry.gif';
  static const String angry2 = 'assets/reaction/angry2.png';
  static const String haha = 'assets/reaction/haha.gif';
  static const String haha2 = 'assets/reaction/haha2.png';
  static const String ic_like = 'assets/reaction/ic_like.png';
  static const String ic_likeA = 'assets/reaction/ic_like_fill.png';
  static const String like = 'assets/reaction/like.gif';
  static const String love = 'assets/reaction/love.gif';
  static const String love2 = 'assets/reaction/love2.png';
  static const String sad = 'assets/reaction/sad.gif';
  static const String sad2 = 'assets/reaction/sad2.png';
  static const String wow = 'assets/reaction/wow.gif';
  static const String wow2 = 'assets/reaction/wow2.png';

  static String fileExtensionIcon(String fileExtension) {
    switch (fileExtension) {
      case '.xlsx':
      case '.xls':
        return 'assets/fileicon/excel.png';
      case '.pdf':
        return 'assets/fileicon/pdf.png';
      case '.doc':
      case '.docx':
        return 'assets/fileicon/word.png';
      case '.pptx':
      case '.ppt':
        return 'assets/fileicon/powerpoint.png';
      case '.jpg':
      case '.png':
        return 'assets/images/gallery.png';
      case '.mp4':
        return 'assets/images/play.png';
      default:
        return 'assets/images/files.png';
    }
  }
}
