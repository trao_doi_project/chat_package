import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

final Rx<ChatMessage> chatMessageCtrl =
    ChatMessage(messageType: MessageType.Text).obs;
final Rx<GroupInfo> createGroupInfoCtrl = GroupInfo().obs;
final RxInt currentGroupId = (-1).obs;
final Rx<GroupInfo> insertUserInGroupCtrl = GroupInfo().obs;
final RxMap updateGroupIconAndGroupNameCtrl = <String, dynamic>{}.obs;
final RxInt deleteGroupCtrl = 0.obs;
final RxInt deleteHistoryMessageCtrl = 0.obs;
final RxMap updateTinhTrangCtrl = <String, dynamic>{}.obs;
final Rx<UserInGroup> deleteUserInGroupCtrl = UserInGroup().obs;
final RxMap thuHoiMessageByMessageIDCtrl = <String, dynamic>{}.obs;
final RxMap deleteMessageByMessageIDCtrl = <String, dynamic>{}.obs;
final RxMap updateGhimMess = <String, dynamic>{}.obs;
final RxMap updateReaction = <String, dynamic>{}.obs;
final Rx<GroupInfo> currentGroupObj = GroupInfo().obs;
final Rx<bool> isResumed = false.obs;
DateTime lastOnline = DateTime.now();

bool isFirstLoad = true;
Map<String, dynamic>? fireBaseOnLoad;
Map<String, dynamic> tempCache = <String, dynamic>{};
