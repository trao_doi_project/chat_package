class LocalVariable {
  static const String apiUser = 'vioffice';
  static const String apiPass = 'vietinfo123';

  // trao doi chính là cái chat
  static const String namePackage = 'chat_package';
  static const String spaceChar = ' ';
  static const String getUsersOnlineKey = 'GetUsersOnline';
  static const String sendMessage = 'SendMessage';
  static const String receiveMessage = 'ReceiveMessage';
  static const String groupCreated = 'GroupCreated';
  static const String getGroupCreated = 'GetGroupCreated';
  static const String lastUpdate = 'LastSyncDate';
  static const String dangSoanTin = 'DangSoanTin';
  static const String getFileMessage = 'FileReceiveMessage';
  static const String updateTinhTrangTin = 'UpdateTinhTrangTin';
  static const String insertUserInGroup = 'InsertUserInGroup';
  static const String getInsertUserInGroup = 'GetInsertUserInGroup';
  static const String deleteHistoryMessageByGroupID =
      'DeleteHistoryMessageByGroupID';
  static const String deleteGroupByGroupID = 'DeleteGroupByGroupID';
  static const String updateGroupIconAndGroupName =
      'UpdateGroupIconAndGroupName';
  static const String thuHoiMessageByMessageID = 'ThuHoiMessageByMessageID';
  static const String deleteMessageByMessageID = 'DeleteMessageByMessageID';
  static const String deleteUserInGroup = 'DeleteUserInGroup';
  static const String updateHinhNenGroup = 'UpdateHinhNenGroup';
  static const String updateGhimMessager = 'UpdateGhimMessager';
  static const String updateMessageReaction = 'UpdateMessageReaction';
  static const String UpdateTinhTrangMess = 'UpdateTinhTrangMess';
}
