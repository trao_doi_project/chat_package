import 'package:flutter/material.dart';

class BaseColor {
  static const Color titleColor = Colors.black45;
  static const Color primaryColor = Color(0xff205072);
  static const Color accentsColor = Color(0xff006885);
  static const Color backGroundColor = Color(0xfff5f5f5);
  static const Color colorStaus = Color(0xff9398b4);
}
