import 'package:chat_package/src/model/trao_doi/message.dart';

extension CheckMessType on MessageType {
  bool check() {
    switch (this) {
      case MessageType.Text:
      case MessageType.Emoticon:
        return false;
      default:
        return true;
    }
  }

  String text(String data) {
    switch (this) {
      case MessageType.Reply:
      case MessageType.Text:
        return data;
      case MessageType.Emoticon:
        return '[Sticker]';
      case MessageType.Image:
        return '[Hình ảnh]';
      case MessageType.Video:
        return '[Video]';
      case MessageType.File:
        return '[Tệp tin]';
      case MessageType.Link:
      case MessageType.Youtube:
        return '[Link]';
      case MessageType.Audio:
        return '[Audio]';
      default:
        return '';
    }
  }
}
