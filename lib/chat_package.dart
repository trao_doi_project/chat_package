library chat_package;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/chat_database.dart';
import 'package:chat_package/services/firebase_config.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/model_export.dart';
import 'package:chat_package/src/traodoi/list_group_send_share/list_group_send_share_ui.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_core_getx_dev/app/data/core_http.dart';
import 'package:flutter_core_getx_dev/app/services/core_connectivity_service.dart';
import 'package:flutter_core_getx_dev/app/utils/strings.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:flutter_screenshot_callback/flutter_screenshot_callback.dart';
import 'package:intl/intl.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';

import 'base/base_local_rx.dart';

export 'package:chat_package/src/widget_export.dart';

export 'chat_pages.dart';
export 'src/model/model_export.dart';

///3 loại link url thì nếu app của team thì dùng 1 loại api(truyền cả 3 nhưng 1 loại api) còn team khác thì truyền theo đúng chức năng của team đó
//link để show các thông tin như avatar, file,... của app khác
String baseUrlUpload = '';
//link api cho cái các chức năng khác
String baseUrlAPI = '';
//link api cho cái chat
String baseUrlApiChat = '';
String userNameChat = '';
int userIDChat = 0;
String appNameChat = '';
String fullNameChat = '';
String avatarChat = '';
String tokenChat = '';
String deviceIDChat = '';
StreamSubscription? _listionConnect;
bool _firstLoad = true;
String rootPageRouter = '';

// variable listen event has new mess
int countNewMessInChatConfig = 0;
StreamController<int> countNewMessInChatConfigCtr =
    StreamController<int>.broadcast();

class ChatConfig {
  static Future init(
      {required String baseUrlUploadDonVi,
      required String baseAPIUrlDonVi,
      required String baseUrlAPIChat,
      required int userID,
      required String userName,
      required String appName,
      required String fullName,
      required String avatar,
      required String token,
      required String deviceID,
      required String jwtToken,
      required String rootRouter}) async {
    ///set variable
    baseUrlUpload = baseUrlUploadDonVi;
    baseUrlAPI = baseAPIUrlDonVi;
    baseUrlApiChat = baseUrlAPIChat;
    userIDChat = userID;
    userNameChat = userName;
    appNameChat = appName.replaceAll(' ', '');
    fullNameChat = fullName;
    avatarChat = avatarChat;
    tokenChat = token;
    deviceIDChat = deviceID;
    rootPageRouter = rootRouter;

    final bool isLoginApp = Storage.instance.readBool('isLoginApp');
    if (!isLoginApp) {
      Storage.instance.writeString(apiAuthBearerOrBasic, 'Bearer $jwtToken');
      Storage.instance.writeBool('isLoginApp', true);
      await _appTraoDoiV2AddToken();
    }

    ScreenshotCallback.instance.startScreenshot();
    await ChatDatabase.getInstance();
    await _resetAllInstance();
    await FireBaseConfig.instance.onTerminated();
    SignalR.instance.startServer();

    _listionConnect ??
        CoreConnectivityService.instance.hasConnection.stream
            .listen((event) async {
          if (event && !_firstLoad) {
            await SignalR.instance.startServer(isConnect: true);
            _firstLoad = false;
          }
        });

    SystemChannels.lifecycle.setMessageHandler((msg) async {
      switch (msg) {
        case 'AppLifecycleState.paused':
          // ScreenshotCallback.instance.stopScreenshot();
          isResumed.value = false;
          break;
        case 'AppLifecycleState.inactive':
          isResumed.value = false;
          print('inactive');
          break;
        case 'AppLifecycleState.resumed':
          // ScreenshotCallback.instance.startScreenshot();
          isResumed.value = true;
          await SignalR.instance.startServer(isReload: true);
          print('resumed');
          break;
        case 'AppLifecycleState.suspending':
          isResumed.value = false;
          print('suspending');
          break;
        default:
      }
      return Future.value('');
    });

    await _SyncData();
  }

  static Future _resetAllInstance() async {
    ApiRequest.resetInstance();
    SyncChat.resetInstance();
    SignalR.resetInstance();
    await FireBaseConfig.instance.resetInstance();
  }

  static Future _SyncData() async {
    await SyncChat.instance.synDataUser();
    await SyncChat.instance.syncOnLoad().then((value) async {
      // SignalR.instance.updateTinhTrangReConnect();

      // get total mess not seen
      countNewMessInChatConfig = await SyncChat.instance.getTotalMessNotSeen();
      countNewMessInChatConfigCtr.sink.add(countNewMessInChatConfig);
    });
  }

  static Future<bool> removeAll() async {
    _listionConnect?.cancel();
    await cleanCache();
    final bool _data = await ApiRequest.instance.deleteConnection(
        userID: userIDChat, token: tokenChat, deviceID: deviceIDChat);
    return _data;
  }

  static void receiveShare() {
    // For sharing images coming from outside the app while the app is in the memory
    ReceiveSharingIntent.getMediaStream().listen((List<SharedMediaFile> value) {
      if (value != null && value.isNotEmpty) {
        final List<File> _temFile = value.map((e) => File(e.path)).toList();
        final Map<String, dynamic> _data = {
          'text': null,
          'files': _temFile,
          'type': _checkTypeReceive(value.first.type)
        };
        ReceiveSharingIntent.reset();
        Get.toNamed(ListGroupSendShareUI.ROUTER_NAME, arguments: _data);
      }
    }, onError: (err) {
      print('getIntentDataStream error: $err');
    });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then((List<SharedMediaFile> value) {
      if (value != null && value.isNotEmpty) {
        final List<File> _temFile = value.map((e) => File(e.path)).toList();
        final Map<String, dynamic> _data = {
          'text': null,
          'files': _temFile,
          'type': _checkTypeReceive(value.first.type)
        };

        ReceiveSharingIntent.reset();
        Get.toNamed(ListGroupSendShareUI.ROUTER_NAME, arguments: _data);
      }
    });

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    ReceiveSharingIntent.getTextStream().listen((String value) {
      if (value != null) {
        final Map<String, dynamic> _data = {
          'text': value,
          'files': null,
          'type': MessageType.Text
        };
        ReceiveSharingIntent.reset();
        Get.toNamed(ListGroupSendShareUI.ROUTER_NAME, arguments: _data);
      }
    }, onError: (err) {
      print('getLinkStream error: $err');
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String? value) {
      if (value != null) {
        final Map<String, dynamic> _data = {
          'text': value,
          'files': null,
          'type': MessageType.Text
        };
        ReceiveSharingIntent.reset();
        Get.toNamed(ListGroupSendShareUI.ROUTER_NAME, arguments: _data);
      }
    });
  }

  static MessageType _checkTypeReceive(SharedMediaType type) {
    switch (type) {
      case SharedMediaType.IMAGE:
        // TODO: Handle this case.
        return MessageType.Image;
      case SharedMediaType.VIDEO:
        // TODO: Handle this case.
        return MessageType.Video;
      case SharedMediaType.FILE:
        // TODO: Handle this case.
        return MessageType.File;
    }
  }

  static Future cleanCache() async {
    final Directory _systemPath = Directory.systemTemp;
    if (_systemPath.existsSync()) {
      _systemPath.delete(recursive: true);
    }
  }

  static Future traoDoiCongViecAction(
      {required String traoDoiID,
      required String loaiTraoDoi,
      required String tenLoaiTraoDoi,
      required int userID,
      required List<int> listUserID,
      required Widget chiTietWidget,
      String maTin = ''}) async {
    ///traoDoiID: là id của item
    ///loaiTraoDoi loại trao đổi là gì(khuyến khích viết tắt. vd: 'traodoigiaoviec' == 'tdgv')
    ///userID là userLogin
    ///maTin nếu item đó có mã thì khuyến khích nên truyền vào
    ///listUserID là danh sách userID liên quan
    ///tenLoaiTraoDoi là tên của loại trao đổi vd: Trao Đổi Giao Việc
    ///chiTietWidget là widget để show chi tiết
    final String _groupKey = '${loaiTraoDoi.trim()}@$traoDoiID@$maTin';
    final Map<String, dynamic> _paramRoot = <String, dynamic>{};
    final GroupInfo? _group =
        await SyncChat.instance.checkGroupChatDonVi(_groupKey);
    if (_group != null) {
      final List<DataUsers> listItemUser =
          await SyncChat.instance.getUserByGroupID(_group.groupID, userID);
      _paramRoot.putIfAbsent('itemUser', () => listItemUser);
      _paramRoot.putIfAbsent('groupInfo', () => _group);
      _paramRoot.putIfAbsent('chiTietWidget', () => chiTietWidget);
      Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: _paramRoot);
    } else {
      final List<DataUsers> _listUsers = <DataUsers>[];

      for (final int i in listUserID) {
        final DataUsers? _item = await SyncChat.instance.getDataUserByID(i);
        if (_item != null) _listUsers.add(_item);
      }

      final GroupInfo _groupInfo = GroupInfo()
        ..listUserInGroup = _listUsers
        ..createUserID = userID
        // ..createUserName = _userName
        ..groupName = tenLoaiTraoDoi +
            DateFormat('-HH:mm-dd/MM/yyyy').format(DateTime.now())
        ..lastMessage = ''
        ..isDonVi = true
        ..groupKey = _groupKey
        ..isGroup = true;
      final Map<String, dynamic> _paramRoot = <String, dynamic>{};
      _paramRoot.putIfAbsent('itemUser', () => _listUsers);
      _paramRoot.putIfAbsent('groupInfo', () => _groupInfo);
      _paramRoot.putIfAbsent('chiTietWidget', () => chiTietWidget);
      Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: _paramRoot);
    }
  }

  static void onLoadAppCheckFirebase() {
    if (fireBaseOnLoad != null) {
      Get.offAllNamed(rootPageRouter);
      final Map<String, dynamic> arg = jsonDecode(fireBaseOnLoad!['arguments']);
      final String _routerName = fireBaseOnLoad!['page'];
      if (_routerName == MessageDetailUI.ROUTER_NAME) {
        SyncChat.instance.getGroupByGroupID(arg['groupID']).then((value) {
          if (value != null) {
            final GroupInfo _groupInfo = value;
            SyncChat.instance
                .getUserByGroupID(_groupInfo.groupID, userIDChat)
                .then((value) {
              Get.toNamed(_routerName,
                  arguments: {'itemUser': value, 'groupInfo': _groupInfo});
            });
          } else {}
        });
      } else
        Get.toNamed(_routerName, arguments: arg);
    } else
      Get.offAllNamed(rootPageRouter);
  }

  static Future _appTraoDoiV2AddToken() async {
    final String _url = baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_AddToken';
    final Map<String, dynamic> param = {
      'UserID': userIDChat,
      'UserName': userNameChat,
      'DeviceID': deviceIDChat,
      'Token': tokenChat
    };
    await CoreHttp.instance.postAsync(_url, param);
  }
}
