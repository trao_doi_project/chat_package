import 'package:chat_package/src/traodoi/add_user_in_group/add_user_in_group_ui.dart';
import 'package:chat_package/src/traodoi/chuyen_tiep/chuyen_tiep_ui.dart';
import 'package:chat_package/src/traodoi/create_group/create_group_chat_ui.dart';
import 'package:chat_package/src/traodoi/edit_group/edit_group_ui.dart';
import 'package:chat_package/src/traodoi/group_info/group_info_ui.dart';
import 'package:chat_package/src/traodoi/kho_luu_tru/kho_luu_tru_ui.dart';
import 'package:chat_package/src/traodoi/list_group_send_share/list_group_send_share_ui.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:chat_package/src/traodoi/search_chat/search_chat_ui.dart';
import 'package:chat_package/src/traodoi/tin_nhan_da_ghim/tin_nhan_da_ghim_ui.dart';
import 'package:chat_package/src/traodoi/xem_thanh_vien_group/xem_thanh_vien_group_ui.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class ChatPages {
  static List<GetPage> chatRouter() {
    final pages = [
      GetPage(name: GroupInfoUI.ROUTER_NAME, page: () => GroupInfoUI()),
      GetPage(name: MessageDetailUI.ROUTER_NAME, page: () => MessageDetailUI()),
      GetPage(
          name: CreateGroupChatUI.ROUTER_NAME, page: () => CreateGroupChatUI()),
      GetPage(
          name: AddUserInGroupUI.ROUTER_NAME, page: () => AddUserInGroupUI()),
      GetPage(name: EditGroupUI.ROUTER_NAME, page: () => EditGroupUI()),
      GetPage(
          name: XemThanhVienGroupUI.ROUTER_NAME,
          page: () => XemThanhVienGroupUI()),
      GetPage(name: KhoLuuTruUI.ROUTER_NAME, page: () => KhoLuuTruUI()),
      GetPage(name: TinNhanDaGhimUI.ROUTER_NAME, page: () => TinNhanDaGhimUI()),
      GetPage(name: ChuyenTiepUI.ROUTER_NAME, page: () => ChuyenTiepUI()),
      GetPage(name: SearchChatUI.ROUTER_NAME, page: () => SearchChatUI()),
      GetPage(
          name: ListGroupSendShareUI.ROUTER_NAME,
          page: () => ListGroupSendShareUI()),
    ];

    return pages;
  }
}
