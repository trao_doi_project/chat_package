class GhimMessModel {
  int? id;
  int? groupID;
  String? groupKey;
  String? ghimMessage;
  int? userGhimID;
  String? note;
  int? loaiGhim;
  DateTime? createDate;
  DateTime? lastUpdate;
  bool? isDelete;

  GhimMessModel(
      {this.id,
      this.groupID,
      this.groupKey,
      this.ghimMessage,
      this.userGhimID,
      this.note,
      this.loaiGhim,
      this.createDate,
      this.lastUpdate,
      this.isDelete});

  factory GhimMessModel.fromJson(Map<String, dynamic> json) {
    return GhimMessModel(
        id: json['id'] as int,
        groupID: json['groupID'] as int,
        groupKey: json['groupKey'] as String,
        ghimMessage: json['ghimMessage'] as String,
        userGhimID: json['userGhimID'] as int,
        note: json['note'] as String,
        loaiGhim: json['loaiGhim'] as int,
        createDate: (json['createDate'] != null)
            ? DateTime.parse(json['createDate'])
            : null,
        lastUpdate: (json['lastUpdate'] != null)
            ? DateTime.parse(json['lastUpdate'])
            : null,
        isDelete: (json['isDelete'] is bool)
            ? json['isDelete']
            : json['isDelete'].toString().toLowerCase() == 'true');
  }

  Map<String, dynamic> toJson({bool isString = false}) {
    return {
      'id': id,
      'groupID': groupID,
      'groupKey': groupKey,
      'ghimMessage': ghimMessage,
      'userGhimID': userGhimID,
      'note': note,
      'loaiGhim': loaiGhim,
      'createDate': createDate?.toIso8601String(),
      'lastUpdate': lastUpdate?.toIso8601String(),
      'isDelete': isString ? isDelete.toString() : isDelete
    };
  }
}
