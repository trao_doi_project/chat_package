import 'data_users.dart';
import 'message.dart';

class GroupInfo {
  late int groupID;
  String? groupKey;
  String? groupName;
  String? groupIcon;
  String? hinhNen;
  bool isGroup = false;
  bool isDonVi = false;
  int? createUserID;
  String? createUserName;
  String? lastMessage;
  String? ghimMessage;
  MessageType? lastMessageType;
  DateTime? createDate;
  DateTime? lastUpdate;
  DateTime? lastMessageDate;
  String? lastUpdateStr;
  bool isDelete = false;
  late int countNewMessage;
  List<DataUsers>? listUserInGroup;

  GroupInfo({
    this.groupID = 0,
    this.groupKey,
    this.groupName,
    this.groupIcon,
    this.hinhNen,
    this.isGroup = false,
    this.isDonVi = false,
    this.createUserID,
    this.createUserName,
    this.createDate,
    this.lastUpdate,
    // this.listUserInGroup,
    this.lastUpdateStr,
    this.lastMessage,
    this.ghimMessage,
    this.lastMessageDate,
    this.isDelete = false,
    this.countNewMessage = 0
  });

  GroupInfo.fromMapObject(Map<String, dynamic> json) {
    groupID = json['groupID'] ?? 0;
    countNewMessage =
        (json['countNewMessage'] == null) ? 0 : json['countNewMessage'];
    groupKey = json['groupKey'];
    groupName = json['groupName'];
    groupIcon = json['groupIcon'];
    hinhNen = json['hinhNen'];
    isGroup = (json['isGroup'] is bool)
        ? json['isGroup']
        : json['isGroup'].toString().toLowerCase() == 'true';
    isDonVi = (json['isDonVi'] is bool)
        ? json['isDonVi']
        : json['isDonVi'].toString().toLowerCase() == 'true';
    createUserID = json['createUserID'];
    createUserName = json['createUserName'];
    lastMessage = json['lastMessage'];
    ghimMessage = json['ghimMessage'];
    lastMessageType = (json['lastMessageType'] != null)
        ? MessageType.values[json['lastMessageType']]
        : MessageType.Text;
    lastUpdateStr = json['lastUpdateStr'];
    createDate = (json['createDate'] != null)
        ? DateTime.parse(json['createDate']).toLocal()
        : null;
    lastUpdate = (json['lastUpdate'] != null)
        ? DateTime.parse(json['lastUpdate']).toLocal()
        : null;
    lastMessageDate = (json['lastMessageDate'] != null)
        ? DateTime.parse(json['lastMessageDate']).toLocal()
        : null;
    isDelete = (json['isDelete'] is bool)
        ? json['isDelete']
        : json['isDelete'].toString().toLowerCase() == 'true';
  }

  Map<String, dynamic> toJson(bool isList, bool isString) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['groupID'] = groupID;
    data['groupKey'] = groupKey;
    data['groupName'] = groupName;
    data['groupIcon'] = groupIcon;
    data['hinhNen'] = hinhNen;
    data['isGroup'] = isString ? isGroup.toString() : isGroup;
    data['isDonVi'] = isString ? isDonVi.toString() : isDonVi;
    data['createUserID'] = createUserID;
    data['countNewMessage'] = countNewMessage;
    data['createUserName'] = createUserName;
    data['lastMessage'] = lastMessage;
    data['ghimMessage'] = ghimMessage;
    data['lastMessageType'] = (data['lastMessageType'] != null)
        ? lastMessageType!.index
        : MessageType.Text.index;
    data['lastUpdateStr'] = lastUpdateStr;
    data['createDate'] = createDate?.toIso8601String();
    data['lastUpdate'] = lastUpdate?.toIso8601String();
    data['lastMessageDate'] = lastMessageDate?.toIso8601String();

    if (listUserInGroup != null && isList) {
      data['listUserInGroup'] =
          listUserInGroup?.map((v) => v.toJson()).toList();
    }

    data['isDelete'] = isString ? isDelete.toString() : isDelete;
    return data;
  }
}

class UserInGroup {
  late int id;
  late int groupID;
  String? groupKey;
  late int userID;
  String? userName;
  DateTime? createDate;
  DateTime? lastUpdate;
  late bool isOff;
  late bool isDelete;
  late bool deactivated;
  UserInGroup(
      {this.id = 0,
      this.groupID = 0,
      this.groupKey,
      this.userID = 0,
      this.userName,
      this.createDate,
      this.lastUpdate,
      this.isOff = false,
      this.isDelete = false,
      this.deactivated = false});
  UserInGroup.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    groupID = json['groupID'];
    groupKey = json['groupKey'];
    userID = json['userID'];
    userName = json['username'];
    createDate = (json['createDate'] != null)
        ? DateTime.parse(json['createDate'])
        : null;
    lastUpdate = (json['lastUpdate'] != null)
        ? DateTime.parse(json['lastUpdate'])
        : null;
    isOff = (json['isOff'] is bool)
        ? json['isOff']
        : json['isOff'].toString().toLowerCase() == 'true';
    isDelete = (json['isDelete'] is bool)
        ? json['isDelete']
        : json['isDelete'].toString().toLowerCase() == 'true';
    deactivated = (json['deactivated'] is bool)
        ? json['deactivated']
        : json['deactivated'].toString().toLowerCase() == 'true';
  }
  Map<String, dynamic> toJson({bool isString = false}) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['groupID'] = groupID;
    data['groupKey'] = groupKey;
    data['userID'] = userID;
    data['username'] = userName;
    data['createDate'] = createDate?.toIso8601String();
    data['lastUpdate'] = lastUpdate?.toIso8601String();
    data['isOff'] = isString ? isOff.toString() : isOff;
    data['isDelete'] = isString ? isDelete.toString() : isDelete;
    data['deactivated'] = isString ? deactivated.toString() : deactivated;
    return data;
  }
}
