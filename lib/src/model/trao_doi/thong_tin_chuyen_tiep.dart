class ThongTinModel {
  ThongTinModel({
    this.id,
    this.collection,
    this.title,
    this.image,
    this.noiDungRutGon,
    this.noiDung,
    this.isLink,
    this.isDelete,
    this.createDate,
    this.updateDate,
    this.ghiChu,
  });

  int? id;
  String? collection;
  String? title;
  String? image;
  String? noiDungRutGon;
  String? noiDung;
  bool? isLink;
  bool? isDelete;
  DateTime? createDate;
  DateTime? updateDate;
  String? ghiChu;

  factory ThongTinModel.fromMap(Map<String, dynamic> json) => ThongTinModel(
        id: json['id'],
        collection: json['collection'],
        title: json['title'],
        image: json['image'],
        noiDungRutGon: json['noiDungRutGon'],
        noiDung: json['noiDung'],
        isLink: json['isLink'],
        isDelete: json['isDelete'],
        createDate: DateTime.parse(json['createDate']),
        updateDate: DateTime.parse(json['updateDate']),
        ghiChu: json['ghiChu'],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'collection': collection,
        'title': title,
        'image': image,
        'noiDungRutGon': noiDungRutGon,
        'noiDung': noiDung,
        'isLink': isLink,
        'isDelete': isDelete,
        'createDate': createDate?.toIso8601String(),
        'updateDate': updateDate?.toIso8601String(),
        'ghiChu': ghiChu,
      };
}
