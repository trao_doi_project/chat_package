class FileResults {
  FileResults(
      {this.fileName,
      this.fileSize = 0.0,
      this.linkFile,
      this.fileEx,
      this.fileType,
      this.mimeType,
      this.description,
      this.thumbnail});

  String? fileName;
  double fileSize;
  String? linkFile;
  String? description;
  String? fileEx;
  int? fileType;
  String? mimeType;
  String? thumbnail;

  factory FileResults.fromJson(Map<String, dynamic> json) => FileResults(
        fileName: json['fileName'],
        fileSize: json['fileSize'] != null ? json['fileSize'] * 1.0: 0.0,
        linkFile: json['linkFile'],
        description: json['description'],
        fileEx: json['fileEx'],
        fileType: json['fileType'],
        mimeType: json['mimeType'],
        thumbnail: json['thumbnail'],
      );

  Map<String, dynamic> toJson() => {
        'fileName': fileName,
        'fileSize': fileSize,
        'linkFile': linkFile,
        'description': description,
        'fileEx': fileEx,
        'fileType': fileType,
        'mimeType': mimeType,
        'thumbnail': thumbnail,
      };
}
