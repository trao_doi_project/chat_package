import 'dart:convert';

import 'file_upload.dart';

enum MessageType {
  Text,
  Image,
  Video,
  File,
  Link,
  Youtube,
  Emoticon,
  Reply,
  Audio
}

class ChatMessage {
  int messageID;
  String? messageKey;
  int? groupID = 0;
  String? groupKey;
  int? userIDGui = 0;
  String? userNameGui;
  String? message;
  MessageType messageType;
  int? tinhTrangID;
  String? tenTinhTrang;
  String? userIDDaXem;
  String? fullNameDaXem;
  DateTime? createDate;
  DateTime? updateDate;
  bool? isDownLoad;
  bool? isThuHoi = false;
  String? userDelMess;
  List<FileUpload>? media;
  String? replyData;
  ReplyModel? replyModel;
  String? reaction;
  bool? isSelect = false;
  String? ghiChu;

  ChatMessage(
      {this.messageID = 0,
      this.messageKey,
      this.groupID,
      this.groupKey,
      this.userIDGui,
      this.userNameGui,
      this.message,
      required this.messageType,
      this.tinhTrangID,
      this.tenTinhTrang,
      this.userIDDaXem,
      this.fullNameDaXem,
      this.updateDate,
      this.createDate,
      this.isThuHoi = false,
      this.userDelMess,
      this.media,
      this.replyData,
      this.replyModel,
      this.isSelect = false,
      this.ghiChu,
      this.reaction});

  factory ChatMessage.fromJson(Map<String, dynamic> map,
      {List<dynamic>? files}) {
    return ChatMessage(
        messageID: map['messageID'],
        messageKey: map['messageKey'],
        groupID: map['groupID'],
        groupKey: map['groupKey'],
        userIDGui: map['userIDGui'],
        userNameGui: map['userNameGui'],
        message: map['message'],
        reaction: map['reaction'],
        messageType: MessageType.values[map['messageType']],
        tinhTrangID: map['tinhTrangID'],
        tenTinhTrang: map['tenTinhTrang'],
        userIDDaXem: map['userIDDaXem'],
        fullNameDaXem: map['fullNameDaXem'],
        userDelMess: map['userDelMess'],
        ghiChu: map['ghiChu'],
        media: (files != null && files.isNotEmpty)
            ? files.map((e) => FileUpload.fromJson(e)).toList()
            : null,
        replyData: map['replyData'],
        replyModel:
            (MessageType.values[map['messageType']] == MessageType.Reply)
                ? ReplyModel.fromJson(map['replyData'])
                : null,
        createDate: (map['createDate'] != null)
            ? DateTime.parse(map['createDate'])
            : null,
        updateDate: (map['updateDate'] != null)
            ? DateTime.parse(map['updateDate'])
            : null,
        isThuHoi: (map['isThuHoi'] is bool)
            ? map['isThuHoi']
            : map['isThuHoi'].toString().toLowerCase() == 'true');
  }

  Map<String, dynamic> toJson({bool isString = false, bool isMedia = false}) {
    return {
      'messageID': messageID,
      'messageKey': messageKey,
      'groupID': groupID,
      'groupKey': groupKey,
      'userIDGui': userIDGui,
      'userNameGui': userNameGui,
      'fullNameDaXem': fullNameDaXem,
      'message': message,
      'reaction': reaction,
      'userDelMess': userDelMess,
      'replyData': replyData,
      'messageType': messageType.index,
      'createDate': createDate?.toIso8601String(),
      'ghiChu': ghiChu,
      'isThuHoi': isString ? isThuHoi.toString() : isThuHoi,
      'media': (media != null && media!.isNotEmpty && isMedia)
          ? media!.map((e) => e.toJson()).toList()
          : null
    };
  }
}

class ReplyModel {
  int? messID;
  String? nguoiGui;
  String? oldMess;
  String? newMess;
  String? link;
  int? messType;

  ReplyModel(
      {this.messID,
      this.oldMess,
      this.newMess,
      this.link,
      this.messType,
      this.nguoiGui});

  factory ReplyModel.fromJson(String data) {
    final Map<String, dynamic> json = jsonDecode(data);
    return ReplyModel(
      messID: json['messID'],
      nguoiGui: json['nguoiGui'],
      oldMess: json['oldMess'],
      newMess: json['newMess'],
      link: json['link'],
      messType: json['messType'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'messID': messID,
      'nguoiGui': nguoiGui,
      'oldMess': oldMess,
      'newMess': newMess,
      'link': link,
      'messType': messType,
    };
  }
}

class TinhTrangModel {
  late int userID;
  late int tinhTrangID;
  late String tenTinhTrang;
  late DateTime? createDate;

  TinhTrangModel(
      {this.userID = 0,
      this.tinhTrangID = 0,
      this.tenTinhTrang = '',
      this.createDate});

  factory TinhTrangModel.fromMap(Map<String, dynamic> map) {
    return TinhTrangModel(
        userID: map['userID'],
        tinhTrangID: map['tinhTrangID'],
        tenTinhTrang: map['tenTinhTrang'],
        createDate: (map['createDate'] != null)
            ? DateTime.parse(map['createDate'])
            : null);
  }

  Map<String, dynamic> toMap() {
    return {
      'userID': userID,
      'tinhTrangID': tinhTrangID,
      'tenTinhTrang': tenTinhTrang,
      'createDate': createDate?.toIso8601String(),
    };
  }
}
