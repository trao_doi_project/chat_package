// 'type này là theo thứ tự trong lúc upload'
enum FileType { Image, Video, File, Link, Youtube, Audio }

class FileUpload {
  int userIDGui;
  int fileID;
  int groupID;
  String? groupKey;
  int messageID;
  String? messageKey;
  String? fileName;
  String? thumbnail;
  String? fileUrl;
  FileType? fileType;
  String? moTa;
  double fileSize = 0.0;
  String fileSizeStr = '';
  DateTime? createDate;
  DateTime? lastUpdate;
  bool isDelete = false;

  FileUpload(
      {this.userIDGui = 0,
      this.fileID = 0,
      this.groupID = 0,
      this.groupKey,
      this.messageID = 0,
      this.messageKey,
      this.fileName,
      this.thumbnail,
      this.fileUrl,
      this.fileType,
      this.moTa,
      this.createDate,
      this.lastUpdate,
      this.fileSize = 0.0,
      this.fileSizeStr = '',
      this.isDelete = false});

  factory FileUpload.fromJson(Map<String, dynamic> json) {
    return FileUpload(
        fileID: json['fileID'],
        groupID: json['groupID'],
        userIDGui: json['userIDGui'],
        groupKey: json['groupKey'],
        messageID: json['messageID'],
        messageKey: json['messageKey'],
        fileSizeStr: json['fileSizeStr'],
        fileName: json['fileName'],
        thumbnail: json['thumbnail'],
        fileUrl: json['fileUrl'],
        fileSize: json['fileSize'] != null ? json['fileSize'] * 1.0 : 0.0,
        fileType: (json['fileType'] != null)
            ? FileType.values[json['fileType']]
            : FileType.File,
        moTa: json['moTa'],
        createDate: (json['createDate'] != null)
            ? DateTime.parse(json['createDate'])
            : null,
        lastUpdate: (json['lastUpdate'] != null)
            ? DateTime.parse(json['lastUpdate'])
            : null,
        isDelete: (json['isDelete'] is bool)
            ? json['isDelete']
            : json['isDelete'].toString().toLowerCase() == 'true');
  }

  Map<String, dynamic> toJson({bool isString = false}) {
    return {
      'fileID': fileID,
      'groupID': groupID,
      'userIDGui': userIDGui,
      'groupKey': groupKey,
      'messageID': messageID,
      'messageKey': messageKey,
      'fileName': fileName,
      'thumbnail': thumbnail,
      'fileSize': fileSize,
      'fileUrl': fileUrl,
      'fileSizeStr': fileSizeStr,
      'fileType': fileType?.index,
      'moTa': moTa,
      'createDate': createDate?.toIso8601String(),
      'lastUpdate': lastUpdate?.toIso8601String(),
      'isDelete': isString ? isDelete.toString() : isDelete
    };
  }
}
