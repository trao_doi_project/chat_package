import 'dart:math';

import 'dart:ui';

class DataUsers {
  final List<Color> _lstColor = <Color>[
    const Color(0xff56c596),
    const Color(0xfff294b6),
    const Color(0xff47cacc),
    const Color(0xfff9a966),
    const Color(0xff205072)
  ];

  final Random _random = Random();

  int userID = 0;
  String? userName;
  String? fullName;
  String? avatar;
  int phongBanID = 0;
  String? tenPhongBan;
  int chucVuID = 0;
  String? tenChucVu;
  String? soDT;
  String? email;
  String? nameSort;
  bool isVisible = false;
  Color? keyColor;
  bool isThamGiaGroup = false;

  DataUsers(
      {this.userID = 0,
      this.userName,
      this.fullName,
      this.avatar,
      this.phongBanID = 0,
      this.tenPhongBan,
      this.chucVuID = 0,
      this.tenChucVu,
      this.soDT,
      this.email,
      this.nameSort});

  DataUsers.fromJson(Map<String, dynamic> json) {
    userID = json['userID'];
    userName = json['userName'];
    fullName = json['fullName'];
    avatar = json['avatar'];
    phongBanID = json['phongBanID'] ?? 0;
    tenPhongBan = json['tenPhongBan'];
    chucVuID = json['chucVuID'] ?? 0;
    tenChucVu = json['tenChucVu'];
    soDT = json['soDT'];
    email = json['email'];
    nameSort = json['nameSort'];
    keyColor = _lstColor[_random.nextInt(_lstColor.length)];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userID'] = userID;
    data['userName'] = userName;
    data['fullName'] = fullName;
    data['avatar'] = avatar;
    data['phongBanID'] = phongBanID;
    data['tenPhongBan'] = tenPhongBan;
    data['chucVuID'] = chucVuID;
    data['tenChucVu'] = tenChucVu;
    data['soDT'] = soDT;
    data['email'] = email;
    data['nameSort'] = nameSort;
    return data;
  }
}
