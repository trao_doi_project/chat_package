import 'dart:async';
import 'dart:io';

import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_result.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';

class CreateGroupChatController extends GetxController {
  RxList<DataUsers> listDataUser = <DataUsers>[].obs;
  RxMap<String, DataUsers> listUserSelect = <String, DataUsers>{}.obs;
  Rx<String> groupIconPath = ''.obs;
  String groupName = '';

  Timer? delay;
  List<DataUsers> _listAllDataUser = <DataUsers>[];
  final TextEditingController groupNameController = TextEditingController();
  final TextEditingController searchController = TextEditingController();
  SlidingUpPanelController slidingUpPanelController =
      SlidingUpPanelController();

  final List<BottomSheetItem> bottomItems = <BottomSheetItem>[
    BottomSheetItem(title: 'Máy ảnh', image: ImageVariable.cameraPick, id: 1),
    BottomSheetItem(title: 'Hình ảnh', image: ImageVariable.gallery, id: 2)
  ];

  void actionBottomSheet(int id) {
    switch (id) {
      case 1:
        Get.back();
        Get.to(CameraScreen(
          speciesCamera: 1,
          disableVideoRecord: false,
          onResutl: (value) {
            if (value != null) {
              groupIconPath.value = value.path;
            }
          },
        ));
        break;
      case 2:
        Get.back();
        slidingUpPanelController.anchor();
        break;
    }
  }

  void delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (delay != null) delay!.cancel();
    delay = Timer(duration, callback);
  }

  Future createGroupChat({bool isDonVi = false}) async {
    if (groupNameController.text.trim().isNotEmpty)
      groupName = groupNameController.text.trim();
    if (listUserSelect.values.length > 1) {
      String _groupIconString = '';
      if (groupIconPath.value.isNotEmpty) {
        final List<FileResults>? imageUpload = await ApiRequest.instance
            .uploadFile(files: [File(groupIconPath.value)]);
        if (imageUpload != null) _groupIconString = imageUpload.first.linkFile!;
      }

      final DataUsers? myData =
          await SyncChat.instance.getDataUserByID(userIDChat);
      if (myData != null)
        listUserSelect.putIfAbsent(myData.userName!, () => myData);
      final GroupInfo param = GroupInfo()
        ..listUserInGroup = listUserSelect.values.toList()
        ..createUserID = userIDChat
        ..createUserName = userNameChat
        ..groupName = groupName
        ..groupIcon = _groupIconString
        ..lastMessage = ''
        ..isDonVi = isDonVi
        ..groupKey = null
        ..isGroup = true;
      final GroupInfo? groupInfo =
          await ApiRequest.instance.createGroupMessage(param);
      if (groupInfo != null) {
        //insert group vào sqlite
        await SignalR.instance.connection.send(
            methodName: LocalVariable.groupCreated,
            args: [groupInfo.toJson(true, false)]);
      }
    } else {
      Get.defaultDialog(
          title: 'Thông báo',
          middleText: 'Số thành viên phải hơn 1',
          textCancel: 'Đồng ý');
    }
  }

  void getListUserChat() {
    SyncChat.instance.getDataUser().then((value) {
      if (value != null && value.isNotEmpty) {
        value.removeWhere((a) => a.userID == userIDChat);
        listDataUser.value = value;
        _listAllDataUser = value;
      }
    });
  }

  void getListUserChatBySearch(String keySearch) {
    if (keySearch.isNotEmpty) {
      final String keySearchKoDau = keySearch.xoaDau().toLowerCase();
      final List<DataUsers> items = _listAllDataUser.where((element) {
        final String el = element.fullName!.xoaDau().toLowerCase();
        if (el.contains(keySearchKoDau)) return true;
        return false;
      }).toList();
      listDataUser.value = items;
    } else
      listDataUser.value = _listAllDataUser;
  }

  void selectUser(DataUsers user) {
    if (listUserSelect.containsKey(user.userName)) {
      listUserSelect.remove(user.userName);
      groupName = groupName.replaceAll(user.fullName! + ',', '');
    } else {
      listUserSelect.putIfAbsent(user.userName!, () => user);
      groupName += user.fullName! + ',';
    }
  }

  Future<void> selectGalleryGroupIcon(List<AssetEntity> images) async {
    groupIconPath.value = (await images.first.file)?.path ?? '';
  }

  @override
  void onInit() {
    final groupKey = Get.arguments;
    if (groupKey != null) groupName = groupKey;
    getListUserChat();
    super.onInit();
  }

  @override
  void onClose() {
    groupNameController.dispose();
    searchController.dispose();
    delay?.cancel();
    super.onClose();
  }
}
