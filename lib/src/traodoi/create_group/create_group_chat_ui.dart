import 'dart:io';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/traodoi/create_group/create_group_chat_controller.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';

import '../../../chat_package.dart';

class CreateGroupChatUI extends GetView<CreateGroupChatController> {
  static const ROUTER_NAME = '/CreateGroupChatUI';
  @override
  CreateGroupChatController get controller => super.controller;

  final CreateGroupChatController _createGroupChatController =
      Get.put(CreateGroupChatController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBarWidget(
        backgroundColor: BaseColor.primaryColor,
        title: const Text('Tạo nhóm'),
      ),
      body: Gallery(
        panelController: _createGroupChatController.slidingUpPanelController,
        isSelectMulti: false,
        imagesChoice: (value) {
          _createGroupChatController.selectGalleryGroupIcon(value);
        },
        galleryController: Get.find<GalleryController>(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  GestureDetector(onTap: () {
                    Get.bottomSheet(BottomSheetWithActionWidget(
                      bottomItems: _createGroupChatController.bottomItems,
                      idValue: (value) {
                        _createGroupChatController.actionBottomSheet(value);
                      },
                    ));
                  }, child: Obx(() {
                    if (_createGroupChatController
                        .groupIconPath.value.isNotEmpty) {
                      return SizedBox(
                        height: 40,
                        width: 40,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: Image.file(
                            File(
                                _createGroupChatController.groupIconPath.value),
                            height: 40,
                            width: 40,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    } else
                      return const Icon(
                        Icons.camera_alt,
                        size: 36,
                      );
                  })),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 8),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: TextField(
                          controller:
                              _createGroupChatController.groupNameController,
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Tên nhóm',
                          ),
                        )),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextField(
                  onChanged: (value) async => _createGroupChatController
                      .delaySearch(() => _createGroupChatController
                          .getListUserChatBySearch(value)),
                  controller: _createGroupChatController.searchController,
                  decoration: InputDecoration(
                      icon: Icon(
                        Icons.search,
                        size: Get.width * 0.08,
                        color: Colors.grey,
                      ),
                      border: InputBorder.none,
                      hintText: 'Tìm kiếm'),
                ),
              ),
            ),
            Obx(() {
              if (_createGroupChatController.listUserSelect.isNotEmpty)
                return SizedBox(
                  width: Get.width,
                  height: Get.height * 0.075,
                  child: ScrollConfiguration(
                    behavior: DisableGlowListViewWidget(),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount:
                          _createGroupChatController.listUserSelect.length,
                      itemBuilder: (context, index) {
                        final List<DataUsers> users = _createGroupChatController
                            .listUserSelect.values
                            .toList();
                        return Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Stack(
                            children: [
                              SizedBox(
                                height: Get.height * 0.06,
                                width: Get.height * 0.06,
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: ImageNetworkWidget(
                                      packageName: LocalVariable.namePackage,
                                      urlImageNetwork: baseUrlUpload +
                                          (users[index].avatar ?? ''),
                                      imageAssetDefault: ImageVariable.person,
                                    )),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: GestureDetector(
                                  onTap: () {
                                    _createGroupChatController.listUserSelect
                                        .remove(users[index].userName);
                                    _createGroupChatController
                                            .groupNameController.text =
                                        _createGroupChatController
                                            .groupNameController.text
                                            .replaceAll(
                                                users[index].fullName! + ',',
                                                '');
                                  },
                                  child: const Icon(
                                    Icons.cancel,
                                    color: Colors.black,
                                    size: 16,
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                );
              return const SizedBox.shrink();
            }),
            Expanded(
              child: ScrollConfiguration(
                  behavior: DisableGlowListViewWidget(),
                  child: Obx(() {
                    if (_createGroupChatController.listDataUser.isNotEmpty)
                      return ListView.builder(
                          itemCount:
                              _createGroupChatController.listDataUser.length,
                          itemBuilder: (context, index) {
                            final DataUsers _dataUser =
                                _createGroupChatController.listDataUser[index];
                            return GestureDetector(
                              onTap: () {
                                _createGroupChatController
                                    .selectUser(_dataUser);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                                child: Obx(() => Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: (_createGroupChatController
                                                    .listUserSelect
                                                    .containsKey(
                                                        _dataUser.userName))
                                                ? BaseColor.accentsColor
                                                : Colors.transparent),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4, 8, 4),
                                            child: Stack(
                                              children: [
                                                SizedBox(
                                                  height: Get.height * 0.06,
                                                  width: Get.height * 0.06,
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                      child: ImageNetworkWidget(
                                                        packageName: LocalVariable.namePackage,
                                                        urlImageNetwork:
                                                            baseUrlUpload +
                                                                (_dataUser
                                                                        .avatar ??
                                                                    ''),
                                                        imageAssetDefault:
                                                            ImageVariable
                                                                .person,
                                                      )),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            constraints: BoxConstraints(
                                                maxWidth: Get.width * 0.7),
                                            child: Text(
                                              _dataUser.fullName ?? '',
                                              maxLines: 3,
                                              style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontFamily: 'Medium',
                                              ),
                                            ),
                                          ),
                                          const Spacer(),
                                          if (_createGroupChatController
                                              .listUserSelect
                                              .containsKey(_dataUser.userName))
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(right: 8),
                                              child: Icon(
                                                Icons.check,
                                                color: BaseColor.accentsColor,
                                              ),
                                            )
                                        ],
                                      ),
                                    )),
                              ),
                            );
                          });
                    return const SizedBox.shrink();
                  })),
            ),
            Container(
              height: Get.height * 0.07,
              decoration: const BoxDecoration(
                color: Color(0xfff2f2f2),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Trở lại',
                      style: TextStyle(
                        color: BaseColor.primaryColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      _createGroupChatController.createGroupChat();
                    },
                    child: const Text(
                      'Tạo',
                      style: TextStyle(
                        color: BaseColor.primaryColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
