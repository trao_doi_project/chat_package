import 'dart:async';

import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/custom_widget_chat.dart';
import 'package:chat_package/src/traodoi/search_chat/search_chat_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/widget/no_data_widget.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:intl/intl.dart';

import '../../../chat_package.dart';

class SearchChatUI extends StatefulWidget {
  static const ROUTER_NAME = '/SearchChatUI';

  @override
  _SearchChatUIState createState() => _SearchChatUIState();
}

class _SearchChatUIState extends State<SearchChatUI>
    with SingleTickerProviderStateMixin {
  Timer? _delay;
  late TabController _tabController;
  late TextEditingController _textEditingSearchCtr;

  final SearchChatController _searchChatController =
      Get.put(SearchChatController());

  @override
  void initState() {
    _tabController = TabController(
      vsync: this,
      length: 4,
    );
    _textEditingSearchCtr = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _delay?.cancel();
    _tabController.dispose();
    _textEditingSearchCtr.dispose();
    super.dispose();
  }

  void _delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (_delay != null) _delay!.cancel();
    _delay = Timer(duration, callback);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: BaseColor.primaryColor,
        actions: [
          const SizedBox(
            width: 50,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _textEditingSearchCtr,
                // autofocus: true,
                style: const TextStyle(color: Colors.white),
                onChanged: (value) async => _delaySearch(() {
                  if (value.isNotEmpty)
                    _searchChatController.search(value);
                  else
                    _searchChatController.resetDataSearch();
                }),
                cursorColor: Colors.white,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Tìm kiếm tin nhắn, nhóm',
                    hintStyle: TextStyle(color: Colors.white)),
              ),
            ),
          ),
          GestureDetector(
              onTap: () {
                _textEditingSearchCtr.clear();
                _searchChatController.resetDataSearch();
              },
              child: const Icon(Icons.cancel)),
          const SizedBox(
            width: 10,
          )
        ],
        bottom: TabBar(
          indicatorColor: Colors.white,
          controller: _tabController,
          tabs: const [
            Tab(
              text: 'Tất cả',
            ),
            Tab(
              text: 'Tin nhắn',
            ),
            Tab(
              text: 'Nhóm',
            ),
            Tab(
              text: 'Danh Bạ',
            ),
          ],
        ),
      ),
      body: TabBarView(controller: _tabController, children: [
        Obx(() {
          if (_searchChatController.listMessSearch.isEmpty &&
              _searchChatController.listGroupSearch.isEmpty &&
              _searchChatController.listUserSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return searchAllWidget();
        }),
        Obx(() {
          if (_searchChatController.listMessSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: SingleChildScrollView(
                controller: _searchChatController.scrollCtrListMessSearch,
                child: listMessSearch(),
              ));
        }),
        Obx(() {
          if (_searchChatController.listGroupSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: SingleChildScrollView(
                controller: _searchChatController.scrollCtrListGroupSearch,
                child: listGroupSearch(),
              ));
        }),
        Obx(() {
          if (_searchChatController.listUserSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: SingleChildScrollView(
                controller: _searchChatController.scrollCtrListUserSearch,
                child: listUserSearch(),
              ));
        }),
      ]),
    );
  }

  Widget searchAllWidget() {
    return ScrollConfiguration(
      behavior: DisableGlowListViewWidget(),
      child: SingleChildScrollView(
        child: Column(children: [
          Obx(() {
            if (_searchChatController.listMessSearch.isNotEmpty)
              return listMessSearch(isAll: true);
            return const SizedBox.shrink();
          }),
          Obx(() {
            if (_searchChatController.listGroupSearch.isNotEmpty)
              return listGroupSearch(isAll: true);
            return const SizedBox.shrink();
          }),
          Obx(() {
            if (_searchChatController.listUserSearch.isNotEmpty)
              return listUserSearch(isAll: true);
            return const SizedBox.shrink();
          }),
        ]),
      ),
    );
  }

  Column listMessSearch({bool isAll = false}) {
    final List<Widget> children = <Widget>[];
    final List<ItemMessSearch> listMessTemp;
    if (isAll)
      children.add(
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            'Tin Nhắn',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      );
    if (isAll)
      listMessTemp = _searchChatController.listMessSearch.take(5).toList();
    else
      listMessTemp = _searchChatController.listMessSearch;

    DateTime? createDate = listMessTemp.first.createDate;
    children.add(
      Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: HorizontalLineText(
          Text(
            DateFormat('EEEE - dd/MM/yyyy', 'vi').format(createDate!),
            style: const TextStyle(
                fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
    for (final mess in listMessTemp) {
      if (createDate!.day != mess.createDate!.day ||
          createDate.month != mess.createDate!.month ||
          createDate.year != mess.createDate!.year) {
        children.add(
          HorizontalLineText(
            Text(
              DateFormat('EEEE - dd/MM/yyyy', 'vi').format(mess.createDate!),
              style: const TextStyle(
                  fontSize: 15,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold),
            ),
          ),
        );
        createDate = mess.createDate;
      }
      children.add(GestureDetector(
        onTap: () async {
          await _searchChatController.clickMessOrGroup(
              groupInfo: mess.group!, isSearch: true, messageID: mess.messId!);
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                urlImageNetwork: baseUrlUpload + (mess.avatar ?? ''),
                imageAssetDefault: ImageVariable.person,
                shape: BoxShape.circle,
                imageWidth: 40,
                imageHeight: 40,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        mess.nameGroup ?? '',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w500,
                            fontSize: 16),
                      ),
                      richTextSearch(mess.mess ?? '', _searchChatController.query,
                          styleRichText: const TextStyle(
                              color: Colors.black, fontSize: 15)),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }

    if (_searchChatController.listMessSearch.length > 5 && isAll)
      children.add(GestureDetector(
        onTap: () {
          _tabController.animateTo(1,
              duration: const Duration(milliseconds: 100));
        },
        child: Column(
          children: [
            const Divider(
              thickness: 3,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: SizedBox(
                height: Get.height * 0.03,
                width: Get.width,
                child: const Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Xem thêm',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }

  Column listGroupSearch({bool isAll = false}) {
    final List<GroupInfo> listGroupTemp;
    final List<Widget> children = <Widget>[];
    if (isAll)
      children.add(
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            'Nhóm',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      );
    if (isAll)
      listGroupTemp = _searchChatController.listGroupSearch.take(5).toList();
    else
      listGroupTemp = _searchChatController.listGroupSearch;

    for (final group in listGroupTemp)
      children.add(GestureDetector(
        onTap: () async {
          await _searchChatController.clickMessOrGroup(groupInfo: group);
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                urlImageNetwork: baseUrlApiChat + (group.groupIcon ?? ''),
                imageAssetDefault: group.isGroup ? ImageVariable.groupAvatar : ImageVariable.person,
                shape: BoxShape.circle,
                imageWidth: 40,
                imageHeight: 40,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: richTextSearch(
                      group.groupName ?? '', _searchChatController.query,
                      styleQuery: const TextStyle(
                          color: Colors.red, fontWeight: FontWeight.bold),
                      styleRichText: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 16)),
                ),
              ),
            ],
          ),
        ),
      ));
    if (_searchChatController.listGroupSearch.length > 5 && isAll)
      children.add(GestureDetector(
        onTap: () {
          _tabController.animateTo(2,
              duration: const Duration(milliseconds: 100));
        },
        child: Column(
          children: [
            const Divider(
              thickness: 3,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: SizedBox(
                height: Get.height * 0.03,
                width: Get.width,
                child: const Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Xem thêm',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }

  Column listUserSearch({bool isAll = false}) {
    final List<Widget> children = <Widget>[];
    final List<DataUsers> listUserTemp;
    if (isAll)
      children.add(
        const Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
            'Danh bạ',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      );

    if (isAll)
      listUserTemp = _searchChatController.listUserSearch.take(5).toList();
    else
      listUserTemp = _searchChatController.listUserSearch;
    for (final user in listUserTemp)
      children.add(GestureDetector(
        onTap: () {
          _searchChatController.clickUser(user);
        },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                urlImageNetwork: baseUrlUpload + (user.avatar ?? ''),
                imageAssetDefault: ImageVariable.person,
                shape: BoxShape.circle,
                imageWidth: 40,
                imageHeight: 40,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: richTextSearch(
                      user.fullName ?? '', _searchChatController.query,
                      styleQuery: const TextStyle(
                          color: Colors.red, fontWeight: FontWeight.bold),
                      styleRichText: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 16)),
                ),
              ),
            ],
          ),
        ),
      ));
    if (isAll && _searchChatController.listUserSearch.length > 5)
      children.add(GestureDetector(
        onTap: () {
          _tabController.animateTo(3,
              duration: const Duration(milliseconds: 100));
        },
        child: Column(
          children: [
            const Divider(
              thickness: 3,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 8),
              child: SizedBox(
                height: Get.height * 0.03,
                width: Get.width,
                child: const Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Xem thêm',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }
}
