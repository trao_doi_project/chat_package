import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class SearchChatController extends GetxController {
  RxList<GroupInfo> listGroupSearch = <GroupInfo>[].obs;
  RxList<ItemMessSearch> listMessSearch = <ItemMessSearch>[].obs;
  RxList<DataUsers> listUserSearch = <DataUsers>[].obs;

  List<ItemMessSearch> listMessFirstLoad = <ItemMessSearch>[];
  List<GroupInfo> listGroupFirstLoad = <GroupInfo>[];
  List<DataUsers> listUserFirstLoad = <DataUsers>[];

  String query = '';

  int pageIndexMessSearch = 0;
  bool isLoadMoreListMessSearch = false;
  ScrollController scrollCtrListMessSearch = ScrollController();

  int pageIndexGroupSearch = 0;
  bool isLoadMoreListGroupSearch = false;
  ScrollController scrollCtrListGroupSearch = ScrollController();

  int pageIndexUserSearch = 0;
  bool isLoadMoreListUserSearch = false;
  ScrollController scrollCtrListUserSearch = ScrollController();

  Future<void> getUserFirstLoad() async {
    final List<DataUsers>? users =
        await SyncChat.instance.getDataUserWithNumber(number: 15);
    if (users != null && users.isNotEmpty) {
      listUserFirstLoad.addAll(users);
      listUserSearch.value = users;
    }
  }

  Future<void> getGroupFirstLoad() async {
    final List<GroupInfo>? groups =
        await SyncChat.instance.getDataGroupInfoIsGroupWithNumber(number: 15);
    if (groups != null && groups.isNotEmpty) {
      listGroupFirstLoad.addAll(groups);
      listGroupSearch.value = groups;
    }
  }

  Future<void> getMessFirstLoad() async {
    final List<ChatMessage>? messList =
        await SyncChat.instance.getMessageTextWithNumber(number: 15);
    if (messList != null && messList.isNotEmpty) {
      for (final ChatMessage mess in messList) {
        final GroupInfo? groupInfo =
            await SyncChat.instance.getGroupByGroupID(mess.groupID!);
        if (groupInfo?.isGroup ?? false) {
          listMessFirstLoad.add(ItemMessSearch(
              mess: mess.message ?? '',
              nameGroup: groupInfo?.groupName,
              messId: mess.messageID,
              avatar: groupInfo?.groupIcon,
              createDate: mess.createDate,
              group: groupInfo));
        } else {
          final DataUsers? dataUsers =
              await SyncChat.instance.getDataUserByID(mess.userIDGui!);
          listMessFirstLoad.add(ItemMessSearch(
              mess: mess.message,
              createDate: mess.createDate,
              nameGroup: dataUsers?.fullName,
              messId: mess.messageID,
              avatar: dataUsers?.avatar,
              group: groupInfo));
        }
      }

      listMessSearch.addAll(listMessFirstLoad);
    }
  }

  Future<void> search(String value) async {
    query = value;

    pageIndexMessSearch = 0;
    final List<ChatMessage>? listMessTemp =
        await SyncChat.instance.searchMessageText(query: value, pageIndex: pageIndexMessSearch);
    if (listMessTemp != null) {
      isLoadMoreListMessSearch = true;
      final List<ItemMessSearch> listMessSearchTemp = <ItemMessSearch>[];
      for (final ChatMessage mess in listMessTemp) {
        final GroupInfo? groupInfo =
            await SyncChat.instance.getGroupByGroupID(mess.groupID!);
        if (groupInfo?.isGroup ?? false) {
          listMessSearchTemp.add(ItemMessSearch(
              mess: mess.message,
              nameGroup: groupInfo?.groupName,
              messId: mess.messageID,
              avatar: groupInfo?.groupIcon,
              createDate: mess.createDate,
              group: groupInfo));
        } else {
          final DataUsers? dataUsers =
              await SyncChat.instance.getDataUserByID(mess.userIDGui!);
          listMessSearchTemp.add(ItemMessSearch(
              mess: mess.message,
              nameGroup: dataUsers?.fullName,
              messId: mess.messageID,
              avatar: dataUsers?.avatar,
              createDate: mess.createDate,
              group: groupInfo));
        }
      }

      listMessSearch.value = listMessSearchTemp;
    } else
      listMessSearch.clear();

    pageIndexGroupSearch = 0;
    final List<GroupInfo>? listGroupTemp =
        await SyncChat.instance.searchGroupName(query: value, pageIndex: pageIndexGroupSearch);
    if (listGroupTemp != null) {
      isLoadMoreListGroupSearch = true;
      listGroupSearch.value = listGroupTemp;
    }
    else
      listGroupSearch.clear();

    pageIndexUserSearch = 0;
    final List<DataUsers>? listUserTemp =
        await SyncChat.instance.searchNameDataUser(query: value, pageIndex: pageIndexUserSearch);
    if (listUserTemp != null) {
      isLoadMoreListUserSearch = true;
      listUserSearch.value = listUserTemp;
    }
    else
      listUserSearch.clear();
  }

  Future<void> clickMessOrGroup(
      {GroupInfo? groupInfo, bool isSearch = false, int? messageID}) async {
    final Map<String, dynamic> _paramRoot = <String, dynamic>{};

    final List<DataUsers> listItemUser = await SyncChat.instance
        .getUserByGroupID(groupInfo!.groupID, userIDChat);
    _paramRoot.putIfAbsent('itemUser', () => listItemUser);
    _paramRoot.putIfAbsent('groupInfo', () => groupInfo);
    if (isSearch) _paramRoot.putIfAbsent('messageID', () => messageID);

    await Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: _paramRoot);
  }

  Future<void> clickUser(DataUsers item) async {
    final GroupInfo? _groupInfo =
        await SyncChat.instance.checkUserInGroup(item.userID);
    if (_groupInfo != null) {
      final List<DataUsers> list = await SyncChat.instance
          .getUserByGroupID(_groupInfo.groupID, userIDChat);
      if (list.isNotEmpty)
        Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: {
          'itemUser': [list.first],
          'groupInfo': _groupInfo
        });
    } else {
      Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: {
        'itemUser': [item]
      });
    }
  }

  void resetDataSearch() {
    isLoadMoreListMessSearch = false;
    isLoadMoreListGroupSearch = false;
    isLoadMoreListUserSearch = false;

    query = '';

    listUserSearch.clear();
    listUserSearch.addAll(listUserFirstLoad);

    listGroupSearch.clear();
    listGroupSearch.addAll(listGroupFirstLoad);

    listMessSearch.clear();
    listMessSearch.addAll(listMessFirstLoad);
  }
  /// -------------------------------Start load more-------------------------------------------
  Future<void> loadMoreListMessSearch() async{
    pageIndexMessSearch ++;
    final List<ChatMessage>? listMessTemp =
        await SyncChat.instance.searchMessageText(query: query, pageIndex: pageIndexMessSearch);
    if (listMessTemp != null) {
      final List<ItemMessSearch> listMessSearchTemp = <ItemMessSearch>[];
      for (final ChatMessage mess in listMessTemp) {
        final GroupInfo? groupInfo =
            await SyncChat.instance.getGroupByGroupID(mess.groupID!);
        if (groupInfo?.isGroup ?? false) {
          listMessSearchTemp.add(ItemMessSearch(
              mess: mess.message,
              nameGroup: groupInfo?.groupName,
              messId: mess.messageID,
              avatar: groupInfo?.groupIcon,
              createDate: mess.createDate,
              group: groupInfo));
        } else {
          final DataUsers? dataUsers =
              await SyncChat.instance.getDataUserByID(mess.userIDGui!);
          listMessSearchTemp.add(ItemMessSearch(
              mess: mess.message,
              nameGroup: dataUsers?.fullName,
              messId: mess.messageID,
              avatar: dataUsers?.avatar,
              createDate: mess.createDate,
              group: groupInfo));
        }
      }

      listMessSearch.addAll(listMessSearchTemp);
    } else
      isLoadMoreListMessSearch = false;
  }
  void listenLoadMoreListMessSearch(){
    if(!isLoadMoreListMessSearch) return;
    if (scrollCtrListMessSearch.offset >=
        scrollCtrListMessSearch.position.maxScrollExtent &&
        !scrollCtrListMessSearch.position.outOfRange) {
      loadMoreListMessSearch();
    }
  }

  Future<void> loadMoreListGroupSearch() async{
    pageIndexGroupSearch ++;
    final List<GroupInfo>? listGroupTemp =
    await SyncChat.instance.searchGroupName(query: query, pageIndex: pageIndexGroupSearch);
    if (listGroupTemp != null)
      listGroupSearch.addAll(listGroupTemp);
    else
      isLoadMoreListGroupSearch = false;
  }
  void listenLoadMoreListGroupSearch(){
    if(!isLoadMoreListGroupSearch) return;
    if (scrollCtrListGroupSearch.offset >=
        scrollCtrListGroupSearch.position.maxScrollExtent &&
        !scrollCtrListGroupSearch.position.outOfRange) {
      loadMoreListGroupSearch();
    }
  }

  Future<void> loadMoreListUserSearch() async{
    pageIndexUserSearch ++;
    final List<DataUsers>? listUserTemp =
    await SyncChat.instance.searchNameDataUser(query: query, pageIndex: pageIndexUserSearch);
    if (listUserTemp != null)
      listUserSearch.addAll(listUserTemp);
    else
      isLoadMoreListUserSearch = false;
  }
  void listenLoadMoreListUserSearch(){
    if(!isLoadMoreListUserSearch) return;
    if (scrollCtrListUserSearch.offset >=
        scrollCtrListUserSearch.position.maxScrollExtent &&
        !scrollCtrListUserSearch.position.outOfRange) {
      loadMoreListUserSearch();
    }
  }
  /// -------------------------------End load more---------------------------------------------

  @override
  void onInit() {
    getMessFirstLoad();
    getGroupFirstLoad();
    getUserFirstLoad();
    scrollCtrListMessSearch.addListener(listenLoadMoreListMessSearch);
    scrollCtrListGroupSearch.addListener(listenLoadMoreListGroupSearch);
    scrollCtrListUserSearch.addListener(listenLoadMoreListUserSearch);
    super.onInit();
  }

  @override
  void onClose() {
    scrollCtrListMessSearch.dispose();
    scrollCtrListGroupSearch.dispose();
    scrollCtrListUserSearch.dispose();
    super.onClose();
  }
}

class ItemMessSearch {
  String? mess;
  String? nameGroup;
  String? avatar;
  int? messId;
  DateTime? createDate;
  GroupInfo? group;

  ItemMessSearch(
      {this.mess,
      this.nameGroup,
      this.avatar,
      this.messId,
      this.group,
      this.createDate});
}
