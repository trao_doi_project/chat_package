import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/src/traodoi/group_info/group_info_controller.dart';
import 'package:chat_package/src/traodoi/xem_thanh_vien_group/xem_thanh_vien_group_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/no_data_widget.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';

class XemThanhVienGroupUI extends StatelessWidget {
  static const ROUTER_NAME = '/XemThanhVienGroupUI';

  final XemThanhVienGroupController _xemThanhVienGroupController =
      Get.put(XemThanhVienGroupController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        backgroundColor: BaseColor.primaryColor,
        title: const Text('Thành viên'),
      ),
      body: Obx(() {
        if (_xemThanhVienGroupController.listUser.isNotEmpty)
          return ListView.builder(
              physics: const ClampingScrollPhysics(),
              itemCount: _xemThanhVienGroupController.listUser.length,
              itemBuilder: (context, index) {
                final DataUsers dataUsers =
                    _xemThanhVienGroupController.listUser[index];
                return Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          if (!checkIsMe(dataUsers.userID)) {
                            Get.until(
                                (route) => Get.currentRoute == rootPageRouter);
                            Get.find<GroupInfoController>().checkAndGoChat(
                                listItemUser: [dataUsers],
                                inXemThanhVienPage: true);
                          }
                        },
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                              child: Stack(
                                children: [
                                  SizedBox(
                                    height: Get.height * 0.06,
                                    width: Get.height * 0.06,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(50),
                                      child: ImageNetworkWidget(
                                        packageName: LocalVariable.namePackage,
                                        urlImageNetwork: baseUrlUpload +
                                            (dataUsers.avatar ?? ''),
                                        imageAssetDefault: ImageVariable.person,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              constraints:
                                  BoxConstraints(maxWidth: Get.width * 0.7),
                              child: Text(
                                (checkIsMe(dataUsers.userID))
                                    ? 'Bạn'
                                    : dataUsers.fullName!,
                                maxLines: 3,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: 'Medium',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Spacer(),
                      if (dataUsers.userID ==
                          _xemThanhVienGroupController.group.createUserID)
                        const Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: Icon(
                            Icons.vpn_key,
                            color: Colors.grey,
                          ),
                        )
                      else if (checkUserCreateGroup())
                        Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: GestureDetector(
                            onTap: () {
                              if (_xemThanhVienGroupController.listUser.length <
                                  3)
                                AwesomeDialog(
                                    context: context,
                                    dialogType: DialogType.QUESTION,
                                    animType: AnimType.BOTTOMSLIDE,
                                    title: 'Xóa nhóm',
                                    desc: 'Bạn muốn xóa nhóm này?',
                                    btnCancelText: 'Không',
                                    btnCancelOnPress: () {},
                                    btnOkText: 'Có',
                                    btnOkOnPress: () async {
                                      await ApiRequest.instance.deleteGroup(
                                          _xemThanhVienGroupController
                                              .group.groupID);
                                      // Get.offAllNamed(BottomBar.ROUTER_NAME,
                                      //     arguments: {'pageIndex': 1});
                                    }).show();
                              else
                                AwesomeDialog(
                                    context: context,
                                    dialogType: DialogType.QUESTION,
                                    animType: AnimType.BOTTOMSLIDE,
                                    title: 'Xóa thành viên',
                                    desc:
                                        'Bạn muốn xóa "${dataUsers.fullName}" khỏi nhóm này?',
                                    btnCancelText: 'Không',
                                    btnCancelOnPress: () {},
                                    btnOkText: 'Có',
                                    btnOkOnPress: () {
                                      _xemThanhVienGroupController
                                          .deleteUserInGroup(
                                              _xemThanhVienGroupController
                                                  .group,
                                              dataUsers);
                                    }).show();
                            },
                            child: const Icon(Icons.delete_forever,
                                color: BaseColor.accentsColor),
                          ),
                        )
                    ],
                  ),
                );
              });
        return const NoDataWidget();
      }),
    );
  }

  bool checkIsMe(int id) {
    if (userIDChat == id)
      return true;
    else
      return false;
  }

  bool checkUserCreateGroup() {
    if (userIDChat == _xemThanhVienGroupController.group.createUserID)
      return true;
    else
      return false;
  }
}
