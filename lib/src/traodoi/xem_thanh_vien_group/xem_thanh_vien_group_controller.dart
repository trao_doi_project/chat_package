import 'dart:async';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class XemThanhVienGroupController extends GetxController {

  RxList<DataUsers> listUser = <DataUsers>[].obs;
  late GroupInfo group;
  late String tagCtrMessageDetail;

  StreamSubscription? _subListenDeleteUserInGroup;
  StreamSubscription? _subListenAddUserInGroup;

  void getListUserInGroup(int groupID) {
    SyncChat.instance.getUserByGroupIDHasMe(groupID).then((value) {
      if (value != null) {
        listUser.value = value;
      }
    });
  }

  Future deleteUserInGroup(GroupInfo groupInfo, DataUsers user) async {
    listUser.removeWhere((element) => element.userID == user.userID);
    await ApiRequest.instance.deleteUserInGroup(groupInfo, user);
  }

  void listenSignalR(){
    _subListenDeleteUserInGroup ??= deleteUserInGroupCtrl.stream.listen((userEvent) {
      if(userEvent?.groupID == group.groupID)
      SyncChat.instance.getUserByGroupIDHasMe(group.groupID).then((value) {
        if (value != null) {
          listUser.value = value;
        }
      });
    });

    _subListenAddUserInGroup ??= insertUserInGroupCtrl.stream.listen((groupEvent) {
      if(groupEvent?.groupID == group.groupID)
        SyncChat.instance.getUserByGroupIDHasMe(group.groupID).then((value) {
          if (value != null) {
            listUser.value = value;
          }
        });
    });
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    group = arg['groupInfo'];
    tagCtrMessageDetail = arg['tagCtrMessageDetail'];
    getListUserInGroup(group.groupID);
    listenSignalR();

    super.onInit();
  }

  @override
  void onClose() {
    _subListenAddUserInGroup?.cancel();
    _subListenDeleteUserInGroup?.cancel();
    super.onClose();
  }
}
