import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chat_package/chat_package.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_result.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart'
    hide PermissionStatus;
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/base/base_local_rx.dart';
import 'package:flutter_screenshot_callback/flutter_screenshot_callback.dart';
import 'package:location/location.dart';
import 'package:metadata_fetch/metadata_fetch.dart';
import 'package:path/path.dart' as path;
import 'package:uuid/uuid.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';
import 'package:collection/collection.dart';
import 'package:chat_package/extention/custom_ex.dart';
import 'package:collection/collection.dart';

enum Events { SendMessage, ReciveMessage, SoanTinNhan, LoadMore }

class MessageDetailController extends GetxController {
  RxList<ChatMessage> listChatMessage = <ChatMessage>[].obs;
  RxList<DataUsers> listUserInGroup = <DataUsers>[].obs;
  Rx<GroupInfo> groupMain = GroupInfo().obs;
  RxInt totalMessageNew = 0.obs;
  RxBool isSoanTin = false.obs;
  RxBool isShowBottomMessage = false.obs;
  RxBool sendButtonIcon = false.obs;
  RxList listImageScreenshot = <ScreenshotCallbackData>[].obs;

  bool isFind = false;
  bool isLoadingList = false;

  // screenshot
  RxList listIndexImagePick = <int>[].obs;

  StreamSubscription? _subscriptionTinhTrang;
  StreamSubscription? _subUpdateGroupIconAndGroupName;
  StreamSubscription? _subDeleteGroup;
  StreamSubscription? _subDeleteHistoryMessageGroup;
  StreamSubscription? _subThuHoiMessageByMessageID;
  StreamSubscription? _subDeleteMessageByMessageID;
  StreamSubscription? _subDeleteUserInGroup;
  StreamSubscription? _subCountMessageNew;
  StreamSubscription? _subUpdateGhim;
  StreamSubscription? _subUpdateReaction;
  StreamSubscription? _subAddUserInGroup;

  int pageNum = 0;
  int pageLimit = 30;

  Future<void> eventHandler(Events events,
      {ChatMessage? message, bool? isSoan, List<File>? files}) async {
    switch (events) {
      case Events.SendMessage:
        _sendMessage(message!, files);
        break;
      case Events.ReciveMessage:
        _receiveMessage();
        break;
      case Events.SoanTinNhan:
        try {
          await SignalR.instance.connection
              .send(methodName: LocalVariable.dangSoanTin, args: [
            {
              'userID': userIDChat,
              'groupKey': groupMain.value.groupKey,
              'isSoan': isSoan
            }
          ]);
        } catch (e) {
          print(e);
        }
        break;
      case Events.LoadMore:
        _loadMore();
        break;
    }
  }

  Future _sendMessage(ChatMessage message, List<File>? files) async {
    // no group
    if (groupMain.value.groupID == 0) {
      // click user online
      if (!groupMain.value.isDonVi) {
        // 2 user click to the same group that not create
        final GroupInfo? groupTemp = await SyncChat.instance
            .checkHasGroup2User(listUserInGroup.first.userID);
        if (groupTemp != null) {
          // has group 2 user

          // cap nhat group when 2 user cung click list online, nhung user kia tao nhom truoc
          groupMain.value = groupTemp;
          groupMain.value.groupName = listUserInGroup.first.fullName;
          groupMain.value.groupIcon = listUserInGroup.first.avatar;
          groupMain.refresh();

          message.groupID = groupTemp.groupID;
          message.groupKey = groupTemp.groupKey;
        } else {
          // no group 2 user => create group
          final DataUsers inf1 = DataUsers()
            ..userID = message.userIDGui!
            ..userName = message.userNameGui;

          final GroupInfo param = GroupInfo()
            ..listUserInGroup = [inf1, listUserInGroup.first]
            ..createUserID = message.userIDGui
            ..createUserName = message.userNameGui
            ..isGroup = false;

          final GroupInfo? groupTemp1 = await createGroupTraoDoi(param);
          if (groupTemp1 == null) return;
          // update current groupId when group is create
          currentGroupId.value = groupTemp1.groupID;
          currentGroupObj.value = groupTemp1;
          // update group icon and group name when user create new group
          groupMain.value = groupTemp1;
          groupMain.value.groupName = listUserInGroup.first.fullName;
          groupMain.value.groupIcon = listUserInGroup.first.avatar;
          groupMain.refresh();

          message.groupID = groupTemp1.groupID;
          message.groupKey = groupTemp1.groupKey;
        }
      } else {
        // create group for don vi
        final GroupInfo param1 = GroupInfo()
          ..listUserInGroup = groupMain.value.listUserInGroup
          ..createUserID = message.userIDGui
          ..createUserName = message.userNameGui
          ..groupKey = groupMain.value.groupKey
          ..groupName = groupMain.value.groupName
          ..lastMessage = ''
          ..isGroup = true
          ..isDonVi = true;
        final GroupInfo? groupTemp2 = await createGroupTraoDoi(param1);
        if (groupTemp2 == null) return;
        // update current groupId when group is create
        currentGroupId.value = groupTemp2.groupID;
        groupMain.value = groupTemp2;
        currentGroupObj.value = groupTemp2;
        message.groupID = groupTemp2.groupID;
        message.groupKey = groupTemp2.groupKey;
      }
    }

    switch (message.messageType) {
      case MessageType.Video:
        message.media = await Future.wait(files!.map((e) async {
          final File thumb = await MediaPicker.instance.getVideoThumb(e.path);
          return FileUpload()
            ..fileUrl = e.path
            ..thumbnail = thumb.path;
        }).toList());
        await _upLoad(message, files);
        return;
      case MessageType.Image:
        message.media = files!
            .map((e) => FileUpload(fileUrl: e.path, thumbnail: e.path))
            .toList();
        await _upLoad(message, files);
        return;
      case MessageType.File:
      case MessageType.Audio:
        message.media = files!
            .map((e) =>
                FileUpload(fileUrl: e.path, fileName: path.basename(e.path)))
            .toList();
        await _upLoad(message, files);
        return;
      case MessageType.Link:
      case MessageType.Youtube:
        _getMetadata(message);
        return;
      case MessageType.Emoticon:
      case MessageType.Reply:
      case MessageType.Text:
        break;
    }

    await _sendFunc(message);
  }

  Future _sendFunc(ChatMessage message, {bool isInsert = true}) async {
    try {
      await SignalR.instance.connection.send(
          methodName: LocalVariable.sendMessage, args: [message.toJson()]);
    } catch (e) {
      print(e);
      message.tinhTrangID = 4;
      message.tenTinhTrang = 'Gửi lỗi !';
      if (isInsert) listChatMessage.add(message);
    }
  }

  Future _getMetadata(ChatMessage mess) async {
    try {
      String _url = mess.message!;
      listChatMessage.add(mess);
      if (mess.messageType == MessageType.Link) {
        if (mess.message!.substring(0, 4).toLowerCase() != 'http')
          _url = 'https://' + mess.message!;
        final Metadata? _tempData = await MetadataFetch.extract(_url);
        mess.message = jsonEncode([
          FileResults()
            ..fileName = _tempData?.title
            ..linkFile = _url
            ..thumbnail = _tempData?.image
            ..description = _tempData?.description
            ..fileType = FileType.Link.index
        ]);
      } else {
        final Video _tempData = await YoutubeExplode().videos.get(mess.message);
        mess.message = jsonEncode([
          FileResults()
            ..fileName = _tempData.title
            ..linkFile = _url
            ..thumbnail = _tempData.thumbnails.standardResUrl
            ..description = _tempData.description
            ..fileType = FileType.Youtube.index
        ]);
      }

      await SignalR.instance.connection
          .send(methodName: LocalVariable.sendMessage, args: [mess.toJson()]);
    } catch (e) {
      mess.messageType = MessageType.Text;
      mess.message = mess.message!.replaceAll('https://', '');
      listChatMessage
          .removeWhere((element) => element.messageKey == mess.messageKey);
      await _sendFunc(mess, isInsert: false);
    }
  }

  Future _upLoad(ChatMessage message, List<File> files) async {
    try {
      listChatMessage.add(message);

      if (message.messageType != MessageType.Text &&
          message.messageType != MessageType.Emoticon &&
          message.messageType != MessageType.Reply)
        SyncChat.instance.insertTempUpload(message);
      final dataUpload = await ApiRequest.instance.compressAndUpload(files);
      if (dataUpload != null) {
        final List data = jsonDecode(dataUpload.response!);
        final List<FileResults> imageUpload =
            data.map((e) => FileResults.fromJson(e)).toList();
        if (imageUpload != null) {
          switch (message.messageType) {
            case MessageType.Video:
              final List<FileResults> _lstTemp = imageUpload
                  .where((element) => element.fileType == 1)
                  .toList();

              for (final i in _lstTemp) {
                i.thumbnail = imageUpload
                    .firstWhereOrNull((element) =>
                        element.fileType == 0 &&
                        path.basenameWithoutExtension(element.fileName!) ==
                            path.basenameWithoutExtension(i.fileName!))
                    ?.linkFile;
              }
              // _temp!.thumbnail = imageUpload
              //     .firstWhereOrNull((element) => element.fileType == 0)
              //     ?.linkFile;
              message.message = jsonEncode(_lstTemp);
              break;
            case MessageType.File:
              //do file sẽ bị lẫn lộn với image và video nên khi up xong gán type là 2
              imageUpload.map((e) => e.fileType = 2).toList();
              message.message = jsonEncode(imageUpload);
              break;
            case MessageType.Audio:
              imageUpload.map((e) => e.fileType = 5).toList();
              message.message = jsonEncode(imageUpload);
              break;
            case MessageType.Image:
              final List<FileResults> _temp = imageUpload
                  .where((element) =>
                      !element.fileName!.contains('thumbnailChat_'))
                  .toList();

              for (final FileResults i in _temp) {
                i.thumbnail = imageUpload
                        .firstWhereOrNull((element) =>
                            element.fileName
                                ?.contains('thumbnailChat_${i.fileName}') ??
                            false)!
                        .linkFile ??
                    '';
              }

              // final FileResults? _temp = imageUpload
              //     .firstWhereOrNull((element) => element.fileType == 1);
              //
              // _temp!.thumbnail = imageUpload
              //     .firstWhereOrNull((element) => element.fileType == 0)
              //     ?.linkFile;
              message.message = jsonEncode(_temp);
              break;
            default:
              message.message = jsonEncode(imageUpload);
              break;
          }
          await SignalR.instance.connection.send(
              methodName: LocalVariable.sendMessage, args: [message.toJson()]);
        } else {
          await SyncChat.instance.deleteMesageGhost(groupMain.value.groupID);
          LoadingWidget.instance
              .snackBarThongBao('Thông báo', 'Gửi file lỗi');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future _receiveMessage() async {
    try {
      if (groupMain.value.groupKey != null) {
        await SyncChat.instance
            .getDataMess(groupMain.value.groupID,
                offset: pageNum, limit: pageLimit)
            .then((value) {
          if (value.isNotEmpty) {
            listChatMessage.value = value;
            // final ChatMessage item = listChatMessage.first;
            // if (item.messageID != 0) _updateTinhTrang(item);
            //nên dùng isolate
            for (final ChatMessage i in listChatMessage) {
              if (i.messageID > 0 &&
                  (i.tinhTrangID != 3 ||
                      !userIDChat.toString().contains(i.userIDDaXem ?? '')))
                _updateTinhTrang(i);
            }
          }
        });
      }

      SignalR.instance.connection.on(LocalVariable.dangSoanTin, (arguments) {
        if (arguments![0]['groupKey'] == groupMain.value.groupKey &&
            arguments[0]['userID'] != userIDChat) {
          isSoanTin.value = arguments[0]['isSoan'];
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _loadMore() {
    if (listChatMessage != null && listChatMessage.isNotEmpty) {
      pageNum++;
      SyncChat.instance
          .getDataMess(groupMain.value.groupID,
              offset: pageNum, limit: pageLimit)
          .then((value) {
        if (value != null && value.isNotEmpty) {
          assert(value != null);
          listChatMessage.insertAll(0, value);

          //nên dùng isolate
          for (final ChatMessage i in value) {
            if (i.messageID != 0 && i.tinhTrangID != 3) _updateTinhTrang(i);
          }
        }
      });
    }
  }

  Future reloadList() async {
    final List<ChatMessage> values = await SyncChat.instance.getDataMess(
        groupMain.value.groupID,
        offset: pageNum,
        limit: pageLimit);
    if (values.isNotEmpty) listChatMessage.value = values;
  }

  Future<int> findPosition(int messID, int groupID) async {
    int _index =
        listChatMessage.indexWhere((element) => element.messageID == messID);
    if (_index == -1) {
      isFind = true;
      final List<ChatMessage> _tempList =
          await SyncChat.instance.getDataMessFindPosition(groupID, messID, 40);
      if (_tempList != null && _tempList.isNotEmpty) {
        _index = _tempList.indexWhere((element) => element.messageID == messID);
        listChatMessage.value = _tempList;
      } else {
        //trỏ xuống db tìm
        showSnackBar('Tin nhắn đã bị xóa hoặc không còn lưu trên thiết bị');
      }
    }
    return _index;
  }

  Future loadMoreFindPosition(int messID,
      {int limit = 30, int selectType = 3}) async {
    isLoadingList = true;
    if (listChatMessage != null && listChatMessage.isNotEmpty) {
      final List<ChatMessage> _tempList = await SyncChat.instance
          .getDataMessFindPosition(groupMain.value.groupID, messID, limit,
              selectType: selectType);
      if (_tempList != null && _tempList.isNotEmpty) {
        if (selectType == 2)
          listChatMessage.insertAll(0, _tempList);
        else
          listChatMessage.addAll(_tempList);
      } else {
        showSnackBar('Đã tải hết tin');
        //trỏ xuống db tìm
        // if (selectType != 3) showSnackBar('Đã tải hết tin');
      }
    }
    isLoadingList = false;
  }

  void listenSignRSyncChat(GroupInfo groupInfo) {
    _subUpdateGhim ??= updateGhimMess.stream.listen((event) {
      if (event?['groupID'] == groupMain.value.groupID) {
        groupMain.value.ghimMessage = event!['ghimMessage'] ?? '';
        groupMain.refresh();
      }
    });

    _subCountMessageNew = chatMessageCtrl.listen((data) async {
      if (data.groupID == groupInfo.groupID && data.userIDGui != userIDChat) {
        if (isShowBottomMessage.value) {
          totalMessageNew++;
        }
      }

      //check groupKey để biết được đang ở group nào
      if (data.groupKey == groupMain.value.groupKey) {
        if (data.userIDGui == userIDChat &&
            CheckMessType(data.messageType).check()) {
          // delete mess has ID = 0
          listChatMessage
              .removeWhere((element) => element.messageKey == data.messageKey);

          await Future.delayed(const Duration(milliseconds: 500));
        }

        listChatMessage.add(data);

        try {
          SignalR.instance.connection
              .send(methodName: LocalVariable.dangSoanTin, args: [
            {
              'userID': userIDChat,
              'groupKey': groupMain.value.groupKey,
              'isSoan': false
            }
          ]);
        } catch (e) {
          print(e);
        }

        _updateTinhTrang(data);
      }
      // else // in other group - has new mess
      // if (data.userIDGui != userIDChat)
      //   SyncChat.instance.updateDaXemGroup(data.groupID!);
    });

    _subscriptionTinhTrang ??= updateTinhTrangCtrl.stream.listen((event) {
      if (event?['groupKey'] == groupMain.value.groupKey &&
          event?['userID'] != userIDChat) {
        for (final ChatMessage i in listChatMessage) {
          if (i.messageID == event!['messageID']) {
            if (event['tinhTrangID'] == 3) {
              i.userIDDaXem = event['userIDDaXem'];
              i.tinhTrangID = event['tinhTrangID'];
              i.tenTinhTrang = event['tenTinhTrang'];
            } else if (i.tinhTrangID != 3) {
              i.tinhTrangID = event['tinhTrangID'];
              i.tenTinhTrang = event['tenTinhTrang'];
            }
            break;
          }
        }
        listChatMessage.refresh();
      }
    });

    _subUpdateReaction ??= updateReaction.stream.listen((event) {
      if (event?['groupID'] == groupMain.value.groupID) {
        listChatMessage.firstWhereOrNull((element) {
          if (element.messageID == event!['messageID']) {
            element.reaction = event['reaction'];
            return true;
          }
          return false;
        });
        listChatMessage.refresh();
      }
    });

    _subUpdateGroupIconAndGroupName ??=
        updateGroupIconAndGroupNameCtrl.stream.listen((data) {
      if (data != null) if (data['groupID'].toString() ==
          groupInfo.groupID.toString())
        SyncChat.instance.getGroupByGroupID(groupInfo.groupID).then((value) {
          groupMain.value = value!;
        });
    });

    _subDeleteGroup ??= deleteGroupCtrl.listen((data) {
      Get.offAllNamed(GroupInfoUI.ROUTER_NAME);
    });

    _subDeleteHistoryMessageGroup ??=
        deleteHistoryMessageCtrl.stream.listen((data) {
      if (data == groupInfo.groupID) {
        listChatMessage.clear();
      }
    });

    _subThuHoiMessageByMessageID ??=
        thuHoiMessageByMessageIDCtrl.stream.listen((event) {
      if (groupInfo.groupID == event?['groupID']) {
        final List<String> _listMessageID =
            (event!['oldMessageID'] as String).split(',');
        if (_listMessageID != null && _listMessageID.isNotEmpty) {
          for (final idMessage in _listMessageID) {
            for (final mess in listChatMessage)
              if (mess.messageID == int.parse(idMessage)) mess.isThuHoi = true;
          }
          listChatMessage.refresh();
        }
      }
    });

    _subDeleteMessageByMessageID ??=
        deleteMessageByMessageIDCtrl.stream.listen((event) {
      if (groupInfo.groupID == event?['groupID']) {
        final List<String> _listMessageID =
            (event!['oldMessageID'] as String).split(',');
        if (_listMessageID != null && _listMessageID.isNotEmpty) {
          for (final idMessage in _listMessageID) {
            listChatMessage
                .removeWhere((mess) => mess.messageID == int.parse(idMessage));
          }
          listChatMessage.refresh();
        }
      }
    });

    _subDeleteUserInGroup ??=
        deleteUserInGroupCtrl.stream.listen((userInGroupEvent) {
      if (userInGroupEvent?.groupID == groupMain.value.groupID)
        SyncChat.instance
            .getUserByGroupID(groupMain.value.groupID, userIDChat)
            .then((users) {
          listUserInGroup.value = users;
        });
    });

    _subAddUserInGroup ??= insertUserInGroupCtrl.stream.listen((groupEvent) {
      if (groupEvent?.groupID == groupMain.value.groupID)
        SyncChat.instance
            .getUserByGroupID(groupMain.value.groupID, userIDChat)
            .then((users) {
          listUserInGroup.value = users;
        });
    });
  }

  void showBottomMessage(bool isShow) {
    if (!isShow) {
      totalMessageNew.value = 0;
    }
    isShowBottomMessage.value = isShow;
  }

  Future thuHoiMessageByMessageID(List<ChatMessage> messages, int index) async {
    // check cac tin nhan sau no co bi thu hoi chua
    int indexTemp = 0;
    for (int i = 0; i < index; i++) {
      if (!(listChatMessage[i].isThuHoi ?? false)) {
        indexTemp = index;
        break;
      }
    }
    await ApiRequest.instance.thuHoiMessageByMessageID(
        messages, indexTemp, listChatMessage.length - 1);
  }

  Future deleteMessageByMessageID(List<ChatMessage> messages, int index) async {
    await ApiRequest.instance.deleteMessageByMessageID(
        messages, index, userIDChat, listChatMessage.length - 1);
  }

  Future<GroupInfo?> createGroupTraoDoi(GroupInfo groupInfo) async {
    final GroupInfo? _groupInfo =
        await ApiRequest.instance.createGroupMessage(groupInfo);
    if (_groupInfo != null) {
      await SignalR.instance.connection.send(
          methodName: LocalVariable.groupCreated,
          args: [_groupInfo.toJson(false, false)]);
      return _groupInfo;
    }
    return null;
  }

  void getListUserInGroup() {
    SyncChat.instance
        .getUserByGroupIDHasMe(groupMain.value.groupID)
        .then((value) {
      if (value != null) listUserInGroup.value = value;
    });
  }

  void _updateTinhTrang(ChatMessage data) {
    if (data.userIDGui != userIDChat) {
      if (data.userIDDaXem == null) {
        final Map<String, dynamic> pram = {
          'groupID': data.groupID,
          'groupKey': data.groupKey,
          'messageID': data.messageID,
          'messageKey': data.messageKey,
          'isGroup': groupMain.value.isGroup,
          'userID': userIDChat,
          'userIDGui': data.userIDGui,
          'userIDDaXem': userIDChat.toString(),
          'tinhTrangID': 3,
          'tenTinhTrang': 'Đã xem'
        };
        // pram.putIfAbsent('tinhTrangID', () => 3);
        // pram.putIfAbsent('tenTinhTrang', () => 'Đã xem');
        SignalR.instance.connection
            .send(methodName: LocalVariable.updateTinhTrangTin, args: [pram]);
      } else if (!data.userIDDaXem!.contains(userIDChat.toString())) {
        final String temp = data.userIDDaXem!;
        if (temp != null) data.userIDDaXem = temp + ',$userIDChat';
        final Map<String, dynamic> pram = {
          'groupID': data.groupID,
          'groupKey': data.groupKey,
          'messageID': data.messageID,
          'messageKey': data.messageKey,
          'userID': userIDChat,
          'isGroup': groupMain.value.isGroup,
          'userIDGui': data.userIDGui,
          'userIDDaXem': data.userIDDaXem,
          'tinhTrangID': 3,
          'tenTinhTrang': 'Đã xem'
        };
        // pram.putIfAbsent('tinhTrangID', () => 3);
        // pram.putIfAbsent('tenTinhTrang', () => 'Đã xem');
        SignalR.instance.connection
            .send(methodName: LocalVariable.updateTinhTrangTin, args: [pram]);
      }
    }
  }

  /// ----------------------------Begin search mess in Group-------------------------------------------
  RxList<ChatMessage> listMessSearch = <ChatMessage>[].obs;
  RxList<FileUpload> listMediaSearch = <FileUpload>[].obs;
  bool isLoadMoreSearchMessText = true;
  bool isLoadMoreSearchMedia = true;
  List<ChatMessage> listMessFirstLoad = <ChatMessage>[];
  List<FileUpload> listMediaFirstLoad = <FileUpload>[];

  Future<void> getMessFirstLoad() async {
    final List<ChatMessage>? listMessTemp = await SyncChat.instance
        .getMessageTextInGroupWithNumber(
            number: 15, groupId: groupMain.value.groupID);
    if (listMessTemp != null) {
      listMessFirstLoad.addAll(listMessTemp);
      listMessSearch.value = listMessTemp;
    }
  }

  Future<void> getMediaFirstLoad() async {
    final List<FileUpload>? listFileTemp = await SyncChat.instance
        .getFileAndVideoByGroupIDWithNumber(groupMain.value.groupID, 15);
    if (listFileTemp != null) {
      listMediaFirstLoad.addAll(listFileTemp);
      listMediaSearch.value = listFileTemp;
    }
  }

  Future<void> searchMessageText(
      {required int limit,
      required int pageIndex,
      required String query}) async {
    final List<ChatMessage>? listMessTemp = await SyncChat.instance
        .getSearchMessageTextInGroup(
            groupID: groupMain.value.groupID,
            limit: limit,
            pageIndex: pageIndex,
            query: query);
    if (listMessTemp != null) {
      listMessSearch.value = listMessTemp;
      isLoadMoreSearchMedia = true;
    } else
      listMessSearch.clear();
  }

  Future<void> searchMedia(
      {required int limit,
      required int pageIndex,
      required String keySearch}) async {
    final List<FileUpload>? listMediaTemp = await SyncChat.instance
        .searchFileAndVideoByNameInGroup(
            groupId: groupMain.value.groupID,
            limit: limit,
            pageIndex: pageIndex,
            keySearch: keySearch);
    if (listMediaTemp != null) {
      listMediaSearch.value = listMediaTemp;
      isLoadMoreSearchMedia = true;
    } else
      listMediaSearch.clear();
  }

  Future<void> loadMoreSearchMessageText(
      {required int limit,
      required int pageIndex,
      required String query}) async {
    final List<ChatMessage>? listMessTemp = await SyncChat.instance
        .getSearchMessageTextInGroup(
            groupID: groupMain.value.groupID,
            limit: limit,
            pageIndex: pageIndex,
            query: query);
    if (listMessTemp != null && listMessTemp.isNotEmpty) {
      listMessSearch.addAll(listMessTemp);
    } else
      isLoadMoreSearchMessText = false;
  }

  Future<void> loadMoreSearchMedia(
      {required int limit,
      required int pageIndex,
      required String keySearch}) async {
    final List<FileUpload>? listMediaTemp = await SyncChat.instance
        .searchFileAndVideoByNameInGroup(
            groupId: groupMain.value.groupID,
            limit: limit,
            pageIndex: pageIndex,
            keySearch: keySearch);
    if (listMediaTemp != null && listMediaTemp.isNotEmpty)
      listMediaSearch.addAll(listMediaTemp);
    else
      isLoadMoreSearchMedia = false;
  }

  void resetDataSearch() {
    listMessSearch.value = listMessFirstLoad;
    listMediaSearch.value = listMediaFirstLoad;
  }

  /// ----------------------------End search mess in Group-------------------------------------------

  /// -----------------------------Start share location-----------------------------------------------
  final Location _location = Location();

  bool _serviceEnabled = false;
  late PermissionStatus _permissionGranted;
  late LocationData locationData;

  Future<bool> setUpLocation() async {
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }

    return true;
  }

  Future<void> shareLocation() async {
    final bool canShareLocation = await setUpLocation();
    if (!canShareLocation)
      LoadingWidget.instance.snackBarThongBao(
          'Thông báo', 'Bạn cần cho phép quyền truy cập vị trí trên thiết bị!');
    else {
      locationData = await _location.getLocation();
      final String dataShareLocation =
          'https://www.google.com/maps/search/?api=1&query=${locationData.latitude},${locationData.longitude}';
      _sendMessage(
          ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.Link,
              message: dataShareLocation,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: groupMain.value.groupID,
              groupKey: groupMain.value.groupKey),
          null);
    }
  }

  /// -----------------------------End share location-----------------------------------------------

  StreamSubscription? _onResumed;
  @override
  void onInit() {
    _onResumed ??
        isResumed.stream.listen((event) {
          if (event!) _receiveMessage();
        });
    super.onInit();
  }

  @override
  void onClose() {
    _subscriptionTinhTrang?.cancel();
    _subscriptionTinhTrang = null;
    _subDeleteGroup?.cancel();
    _subDeleteGroup = null;
    _subUpdateGroupIconAndGroupName?.cancel();
    _subUpdateGroupIconAndGroupName = null;
    _subDeleteHistoryMessageGroup?.cancel();
    _subDeleteHistoryMessageGroup = null;
    _subThuHoiMessageByMessageID?.cancel();
    _subAddUserInGroup?.cancel();
    _subThuHoiMessageByMessageID = null;
    _subDeleteMessageByMessageID?.cancel();
    _subDeleteMessageByMessageID = null;
    _subDeleteUserInGroup?.cancel();
    _subDeleteUserInGroup = null;
    _subCountMessageNew?.cancel();
    _subCountMessageNew = null;
    _subUpdateGhim?.cancel();
    _subUpdateGhim = null;
    _subUpdateReaction?.cancel();
    _subUpdateReaction = null;
    _onResumed?.cancel();
    _onResumed = null;
    _subAddUserInGroup?.cancel();
    _subAddUserInGroup = null;
    super.onClose();
  }
}
