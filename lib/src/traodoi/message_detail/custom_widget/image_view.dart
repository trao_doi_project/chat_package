import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';

import '../../../../chat_package.dart';
import 'full_photo.dart';

class ImageView extends StatelessWidget {
  final ChatMessage chatMessage;
  final List<DataUsers> itemUser;
  const ImageView(this.chatMessage, this.itemUser);
  @override
  Widget build(BuildContext context) {
    if (chatMessage.media == null || chatMessage.media!.isEmpty)
      return const SizedBox.shrink();
    final FileUpload _item =
        ((chatMessage.media != null && chatMessage.media!.isNotEmpty)
            ? chatMessage.media!.first
            : null)!;
    final int _countImage = chatMessage.media!.length;
    if (_item.messageID != null) {
      if (chatMessage.media!.length == 1)
        return Container(
          constraints: BoxConstraints(
            maxWidth: Get.width * 0.5,
            maxHeight: Get.width * 0.7,
          ),
          child: GestureDetector(
            onTap: () => onTapImage(0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                urlImageNetwork: baseUrlApiChat +
                    (chatMessage.media!.first.thumbnail ??
                        chatMessage.media!.first.fileUrl ??
                        ''),
                imageAssetDefault: ImageVariable.noimg,
              ),
            ),
          ),
        );
      else
        return Container(
          constraints: BoxConstraints(
            maxWidth: Get.width * 0.7,
          ),
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: GridView.builder(
                physics: const NeverScrollableScrollPhysics(),
                itemCount: (_countImage > 6) ? 6 : _countImage,
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: (_countImage == 2) ? 2 : 3,
                    mainAxisSpacing: 5,
                    crossAxisSpacing: 5),
                itemBuilder: (context, index) {
                  return GestureDetector(
                      onTap: () => onTapImage(index),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(6),
                        child: (_countImage > 6 && index == 5)
                            ? Stack(
                                fit: StackFit.expand,
                                alignment: Alignment.center,
                                children: [
                                  ImageNetworkWidget(
                                    packageName: LocalVariable.namePackage,
                                    urlImageNetwork: baseUrlApiChat +
                                        (chatMessage.media![index].thumbnail ??
                                            chatMessage.media![index].fileUrl ??
                                            ''),
                                    imageAssetDefault: ImageVariable.noimg,
                                  ),
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(0.8),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          '${_countImage - 5}',
                                          style: const TextStyle(
                                            color: Colors.grey,
                                            fontSize: 32,
                                          ),
                                        ),
                                        const Icon(
                                          Icons.add,
                                          color: Colors.grey,
                                          size: 32,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                            : ImageNetworkWidget(
                          packageName: LocalVariable.namePackage,
                                urlImageNetwork: baseUrlApiChat +
                                    (chatMessage.media![index].thumbnail ??
                                        chatMessage.media![index].fileUrl ??
                                        ''),
                                imageAssetDefault: ImageVariable.noimg,
                              ),
                      ));
                }),
          ),
        );
    } else {
      if (chatMessage.media!.length == 1)
        return GestureDetector(
          onTap: () => onTapImage(0),
          child: ImageNetworkWidget(
            packageName: LocalVariable.namePackage,
            urlImageOffline: chatMessage.media!.first.fileUrl!,
            imageAssetDefault: ImageVariable.noimg,
            isOnline: false,
          ),
        );
      else
        return Directionality(
          textDirection: TextDirection.rtl,
          child: GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              itemCount: (_countImage > 6) ? 6 : _countImage,
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, mainAxisSpacing: 5, crossAxisSpacing: 5),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => onTapImage(index),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: (_countImage > 6 && index == 5)
                        ? Stack(
                            fit: StackFit.expand,
                            alignment: Alignment.center,
                            children: [
                              ImageNetworkWidget(
                                packageName: LocalVariable.namePackage,
                                urlImageOffline:
                                    chatMessage.media![index].fileUrl!,
                                imageAssetDefault: ImageVariable.noimg,
                                isOnline: false,
                              ),
                              DecoratedBox(
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.5),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      '${_countImage - 5}',
                                      style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 32,
                                      ),
                                    ),
                                    const Icon(
                                      Icons.add,
                                      color: Colors.grey,
                                      size: 32,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        : ImageNetworkWidget(
                      packageName: LocalVariable.namePackage,
                            urlImageOffline: chatMessage.media![index].fileUrl!,
                            imageAssetDefault: ImageVariable.noimg,
                            isOnline: false,
                          ),
                  ),
                );
              }),
        );
    }
  }

  void onTapImage(int index) {
    Get.dialog(FullPhotoUI(
      title: itemUser
              .firstWhereOrNull(
                  (element) => element.userID == chatMessage.userIDGui)
              ?.fullName ??
          fullNameChat,
      images: chatMessage.media!,
      currentIndex: index,
    ));
  }
}
