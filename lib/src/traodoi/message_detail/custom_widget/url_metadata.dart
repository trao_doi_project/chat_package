import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';

import '../../../../chat_package.dart';

class URLMetadata extends StatefulWidget {
  final ChatMessage mess;
  const URLMetadata(this.mess);

  @override
  _URLMetadataState createState() => _URLMetadataState();
}

class _URLMetadataState extends State<URLMetadata> {
  String locationDescription = '';
  @override
  void initState() {
    if (widget.mess.userIDGui == userIDChat)
      locationDescription = 'Vị trí của bạn';
    else
      SyncChat.instance.getDataUserByID(widget.mess.userIDGui!).then((value) {
        setState(() {
          locationDescription = 'Vị trí của ${value!.fullName}';
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.mess.messageID == 0 ||
        widget.mess.media == null ||
        widget.mess.media!.isEmpty)
      return GestureDetector(
        onTap: () async {
          if (await canLaunch(widget.mess.message!))
            launch(widget.mess.message!);
        },
        child: (widget.mess.message!.contains('maps/search/?api=1'))
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    ImageVariable.location,
                    package: 'chat_package',
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    locationDescription,
                    style: const TextStyle(color: Colors.blue, fontSize: 16),
                  )
                ],
              )
            : Text(
                widget.mess.message ?? '',
                style: const TextStyle(color: Colors.blue),
              ),
      );

    final FileUpload _tempData = widget.mess.media!.first;
    if (widget.mess.messageType == MessageType.Youtube) {
      return GestureDetector(
        onTap: () async {
          launch(_tempData.fileUrl!, universalLinksOnly: true);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _tempData.fileName ?? '',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              _tempData.moTa ?? '',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            Text(
              _tempData.fileUrl ?? '',
              style: const TextStyle(
                  color: Colors.blue, decoration: TextDecoration.underline),
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  imageAssetDefault: ImageVariable.noimg,
                  urlImageNetwork: _tempData.thumbnail,
                ),
                Opacity(
                  opacity: 0.5,
                  child: Image.asset(
                    ImageVariable.youtube,
                    width: 40,
                    package: 'chat_package',
                  ),
                )
              ],
            ),
          ],
        ),
      );
    } else {
      if (_tempData.fileUrl!.contains('maps/search/?api=1'))
        return GestureDetector(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ExtendedImage.network(
                _tempData.thumbnail ?? '$baseUrlApiChat/noimg.png',
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                loadStateChanged: (ExtendedImageState state) {
                  switch (state.extendedImageLoadState) {
                    case LoadState.completed:
                      return ExtendedRawImage(
                        image: state.extendedImageInfo?.image,
                        fit: BoxFit.cover,
                      );
                    default:
                      return Image.asset(
                        ImageVariable.location,
                        fit: BoxFit.cover,
                        package: 'chat_package',
                      );
                  }
                },
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                locationDescription,
                style: const TextStyle(color: Colors.blue, fontSize: 16),
              )
            ],
          ),
          onTap: () async {
            final String data = _tempData.fileUrl?.split('=').last ?? '';
            final List<String> ll = data.split(',');
            final double lat = double.parse(ll[0]);
            final double long = double.parse(ll[1]);

            final availableMaps = await MapLauncher.installedMaps;

            await availableMaps.first.showMarker(
                coords: Coords(lat, long),
                title: locationDescription,
                zoom: 12);
          },
        );
      return GestureDetector(
        onTap: () async {
          launch(_tempData.fileUrl!, universalLinksOnly: true);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ImageNetworkWidget(
              packageName: LocalVariable.namePackage,
              imageAssetDefault: ImageVariable.noimg,
              urlImageNetwork: _tempData.thumbnail,
            ),
            Text(
              _tempData.fileUrl ?? '',
              style: const TextStyle(
                  color: Colors.blue, decoration: TextDecoration.underline),
            ),
            Text(
              _tempData.fileName ?? '',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(_tempData.moTa ?? ''),
          ],
        ),
      );
    }
  }
}
