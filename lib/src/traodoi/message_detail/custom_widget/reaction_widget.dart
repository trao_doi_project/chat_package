import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/customlayout/facebook_reaction/flutter_reaction_button.dart';
import 'package:flutter/material.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
part 'bien_chung.dart';

class ReactionWidget extends StatelessWidget {
  final ChatMessage chatMessage;
  final double size;
  const ReactionWidget(this.chatMessage, {this.size = 17});
  @override
  Widget build(BuildContext context) {
    final Image _icon = (chatMessage.reaction != null)
        ? Image.asset(
            chatMessage.reaction!,
            package: 'chat_package',
          )
        : Image.asset(
            ImageVariable.ic_like,
            package: 'chat_package',
          );
    return SizedBox(
      height: size,
      child: FlutterReactionButtonCheck(
        onReactionChanged: (reaction, isChecked) async {
          final String _itemReaction =
              ((reaction!.icon as Image).image as AssetImage).assetName;
          await ApiRequest.instance.appTraoDoiV2Reaction({
            'groupID': chatMessage.groupID,
            'messageID': chatMessage.messageID,
            'replyData': _itemReaction,
            'message': chatMessage.userIDGui.toString(),
          });
        },
        reactions: reactionLst,
        initialReaction: Reaction(id: 7, icon: _icon),
        selectedReaction: Reaction(
          id: 7,
          icon: _icon,
        ),
      ),
    );
  }
}
