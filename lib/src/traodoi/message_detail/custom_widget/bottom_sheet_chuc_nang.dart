import 'dart:ui';

import 'package:chat_package/base/image_variable.dart';
import 'package:flutter/material.dart';

class BottomSheetChucNang extends StatelessWidget {
  final List<BottomSheetItem> _btomItems = <BottomSheetItem>[
    BottomSheetItem(1, 'Máy ảnh', image: ImageVariable.cameraPick),
    BottomSheetItem(2, 'Thư viện', image: ImageVariable.gallery),
    BottomSheetItem(3, 'Video', image: ImageVariable.videoPlay),
    BottomSheetItem(4, 'Tệp tin', image: ImageVariable.files),
    BottomSheetItem(5, 'Vị trí', image: ImageVariable.location)
    // BottomSheetItem(5, 'Clean Cache', image: ImageVariable.angry2)
    // BottomSheetItem(6, 'test', image: ImageVariable.iconGroup)
  ];

  final Function(int id) actionClick;
  BottomSheetChucNang({required this.actionClick});

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
      child: DecoratedBox(
          decoration: const BoxDecoration(color: Colors.white),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              itemCount: _btomItems.length,
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
              ),
              itemBuilder: (context, index) {
                final BottomSheetItem _item = _btomItems[index];
                return GestureDetector(
                  onTap: () => actionClick(_item.iD),
                  child: Column(
                    children: [
                      Image.asset(
                        _item.image!,
                        width: 50,
                        package: 'chat_package',
                      ),
                      Text(_item.title)
                    ],
                  ),
                );
              },
            ),
          )),
    );
  }
}

class BottomSheetItem {
  int iD;
  String title;
  String? image;
  BottomSheetItem(this.iD, this.title, {this.image});
}
