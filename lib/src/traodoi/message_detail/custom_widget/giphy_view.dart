import 'package:chat_package/base/local_variable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';

// class GiphyView extends StatelessWidget {
//   final String url;
//   const GiphyView(this.url);
//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       alignment: Alignment.center,
//       children: [
//         ImageNetworkWidget(
//           urlImageNetwork: url,
//           imageAssetDefault: ImageVariable.noimg,
//           imageWidth: Get.width * 0.5,
//           imageHeight: Get.width * 0.5,
//           borderRadius: BorderRadius.circular(12),
//         ),
//         Positioned(
//           bottom: 0,
//           right: 0,
//           child: Image.asset(
//             'assets/giphylogo.png',
//             width: Get.width * 0.1,
//             package: 'chat_package',
//           ),
//         )
//       ],
//     );
//   }
// }

class GiphyView extends StatefulWidget {
  final String url;
  const GiphyView(this.url);

  @override
  _GiphyViewState createState() => _GiphyViewState();
}

class _GiphyViewState extends State<GiphyView> {
  String _urlImage = '';
  String _tempImage = '';
  bool _isStop = false;

  @override
  void initState() {
    _urlImage = widget.url.replaceAll('giphy.gif?', '200w.gif?');
    // TODO: implement initState
    if (mounted) _getMetadata();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        GestureDetector(
          onTap: _onTap,
          child: ImageNetworkWidget(
            packageName: LocalVariable.namePackage,
            urlImageNetwork: _urlImage,
            imageAssetDefault: ImageVariable.noimg,
            imageWidth: Get.width * 0.5,
            imageHeight: Get.width * 0.5,
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Image.asset(
            'assets/giphylogo.png',
            width: Get.width * 0.1,
            package: 'chat_package',
          ),
        )
      ],
    );
  }

  Future<void> _getMetadata() async {
    Future.delayed(const Duration(seconds: 5)).whenComplete(() {
      _urlImage = widget.url.replaceAll('giphy.gif?', '200_s.gif?');
      _tempImage = _urlImage;
      _isStop = true;

      if (mounted) setState(() {});
    });
  }

  void _onTap() {
    _isStop = !_isStop;
    if (!_isStop) {
      _urlImage = widget.url.replaceAll('giphy.gif?', '200w.gif?');
      Future.delayed(const Duration(seconds: 5)).whenComplete(() {
        _urlImage = _tempImage;
        _isStop = true;
        if (mounted) setState(() {});
      });
    } else
      _urlImage = _tempImage;
    if (mounted) setState(() {});
  }
}
