import 'package:chat_package/base/local_variable.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:path/path.dart' as path;

import '../../../../chat_package.dart';

class ShowGhimMess extends StatelessWidget {
  final ReplyModel? ghimMessData;
  final Function(int messID)? timViTri;
  final Function(bool boGhim)? optionMessaGhim;
  final IconData? iconData;
  const ShowGhimMess(
      {this.ghimMessData, this.timViTri, this.optionMessaGhim, this.iconData});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: _showGhimTin(),
      onTap: (timViTri == null) ? null : () => timViTri!(ghimMessData!.messID!),
    );
  }

  Container _showGhimTin() {
    final Text _name = Text(ghimMessData!.nguoiGui!,
        style: const TextStyle(fontWeight: FontWeight.bold));
    switch (MessageType.values[ghimMessData!.messType ?? 0]) {
      case MessageType.Reply:
      case MessageType.Text:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(
                        ghimMessData!.oldMess!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.Image:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          height: Get.height * 0.07,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  imageAssetDefault: ImageVariable.noimg,
                  imageWidth: 35,
                  urlImageNetwork: baseUrlApiChat + (ghimMessData!.link ?? ''),
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Hình Ảnh]'),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.Video:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    ImageNetworkWidget(
                      packageName: LocalVariable.namePackage,
                      imageAssetDefault: ImageVariable.noimg,
                      imageWidth: 35,
                      urlImageNetwork:
                          baseUrlApiChat + (ghimMessData!.link ?? ''),
                    ),
                    Image.asset(
                      ImageVariable.playVideo,
                      width: Get.width * 0.05,
                      package: 'chat_package',
                    )
                  ],
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Video]'),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.File:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Image.asset(
                  ImageVariable.fileExtensionIcon(path.extension(
                    ghimMessData!.oldMess!,
                  )),
                  width: 35,
                  fit: BoxFit.cover,
                  package: 'chat_package',
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(
                        ghimMessData!.oldMess!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.Link:
      case MessageType.Youtube:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ExtendedImage.network(
                  ghimMessData!.link!,
                  width: 35,
                  cache: true,
                  enableMemoryCache: true,
                  fit: BoxFit.cover,
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(ghimMessData!.oldMess!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.underline)),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.Emoticon:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  imageAssetDefault: ImageVariable.noimg,
                  imageWidth: 35,
                  urlImageNetwork: ghimMessData!.link!,
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Sticker]'),
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      case MessageType.Audio:
        return Container(
          color: Colors.grey.shade100.withOpacity(0.8),
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Image.asset(
                  ImageVariable.audio,
                  width: 35,
                  fit: BoxFit.cover,
                  package: 'chat_package',
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    _name,
                    Text(
                      ghimMessData!.oldMess ?? 'Audio',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
                const SizedBox(
                  width: 5,
                ),
                IconButton(
                    icon: Icon(iconData),
                    onPressed: () => optionMessaGhim!(true))
              ],
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
