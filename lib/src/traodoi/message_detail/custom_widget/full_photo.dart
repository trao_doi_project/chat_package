import 'dart:ui';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';

import '../../../../chat_package.dart';

class FullPhotoUI extends StatefulWidget {
  final String title;
  final List<FileUpload> images;
  final int currentIndex;
  const FullPhotoUI(
      {required this.title, required this.images, required this.currentIndex});
  @override
  _FullPhotoUIState createState() => _FullPhotoUIState();
}

class _FullPhotoUIState extends State<FullPhotoUI> {
  late int tempCurrentIndex;
  final ScrollController _scrollController = ScrollController();
  late PageController _pageController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white12,
      appBar: AppBar(
        backgroundColor: Colors.white12,
        title: Text(widget.title),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.download_rounded,
              size: 28,
              color: Colors.white,
            ),
            onPressed: () async {
              await ApiRequest.instance
                  .downloadMediaInChat(widget.images[tempCurrentIndex]);
            },
          ).paddingOnly(right: 10)
        ],
      ),
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Column(
          children: [
            Expanded(
              child: ScrollConfiguration(
                behavior: DisableGlowListViewWidget(),
                child: ExtendedImageGesturePageView.builder(
                  itemCount: widget.images.length,
                  itemBuilder: (BuildContext context, int index) {
                    final item =
                        baseUrlApiChat + (widget.images[index].fileUrl ?? '');
                    final Widget image = ExtendedImage.network(
                      item,
                      fit: BoxFit.scaleDown,
                      mode: ExtendedImageMode.gesture,
                      initGestureConfigHandler: (state) {
                        return GestureConfig(
                          minScale: 1,
                          animationMinScale: 0.5,
                          maxScale: 2,
                          animationMaxScale: 2.5,
                          speed: 1,
                          inertialSpeed: 100.0,
                          initialScale: 1.0,
                          inPageView: true,
                          initialAlignment: InitialAlignment.center,
                        );
                      },
                    );
                    return Padding(
                      padding: const EdgeInsets.all(2),
                      child: image,
                    );
                  },
                  controller: _pageController,
                  onPageChanged: (value) {
                    setState(() {
                      tempCurrentIndex = value;
                    });
                  },
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ),
            if (widget.images.length > 1)
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10),
                child: SizedBox(
                  height: Get.width * 0.2,
                  child: ScrollConfiguration(
                    behavior: DisableGlowListViewWidget(),
                    child: ListView.builder(
                        controller: _scrollController,
                        scrollDirection: Axis.horizontal,
                        itemCount: widget.images.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: (tempCurrentIndex == index)
                                          ? Colors.blue
                                          : Colors.transparent,
                                      width: 3),
                                  borderRadius: BorderRadius.circular(8)),
                              child: GestureDetector(
                                onTap: () {
                                  _pageController.animateToPage(index,
                                      curve: Curves.linear,
                                      duration:
                                          const Duration(milliseconds: 100));
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: ExtendedImage.network(
                                    baseUrlApiChat +
                                        (widget.images[index].fileUrl ?? ''),
                                    cache: true,
                                    fit: BoxFit.cover,
                                    width: 40,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                ),
              )
            else
              const SizedBox(
                height: 10,
              )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _pageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _pageController = PageController(
      initialPage: widget.currentIndex,
    );

    _pageController.addListener(() {
      _scrollController.animateTo(tempCurrentIndex * 40.0,
          curve: Curves.linear, duration: const Duration(milliseconds: 100));
    });
    tempCurrentIndex = widget.currentIndex;
    super.initState();
  }
}
