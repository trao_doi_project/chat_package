import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';

class ChewieDemo extends StatefulWidget {
  const ChewieDemo(this.url, this.isFile,
      {this.title = 'Video Play', this.file, this.isDownLoad = false});
  final String title;
  final String url;
  final bool isFile;
  final bool isDownLoad;
  final FileUpload? file;

  @override
  State<StatefulWidget> createState() {
    return _ChewieDemoState();
  }
}

class _ChewieDemoState extends State<ChewieDemo> {
  late VideoPlayerController _videoPlayerController;
  late ChewieController _chewieController;
  late Future<void> _futureInitVideoPlayer;

  @override
  void initState() {
    super.initState();
    if (widget.isFile)
      _videoPlayerController = VideoPlayerController.file(File(widget.url));
    else
      _videoPlayerController = VideoPlayerController.network(widget.url);

    _futureInitVideoPlayer = initVideoPlayer();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();

    super.dispose();
  }

  Future<void> initVideoPlayer() async {
    await _videoPlayerController.initialize();
    setState(() {
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController,
        showControls: true,
        aspectRatio: _videoPlayerController.value.aspectRatio,
        autoPlay: true,
        looping: false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
        actions: <Widget>[
          if (widget.isDownLoad)
            IconButton(
              icon: const Icon(
                Icons.download_rounded,
                size: 28,
                color: Colors.white,
              ),
              onPressed: () async {
                await ApiRequest.instance.downloadMediaInChat(widget.file!);
              },
            ).paddingOnly(right: 10)
        ],
      ),
      body: Center(
          child: FutureBuilder(
        future: _futureInitVideoPlayer,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            return Center(
              child: Chewie(
                controller: _chewieController,
              ),
            );
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      )),
    );
  }
}
