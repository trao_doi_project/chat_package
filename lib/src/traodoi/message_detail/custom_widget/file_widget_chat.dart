import 'package:chat_package/base/base_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/download_file_widget.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:path/path.dart' as path;

import '../../../../chat_package.dart';

class FileWidgetChat extends StatelessWidget {
  final ChatMessage message;
  const FileWidgetChat(this.message);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    ImageVariable.fileExtensionIcon(
                        path.extension(message.media![0].fileName!)),
                    height: 30,
                    width: 30,
                    package: 'chat_package',
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4),
                      child: Text(
                        message.media![0].fileName ?? '',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      path
                          .extension(message.media![0].fileName!)
                          .replaceAll('.', '')
                          .toUpperCase(),
                      style:
                          const TextStyle(color: Colors.black54, fontSize: 12),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      message.media![0].fileSizeStr,
                      style:
                          const TextStyle(color: Colors.black38, fontSize: 10),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        DownLoadFileWidget(
          buttonSize: 30,
          buttonColor: BaseColor.primaryColor,
          iconSize: 20,
          fileName: path.basename(message.media![0].fileUrl!),
          linkFile: '$baseUrlApiChat${message.media![0].fileUrl!}',
        )
      ],
    );
  }
}
