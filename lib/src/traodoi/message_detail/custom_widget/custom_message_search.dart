import 'package:chat_package/base/local_variable.dart';
import 'package:flutter/material.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:collection/collection.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:intl/intl.dart';

import '../../../../chat_package.dart';
import '../message_detail_controller.dart';

class CustomMessageSearchDelegate extends SearchDelegate<int> {
  final MessageDetailController messageDetailController;
  int pageIndex = 1;
  final ScrollController _listScrollCtrlSearchMessage = ScrollController();

  CustomMessageSearchDelegate({required this.messageDetailController})
      : super(searchFieldLabel: 'Tìm tin nhắn') {
    messageDetailController.getListUserInGroup();
    _listScrollCtrlSearchMessage.addListener(_scrollListenerAllMessage);
  }

  void _scrollListenerAllMessage() {
    if (messageDetailController.isLoadMoreSearchMessText == false) {
      pageIndex = 1;
      return;
    }
    if (_listScrollCtrlSearchMessage.offset >=
            _listScrollCtrlSearchMessage.position.maxScrollExtent &&
        !_listScrollCtrlSearchMessage.position.outOfRange) {
      pageIndex++;
      messageDetailController.loadMoreSearchMessageText(
          limit: 20, pageIndex: pageIndex, query: query);
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
          messageDetailController.listMessSearch.value =
              messageDetailController.listMessFirstLoad;
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, -2);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _listReturn(messageDetailController.listUserInGroup);
  }

  Widget _listReturn(List<DataUsers> users) {
    return Obx(() {
      if (messageDetailController.listMessSearch.isEmpty)
        return const NoDataWidget(
          noiDung: 'Không có dữ liệu',
        );
      return ScrollConfiguration(
        behavior: DisableGlowListViewWidget(),
        child: ListView.builder(
            controller: _listScrollCtrlSearchMessage,
            itemCount: messageDetailController.listMessSearch.length,
            itemBuilder: (context, index) {
              final ChatMessage message =
                  messageDetailController.listMessSearch[index];
              final DataUsers? user = users.firstWhereOrNull(
                  (element) => element.userID == message.userIDGui);
              final String time = DateFormat('kk:mm dd/MM/yyyy')
                  .format(message.createDate ?? DateTime.now());
              return GestureDetector(
                onTap: () => close(context, message.messageID),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 40,
                        width: 40,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: ImageNetworkWidget(
                            packageName: LocalVariable.namePackage,
                            urlImageNetwork:
                                baseUrlUpload + (user?.avatar ?? ''),
                            imageAssetDefault: ImageVariable.person,
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                user?.fullName ?? '',
                                style: const TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16),
                              ),
                              richTextSearch(message.message!, query,
                                  styleRichText: const TextStyle(
                                      color: Colors.black, fontSize: 16)),
                              Text(
                                time,
                                style: const TextStyle(
                                    color: Colors.grey, fontSize: 12),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty)
      messageDetailController.getMessFirstLoad();
    else
      messageDetailController.searchMessageText(
          limit: 20, pageIndex: 0, query: query);
    return _listReturn(messageDetailController.listUserInGroup);
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme.copyWith(
        primaryColor: BaseColor.accentsColor,
        primaryIconTheme: theme.primaryIconTheme,
        primaryColorBrightness: theme.primaryColorBrightness,
        inputDecorationTheme: InputDecorationTheme(
            hintStyle: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(color: Colors.white)));
  }
}
