import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OptionMessage extends StatelessWidget {
  final IconData? iconData;
  final Color? colorIcon;
  final double? sizeIcon;
  final String? label;
  const OptionMessage(
      {this.iconData, this.colorIcon, this.sizeIcon, this.label});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconData ?? Icons.ac_unit,
          color: colorIcon,
          size: sizeIcon ?? Get.height * 0.05,
        ),
        Text(
          label!,
          style: const TextStyle(color: Colors.black, fontSize: 12),
        )
      ],
    );
  }
}
