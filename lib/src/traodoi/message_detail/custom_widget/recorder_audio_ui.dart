import 'dart:async';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:flutter_sound_lite/public/flutter_sound_player.dart';
import 'package:flutter_sound_lite/public/flutter_sound_recorder.dart';
import 'package:flutter_sound_lite/public/tau.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

const int tSampleRate = 44000;
const int bitRate = 8000;
typedef _Fn = void Function();

class RecorderAudioUI extends StatefulWidget {
  final ValueChanged<String?> pathAudio;
  const RecorderAudioUI({required this.pathAudio});

  @override
  _RecorderAudioUIState createState() => _RecorderAudioUIState();
}

class _RecorderAudioUIState extends State<RecorderAudioUI> {
  FlutterSoundPlayer? _mPlayer = FlutterSoundPlayer();
  FlutterSoundRecorder? _mRecorder = FlutterSoundRecorder();

  bool _mPlayerIsInit = false;
  bool _mRecorderIsInit = false;
  bool _mPlayBackReady = false;
  String? _mPath;
  final Codec _codec = Codec.aacADTS;

  StreamSubscription? _mRecordingDataSubscription;
  StreamSubscription? _timeRecorderSubscription;
  StreamSubscription? _timePlaySubscription;

  final StreamController<String> _timeRecorderCtrl = StreamController<String>();
  final StreamController<String> _timePlayCtrl = StreamController<String>();

  String _timeAudio = '00:00';

  Future<void> _openRecorder() async {
    final status = await Permission.microphone.request().isGranted;
    if (!status) {
      throw RecordingPermissionException('Microphone permission not granted');
    }
    await _mRecorder!.openAudioSession(
        focus: AudioFocus.requestFocusAndStopOthers,
        category: SessionCategory.playAndRecord,
        mode: SessionMode.modeDefault,
        device: AudioDevice.speaker).then((value) async{
       _mPlayer!.openAudioSession(
          focus: AudioFocus.requestFocusAndStopOthers,
          category: SessionCategory.playAndRecord,
          mode: SessionMode.modeDefault,
          device: AudioDevice.speaker).then((value){
        _mPlayerIsInit = true;
      });

      setState(() {
        _mRecorderIsInit = true;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _openRecorder();
  }

  @override
  void dispose() {
    stopPlayer();
    _mPlayer!.closeAudioSession();
    _mPlayer = null;

    stopRecorder();
    _mRecorder!.closeAudioSession();
    _mRecorder = null;

    _timePlayCtrl.close();
    _timeRecorderCtrl.close();

    _timePlaySubscription?.cancel();
    _timeRecorderSubscription?.cancel();
    _mRecordingDataSubscription?.cancel();

    super.dispose();
  }

  Future<void> record() async {
    assert(_mRecorderIsInit && _mPlayer!.isStopped);
    _mPlayBackReady = false;
    final tempDir = await getTemporaryDirectory();
    final String _path =
        '${tempDir.path}/chat_record_${DateFormat('yyyy_MM_dd_HH_mm_ss').format(DateTime.now())}.m4a';

    await _mRecorder!.startRecorder(
      toFile: _path,
      codec: _codec,
      bitRate: bitRate,
      numChannels: 1,
      sampleRate: bitRate,
    );

    _timePlaySubscription = _mRecorder!.onProgress!.listen((e) {
      final DateTime date = DateTime.fromMillisecondsSinceEpoch(
          e.duration.inMilliseconds,
          isUtc: true);
      final String txt = DateFormat('mm:ss').format(date);

      if (!_timeRecorderCtrl.isClosed) _timeRecorderCtrl.sink.add(txt);
      _timeAudio = txt;
    });

    _mPath = _path;

    setState(() {});
  }

  Future<void> stopRecorder() async {
    await _mRecorder!.stopRecorder();
    if (_mRecordingDataSubscription != null) {
      await _mRecordingDataSubscription!.cancel();
      _mRecordingDataSubscription = null;
    }
    if (!_timeRecorderCtrl.isClosed) _timeRecorderCtrl.sink.add('00:00');
    _mPlayBackReady = true;
  }

  _Fn? getRecorderFn() {
    if (!_mRecorderIsInit || !_mPlayer!.isStopped) {
      return null;
    }
    return _mRecorder!.isStopped
        ? record
        : () {
            stopRecorder().then((value) => setState(() {}));
          };
  }

  Future<void> play() async {
    assert(_mPlayerIsInit &&
        _mPlayBackReady &&
        _mRecorder!.isStopped &&
        _mPlayer!.isStopped);
    await _mPlayer!.startPlayer(
        fromURI: _mPath,
        sampleRate: bitRate,
        codec: _codec,
        numChannels: 1,
        whenFinished: () {
          setState(() {});
        });

    _timeRecorderSubscription = _mPlayer!.onProgress!.listen((e) {
      final DateTime date = DateTime.fromMillisecondsSinceEpoch(
          e.duration.inMilliseconds,
          isUtc: true);
      final String txt = DateFormat('mm:ss').format(date);

      _timePlayCtrl.sink.add(txt);
    });

    setState(() {});
  }

  Future<void> stopPlayer() async {
    await _mPlayer!.stopPlayer();
  }

  _Fn? getPlaybackFn() {
    if (!_mPlayerIsInit || !_mPlayBackReady || !_mRecorder!.isStopped) {
      return null;
    }
    return _mPlayer!.isStopped
        ? play
        : () {
            stopPlayer().then((value) => setState(() {}));
          };
  }

  Future<void> deleteAudio() async {
    await stopPlayer();
    _mPlayBackReady = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: Get.width,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          StreamBuilder(
            initialData: '00:00',
            stream: _timeRecorderCtrl.stream,
            builder: (context, AsyncSnapshot<String> snapshot) {
              return Text(
                snapshot.data ?? '00:00',
                style: const TextStyle(fontSize: 24),
              );
            },
          ),
          AvatarGlow(
            animate: _mRecorder!.isRecording,
            glowColor: Theme.of(context).primaryColor,
            endRadius: 75.0,
            duration: const Duration(milliseconds: 2000),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            child: GestureDetector(
              onTap: getRecorderFn(),
              child: Material(
                elevation: 8.0,
                shape: const CircleBorder(),
                child: CircleAvatar(
                  backgroundColor: Colors.blue,
                  child: Icon(
                    _mRecorder!.isRecording
                        ? Icons.stop
                        : Icons.keyboard_voice_outlined,
                    color: Colors.white,
                    size: 36,
                  ),
                  radius: 36.0,
                ),
              ),
            ),
          ),
          if (_mPlayBackReady)
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 0, 8, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () async => deleteAudio(),
                    child: const Material(
                      elevation: 8.0,
                      shape: CircleBorder(),
                      child: CircleAvatar(
                        backgroundColor: Colors.red,
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                          size: 22,
                        ),
                        radius: 22.0,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: getPlaybackFn(),
                            child: Icon(
                              _mPlayer!.isPlaying
                                  ? Icons.stop_circle_outlined
                                  : Icons.play_circle_fill,
                              size: 46,
                              color: Colors.black45,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(_timeAudio, style: const TextStyle(fontSize: 20))
                          // StreamBuilder(
                          //   initialData: '00:00',
                          //   stream: _timePlayCtrl.stream,
                          //   builder:
                          //       (context, AsyncSnapshot<String> snapshot) {
                          //     return Text('${'00:00'}/$_timeAudio');
                          //   },
                          // )
                        ]),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    onTap: () async {
                      widget.pathAudio(_mPath);
                    },
                    child: const Material(
                      // Replace this child with your own
                      elevation: 8.0,
                      shape: CircleBorder(),
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        child: Icon(
                          Icons.send,
                          color: Colors.white,
                          size: 22,
                        ),
                        radius: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
