import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/smart_text.dart';
import 'package:path/path.dart' as path;

import '../../../../chat_package.dart';

class ReplyChatItem extends StatelessWidget {
  final ChatMessage chatMessage;
  final Function(int messID, int groupID) onTap;
  final List<DataUsers> users;
  const ReplyChatItem(this.chatMessage, this.users, {required this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(chatMessage.replyModel!.messID!, chatMessage.groupID!),
      child: IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _showRepTin(),
            SmartText(
              myDataUser: DataUsers(fullName: fullNameChat),
              listUser: users,
              text: chatMessage.message ?? '',
              style: TextStyle(
                  color: (chatMessage.userIDGui == userIDChat)
                      ? Colors.black
                      : Colors.white,
                  fontSize: 16),
            )
          ],
        ),
      ),
    );
  }

  Row _showRepTin() {
    final Text _name = Text(chatMessage.replyModel!.nguoiGui!,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: (chatMessage.userIDGui == userIDChat)
                ? Colors.black
                : Colors.white));
    switch (MessageType.values[chatMessage.replyModel!.messType!]) {
      case MessageType.Text:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  Text(
                    chatMessage.replyModel!.oldMess ?? '',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
          ],
        );
      case MessageType.Image:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            ExtendedImage.network(
              '$baseUrlApiChat/${chatMessage.replyModel!.link}',
              width: 40,
              height: 40,
              cache: true,
              enableMemoryCache: true,
              fit: BoxFit.cover,
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  const Text('[Hình Ảnh]'),
                ],
              ),
            ),
          ],
        );
      case MessageType.Video:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                ExtendedImage.network(
                  baseUrlApiChat + chatMessage.replyModel!.link!,
                  width: 40,
                  height: 40,
                  cache: true,
                  enableMemoryCache: true,
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  ImageVariable.playVideo,
                  width: Get.width * 0.05,
                  package: 'chat_package',
                )
              ],
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  const Text('[Video]'),
                ],
              ),
            ),
          ],
        );
      case MessageType.File:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(width: 5),
            Image.asset(
              ImageVariable.fileExtensionIcon(
                  path.extension(chatMessage.replyModel!.oldMess!)),
              width: 35,
              fit: BoxFit.cover,
              package: 'chat_package',
            ),
            const SizedBox(width: 5),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  Text(
                    chatMessage.replyModel!.oldMess!,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
          ],
        );
      case MessageType.Link:
      case MessageType.Youtube:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(width: 5),
            ExtendedImage.network(
              chatMessage.replyModel!.link!,
              width: 35,
              cache: true,
              enableMemoryCache: true,
              fit: BoxFit.cover,
            ),
            const SizedBox(width: 5),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  Text(
                    chatMessage.replyModel!.oldMess!,
                    style: const TextStyle(
                        color: Colors.blue,
                        decoration: TextDecoration.underline),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
          ],
        );
      case MessageType.Emoticon:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(width: 5),
            ExtendedImage.network(
              chatMessage.replyModel!.link!,
              width: 35,
              cache: true,
              enableMemoryCache: true,
              fit: BoxFit.cover,
            ),
            const SizedBox(width: 5),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  const Text('[Sticker]'),
                ],
              ),
            ),
          ],
        );
      case MessageType.Audio:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              width: 5,
              height: 50,
              child: DecoratedBox(
                decoration: BoxDecoration(color: BaseColor.accentsColor),
              ),
            ),
            const SizedBox(width: 5),
            Image.asset(
              ImageVariable.audio,
              width: 35,
              fit: BoxFit.cover,
              package: 'chat_package',
            ),
            const SizedBox(width: 5),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _name,
                  const Text('[Audio]',
                      maxLines: 2, overflow: TextOverflow.ellipsis),
                ],
              ),
            ),
          ],
        );
      default:
        return Row();
    }
  }
}
