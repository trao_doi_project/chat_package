import 'dart:convert';

import 'package:chat_package/chat_package.dart';
import 'package:chat_package/customlayout/circle_avatar_widget.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

import 'custom_widget_chat.dart';

class ChiTietByID extends StatelessWidget {
  final ChatMessage chatMessage;
  final List<DataUsers> dataUsers;
  final bool isRight;
  List<TinhTrangModel>? _daNhan;
  List<TinhTrangModel>? _daXem;
  final RxInt _countDaNhan = 0.obs;
  final RxInt _countDaXem = 0.obs;

  ChiTietByID(this.chatMessage, this.dataUsers, this.isRight);

  @override
  Widget build(BuildContext context) {
    if (isRight) getList();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chi tiết tin nhắn'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  FrameMessage(
                    chatMessage: chatMessage,
                    firstMessage: false,
                    isLeftMessage: !isRight,
                    lastMessage: true,
                    listUser: dataUsers,
                    timViTriFunc: (messID, groupID) {},
                  )
                ],
              ),
              if (isRight)
                Obx(() {
                  if (_countDaNhan > 0)
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Đã nhận($_countDaNhan)',
                            style: const TextStyle(
                                color: Colors.deepPurple,
                                fontWeight: FontWeight.bold)),
                        GridView.count(
                          primary: false,
                          padding: const EdgeInsets.all(10),
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 0,
                          crossAxisCount: 3,
                          childAspectRatio: 1.5,
                          children: _daNhan!.map((e) {
                            final DataUsers? _user = dataUsers
                                .firstWhereOrNull((i) => i.userID == e.userID);
                            if (_user != null)
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CircleAvatarWidget(
                                    '$baseUrlUpload${_user.avatar}',
                                  ),
                                  const SizedBox(width: 10),
                                  Text(
                                    _user.fullName!,
                                    style: const TextStyle(
                                        color: Colors.blue,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    e.createDate!
                                        .toLocal()
                                        .format('HH:mm dd/MM/yyyy'),
                                    style: const TextStyle(
                                        color: Colors.red, fontSize: 9),
                                  )
                                ],
                              );
                            else
                              return const Text(
                                'Người nhận đã rời nhóm',
                                textAlign: TextAlign.center,
                              );
                          }).toList(),
                        )
                      ],
                    );
                  return const SizedBox.shrink();
                }),
              if (isRight)
                Obx(() {
                  if (_countDaXem > 0)
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Đã xem($_countDaXem)',
                            style: const TextStyle(
                                color: Colors.deepPurple,
                                fontWeight: FontWeight.bold)),
                        GridView.count(
                          primary: false,
                          padding: const EdgeInsets.all(10),
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 0,
                          crossAxisCount: 3,
                          childAspectRatio: 1,
                          children: _daXem!.map((e) {
                            final DataUsers? _user = dataUsers
                                .firstWhereOrNull((i) => i.userID == e.userID);
                            if (_user != null)
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CircleAvatarWidget(
                                    '$baseUrlUpload${_user.avatar}',
                                  ),
                                  const SizedBox(width: 10),
                                  Text(
                                    _user.fullName!,
                                    style: const TextStyle(
                                        color: Colors.blue,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    e.createDate!
                                        .toLocal()
                                        .format('HH:mm dd/MM/yyyy'),
                                    style: const TextStyle(
                                        color: Colors.red, fontSize: 9),
                                  )
                                ],
                              );
                            return const Text(
                              'Người xem đã rời nhóm',
                              textAlign: TextAlign.center,
                            );
                          }).toList(),
                        )
                      ],
                    );
                  return const SizedBox.shrink();
                })
            ],
          ),
        ),
      ),
    );
  }

  Future getList() async {
    final String _item =
        await SyncChat.instance.getTinhTrangByMessID(chatMessage.messageID);
    if (_item != null && _item.isNotEmpty) {
      final List _data = jsonDecode(_item);
      final List<TinhTrangModel> _lstTinhTrang =
          _data.map((e) => TinhTrangModel.fromMap(e)).toList();
      if (_lstTinhTrang != null) {
        _daNhan =
            _lstTinhTrang.where((element) => element.tinhTrangID == 2).toList();
        _daXem =
            _lstTinhTrang.where((element) => element.tinhTrangID == 3).toList();
        _countDaNhan.value = _daNhan?.length ?? 0;
        _countDaXem.value = _daXem?.length ?? 0;
        _countDaNhan.refresh();
        _countDaXem.refresh();
      }
    }
  }
}
