import 'package:flutter/material.dart';

class HorizontalLineText extends StatelessWidget {
  const HorizontalLineText(this.label,
      {this.height = 10, this.color = Colors.grey});

  final Text label;
  final double height;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 15.0),
          child: SizedBox(
              child: Divider(
            color: color,
            height: height,
          )),
        ),
      ),
      label,
      Expanded(
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 15.0),
          child: SizedBox(
              child: Divider(
            color: color,
            height: height,
          )),
        ),
      ),
    ]);
  }
}
