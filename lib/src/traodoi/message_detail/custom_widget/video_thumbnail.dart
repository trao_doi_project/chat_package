import 'dart:io';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart' as cache;
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/video_play.dart';

import '../../../../chat_package.dart';

class VideoThumbnail extends StatelessWidget {
  final ChatMessage data;
  const VideoThumbnail(this.data);
  @override
  Widget build(BuildContext context) {
    if (data.media!.length > 1)
      return Directionality(
        textDirection: TextDirection.rtl,
        child: GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemCount: data.media?.length ?? 0,
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, mainAxisSpacing: 5, crossAxisSpacing: 5),
            itemBuilder: (context, index) {
              if (data.messageID > 0)
                return GestureDetector(
                  onTap: () async {
                    final String _tempUrl =
                        '$baseUrlApiChat/${data.media?[index].fileUrl}';
                    final cache.FileInfo? _tempFile = await CacheManagerCustom
                        .instance
                        .getFileInCache(_tempUrl);
                    if (_tempFile != null)
                      // await ApiRequest.instance.openFile(_tempFile.file.path);
                      Get.to(ChewieDemo(
                        _tempFile.file.path,
                        true,
                        isDownLoad: true,
                        file: data.media?[index],
                      ));
                    else
                      // await launch(_tempUrl);
                      Get.to(ChewieDemo(
                        _tempUrl,
                        false,
                        isDownLoad: true,
                        file: data.media?[index],
                      ));
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      ExtendedImage.network(
                        (data.media?.isNotEmpty ?? false)
                            ? baseUrlApiChat +
                                '/' +
                                (data.media?[index].thumbnail ?? '')
                            : '',
                        shape: BoxShape.rectangle,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        enableMemoryCache: true,
                        loadStateChanged: (ExtendedImageState state) {
                          switch (state.extendedImageLoadState) {
                            case LoadState.completed:
                              return ExtendedRawImage(
                                image: state.extendedImageInfo?.image,
                                fit: BoxFit.cover,
                                width: Get.width * 0.4,
                                height: Get.width * 0.4,
                              );
                            case LoadState.failed:
                              return GestureDetector(
                                child: Image.asset(
                                  ImageVariable.novideo,
                                  fit: BoxFit.cover,
                                  width: Get.width * 0.4,
                                  height: Get.width * 0.4,
                                  package: 'chat_package',
                                ),
                                onTap: () {
                                  state.reLoadImage();
                                },
                              );
                            default:
                              return Image.asset(
                                ImageVariable.novideo,
                                fit: BoxFit.cover,
                                width: Get.width * 0.4,
                                height: Get.width * 0.4,
                                package: 'chat_package',
                              );
                          }
                        },
                      ),
                      Image.asset(
                        ImageVariable.playVideo,
                        width: 30,
                        package: 'chat_package',
                      ),
                    ],
                  ),
                );
              else
                return GestureDetector(
                  onTap: () async {
                    // await ApiRequest.instance.openFile(data.media?.first?.fileUrl);
                    Get.to(ChewieDemo(
                      data.media![index].fileUrl!,
                      true,
                    ));
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      ExtendedImage.file(
                        File(data.media![index].thumbnail!),
                        shape: BoxShape.rectangle,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        border: Border.all(width: 1.0),
                        enableMemoryCache: true,
                        loadStateChanged: (ExtendedImageState state) {
                          switch (state.extendedImageLoadState) {
                            case LoadState.completed:
                              return ExtendedRawImage(
                                image: state.extendedImageInfo?.image,
                                fit: BoxFit.cover,
                                width: Get.width * 0.4,
                                height: Get.width * 0.4,
                              );
                            case LoadState.failed:
                              return GestureDetector(
                                child: Image.asset(
                                  ImageVariable.novideo,
                                  fit: BoxFit.cover,
                                  width: Get.width * 0.4,
                                  height: Get.width * 0.4,
                                  package: 'chat_package',
                                ),
                                onTap: () {
                                  state.reLoadImage();
                                },
                              );
                            default:
                              return Image.asset(
                                ImageVariable.novideo,
                                fit: BoxFit.cover,
                                width: Get.width * 0.4,
                                height: Get.width * 0.4,
                                package: 'chat_package',
                              );
                          }
                        },
                      ),
                      Image.asset(
                        ImageVariable.playVideo,
                        width: 30,
                        package: 'chat_package',
                      ),
                      const Positioned(
                        left: 5,
                        top: 5,
                        child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                              backgroundColor: Colors.white),
                        ),
                      )
                    ],
                  ),
                );
            }),
      );
    else if (data.messageID > 0)
      return GestureDetector(
        onTap: () async {
          final String _tempUrl =
              '$baseUrlApiChat/${data.media?.first.fileUrl}';
          final cache.FileInfo? _tempFile =
              await CacheManagerCustom.instance.getFileInCache(_tempUrl);
          if (_tempFile != null)
            // await ApiRequest.instance.openFile(_tempFile.file.path);
            Get.to(ChewieDemo(
              _tempFile.file.path,
              true,
              isDownLoad: true,
              file: data.media?.first,
            ));
          else
            // await launch(_tempUrl);
            Get.to(ChewieDemo(
              _tempUrl,
              false,
              isDownLoad: true,
              file: data.media?.first,
            ));
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            ExtendedImage.network(
              (data.media?.isNotEmpty ?? false)
                  ? baseUrlApiChat + '/' + (data.media?.first.thumbnail ?? '')
                  : '',
              shape: BoxShape.rectangle,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              enableMemoryCache: true,
              loadStateChanged: (ExtendedImageState state) {
                switch (state.extendedImageLoadState) {
                  case LoadState.completed:
                    return ExtendedRawImage(
                      image: state.extendedImageInfo?.image,
                      fit: BoxFit.cover,
                      width: Get.width * 0.4,
                      height: Get.width * 0.4,
                    );
                  case LoadState.failed:
                    return GestureDetector(
                      child: Image.asset(
                        ImageVariable.novideo,
                        fit: BoxFit.cover,
                        width: Get.width * 0.4,
                        height: Get.width * 0.4,
                        package: 'chat_package',
                      ),
                      onTap: () {
                        state.reLoadImage();
                      },
                    );
                  default:
                    return Image.asset(
                      ImageVariable.novideo,
                      fit: BoxFit.cover,
                      width: Get.width * 0.4,
                      height: Get.width * 0.4,
                      package: 'chat_package',
                    );
                }
              },
            ),
            Image.asset(
              ImageVariable.playVideo,
              width: 50,
              package: 'chat_package',
            ),
          ],
        ),
      );
    else
      return GestureDetector(
        onTap: () async {
          // await ApiRequest.instance.openFile(data.media?.first?.fileUrl);
          Get.to(ChewieDemo(
            data.media!.first.fileUrl!,
            true,
          ));
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            ExtendedImage.file(
              File(data.media!.first.thumbnail!),
              shape: BoxShape.rectangle,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              border: Border.all(width: 1.0),
              enableMemoryCache: true,
              loadStateChanged: (ExtendedImageState state) {
                switch (state.extendedImageLoadState) {
                  case LoadState.completed:
                    return ExtendedRawImage(
                      image: state.extendedImageInfo?.image,
                      fit: BoxFit.cover,
                      width: Get.width * 0.4,
                      height: Get.width * 0.4,
                    );
                  case LoadState.failed:
                    return GestureDetector(
                      child: Image.asset(
                        ImageVariable.novideo,
                        fit: BoxFit.cover,
                        width: Get.width * 0.4,
                        height: Get.width * 0.4,
                        package: 'chat_package',
                      ),
                      onTap: () {
                        state.reLoadImage();
                      },
                    );
                  default:
                    return Image.asset(
                      ImageVariable.novideo,
                      fit: BoxFit.cover,
                      width: Get.width * 0.4,
                      height: Get.width * 0.4,
                      package: 'chat_package',
                    );
                }
              },
            ),
            Image.asset(
              ImageVariable.playVideo,
              width: 50,
              package: 'chat_package',
            ),
            const Positioned(
              left: 5,
              top: 5,
              child: SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(backgroundColor: Colors.white),
              ),
            )
          ],
        ),
      );
  }
}
