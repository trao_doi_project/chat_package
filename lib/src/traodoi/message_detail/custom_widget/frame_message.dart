import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/smart_text.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../chat_package.dart';
import 'custom_widget_chat.dart';
import 'mess_audio_widget_v2.dart';

class FrameMessage extends StatelessWidget {
  final Color? frameColor;
  final DataUsers? users;
  final List<DataUsers> listUser;
  final ChatMessage chatMessage;
  final bool lastMessage;
  final bool? firstMessage;
  final isLeftMessage;
  final Function(int messID, int groupID) timViTriFunc;
  const FrameMessage({
    this.frameColor = Colors.grey,
    this.users,
    required this.chatMessage,
    required this.lastMessage,
    required this.firstMessage,
    required this.isLeftMessage,
    required this.listUser,
    required this.timViTriFunc,
  });

  List<int>? getAvatarUserSeen(ChatMessage message, List<DataUsers> users) {
    if (message.userIDDaXem == null || message.userIDDaXem!.isEmpty)
      return null;
    else {
      final List<String>? stringUserIDs = message.userIDDaXem?.split(',');
      if (stringUserIDs != null) {
        final List<int> userIDs = stringUserIDs
            .map((data) => int.parse((data == 'null') ? '0' : data))
            .toList();
        if (userIDs.length > 5) return userIDs.getRange(0, 5).toList();
        return userIDs;
      }
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLeftMessage)
      return Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: ConstrainedBox(
            constraints:
                BoxConstraints(maxWidth: Get.width * 0.6, minHeight: 30),
            child: chatMessage.isThuHoi ?? false
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (firstMessage ?? false)
                        Text(
                          users?.fullName ?? 'Người gửi đã rời nhóm',
                          style: const TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.black12,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: const Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Text(
                            'Tin nhắn đã được thu hồi',
                            style:
                                TextStyle(color: Colors.white70, fontSize: 16),
                          ),
                        ),
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (firstMessage ?? false)
                        Text(
                          users?.fullName ?? 'Người gửi đã rời nhóm',
                          style: const TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      _messageLeft(),
                    ],
                  ),
          ),
        ),
      );
    else {
      return Align(
        alignment: Alignment.centerRight,
        child: Container(
          constraints: BoxConstraints(maxWidth: Get.width * 0.6, minHeight: 30),
          child: chatMessage.isThuHoi ?? false
              ? DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text(
                      'Tin nhắn đã được thu hồi',
                      style: TextStyle(color: Colors.white70, fontSize: 16),
                    ),
                  ),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Stack(
                      children: [
                        _messageRight(),
                        if (chatMessage.reaction != null)
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Image.asset(
                              chatMessage.reaction!,
                              height: 17,
                              package: 'chat_package',
                            ),
                          ),
                      ],
                    ),
                    //show tình trạng và thời gian xem tin
                    if (lastMessage)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text.rich(
                            TextSpan(children: [
                              TextSpan(
                                text: DateFormat('HH:mm').format(
                                    chatMessage.createDate ?? DateTime.now()),
                                style: const TextStyle(
                                    color: Colors.blue,
                                    fontSize: 12.0,
                                    fontStyle: FontStyle.italic),
                              ),
                              const TextSpan(
                                text: ' - ',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 12.0,
                                    fontStyle: FontStyle.italic),
                              ),
                              TextSpan(
                                text: chatMessage.tenTinhTrang,
                                style: TextStyle(
                                    color: (chatMessage.tinhTrangID != 4)
                                        ? Colors.blue
                                        : Colors.red,
                                    fontSize: 12.0,
                                    fontStyle: FontStyle.italic),
                              ),
                            ]),
                          ),
                          if (listUser.length > 1)
                            Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: SizedBox(
                                height: 15,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  reverse: true,
                                  itemCount:
                                      getAvatarUserSeen(chatMessage, listUser)
                                              ?.length ??
                                          0,
                                  itemBuilder: (context, index) {
                                    final List<int> _items = getAvatarUserSeen(
                                        chatMessage, listUser)!;
                                    if (_items.length == 5) {
                                      if (index == 4)
                                        return Container(
                                          child: const Center(
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.white,
                                              size: 13,
                                            ),
                                          ),
                                          height: 15,
                                          width: 15,
                                          decoration: const BoxDecoration(
                                              color: Colors.black38,
                                              shape: BoxShape.circle),
                                        );
                                      return ImageNetworkWidget(
                                        packageName: LocalVariable.namePackage,
                                        fit: BoxFit.cover,
                                        shape: BoxShape.circle,
                                        imageWidth: 15,
                                        imageAssetDefault: ImageVariable.person,
                                        urlImageNetwork: '$baseUrlUpload'
                                            '${listUser.firstWhereOrNull((element) => element.userID == _items[index])?.avatar}',
                                      );
                                    }
                                    return ImageNetworkWidget(
                                      packageName: LocalVariable.namePackage,
                                      fit: BoxFit.cover,
                                      shape: BoxShape.circle,
                                      imageWidth: 15,
                                      imageAssetDefault: ImageVariable.person,
                                      urlImageNetwork: '$baseUrlUpload'
                                          '${listUser.firstWhereOrNull((element) => element.userID == _items[index])?.avatar}',
                                    );
                                  },
                                ),
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
        ),
      );
    }
  }

  Widget _messageLeft() {
    switch (chatMessage.messageType) {
      case MessageType.Text:
        return ConstrainedBox(
          constraints: BoxConstraints(minWidth: Get.width * 0.2),
          child: DecoratedBox(
            decoration: const BoxDecoration(
              color: Color(0xff56c596),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: _getStack(Padding(
              padding: const EdgeInsets.all(15),
              child: SmartText(
                text: chatMessage.message ?? '',
                style: const TextStyle(color: Colors.white, fontSize: 17),
                onUserTagClick: (url) =>
                    Get.dialog(UserInfoDialog(url, listUser)),
                listUser: listUser,
                myDataUser: DataUsers(fullName: fullNameChat),
              ),
            )),
          ),
        );
      case MessageType.Image:
        return _getStack(ImageView(chatMessage, listUser));
      case MessageType.File:
        return _getStack(DecoratedBox(
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: (chatMessage.media != null && chatMessage.media!.isNotEmpty)
                ? FileWidgetChat(chatMessage)
                : const Text('Không tìm thấy file'),
          ),
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              )),
        ));
      case MessageType.Video:
        return _getStack(VideoThumbnail(chatMessage));
      case MessageType.Emoticon:
        return _getStack(GiphyView(chatMessage.message!));
      case MessageType.Link:
      case MessageType.Youtube:
        return _getStack(DecoratedBox(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: URLMetadata(chatMessage),
            )));
      case MessageType.Reply:
        return _getStack(DecoratedBox(
          decoration: const BoxDecoration(
              color: Color(0xfff7a3a3),
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              )),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: ReplyChatItem(chatMessage, listUser,
                onTap: (messID, groupID) async =>
                    await timViTriFunc(messID, groupID)),
          ),
        ));
      case MessageType.Audio:
        return DecoratedBox(
            decoration: BoxDecoration(
                color: Colors.black12, borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: MessAudioWidgetV2(
                message: chatMessage,
              ),
            ));
      default:
        return const SizedBox.shrink();
    }
  }

  Widget _messageRight() {
    switch (chatMessage.messageType) {
      case MessageType.Text:
        return DecoratedBox(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: SmartText(
              text: chatMessage.message ?? '',
              style: const TextStyle(color: Colors.black, fontSize: 17),
              onUserTagClick: (url) =>
                  Get.dialog(UserInfoDialog(url, listUser)),
              listUser: listUser,
              myDataUser: DataUsers(fullName: fullNameChat),
            ),
          ),
        );
      case MessageType.Image:
        return ImageView(chatMessage, listUser);
      case MessageType.File:
        return DecoratedBox(
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: FileWidgetChat(chatMessage),
          ),
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              )),
        );
      case MessageType.Video:
        return VideoThumbnail(chatMessage);
      case MessageType.Emoticon:
        return GiphyView(chatMessage.message!);
      case MessageType.Link:
      case MessageType.Youtube:
        return DecoratedBox(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: URLMetadata(chatMessage),
            ));
      case MessageType.Reply:
        return DecoratedBox(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              )),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: ReplyChatItem(chatMessage, listUser,
                onTap: (messID, groupID) async =>
                    await timViTriFunc(messID, groupID)),
          ),
        );
      case MessageType.Audio:
        return DecoratedBox(
            decoration: BoxDecoration(
                color: Colors.black12, borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: MessAudioWidgetV2(
                message: chatMessage,
              ),
            ));
      default:
        return const SizedBox.shrink();
    }
  }

  Stack _getStack(Widget layout) {
    return Stack(
      alignment: Alignment.center,
      children: [
        layout,
        Positioned(
          right: 1,
          top: 1,
          child: Container(
            decoration: const BoxDecoration(
              color: Color(0x80aaaaaa),
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Text(
                DateFormat('HH:mm').format(chatMessage.createDate!),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 9.0,
                    fontStyle: FontStyle.italic),
              ),
            ),
          ),
        ),
        if (chatMessage.reaction != null && chatMessage.reaction!.isNotEmpty)
          // lastMessage
          Positioned(
            right: 1,
            bottom: 0,
            child: Image.asset(
              chatMessage.reaction!,
              height: 17,
              package: 'chat_package',
            ),
          )
      ],
    );
  }
}

class UserInfoDialog extends StatelessWidget {
  final String userText;
  final List<DataUsers> lstDataUser;
  const UserInfoDialog(this.userText, this.lstDataUser);
  @override
  Widget build(BuildContext context) {
    final DataUsers? _item = lstDataUser.firstWhereOrNull((element) =>
        element.fullName ==
        userText.replaceAll(LocalVariable.spaceChar, ' ').replaceAll('@', ''));
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ImageNetworkWidget(
                    packageName: LocalVariable.namePackage,
                    urlImageNetwork: baseUrlUpload + (_item?.avatar ?? ''),
                    shape: BoxShape.circle,
                    imageWidth: Get.width * 0.25,
                    imageHeight: Get.width * 0.25,
                    imageAssetDefault: ImageVariable.person),
                const SizedBox(
                  width: 10,
                ),
                Flexible(
                  child: Container(
                    height: Get.height * 0.20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.9),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text.rich(TextSpan(children: [
                            const TextSpan(text: '👨 '),
                            TextSpan(
                                text: _item?.fullName ?? '',
                                style: const TextStyle(
                                    color: BaseColor.accentsColor,
                                    fontWeight: FontWeight.bold)),
                          ])),
                          GestureDetector(
                            onTap: () => launch('tel:${_item?.soDT ?? ''}'),
                            child: Text.rich(TextSpan(children: [
                              const TextSpan(text: '📱 ‍'),
                              TextSpan(
                                text: _item?.soDT ?? '',
                                style: const TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.bold),
                              ),
                            ])),
                          ),
                          GestureDetector(
                            onTap: () => launch('mailto:${_item?.email ?? ''}'),
                            child: Text.rich(TextSpan(children: [
                              const TextSpan(text: '📧 '),
                              TextSpan(
                                  text: _item?.email ?? '',
                                  style: const TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold)),
                            ])),
                          ),
                          Text.rich(TextSpan(children: [
                            const TextSpan(text: '🏷 '),
                            TextSpan(
                                text: _item?.tenChucVu ?? '',
                                style: const TextStyle(
                                    color: BaseColor.accentsColor,
                                    fontWeight: FontWeight.bold)),
                          ])),
                          Text.rich(
                            TextSpan(children: [
                              const TextSpan(text: '🗃 '),
                              TextSpan(
                                  text: _item?.tenPhongBan ?? '',
                                  style: const TextStyle(
                                      color: BaseColor.accentsColor,
                                      fontWeight: FontWeight.bold)),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
