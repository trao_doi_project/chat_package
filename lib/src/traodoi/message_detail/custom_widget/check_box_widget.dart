import 'package:flutter/material.dart';

class CheckBoxWidget extends StatefulWidget {
  final double size;
  final Color color;
  final BoxShape shape;
  final ValueChanged<bool> change;

  const CheckBoxWidget(
      {this.size = 24,
      this.color = Colors.lightBlue,
      this.shape = BoxShape.circle,
      required this.change});

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool _check = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _check = !_check;
        });
        widget.change(_check);
      },
      child: Container(
          decoration:
              const BoxDecoration(shape: BoxShape.circle),
          child: _check
              ? Icon(
                  Icons.check_circle,
                  color: widget.color,
                  size: widget.size,
                )
              : Icon(
                  Icons.radio_button_off,
                  color: widget.color,
                  size: widget.size,
                )),
    );
  }
}
