import 'dart:async';

import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/widget/no_data_widget.dart';
import 'package:get/get.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';
import 'package:path/path.dart' as path;

import '../../../../chat_package.dart';
import '../message_detail_controller.dart';

class SearchMessInGroupUI extends StatefulWidget {
  static const ROUTER_NAME = '/SearchMessInGroupUI';

  final MessageDetailController messageDetailController;

  const SearchMessInGroupUI({required this.messageDetailController});

  @override
  _SearchMessInGroupUIState createState() => _SearchMessInGroupUIState();
}

class _SearchMessInGroupUIState extends State<SearchMessInGroupUI>
    with SingleTickerProviderStateMixin {
  Timer? _delay;
  late TabController _tabController;
  late TextEditingController _textEditingSearchCtr;
  int pageIndexMessText = 0;
  int pageIndexMedia = 0;
  final ScrollController _listScrollCtrlSearchMessageText = ScrollController();
  final ScrollController _listScrollCtrlSearchMedia = ScrollController();
  String query = '';

  @override
  void initState() {
    _tabController = TabController(
      vsync: this,
      length: 2,
    );
    _textEditingSearchCtr = TextEditingController();
    widget.messageDetailController.getListUserInGroup();
    widget.messageDetailController.getMessFirstLoad();
    widget.messageDetailController.getMediaFirstLoad();
    _listScrollCtrlSearchMessageText.addListener(_scrollListenerMessageText);
    _listScrollCtrlSearchMedia.addListener(_scrollListenerMedia);
    super.initState();
  }

  @override
  void dispose() {
    _delay?.cancel();
    _tabController.dispose();
    _textEditingSearchCtr.dispose();
    _listScrollCtrlSearchMedia.dispose();
    _listScrollCtrlSearchMessageText.dispose();
    super.dispose();
  }

  void _delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (_delay != null) _delay!.cancel();
    _delay = Timer(duration, callback);
  }

  void _scrollListenerMessageText() {
    if (widget.messageDetailController.isLoadMoreSearchMessText == false) {
      return;
    }
    if (_listScrollCtrlSearchMessageText.offset >=
            _listScrollCtrlSearchMessageText.position.maxScrollExtent &&
        !_listScrollCtrlSearchMessageText.position.outOfRange) {
      pageIndexMessText++;
      widget.messageDetailController.loadMoreSearchMessageText(
          limit: 20, pageIndex: pageIndexMessText, query: query);
    }
  }

  void _scrollListenerMedia() {
    if (widget.messageDetailController.isLoadMoreSearchMedia == false) {
      return;
    }
    if (_listScrollCtrlSearchMedia.offset >=
            _listScrollCtrlSearchMedia.position.maxScrollExtent &&
        !_listScrollCtrlSearchMedia.position.outOfRange) {
      pageIndexMedia++;
      widget.messageDetailController.loadMoreSearchMedia(
          limit: 20, pageIndex: pageIndexMedia, keySearch: query);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: BaseColor.primaryColor,
        actions: [
          const SizedBox(
            width: 50,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _textEditingSearchCtr,
                autofocus: true,
                style: const TextStyle(color: Colors.white),
                onChanged: (value) async => _delaySearch(() {
                  query = value;
                  if (value.isEmpty)
                    widget.messageDetailController.resetDataSearch();
                  else {
                    pageIndexMessText = 0;
                    pageIndexMedia = 0;
                    widget.messageDetailController.searchMessageText(
                        limit: 20, pageIndex: pageIndexMessText, query: value);
                    widget.messageDetailController.searchMedia(
                        limit: 20, pageIndex: pageIndexMedia, keySearch: value);
                  }
                }),
                cursorColor: Colors.white,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Tìm kiếm',
                    hintStyle: TextStyle(color: Colors.white)),
              ),
            ),
          ),
          GestureDetector(
              onTap: () {
                _textEditingSearchCtr.clear();
                query = '';
                widget.messageDetailController.resetDataSearch();
              },
              child: const Icon(Icons.cancel)),
          const SizedBox(
            width: 10,
          )
        ],
        bottom: TabBar(
          indicatorColor: Colors.white,
          controller: _tabController,
          tabs: const [
            Tab(
              text: 'Tin nhắn',
            ),
            Tab(
              text: 'Tệp tin',
            ),
          ],
        ),
      ),
      body: TabBarView(controller: _tabController, children: [
        Obx(() {
          if (widget.messageDetailController.listMessSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return ScrollConfiguration(
            behavior: DisableGlowListViewWidget(),
            child: ListView.builder(
                controller: _listScrollCtrlSearchMessageText,
                itemCount: widget.messageDetailController.listMessSearch.length,
                itemBuilder: (context, index) {
                  final ChatMessage message =
                      widget.messageDetailController.listMessSearch[index];
                  final DataUsers? user = widget
                      .messageDetailController.listUserInGroup
                      .firstWhereOrNull(
                          (element) => element.userID == message.userIDGui);
                  final String time = DateFormat('kk:mm dd/MM/yyyy')
                      .format(message.createDate ?? DateTime.now());
                  return GestureDetector(
                    onTap: () => Get.back(result: message.messageID),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: ImageNetworkWidget(
                                packageName: LocalVariable.namePackage,
                                urlImageNetwork:
                                    baseUrlUpload + (user?.avatar ?? ''),
                                imageAssetDefault: ImageVariable.person,
                                shape: BoxShape.circle,
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    user?.fullName ?? '',
                                    style: const TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                  ),
                                  richTextSearch(message.message ?? '', query,
                                      styleRichText: const TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  Text(
                                    time,
                                    style: const TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          );
        }),
        Obx(() {
          if (widget.messageDetailController.listMediaSearch.isEmpty)
            return const NoDataWidget(
              noiDung: 'Không có dữ liệu',
            );
          return ScrollConfiguration(
            behavior: DisableGlowListViewWidget(),
            child: ListView.builder(
                controller: _listScrollCtrlSearchMedia,
                itemCount:
                    widget.messageDetailController.listMediaSearch.length,
                itemBuilder: (context, index) {
                  final FileUpload media =
                      widget.messageDetailController.listMediaSearch[index];
                  final DataUsers? user = widget
                      .messageDetailController.listUserInGroup
                      .firstWhereOrNull(
                          (element) => element.userID == media.userIDGui);
                  final String time = DateFormat('kk:mm dd/MM/yyyy')
                      .format(media.createDate ?? DateTime.now());
                  return GestureDetector(
                    onTap: () => Get.back(result: media.messageID),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: ImageNetworkWidget(
                                packageName: LocalVariable.namePackage,
                                urlImageNetwork:
                                    baseUrlUpload + (user?.avatar ?? ''),
                                imageAssetDefault: ImageVariable.person,
                                shape: BoxShape.circle,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    user?.fullName ?? '',
                                    style: const TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                  ),
                                  richTextSearch(media.fileName ?? '', query,
                                      styleRichText: const TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  Text(
                                    time,
                                    style: const TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Image.asset(
                            ImageVariable.fileExtensionIcon(
                                path.extension(media.fileName ?? '')),
                            height: 40,
                            width: 40,
                            package: 'chat_package',
                          ),
                          const SizedBox(
                            width: 10,
                          )
                        ],
                      ),
                    ),
                  );
                }),
          );
        }),
      ]),
    );
  }
}
