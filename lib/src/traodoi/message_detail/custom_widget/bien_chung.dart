part of 'reaction_widget.dart';

final List<Reaction> reactionLst = [
  Reaction(
    id: 1,
    previewIcon: Image.asset(
      ImageVariable.like,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.ic_likeA,
      package: 'chat_package',
    ),
  ),
  Reaction(
    id: 2,
    previewIcon: Image.asset(
      ImageVariable.love,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.love2,
      package: 'chat_package',
    ),
  ),
  Reaction(
    id: 3,
    previewIcon: Image.asset(
      ImageVariable.wow,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.wow2,
      package: 'chat_package',
    ),
  ),
  Reaction(
    id: 4,
    previewIcon: Image.asset(
      ImageVariable.haha,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.haha2,
      package: 'chat_package',
    ),
  ),
  Reaction(
    id: 5,
    previewIcon: Image.asset(
      ImageVariable.sad,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.sad2,
      package: 'chat_package',
    ),
  ),
  Reaction(
    id: 6,
    previewIcon: Image.asset(
      ImageVariable.angry,
      height: 40,
      width: 40,
      package: 'chat_package',
    ),
    icon: Image.asset(
      ImageVariable.angry2,
      package: 'chat_package',
    ),
  ),
];
