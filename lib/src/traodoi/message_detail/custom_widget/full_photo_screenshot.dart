import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_controller.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:uuid/uuid.dart';

import '../../../../chat_package.dart';

class FullPhotoScreenshotUI extends StatefulWidget {
  final List<File> images;
  final int currentIndex;
  final String tagController;
  const FullPhotoScreenshotUI(
      {required this.images,
      required this.currentIndex,
      required this.tagController});
  @override
  _FullPhotoScreenshotUIState createState() => _FullPhotoScreenshotUIState();
}

class _FullPhotoScreenshotUIState extends State<FullPhotoScreenshotUI> {
  late int tempCurrentIndex;
  final ScrollController _scrollController = ScrollController();
  late PageController _pageController;

  late MessageDetailController _messageDetailController;

  @override
  void initState() {
    _messageDetailController =
        Get.find<MessageDetailController>(tag: widget.tagController);

    _pageController = PageController(
      initialPage: widget.currentIndex,
    );

    _pageController.addListener(() {
      _scrollController.animateTo(tempCurrentIndex * 40.0,
          curve: Curves.linear, duration: const Duration(milliseconds: 100));
    });
    tempCurrentIndex = widget.currentIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
        actions: [
          if (widget.images.length > 1)
            Row(
              children: [
                Obx(() {
                  if (_messageDetailController.listIndexImagePick.isNotEmpty)
                    return Text(
                      '${_messageDetailController.listIndexImagePick.length}',
                      style: const TextStyle(color: Colors.white, fontSize: 22),
                    );
                  return const SizedBox.shrink();
                }),
                const SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  onTap: () {
                    if (_messageDetailController.listIndexImagePick
                        .contains(tempCurrentIndex))
                      _messageDetailController.listIndexImagePick
                          .remove(tempCurrentIndex);
                    else
                      _messageDetailController.listIndexImagePick
                          .add(tempCurrentIndex);
                  },
                  child: Obx(() {
                    if (_messageDetailController.listIndexImagePick
                        .contains(tempCurrentIndex))
                      return const Icon(
                        Icons.check_circle_outline_outlined,
                        color: Colors.white,
                        size: 28,
                      );
                    return const Icon(
                      Icons.radio_button_unchecked,
                      color: Colors.white,
                      size: 28,
                    );
                  }),
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            )
          else
            const SizedBox.shrink(),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                ScrollConfiguration(
                  behavior: DisableGlowListViewWidget(),
                  child: ExtendedImageGesturePageView.builder(
                    itemCount: widget.images.length,
                    itemBuilder: (BuildContext context, int index) {
                      final Widget image = ExtendedImage.file(
                        widget.images[index],
                        fit: BoxFit.scaleDown,
                        mode: ExtendedImageMode.gesture,
                        initGestureConfigHandler: (state) {
                          return GestureConfig(
                            minScale: 1,
                            animationMinScale: 0.5,
                            maxScale: 2,
                            animationMaxScale: 2.5,
                            speed: 1,
                            inertialSpeed: 100.0,
                            initialScale: 1.0,
                            inPageView: true,
                            initialAlignment: InitialAlignment.center,
                          );
                        },
                      );
                      return Padding(
                        padding: const EdgeInsets.all(2),
                        child: image,
                      );
                    },
                    controller: _pageController,
                    onPageChanged: (value) {
                      setState(() {
                        tempCurrentIndex = value;
                      });
                    },
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                Positioned(
                    right: 10,
                    bottom: 5,
                    child: GestureDetector(
                      onTap: () {
                        if (widget.images.length == 1) {
                          _sendImage([widget.images[tempCurrentIndex]]);
                          Get.back();
                        } else {
                          if (_messageDetailController
                              .listIndexImagePick.isEmpty) {
                            _sendImage([widget.images[tempCurrentIndex]]);
                            LoadingWidget.instance.snackBarThongBao('Thông báo', 'Gửi ảnh thành công');
                          } else {
                            final List<File> images = <File>[];
                            if (widget.images.length ==
                                _messageDetailController
                                    .listIndexImagePick.length) {
                              _sendImage(widget.images);
                              _messageDetailController.listIndexImagePick
                                  .clear();
                              Get.back();
                            } else {
                              for (final index in _messageDetailController
                                  .listIndexImagePick)
                                images.add(widget.images[index]);
                              _sendImage(images);
                              _messageDetailController.listIndexImagePick
                                  .clear();
                              Get.back();
                            }
                          }
                        }
                      },
                      child: const Material(
                        // Replace this child with your own
                        elevation: 8.0,
                        shape: CircleBorder(),
                        child: CircleAvatar(
                          backgroundColor: Colors.blue,
                          child: Icon(
                            Icons.send,
                            color: Colors.white,
                            size: 22,
                          ),
                          radius: 22.0,
                        ),
                      ),
                    ))
              ],
            ),
          ),
          if (widget.images.length > 1)
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10),
              child: SizedBox(
                height: Get.width * 0.2,
                child: ScrollConfiguration(
                  behavior: DisableGlowListViewWidget(),
                  child: ListView.builder(
                      controller: _scrollController,
                      scrollDirection: Axis.horizontal,
                      itemCount: widget.images.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: (tempCurrentIndex == index)
                                        ? Colors.blue
                                        : Colors.transparent,
                                    width: 3),
                                borderRadius: BorderRadius.circular(8)),
                            child: GestureDetector(
                              onTap: () {
                                _pageController.animateToPage(index,
                                    curve: Curves.linear,
                                    duration:
                                        const Duration(milliseconds: 100));
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child: ExtendedImage.file(
                                  widget.images[index],
                                  fit: BoxFit.cover,
                                  width: 40,
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            )
          else
            const SizedBox(
              height: 10,
            )
        ],
      ),
    );
  }

  void _sendImage(List<File> images) {
    _messageDetailController.eventHandler(Events.SendMessage,
        message: ChatMessage(
            messageID: 0,
            messageKey: const Uuid().v4(),
            messageType: MessageType.Image,
            message: null,
            userNameGui: userNameChat,
            userIDGui: userIDChat,
            tinhTrangID: 0,
            tenTinhTrang: 'Đang gửi',
            createDate: DateTime.now(),
            groupID: _messageDetailController.groupMain.value.groupID,
            groupKey: _messageDetailController.groupMain.value.groupKey),
        files: images);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _pageController.dispose();
    super.dispose();
  }
}
