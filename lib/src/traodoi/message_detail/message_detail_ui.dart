import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:ui';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/customlayout/circle_avatar_widget.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/traodoi/group_info/group_info_controller.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/chitiet_byid.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/recorder_audio_ui.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/check_box_widget.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_controller.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart' as cache;
import 'package:flutter_core_getx_dev/app/services/cache_manager_custom.dart';
import 'package:flutter_core_getx_dev/app/services/media_picker.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_mentions/flutter_mentions.dart';
import 'package:flutter_plugin_camera/flutter_plugin_camera.dart';
import 'package:flutter_plugin_gallery/flutter_plugin_gallery.dart';
import 'package:flutter_screenshot_callback/flutter_screenshot_callback.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:giphy_get/giphy_get.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/chuyen_tiep/chuyen_tiep_ui.dart';
import 'package:chat_package/src/traodoi/edit_group/edit_group_ui.dart';
import 'package:path/path.dart' as path;
import 'package:photo_manager/photo_manager.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:share/share.dart';
import 'package:uuid/uuid.dart';
import 'package:collection/collection.dart';

import '../../../chat_package.dart';
import 'custom_widget/custom_widget_chat.dart';
import 'custom_widget/full_photo_screenshot.dart';
import 'custom_widget/search_mess_in_group_ui.dart';

class MessageDetailUI extends StatefulWidget {
  static const ROUTER_NAME = '/MessageDetailUI';
  @override
  _MessageDetailUIState createState() => _MessageDetailUIState();
}

class _MessageDetailUIState extends State<MessageDetailUI>
    with WidgetsBindingObserver {
  final String _tagCtrMessageDetail = const Uuid().v4();

  late MessageDetailController _messageDetailController;
  GlobalKey<FlutterMentionsState> keyMention =
      GlobalKey<FlutterMentionsState>();
  int _count = 0;
  bool isMulti = false;
  int _countSelect = 0;
  bool isReply = false;
  ChatMessage? _itemSelected;
  ReplyModel? _replyModel;
  final RxBool _emojiShowing = false.obs;

  final RxList<ScreenshotCallbackData> _screenshotData =
      <ScreenshotCallbackData>[].obs;

  final FocusNode _textSendFocus = FocusNode();

  AutoScrollController listScrollController = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, Get.context!.mediaQueryPadding.bottom));

  final SlidingUpPanelController _slideController = SlidingUpPanelController();
  final GalleryController _galleryController = Get.find<GalleryController>();

  late StreamSubscription _subscriptionScreenshot;

  Widget? _chiTietWidget;

  @override
  void initState() {
    _messageDetailController =
        Get.put(MessageDetailController(), tag: _tagCtrMessageDetail);

    listScrollController.addListener(_scrollListener);
    _textSendFocus.addListener(() {
      _emojiShowing.value = false;
    });

    // reset current groupId
    currentGroupId.value = -1;

    final Map<String, dynamic> arg = Get.arguments;
    _messageDetailController.listUserInGroup.value = arg['itemUser'];
    if (arg.containsKey('groupInfo')) {
      _messageDetailController.groupMain.value = arg['groupInfo'];
      currentGroupObj.value = arg['groupInfo'];
      // listen current groupId
      currentGroupId.value = _messageDetailController.groupMain.value.groupID;

      // reset count mess not seen in sqlite
      SyncChat.instance
          .resetDaXemGroup(_messageDetailController.groupMain.value.groupID);

      // update count mess new in bottom bar
      countNewMessInChatConfig -= currentGroupObj.value.countNewMessage;
      final Map<String, dynamic> _param = {
        'groupID': _messageDetailController.groupMain.value.groupID,
        'userID': userIDChat,
        'count': countNewMessInChatConfig
      };
      ApiRequest.instance.updateCountNewMess(_param);
      countNewMessInChatConfigCtr.sink.add(countNewMessInChatConfig);

      if (!_messageDetailController.groupMain.value.isGroup) {
        // create group name, group icon for chat 2 user
        _messageDetailController.groupMain.value.groupName =
            _messageDetailController.listUserInGroup.first.fullName;
        _messageDetailController.groupMain.value.groupIcon =
            _messageDetailController.listUserInGroup.first.avatar ?? '';
      }
    } else {
      // click list user online: no group => create group
      _messageDetailController.groupMain.value = GroupInfo()
        ..groupName = _messageDetailController.listUserInGroup.first.fullName
        ..groupIcon =
            _messageDetailController.listUserInGroup.first.avatar ?? '';
    }

    if (arg.containsKey('chiTietWidget')) {
      _chiTietWidget = arg['chiTietWidget'];
    }

    _messageDetailController.eventHandler(
      Events.ReciveMessage,
    );

    _messageDetailController
        .listenSignRSyncChat(_messageDetailController.groupMain.value);

    _screenshotData.value = ScreenshotCallback.instance.getData();
    _subscriptionScreenshot = ScreenshotCallback
        .instance.streamCtrlDataScreenshot.stream
        .listen((event) {
      _screenshotData.value = event;
      _screenshotData.refresh();
    });

    SchedulerBinding.instance!.addPostFrameCallback((_) {
      if (arg.containsKey('messageID'))
        _timViTriMess(
            arg['messageID'], _messageDetailController.groupMain.value.groupID);
    });

    super.initState();
  }

  Future<bool> _onWillPop() async {
    if (isMulti) {
      setState(() {
        isMulti = false;
        for (final ChatMessage _element in _messageDetailController
            .listChatMessage) _element.isSelect = false;
        _countSelect = 0;
      });
    } else if (_emojiShowing.value) {
      // reload list group in screen group to update change
      _emojiShowing.value = false;
    } else
      Get.back(result: true);

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: KeyboardDismisser(
        gestures: const [
          GestureType.onTap,
          GestureType.onPanUpdateDownDirection,
        ],
        child: Gallery(
          panelController: _slideController,
          isSelectMulti: true,
          totalImageSeclect: 20,
          totalVideoSeclect: 6,
          maxSizeFileMB: 150,
          primaryColor: BaseColor.primaryColor,
          imagesChoice: (List<AssetEntity> value) {
            _receiveDataMedia(listAsset: value);
          },
          galleryController: _galleryController,
          child: Portal(
            child: Scaffold(
              appBar: AppBar(
                elevation: 0,
                automaticallyImplyLeading: false,
                backgroundColor: BaseColor.accentsColor,
                flexibleSpace: SafeArea(
                  child: (!isMulti)
                      ? Container(
                          color: Colors.white,
                          padding: const EdgeInsets.only(right: 16),
                          child: Obx(() {
                            return Row(
                              children: <Widget>[
                                IconButton(
                                  onPressed: () {
                                    Get.back(result: true);
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    color: BaseColor.primaryColor,
                                  ),
                                ),
                                const SizedBox(
                                  width: 2,
                                ),
                                CircleAvatarWidget(
                                  (_messageDetailController
                                              .groupMain.value.isGroup
                                          ? baseUrlApiChat
                                          : baseUrlUpload) +
                                      (_messageDetailController
                                              .groupMain.value.groupIcon ??
                                          ''),
                                  isGroup: _messageDetailController
                                      .groupMain.value.isGroup,
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        _messageDetailController
                                                .groupMain.value.groupName ??
                                            '',
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: const TextStyle(
                                            fontSize: 16,
                                            // fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w600,
                                            color: BaseColor.primaryColor),
                                      ),
                                    ],
                                  ),
                                ),
                                if (_messageDetailController
                                        .groupMain.value.groupID >
                                    0)
                                  Row(
                                    children: [
                                      if (_messageDetailController
                                              .groupMain.value.isDonVi &&
                                          _chiTietWidget != null)
                                        IconButton(
                                          onPressed: () async {
                                            Get.bottomSheet(_chiTietWidget!);
                                          },
                                          icon: const Icon(
                                              Icons.article_outlined),
                                        ),
                                      IconButton(
                                        onPressed: () async {
                                          final _result = await Get.toNamed(
                                              EditGroupUI.ROUTER_NAME,
                                              arguments: {
                                                'groupInfo':
                                                    _messageDetailController
                                                        .groupMain.value,
                                                'tagCtrMessageDetail':
                                                    _tagCtrMessageDetail,
                                              });
                                          if (_result != null) {
                                            if (_result['pageRouter'] ==
                                                    EditGroupUI.ROUTER_NAME &&
                                                _result['hinhNenNew'] != null)
                                              setState(() {
                                                if (_result['hinhNenNew'] ==
                                                    'default')
                                                  _messageDetailController
                                                      .groupMain
                                                      .value
                                                      .hinhNen = null;
                                                else
                                                  _messageDetailController
                                                          .groupMain
                                                          .value
                                                          .hinhNen =
                                                      _result['hinhNenNew'];
                                              });
                                            else if (_result['pageRouter'] ==
                                                TinNhanDaGhimUI.ROUTER_NAME)
                                              _timViTriMess(
                                                  _result['data'],
                                                  _messageDetailController
                                                      .groupMain.value.groupID);
                                          }
                                        },
                                        icon: const Icon(Icons.settings),
                                      ),
                                      IconButton(
                                        onPressed: () async {
                                          final int? _messID =
                                              await Get.to(SearchMessInGroupUI(
                                            messageDetailController:
                                                _messageDetailController,
                                          ));
                                          if (_messID != null && _messID > 0) {
                                            await _timViTriMess(
                                                _messID,
                                                _messageDetailController
                                                    .groupMain.value.groupID);
                                          }
                                        },
                                        icon: const Icon(
                                          Icons.search,
                                          size: 28,
                                        ),
                                      ),
                                    ],
                                  )
                              ],
                            );
                          }),
                        )
                      : _deleteMulti(),
                ),
              ),
              body: Stack(
                children: [
                  Obx(
                    () => Column(
                      children: [
                        buildListMessage(),
                        if (!isMulti)
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Row(
                                children: <Widget>[
                                  GestureDetector(
                                      child: const Icon(
                                        Icons.sentiment_very_satisfied,
                                        size: 25,
                                      ),
                                      onTap: () {
                                        SystemChannels.textInput
                                            .invokeMethod('TextInput.hide');
                                        _emojiShowing.value =
                                            !_emojiShowing.value;
                                      }),
                                  GestureDetector(
                                    child: const Icon(
                                      Icons.gif_sharp,
                                      size: 25,
                                    ),
                                    onTap: () => _sendGiphy(),
                                  ),
                                  const SizedBox(width: 5),
                                  Expanded(
                                    child: FlutterMentions(
                                      maxLines: 5,
                                      minLines: 1,
                                      key: keyMention,
                                      suggestionListHeight: Get.height * 0.3,
                                      textInputAction: TextInputAction.newline,
                                      focusNode: _textSendFocus,
                                      decoration: const InputDecoration(
                                        hintText: 'Nhập tin nhắn',
                                        border: InputBorder.none,
                                      ),
                                      suggestionPosition:
                                          SuggestionPosition.Top,
                                      keyboardType: TextInputType.multiline,
                                      onSubmitted: (value) {
                                        _sendMessage();
                                      },
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      onChanged: (value) => _soanTin(value),
                                      hideSuggestionList: false,
                                      mentions: [
                                        Mention(
                                            trigger: '@',
                                            style: const TextStyle(
                                              color: Colors.blue,
                                            ),
                                            matchAll: false,
                                            data: _messageDetailController
                                                .listUserInGroup
                                                .map((e) => {
                                                      'avatar': e.avatar,
                                                      'display': e.fullName
                                                          ?.replaceAll(
                                                              ' ',
                                                              LocalVariable
                                                                  .spaceChar),
                                                      'id': '${e.userID}'
                                                    })
                                                .toList(),
                                            suggestionBuilder: (data) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 4, top: 4),
                                                child: Container(
                                                  margin: const EdgeInsets.only(
                                                      left: 16),
                                                  child: Row(
                                                    children: [
                                                      SizedBox(
                                                        height: 40,
                                                        width: 40,
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(50),
                                                          child: ImageNetworkWidget(
                                                              packageName:
                                                                  LocalVariable
                                                                      .namePackage,
                                                              urlImageNetwork:
                                                                  baseUrlUpload +
                                                                      '${data['avatar']}',
                                                              imageAssetDefault:
                                                                  ImageVariable
                                                                      .person),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(data['display']),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }),
                                      ],
                                    ),
                                  ),
                                  GestureDetector(
                                      onTap: () =>
                                          Get.bottomSheet(RecorderAudioUI(
                                            pathAudio: (path) {
                                              if (path != null)
                                                _sendAudio([File(path)]);
                                              Get.back();
                                            },
                                          )),
                                      child: const Icon(Icons.mic)),
                                  Obx(() {
                                    return Center(
                                      child: IconButton(
                                        onPressed: () {
                                          if (_messageDetailController
                                              .sendButtonIcon.value)
                                            _checkLink();
                                          else {
                                            //check chỗ này
                                            FocusScope.of(context)
                                                .requestFocus(FocusNode());
                                            Get.bottomSheet(BottomSheetChucNang(
                                              actionClick: (id) =>
                                                  _actionButtomClick(id),
                                            ));
                                          }
                                        },
                                        icon: (_messageDetailController
                                                .sendButtonIcon.value)
                                            ? const Icon(Icons.send)
                                            : const Icon(Icons.add),
                                      ),
                                    );
                                  }),
                                ],
                              ),
                            ),
                          ),
                        _emojiWidget()
                      ],
                    ),
                  ),
                  Positioned(
                      right: 25,
                      bottom: 60,
                      child: Obx(() {
                        if (_screenshotData
                            .isNotEmpty) if (_screenshotData.length > 1)
                          return _screenShotMultiImageWidget();
                        else
                          return _screenShotOneImageWidget();
                        return const SizedBox.shrink();
                      }))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index, ChatMessage currentItem, ChatMessage previousItem,
      {required int lengthListMessage}) {
    if (currentItem.userIDGui == userIDChat) {
      // Right (my message)
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
              padding: const EdgeInsets.all(10),
              child: GestureDetector(
                onLongPress: () async {
                  if (!(currentItem.isThuHoi ?? false)) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return _diaLogMessageOption(
                              currentItem, fullNameChat, true,
                              index: index);
                        });
                  }
                },
                child: FrameMessage(
                  isLeftMessage: false,
                  chatMessage: currentItem,
                  listUser: _messageDetailController.listUserInGroup,
                  lastMessage: isLastMessageRight(index, lengthListMessage,
                      currentItem: currentItem, previousItem: previousItem),
                  timViTriFunc: (messID, groupID) =>
                      _timViTriMess(messID, groupID),
                  firstMessage: null,
                ),
              )),
          if (isMulti)
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: CheckBoxWidget(
                  color: BaseColor.primaryColor,
                  change: (value) {
                    setState(() {
                      currentItem.isSelect = value;
                      _countSelect = _messageDetailController.listChatMessage
                          .where((element) => element.isSelect == true)
                          .toList()
                          .length;
                    });
                  }),
            ),
        ],
      );
    } else {
      // Left (peer message)
      final DataUsers? _user =
          _messageDetailController.listUserInGroup.firstWhereOrNull(
        (element) => element.userID == currentItem.userIDGui,
      );
      if (_user != null)
        return Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              if (isLastMessageLeft(index, lengthListMessage, _user,
                  currentItem: currentItem, previousItem: previousItem))
                GestureDetector(
                  onTap: () {
                    if (_messageDetailController.groupMain.value.isGroup)
                      Get.find<GroupInfoController>()
                          .checkAndGoChat(listItemUser: [_user], isOff: true);
                  },
                  child: SizedBox(
                    height: 35,
                    width: 35,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(75),
                        child: ImageNetworkWidget(
                            packageName: LocalVariable.namePackage,
                            urlImageNetwork:
                                baseUrlUpload + (_user.avatar ?? ''),
                            imageAssetDefault: ImageVariable.person)),
                  ),
                )
              else
                const SizedBox(width: 35.0),
              GestureDetector(
                onLongPress: () async {
                  if (!(currentItem.isThuHoi ?? false)) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return _diaLogMessageOption(
                              currentItem, _user.fullName!, false);
                        });
                  }
                },
                child: FrameMessage(
                  isLeftMessage: true,
                  chatMessage: currentItem,
                  users: _user,
                  lastMessage: isLastMessageLeft(
                      index, lengthListMessage, _user,
                      currentItem: currentItem, previousItem: previousItem),
                  firstMessage: isFirstMessageLeft(index, _user,
                      lengthListMessage, currentItem, previousItem),
                  listUser: _messageDetailController.listUserInGroup,
                  timViTriFunc: (messID, groupID) =>
                      _timViTriMess(messID, groupID),
                ),
              ),
              const Spacer(),
              if (isMulti)
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: CheckBoxWidget(
                      color: BaseColor.primaryColor,
                      change: (value) {
                        setState(() {
                          currentItem.isSelect = value;
                          _countSelect = _messageDetailController
                              .listChatMessage
                              .where((element) => element.isSelect == true)
                              .toList()
                              .length;
                        });
                      }),
                ),
            ],
          ),
        );
      return const SizedBox.shrink();
    }
  }

  Widget buildListMessage() {
    return Expanded(
      child: Stack(
        children: [
          if (_messageDetailController.groupMain.value.hinhNen != null)
            ExtendedImage.network(
              baseUrlApiChat +
                  (_messageDetailController.groupMain.value.hinhNen ?? ''),
              shape: BoxShape.rectangle,
              enableMemoryCache: true,
              fit: BoxFit.cover,
              loadStateChanged: (ExtendedImageState state) {
                switch (state.extendedImageLoadState) {
                  case LoadState.completed:
                    return ExtendedRawImage(
                      image: state.extendedImageInfo?.image,
                      fit: BoxFit.cover,
                      height: Get.height,
                      width: Get.width,
                    );
                  case LoadState.failed:
                    return GestureDetector(
                      child: SizedBox(
                        height: Get.height,
                        width: Get.width,
                      ),
                    );
                  default:
                    return GestureDetector(
                      child: SizedBox(
                        height: Get.height,
                        width: Get.width,
                      ),
                    );
                }
              },
            ),
          Obx(() {
            return ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: SingleChildScrollView(
                controller: listScrollController,
                reverse: true,
                child: Column(
                  children: _messageDetailController.listChatMessage.map((e) {
                    final int index =
                        _messageDetailController.listChatMessage.indexOf(e);
                    final ChatMessage _previousItem = (index > 0)
                        ? _messageDetailController.listChatMessage[index - 1]
                        : e;
                    final _listlength =
                        _messageDetailController.listChatMessage.length;

                    return AutoScrollTag(
                      key: ValueKey(index),
                      highlightColor: Colors.yellowAccent.withOpacity(0.5),
                      controller: listScrollController,
                      index: index,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: (e.createDate!.day >
                                    _previousItem.createDate!.day ||
                                e.createDate!.month >
                                    _previousItem.createDate!.month ||
                                e.createDate!.year >
                                    _previousItem.createDate!.year)
                            ? Column(
                                children: [
                                  HorizontalLineText(
                                    Text(
                                      DateFormat('EEEE - dd/MM/yyyy', 'vi')
                                          .format(e.createDate!),
                                      style: const TextStyle(
                                          fontSize: 15,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  buildItem(index, e, _previousItem,
                                      lengthListMessage: _listlength),
                                ],
                              )
                            : buildItem(index, e, _previousItem,
                                lengthListMessage: _listlength),
                      ),
                    );
                  }).toList(),
                ),
              ),
            );
          }),
          Positioned(
            left: 10,
            bottom: 0,
            child: Obx(() {
              if (_messageDetailController.isSoanTin.value)
                return Row(
                  children: const [
                    SpinKitWanderingCubes(
                      color: Colors.cyan,
                      shape: BoxShape.circle,
                      size: 10,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      'Đang soạn tin',
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 12,
                          color: Colors.cyan),
                    ),
                  ],
                );
              return const SizedBox.shrink();
            }),
          ), //đang xoạn tin nhắn
          Positioned(
            right: Get.width / 2 - 20,
            bottom: 20,
            child: Obx(() {
              if (_messageDetailController.isShowBottomMessage.value)
                return GestureDetector(
                  onTap: () {
                    rollListMessToStart();
                  },
                  child: Stack(
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: BaseColor.accentsColor),
                        child: const Center(
                          child: Icon(
                            Icons.arrow_downward,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                      ),
                      Obx(() {
                        if (_messageDetailController.totalMessageNew.value > 0)
                          return Positioned(
                            top: 0,
                            right: 0,
                            child: Container(
                              height: 15,
                              width: 15,
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.red),
                              child: Center(
                                  child: Text(
                                '${_messageDetailController.totalMessageNew.value}',
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 10),
                              )),
                            ),
                          );
                        return const SizedBox.shrink();
                      })
                    ],
                  ),
                );
              return const SizedBox.shrink();
            }),
          ),
          Positioned(bottom: 10, right: 5, child: _screenshotWidget()),
          if (isReply)
            Positioned(
              bottom: 2,
              child: _showRepTin(),
            ), //show thông tin reply tin
          if (_messageDetailController.groupMain.value.ghimMessage != null &&
              _messageDetailController.groupMain.value.ghimMessage!.isNotEmpty)
            Positioned(
              top: 2,
              child: ShowGhimMess(
                ghimMessData: ReplyModel.fromJson(
                    _messageDetailController.groupMain.value.ghimMessage!),
                optionMessaGhim: (boGhim) =>
                    _actionGhimMessage(null, xoaGhim: boGhim),
                iconData: Icons.close,
                timViTri: (messID) => _timViTriMess(
                    messID, _messageDetailController.groupMain.value.groupID),
              ),
            ) //Show tin nhắn được ghim\
        ],
      ),
    );
  }

  Widget _screenshotWidget() {
    return Obx(() {
      if (_messageDetailController.listImageScreenshot.isNotEmpty)
        return Column(
          children: [
            if (_messageDetailController.listImageScreenshot.length == 1)
              ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                imageAssetDefault: ImageVariable.noimg,
                isOnline: false,
                urlImageOffline:
                    _messageDetailController.listImageScreenshot.first.path,
                imageHeight: 70,
                imageWidth: 50,
                fit: BoxFit.fill,
              )
            else
              Expanded(
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    ImageNetworkWidget(
                      packageName: LocalVariable.namePackage,
                      imageAssetDefault: ImageVariable.noimg,
                      isOnline: false,
                      urlImageOffline: _messageDetailController
                          .listImageScreenshot.last.path,
                      imageHeight: 70,
                      imageWidth: 50,
                    ),
                    Positioned(
                      left: 5,
                      child: ImageNetworkWidget(
                        packageName: LocalVariable.namePackage,
                        imageAssetDefault: ImageVariable.noimg,
                        isOnline: false,
                        urlImageOffline: _messageDetailController
                            .listImageScreenshot[_messageDetailController
                                    .listImageScreenshot.length -
                                2]
                            .path,
                        imageHeight: 70,
                        imageWidth: 50,
                      ),
                    )
                  ],
                ),
              ),
            const SizedBox(
              height: 5,
            ),
            GestureDetector(
              onTap: () {},
              child: SizedBox(
                height: 20,
                width: 70,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      color: BaseColor.primaryColor,
                      borderRadius: BorderRadius.circular(8)),
                  child: const Center(
                    child:
                        Text('Gửi ngay', style: TextStyle(color: Colors.white)),
                  ),
                ),
              ),
            )
          ],
        );
      return const SizedBox.shrink();
    });
  }

  Offstage _emojiWidget() {
    return Offstage(
        offstage: !_emojiShowing.value,
        child: SizedBox(
          height: 250,
          child: EmojiPicker(
              onEmojiSelected: (Category category, Emoji emoji) {
                keyMention.currentState!.controller!.text += emoji.emoji;
              },
              // onBackspacePressed: _onBackspacePressed,
              config: const Config(
                  columns: 7,
                  emojiSizeMax: 30.0,
                  verticalSpacing: 0,
                  horizontalSpacing: 0,
                  initCategory: Category.RECENT,
                  bgColor: Color(0xFFF2F2F2),
                  indicatorColor: Colors.blue,
                  iconColor: Colors.grey,
                  iconColorSelected: Colors.blue,
                  progressIndicatorColor: Colors.blue,
                  backspaceColor: Colors.blue,
                  showRecentsTab: true,
                  recentsLimit: 28,
                  noRecentsText: 'No Recents',
                  noRecentsStyle:
                      TextStyle(fontSize: 20, color: Colors.black26),
                  categoryIcons: CategoryIcons(),
                  buttonMode: ButtonMode.MATERIAL)),
        ));
  }

  @override
  void dispose() {
    SignalR.instance.connection.off(LocalVariable.dangSoanTin);
    keyMention.currentState?.controller?.dispose();
    listScrollController.dispose();

    // reset current groupId
    currentGroupId.value = -1;
    currentGroupObj.value = GroupInfo();
    _subscriptionScreenshot.cancel();
    ScreenshotCallback.instance.clearData();

    // reset mess new of group
    SyncChat.instance
        .resetDaXemGroup(_messageDetailController.groupMain.value.groupID);

    // update count new mess in bottom bar
    updateCountNewMessInBottomBar();

    super.dispose();
  }

  bool isLastMessageLeft(int index, int lengthListChatMessage, DataUsers user,
      {required ChatMessage currentItem, required ChatMessage previousItem}) {
    if (index > 0) {
      if (index < (lengthListChatMessage - 1) &&
          _messageDetailController.listChatMessage[index + 1].userIDGui !=
              user.userID)
        return true;
      else if (index == (lengthListChatMessage - 1) &&
          currentItem.userIDGui == user.userID) return true;
      return false;
    }
    return false;
  }

  bool isFirstMessageLeft(int index, DataUsers user, int lengthListMessage,
      ChatMessage currentItem, ChatMessage previousItem) {
    if (index > 0) {
      if (currentItem.userIDGui != previousItem.userIDGui) return true;
      return false;
    } else {
      if (currentItem.userIDGui == userIDChat) return false;
      return true;
    }
  }

  bool isLastMessageRight(int index, int lengthListChatMessage,
      {required ChatMessage currentItem, required ChatMessage previousItem}) {
    if (index > 0 &&
        (index < lengthListChatMessage - 1) &&
        _messageDetailController.listChatMessage[index + 1].userIDGui !=
            userIDChat)
      return true;
    else if (index == lengthListChatMessage - 1 &&
        _messageDetailController.listChatMessage.last.userIDGui == userIDChat)
      return true;
    return false;
  }

  Future<void> _actionButtomClick(int index) async {
    Get.back();
    switch (index) {
      case 1:
        Get.to(CameraScreen(
          onResutl: (value) {
            if (value != null) _sendFileFromCamera(value);
          },
        ));
        break;
      case 2:
        await _galleryController.getMedia(galleryType: GalleryType.image);
        _slideController.anchor();
        break;
      case 3:
        await _galleryController.getMedia(galleryType: GalleryType.video);
        _slideController.anchor();
        break;
      case 4:
        final List<File>? files = await MediaPicker.instance.pickFile();
        _sendFile(files);
        break;
      case 5:
        LoadingWidget.instance.show();
        await _messageDetailController.shareLocation();
        Get.back();
        break;
      case 6:
        LoadingWidget.instance.show();
        ChatConfig.cleanCache().then((value) {
          LoadingWidget.instance.hide();
          LoadingWidget.instance.snackBarThongBao('Thông báo', 'Đã xóa cache');
        });
        break;
    }
  }

  Future _actionGhimMessage(ChatMessage? chatMessage,
      {bool xoaGhim = false}) async {
    if (xoaGhim ||
        (_messageDetailController.groupMain.value.ghimMessage != null &&
            _messageDetailController.groupMain.value.ghimMessage!.isNotEmpty)) {
      AwesomeDialog(
          context: context,
          dialogType: DialogType.QUESTION,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Xóa tin nhắn đã ghim',
          desc: 'Bạn có chắc muốn xóa tin nhắn đã ghim ?',
          btnCancelText: 'Không',
          btnCancelOnPress: () {},
          btnOkText: 'Có',
          btnOkOnPress: () async {
            final GroupInfo _params = GroupInfo()
              ..groupID = _messageDetailController.groupMain.value.groupID
              ..groupKey = _messageDetailController.groupMain.value.groupKey
              ..ghimMessage =
                  (!xoaGhim) ? jsonEncode(_ghimModelFu(chatMessage!)) : null
              ..createUserID = userIDChat;
            await ApiRequest.instance.appTraoDoiV2GhimMessage(_params);
          }).show();
    } else {
      final GroupInfo _params = GroupInfo()
        ..groupID = _messageDetailController.groupMain.value.groupID
        ..groupKey = _messageDetailController.groupMain.value.groupKey
        ..ghimMessage =
            (!xoaGhim) ? jsonEncode(_ghimModelFu(chatMessage!)) : null
        ..createUserID = userIDChat;
      await ApiRequest.instance.appTraoDoiV2GhimMessage(_params);
    }
  }

  void _checkLink() {
    if (keyMention.currentState!.controller!.text.trim().isLink()) {
      if (keyMention.currentState!.controller!.text.trim().isYoutube())
        _sendLinkOrYoutube(MessageType.Youtube);
      else
        _sendLinkOrYoutube(MessageType.Link);
    } else
      _sendMessage();
  }

  String _congChuoi(List<String> data) {
    String _temp = '';
    for (int i = 0; i < 10; i++) {
      _temp = _temp + ' ' + data[i];
    }
    return _temp.trim() + '...';
  }

  Row _deleteMulti() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextButton(
            onPressed: () {
              setState(() {
                isMulti = false;
                for (final element in _messageDetailController.listChatMessage)
                  element.isSelect = false;
                _countSelect = 0;
              });
            },
            child: const Icon(
              Icons.close,
              color: Colors.white,
            )),
        Text(
          'Đã chọn $_countSelect tin nhắn',
          style: const TextStyle(color: Colors.white),
        ),
        TextButton(
            onPressed: () async {
              final List<ChatMessage> _tempList = _messageDetailController
                  .listChatMessage
                  .where((element) => element.isSelect == true)
                  .toList();
              if (_tempList == null || _tempList.isEmpty) {
                showSnackBar('Vui lòng chọn tin nhắn để xóa');
                return;
              }
              AwesomeDialog(
                      context: context,
                      dialogType: DialogType.QUESTION,
                      animType: AnimType.BOTTOMSLIDE,
                      title: 'Xóa tin nhắn',
                      desc: 'Bạn có chắc muốn xóa những tin nhắn này?',
                      btnCancelText: 'Không',
                      btnCancelOnPress: () {},
                      btnOkText: 'Có',
                      btnOkOnPress: () {})
                  .show();
            },
            child: Column(
              children: const [
                Icon(Icons.delete, color: Colors.white),
                Text(
                  'Xóa',
                  style: TextStyle(color: Colors.white),
                )
              ],
            )),
      ],
    );
  }

  // chức năng dialog
  BackdropFilter _diaLogMessageOption(
      ChatMessage chatMessage, String hoTenUserGui, bool right,
      {int? index}) {
    final String time = DateFormat('kk:mm - dd/MM/yyyy')
        .format(chatMessage.createDate ?? DateTime.now());
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
      child: Dialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          margin: const EdgeInsets.only(left: 8),
          height: Get.height * 0.25,
          child: Column(
            children: [
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(children: <TextSpan>[
                            const TextSpan(
                                text: 'Người gửi: ',
                                style: TextStyle(color: Colors.black)),
                            TextSpan(
                                text: right ? 'Bạn' : hoTenUserGui,
                                style: const TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                        RichText(
                          text: TextSpan(children: <TextSpan>[
                            const TextSpan(
                                text: 'Gửi lúc: ',
                                style: TextStyle(color: Colors.black)),
                            TextSpan(
                                text: time,
                                style: const TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                  onTap: () async {
                                    Get.back();
                                    setState(() {
                                      isReply = true;
                                      _itemSelected = chatMessage;
                                    });
                                  },
                                  child: const OptionMessage(
                                      iconData: Icons.reply,
                                      colorIcon: Colors.deepPurple,
                                      label: 'Trả lời')),
                              GestureDetector(
                                  onTap: () async {
                                    Get.back();
                                    await _actionGhimMessage(chatMessage);
                                  },
                                  child: const OptionMessage(
                                      iconData: Icons.push_pin_outlined,
                                      colorIcon: Colors.redAccent,
                                      label: 'Ghim')),
                              GestureDetector(
                                  onTap: () async => _shareMess(chatMessage),
                                  child: const OptionMessage(
                                      iconData: Icons.share_outlined,
                                      colorIcon: Colors.blue,
                                      label: 'Chia sẻ')),
                              GestureDetector(
                                onTap: () {
                                  Get.back();
                                  Get.toNamed(ChuyenTiepUI.ROUTER_NAME,
                                      arguments: {
                                        'groupInfo': _messageDetailController
                                            .groupMain.value,
                                        'chatMessage': chatMessage
                                      });
                                },
                                child: const OptionMessage(
                                    iconData: Icons.redo,
                                    colorIcon: Colors.brown,
                                    label: 'Chuyển tiếp'),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.back();
                                Get.to(ChiTietByID(
                                    chatMessage,
                                    _messageDetailController.listUserInGroup,
                                    right));
                              },
                              child: const OptionMessage(
                                  iconData: Icons.info_outline,
                                  colorIcon: Colors.deepOrangeAccent,
                                  label: 'Chi tiết'),
                            ),
                            if (chatMessage.messageType.index == 0)
                              GestureDetector(
                                  onTap: () {
                                    Get.back();
                                    FlutterClipboard.copy(chatMessage.message!)
                                        .then((value) =>
                                            showSnackBar('Đã sao chép'));
                                  },
                                  child: const OptionMessage(
                                      iconData: Icons.copy,
                                      colorIcon: Colors.amber,
                                      label: 'Sao chép')),
                            if (right)
                              GestureDetector(
                                  onTap: () {
                                    Get.back();
                                    AwesomeDialog(
                                        context: context,
                                        dialogType: DialogType.QUESTION,
                                        animType: AnimType.BOTTOMSLIDE,
                                        title: 'Xóa tin nhắn',
                                        desc:
                                            'Bạn có chắc muốn xóa tin nhắn này?',
                                        btnCancelText: 'Không',
                                        btnCancelOnPress: () {},
                                        btnOkText: 'Có',
                                        btnOkOnPress: () {
                                          _messageDetailController
                                              .deleteMessageByMessageID(
                                                  [chatMessage], index!);
                                        }).show();
                                  },
                                  child: const OptionMessage(
                                      iconData: Icons.delete,
                                      colorIcon: Colors.greenAccent,
                                      label: 'Xóa')),
                            // if (right)
                            //   GestureDetector(
                            //       onTap: () {
                            //         setState(() {
                            //           isMulti = true;
                            //         });
                            //         Get.back();
                            //       },
                            //       child: const OptionMessage(
                            //           iconData: Icons.library_add_check_outlined,
                            //           colorIcon: Colors.orange,
                            //           label: 'Chọn nhiều')),
                            if (right)
                              GestureDetector(
                                  onTap: () {
                                    Get.back();
                                    AwesomeDialog(
                                        context: context,
                                        dialogType: DialogType.QUESTION,
                                        animType: AnimType.BOTTOMSLIDE,
                                        title: 'Thu hồi tin nhắn',
                                        desc:
                                            'Bạn có chắc muốn thu hồi tin nhắn này?',
                                        btnCancelText: 'Không',
                                        btnCancelOnPress: () {},
                                        btnOkText: 'Có',
                                        btnOkOnPress: () {
                                          _messageDetailController
                                              .thuHoiMessageByMessageID(
                                                  [chatMessage], index!);
                                        }).show();
                                  },
                                  child: const OptionMessage(
                                      iconData: Icons.refresh,
                                      colorIcon: Colors.black54,
                                      label: 'Thu hồi')),
                          ],
                        ),
                      ],
                    ),
                  ),
                  if (chatMessage.userIDGui != userIDChat)
                    Positioned(
                      top: 3,
                      right: 3,
                      child: ReactionWidget(
                        chatMessage,
                        size: 30,
                      ),
                    )
                  else if (chatMessage.reaction != null &&
                      chatMessage.reaction!.isNotEmpty)
                    Positioned(
                      top: 3,
                      right: 3,
                      child: Image.asset(
                        chatMessage.reaction!,
                        height: 30,
                        package: 'chat_package',
                      ),
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  ReplyModel _ghimModelFu(ChatMessage chatMess) {
    final String? fullName = _messageDetailController.listUserInGroup
        .firstWhereOrNull((element) => element.userID == chatMess.userIDGui)
        ?.fullName;
    final Text _name = Text((fullName != null) ? fullName : fullNameChat,
        style: const TextStyle(fontWeight: FontWeight.bold));

    final ReplyModel _ghimModel = ReplyModel(
        messID: chatMess.messageID,
        messType: chatMess.messageType.index,
        nguoiGui: _name.data);

    switch (chatMess.messageType) {
      case MessageType.Reply:
      case MessageType.Text:
        _ghimModel.oldMess = (chatMess.message!.split(' ').length > 10)
            ? _congChuoi(chatMess.message!.split(' '))
            : chatMess.message;
        return _ghimModel;
      case MessageType.Image:
        _ghimModel.oldMess = '[HinhAnh]';
        _ghimModel.link = chatMess.media!.first.fileUrl;
        return _ghimModel;
      case MessageType.Video:
        _ghimModel.oldMess = '[Video]';
        _ghimModel.link = chatMess.media!.first.thumbnail;
        return _ghimModel;
      case MessageType.File:
        _ghimModel.oldMess = chatMess.media!.first.fileName;
        return _ghimModel;
      case MessageType.Audio:
        _ghimModel.oldMess = '[Audio]';
        // _ghimModel.link = chatMess.media!.first.thumbnail;
        return _ghimModel;
      case MessageType.Link:
      case MessageType.Youtube:
        _ghimModel.oldMess = chatMess.media!.first.fileUrl;
        _ghimModel.link = chatMess.media!.first.thumbnail;
        return _ghimModel;
      case MessageType.Emoticon:
        _ghimModel.oldMess = '[Sticker]';
        _ghimModel.link = chatMess.message;
        return _ghimModel;
      default:
        return _ghimModel;
    }
  }

  Future<void> _shareMess(ChatMessage chatMessage) async {
    try {
      Get.back();
      switch (chatMessage.messageType) {
        case MessageType.Reply:
        case MessageType.Emoticon:
        case MessageType.Text:
          Share.share(chatMessage.message ?? '');
          break;
        case MessageType.Audio:
        case MessageType.Image:
        case MessageType.Video:
        case MessageType.File:
          LoadingWidget.instance.show();

          if (chatMessage.messageID > 0) {
            final List<String> _tempUrls = chatMessage.media!
                .map((e) => baseUrlApiChat + e.fileUrl!)
                .toList();

            final List<cache.FileInfo>? _file = await CacheManagerCustom
                .instance
                .getListFileAwaitInCache(_tempUrls);
            if (_file != null) {
              Share.shareFiles(_file.map((e) => e.file.path).toList());
            } else {
              LoadingWidget.instance.hide();
              LoadingWidget.instance.snackBarThongBao(
                'Thông báo',
                'Tệp không tồn tại, vui lòng kiểm tra và thử lại!',
              );

              break;
            }
          } else {
            final List<String> _tempUrls =
                chatMessage.media!.map((e) => e.fileUrl!).toList();
            Share.shareFiles(_tempUrls);
          }

          LoadingWidget.instance.hide();
          break;
        case MessageType.Link:
        case MessageType.Youtube:
          Share.share(chatMessage.media!.first.fileUrl ?? '');
          break;
      }
    } catch (e) {}
  }

  void _scrollListener() {
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      if (!_messageDetailController.isFind)
        _messageDetailController.eventHandler(Events.LoadMore);
      else
        _messageDetailController.loadMoreFindPosition(
            _messageDetailController.listChatMessage.first.messageID,
            selectType: 2);
    }

    if (listScrollController.offset >= 60)
      _messageDetailController.showBottomMessage(true);

    // if (listScrollController.offset - 60 <=
    //         listScrollController.position.minScrollExtent &&
    //     !listScrollController.position.outOfRange &&
    //     _messageDetailController.isFind &&
    //     !isHight &&
    //     !_messageDetailController.isLoadingList)
    //   _messageDetailController.loadMoreFindPosition(
    //       _messageDetailController.listChatMessage.last.messageID);

    if (listScrollController.offset <=
            listScrollController.position.minScrollExtent &&
        !listScrollController.position.outOfRange)
      _messageDetailController.showBottomMessage(false);
  }

  void _sendFile(List<File>? files) {
    if (files != null) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.File,
              message: null,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey),
          files: files);
    }
  }

  void _sendAudio(List<File>? files) {
    if (files != null) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.Audio,
              message: null,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey),
          files: files);
    }
  }

  Future _sendGiphy() async {
    final GiphyGif? gif = await GiphyGet.getGif(
        context: context, //Required
        apiKey: 'bUXmATo1C2o1FTqGO2325XeRq7Sd92B9', //Required.
        lang: GiphyLanguage.vietnamese, //Optional - Language for query.
        searchText: 'Tìm kiếm' // Optional- default accent color.
        );
    if (gif != null) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.Emoticon,
              message: gif.images!.original!.url,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey));
    }
  }

  void _sendImage(List<File>? images) {
    if (images != null) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.Image,
              message: null,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey),
          files: images);
    }
  }

  void _sendLinkOrYoutube(MessageType type) {
    if (keyMention.currentState!.controller!.text.trim().isNotEmpty) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: type,
              message: keyMention.currentState!.controller!.text.trim(),
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey));
      keyMention.currentState!.controller!.clear();
      keyMention.currentState!.showSuggestions.value = false;
      _messageDetailController.sendButtonIcon.value = false;
    }
  }

  void _sendMessage() {
    if (keyMention.currentState!.controller!.text.trim().isNotEmpty) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      if (isReply) {
        _messageDetailController.eventHandler(Events.SendMessage,
            message: ChatMessage(
                messageID: 0,
                messageKey: const Uuid().v4(),
                messageType: MessageType.Reply,
                message: keyMention.currentState!.controller!.text.trim(),
                replyData: jsonEncode(_replyModel!.toJson()),
                userNameGui: userNameChat,
                userIDGui: userIDChat,
                tinhTrangID: 0,
                tenTinhTrang: 'Đang gửi',
                createDate: DateTime.now(),
                groupID: _messageDetailController.groupMain.value.groupID,
                groupKey: _messageDetailController.groupMain.value.groupKey));
        keyMention.currentState!.controller!.clear();
        _messageDetailController.sendButtonIcon.value = false;
        setState(() {
          isReply = false;
          _itemSelected = null;
          _replyModel = null;
        });
      } else {
        _messageDetailController.eventHandler(Events.SendMessage,
            message: ChatMessage(
                messageID: 0,
                messageKey: const Uuid().v4(),
                messageType: MessageType.Text,
                message: keyMention.currentState!.controller!.text.trim(),
                userNameGui: userNameChat,
                userIDGui: userIDChat,
                tinhTrangID: 0,
                tenTinhTrang: 'Đang gửi',
                createDate: DateTime.now(),
                groupID: _messageDetailController.groupMain.value.groupID,
                groupKey: _messageDetailController.groupMain.value.groupKey));
        keyMention.currentState!.controller!.clear();
        keyMention.currentState!.showSuggestions.value = false;
        _messageDetailController.sendButtonIcon.value = false;
      }
    }
  }

  void _sendFileFromCamera(File file) {
    if (_messageDetailController.isShowBottomMessage.value)
      rollListMessToStart(milliseconds: 1000);

    final String mimeType = mime(path.basename(file.path))!;
    if (mimeType.contains('image/')) {
      _sendImage([file]);
    }

    if (mimeType.contains('video/')) {
      _sendVideo([file]);
    }
  }

  void _sendVideo(List<File>? videos) {
    if (videos != null) {
      if (_messageDetailController.isShowBottomMessage.value)
        rollListMessToStart(milliseconds: 1000);
      _messageDetailController.eventHandler(Events.SendMessage,
          message: ChatMessage(
              messageID: 0,
              messageKey: const Uuid().v4(),
              messageType: MessageType.Video,
              message: null,
              userNameGui: userNameChat,
              userIDGui: userIDChat,
              tinhTrangID: 0,
              tenTinhTrang: 'Đang gửi',
              createDate: DateTime.now(),
              groupID: _messageDetailController.groupMain.value.groupID,
              groupKey: _messageDetailController.groupMain.value.groupKey),
          files: videos);
    }
  }

  Future rollListMessToStart({int milliseconds = 2000}) async {
    if (_messageDetailController.isFind) {
      _messageDetailController.pageNum = 0;
      await _messageDetailController.reloadList();
      _messageDetailController.isFind = false;
    }

    listScrollController.jumpTo(listScrollController.position.minScrollExtent);
    _messageDetailController.showBottomMessage(false);
  }

  Container _showRepTin() {
    final String? fullName = _messageDetailController.listUserInGroup
        .firstWhereOrNull(
            (element) => element.userID == _itemSelected!.userIDGui)
        ?.fullName;
    final Text _name = Text((fullName != null) ? fullName : fullNameChat,
        style: const TextStyle(fontWeight: FontWeight.bold));

    _replyModel = ReplyModel(
        messID: _itemSelected!.messageID,
        messType: _itemSelected!.messageType.index,
        nguoiGui: _name.data);

    switch (_itemSelected!.messageType) {
      case MessageType.Text:
        _replyModel!.oldMess = (_itemSelected!.message!.split(' ').length > 10)
            ? _congChuoi(_itemSelected!.message!.split(' '))
            : _itemSelected!.message;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(
                        _itemSelected!.message ?? '',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.Image:
        _replyModel!.oldMess = '[HinhAnh]';
        _replyModel!.link = _itemSelected!.media!.first.fileUrl;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork: baseUrlApiChat +
                      (_itemSelected!.media!.first.fileUrl ?? ''),
                  imageAssetDefault: ImageVariable.noimg,
                  isOnTapReload: false,
                  imageWidth: 35,
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Hình Ảnh]'),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.Video:
        _replyModel!.oldMess = '[Video]';
        _replyModel!.link = _itemSelected!.media!.first.thumbnail;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(width: 5),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    ImageNetworkWidget(
                      packageName: LocalVariable.namePackage,
                      urlImageNetwork: baseUrlApiChat +
                          (_itemSelected!.media!.first.thumbnail ?? ''),
                      imageAssetDefault: ImageVariable.noimg,
                      imageWidth: 35,
                    ),
                    Image.asset(
                      ImageVariable.playVideo,
                      width: Get.width * 0.05,
                      package: 'chat_package',
                    )
                  ],
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Video]'),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.File:
        _replyModel!.oldMess = _itemSelected!.media!.first.fileName;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(width: 5),
                Image.asset(
                  ImageVariable.fileExtensionIcon(
                      path.extension(_itemSelected!.media!.first.fileName!)),
                  width: 35,
                  fit: BoxFit.cover,
                  package: 'chat_package',
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(_itemSelected!.media!.first.fileName!),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.Link:
      case MessageType.Youtube:
        _replyModel!.oldMess = _itemSelected!.media!.first.fileUrl;
        _replyModel!.link = _itemSelected!.media!.first.thumbnail;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork: _itemSelected!.media!.first.thumbnail ?? '',
                  imageAssetDefault: ImageVariable.noimg,
                  imageWidth: 35,
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      Text(_itemSelected!.media!.first.fileUrl ?? ''),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.Emoticon:
        _replyModel!.oldMess = '[Sticker]';
        _replyModel!.link = _itemSelected!.message;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork: _itemSelected!.message ?? '',
                  imageAssetDefault: ImageVariable.noimg,
                  imageWidth: 35,
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Sticker]'),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      case MessageType.Reply:
        return Container();
      case MessageType.Audio:
        _replyModel!.oldMess = _itemSelected!.media!.first.fileName;
        return Container(
          color: Colors.grey.shade100,
          width: Get.width,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                const SizedBox(
                  width: 5,
                  height: 50,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: BaseColor.accentsColor),
                  ),
                ),
                const SizedBox(width: 5),
                Image.asset(
                  ImageVariable.audio,
                  width: 35,
                  fit: BoxFit.cover,
                  package: 'chat_package',
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      _name,
                      const Text('[Audio]'),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      isReply = false;
                      _itemSelected = null;
                      _replyModel = null;
                    });
                  },
                )
              ],
            ),
          ),
        );
      default:
        return Container();
    }
  }

  void _soanTin(String value) {
    if (value.trim().isNotEmpty) {
      _messageDetailController.sendButtonIcon.value = true;
      if (_count == 0) {
        _count = 1;
        _messageDetailController.eventHandler(Events.SoanTinNhan, isSoan: true);
      }
    } else {
      _messageDetailController.sendButtonIcon.value = false;
      _count = 0;
      _messageDetailController.eventHandler(Events.SoanTinNhan, isSoan: false);
    }
  }

  Future _timViTriMess(int messID, int groupID) async {
    FocusScope.of(context).unfocus();
    LoadingWidget.instance.show();

    final int index =
        await _messageDetailController.findPosition(messID, groupID);

    Future.delayed(const Duration(milliseconds: 1000)).then((value) {
      LoadingWidget.instance.hide();
      bool isHight = true;
      listScrollController.scrollToIndex(
        index,
        duration: const Duration(milliseconds: 2000),
        preferPosition: AutoScrollPosition.middle,
      );

      listScrollController.addListener(() {
        if (!listScrollController.isAutoScrolling && isHight) {
          listScrollController.highlight(index,
              animated: true,
              highlightDuration: const Duration(milliseconds: 1000));
          isHight = false;
        }
      });
    });
  }

  Future _receiveDataMedia({List<AssetEntity>? listAsset}) async {
    if (listAsset != null) {
      final List<File?> _lstFile =
          await Stream.fromFutures(listAsset.map((e) => e.file)).toList();
      final List<File> _listVideo = _lstFile
          .where((element) =>
              mime(path.extension(element!.path))?.contains('video/') ?? false)
          .whereNotNull()
          .toList();

      if (_listVideo.isNotEmpty) {
        _sendVideo(_listVideo);
      }

      final List<File> _listImage = _lstFile
          .where((element) =>
              mime(path.extension(element!.path))?.contains('image/') ?? false)
          .whereNotNull()
          .toList();
      if (_listImage.isNotEmpty) _sendImage(_listImage);
    }
  }

  Row _screenShotOneImageWidget() {
    return Row(
      children: [
        GestureDetector(
            onTap: () => _clearDataScreenshot(),
            child: const Icon(
              Icons.cancel,
              color: Colors.black38,
              size: 32,
            )),
        const SizedBox(
          width: 5,
        ),
        Column(
          children: [
            GestureDetector(
              onTap: () async {
                await Get.to(FullPhotoScreenshotUI(
                  images: [File(_screenshotData.first.path)],
                  currentIndex: 0,
                  tagController: _tagCtrMessageDetail,
                ));
                _clearDataScreenshot();
                if (_messageDetailController.isShowBottomMessage.value)
                  rollListMessToStart(milliseconds: 1000);
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38, width: 3),
                    borderRadius: BorderRadius.circular(15)),
                child: ClipRRect(
                  child: Image.file(
                    File(_screenshotData.first.path),
                    width: 80,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            GestureDetector(
              onTap: () {
                final List<File> images = <File>[];
                for (final image in _screenshotData) {
                  images.add(File(image.path));
                }
                _sendImage(images);
                _clearDataScreenshot();
              },
              child: SizedBox(
                height: 30,
                width: 80,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      color: BaseColor.primaryColor,
                      borderRadius: BorderRadius.circular(12)),
                  child: const Center(
                      child: Text('Gửi ngay',
                          style: TextStyle(color: Colors.white))),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Row _screenShotMultiImageWidget() {
    return Row(
      children: [
        GestureDetector(
            onTap: () => _clearDataScreenshot(),
            child: const Icon(
              Icons.cancel,
              color: Colors.black38,
              size: 32,
            )),
        Column(
          children: [
            GestureDetector(
              onTap: () async {
                final List<File> images = <File>[];
                for (final image in _screenshotData) {
                  images.add(File(image.path));
                }
                await Get.to(FullPhotoScreenshotUI(
                  images: images,
                  currentIndex: _screenshotData.length - 1,
                  tagController: _tagCtrMessageDetail,
                ));
                _clearDataScreenshot();
                if (_messageDetailController.isShowBottomMessage.value)
                  rollListMessToStart(milliseconds: 1000);
              },
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38, width: 3),
                        borderRadius: BorderRadius.circular(15)),
                    child: ClipRRect(
                      child: Image.file(
                        File(_screenshotData[_screenshotData.length - 2].path),
                        width: 70,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  Positioned(
                      top: -10,
                      left: 10,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black38, width: 3),
                            borderRadius: BorderRadius.circular(15)),
                        child: ClipRRect(
                          child: Image.file(
                            File(_screenshotData.last.path),
                            width: 80,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ))
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 15,
                ),
                GestureDetector(
                  onTap: () {
                    final List<File> images = <File>[];
                    for (final image in _screenshotData) {
                      images.add(File(image.path));
                    }
                    _sendImage(images);
                    _clearDataScreenshot();
                  },
                  child: SizedBox(
                    height: 30,
                    width: 90,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                          color: BaseColor.primaryColor,
                          borderRadius: BorderRadius.circular(12)),
                      child: const Center(
                          child: Text('Gửi ngay',
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }

  void _clearDataScreenshot() {
    ScreenshotCallback.instance.clearData();
    _screenshotData.clear();
  }

  Future<void> updateCountNewMessInBottomBar() async {
    // truong hop khi user vao cac man hinh ben trong mess detail
    // co tin nhan den, sau do back lai

    final GroupInfo? group = await SyncChat.instance
        .getGroupByGroupID(currentGroupObj.value.groupID);
    if (group != null) {
      countNewMessInChatConfig -= group.countNewMessage;
      final Map<String, dynamic> _param = {
        'groupID': _messageDetailController.groupMain.value.groupID,
        'userID': userIDChat,
        'count': countNewMessInChatConfig
      };
      ApiRequest.instance.updateCountNewMess(_param);
      countNewMessInChatConfigCtr.sink.add(countNewMessInChatConfig);
    }
  }
}
