import 'dart:math';
import 'dart:ui';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/traodoi/edit_group/edit_group_controller.dart';
import 'package:chat_package/src/traodoi/kho_luu_tru/kho_luu_tru_ui.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/add_user_in_group/add_user_in_group_ui.dart';
import 'package:chat_package/src/traodoi/tin_nhan_da_ghim/tin_nhan_da_ghim_ui.dart';
import 'package:chat_package/src/traodoi/xem_thanh_vien_group/xem_thanh_vien_group_ui.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_core_getx_dev/app/widget/loader_widget.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:collection/collection.dart';

import '../../../chat_package.dart';

class EditGroupUI extends StatelessWidget {
  static const ROUTER_NAME = '/EditGroupUI';

  final EditGroupController _editGroupController = Get.put(EditGroupController());

  Future<bool> _onWillPop() async {
    Get.back(result: {
      'pageRouter': EditGroupUI.ROUTER_NAME,
      'hinhNenNew': _editGroupController.pathHinhNen
    });
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: Colors.white,
          ),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: Gallery(
          isSelectMulti: false,
          imagesChoice: (images) {
            if (_editGroupController.isPickGroupIcon)
              _editGroupController.selectGalleryGroupIconAndUpload(images);
            else {
              _editGroupController.updateHinhNenGroupWithImageGallery(images);
            }
          },
          panelController: _editGroupController.slidingUpPanelController,
          galleryController: _editGroupController.galleryController,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      CustomPaint(
                        painter: AvatarPainter(),
                        child: SizedBox(
                          height: Get.width * 0.3,
                          width: Get.width,
                        ),
                      ),
                      Center(
                        child: Stack(
                          children: [
                            Obx(() {
                              return Padding(
                                padding: EdgeInsets.only(top: Get.width * 0.15),
                                child: GestureDetector(
                                  onTap: () {
                                    Get.dialog(FullPhotoUI(
                                      group: _editGroupController.group,
                                      currentIndex: 0,
                                    ));
                                  },
                                  child: SizedBox(
                                    height: Get.width * 0.3,
                                    width: Get.width * 0.3,
                                    child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        child: ImageNetworkWidget(
                                          packageName: LocalVariable.namePackage,
                                          shape: BoxShape.circle,
                                          urlImageNetwork: (_editGroupController
                                                      .group.isGroup
                                                  ? baseUrlApiChat
                                                  : baseUrlUpload) +
                                              _editGroupController
                                                  .groupIcon.value,
                                          imageAssetDefault:
                                          _editGroupController
                                              .group.isGroup ? ImageVariable.groupAvatar : ImageVariable.person,
                                          isOnline: _editGroupController
                                                  .groupIconPick ==
                                              null,
                                          urlImageOffline: _editGroupController
                                              .groupIconPick?.path,
                                        )),
                                  ),
                                ),
                              );
                            }),
                            if (_editGroupController.group.isGroup)
                              Positioned(
                                bottom: 5,
                                right: 5,
                                child: GestureDetector(
                                  onTap: (){
                                    _editGroupController.isPickGroupIcon = true;
                                    Get.bottomSheet(BottomSheetWithActionWidget(
                                      bottomItems:
                                          _editGroupController.bottomItems,
                                      idValue: (value) {
                                         _editGroupController
                                            .actionBottomSheet(value);
                                      },
                                    ));
                                  },
                                  child: const Icon(
                                    Icons.camera_alt,
                                    size: 28,
                                    color: Colors.black54,
                                  ),
                                ),
                              )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: (_editGroupController.group.isGroup)
                        ? const EdgeInsets.only(left: 36)
                        : const EdgeInsets.only(left: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Obx(() {
                          return Flexible(
                            child: Text(
                              _editGroupController.groupName.value,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 20,
                              ),
                            ),
                          );
                        }),
                        if (_editGroupController.group.isGroup)
                          IconButton(
                            onPressed: () {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Dialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              20.0)), //this right here
                                      child: SizedBox(
                                        height: Get.height * 0.2,
                                        child: Padding(
                                          padding: const EdgeInsets.all(12.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              const Text(
                                                'Cập nhật tên nhóm',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18),
                                              ),
                                              TextField(
                                                autofocus: true,
                                                controller: _editGroupController
                                                    .groupNameGroupCtrl,
                                                decoration:
                                                    const InputDecoration(
                                                        border:
                                                            InputBorder.none),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      Get.back();
                                                    },
                                                    child: const Text(
                                                      'Hủy',
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.red,
                                                      ),
                                                      textAlign:
                                                          TextAlign.right,
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      if (_editGroupController
                                                          .groupNameGroupCtrl
                                                          .text
                                                          .trim()
                                                          .isNotEmpty)
                                                        _editGroupController
                                                            .updateGroupName();
                                                    },
                                                    child: const Text(
                                                      'Cập nhật',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black),
                                                      textAlign:
                                                          TextAlign.right,
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                            },
                            icon: const Icon(
                              Icons.edit,
                              size: 24,
                              color: Colors.black54,
                            ),
                          )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (_editGroupController.group.isGroup &&
                            userIDChat ==
                                _editGroupController.group.createUserID)
                          GestureDetector(
                              onTap: () {
                                Get.toNamed(AddUserInGroupUI.ROUTER_NAME,
                                    arguments: {
                                      'groupInfo': _editGroupController.group
                                    });
                              },
                              child: circleButton(
                                  icon: Icons.person_add,
                                  label: 'Thêm\nthành viên')),
                        GestureDetector(
                          onTap: () {
                            Get.bottomSheet(
                                _bottomSheetChangeBackgroundImage());
                          },
                          child: circleButton(
                            icon: Icons.image,
                            label: 'Đổi\nhình nền',
                          ),
                        ),
                        Obx(() {
                          final bool _check =
                              _editGroupController.tatThongBao.value;
                          return GestureDetector(
                            onTap: () async {
                              await _editGroupController.tatTinNhan(_check);
                            },
                            child: (!_check)
                                ? circleButton(
                                    icon: Icons.notifications_active_outlined,
                                    label: 'Tắt\nthông báo')
                                : circleButton(
                                    icon: Icons.notifications_off_outlined,
                                    label: 'Bật\nthông báo'),
                          );
                        })
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  if (_editGroupController.group.isGroup)
                    Obx(() {
                      return GestureDetector(
                          onTap: () async {
                            await Get.toNamed(XemThanhVienGroupUI.ROUTER_NAME,
                                arguments: {
                                  'groupInfo': _editGroupController.group,
                                  'tagCtrMessageDetail':
                                      _editGroupController.tagCtrMessageDetail
                                });
                          },
                          child: itemSetting(
                            iconData: Icons.supervisor_account,
                            label: (_editGroupController
                                    .listUserInGroup.isNotEmpty)
                                ? 'Xem thành viên(${_editGroupController.listUserInGroup.length})'
                                : 'Xem thành viên',
                          ));
                    })
                  else
                    const SizedBox.shrink(),
                  const Divider(
                    color: BaseColor.backGroundColor,
                    thickness: 8,
                  ),
                  GestureDetector(
                      onTap: () {
                        Get.toNamed(KhoLuuTruUI.ROUTER_NAME, arguments: {
                          'groupInfo': _editGroupController.group
                        });
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          itemSetting(
                              iconData: Icons.perm_media, label: 'Kho lưu trữ'),
                          const Padding(
                            padding: EdgeInsets.only(left: 40, bottom: 10),
                            child: Text('Xem ảnh, link, file đã chia sẻ'),
                          ),
                        ],
                      )),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 40, right: 20, bottom: 10),
                    child: ScrollConfiguration(
                      behavior: DisableGlowListViewWidget(),
                      child: Obx(() {
                        if (_editGroupController.list5Image.isNotEmpty) {
                          return InkWell(
                            onTap: () {
                              Get.toNamed(KhoLuuTruUI.ROUTER_NAME, arguments: {
                                'groupInfo': _editGroupController.group
                              });
                            },
                            child: GridView.builder(
                                shrinkWrap: true,
                                itemCount:
                                    _editGroupController.list5Image.length + 1,
                                physics: const NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.zero,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        mainAxisSpacing: 5,
                                        crossAxisSpacing: 5),
                                itemBuilder: (context, index) {
                                  return Container(
                                    decoration: BoxDecoration(
                                        color: BaseColor.backGroundColor,
                                        borderRadius: BorderRadius.circular(4)),
                                    margin: const EdgeInsets.all(4),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: (index !=
                                              _editGroupController
                                                  .list5Image.length)
                                          ? ImageNetworkWidget(
                                        packageName: LocalVariable.namePackage,
                                              urlImageNetwork: baseUrlApiChat +
                                                  (_editGroupController
                                                          .list5Image[index]
                                                          .fileUrl ??
                                                      ''),
                                              imageAssetDefault:
                                                  ImageVariable.noimg,
                                            )
                                          : const Center(
                                              child: Icon(
                                                Icons.add,
                                                color: BaseColor.accentsColor,
                                                size: 36,
                                              ),
                                            ),
                                    ),
                                  );
                                }),
                          );
                        } else
                          return Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Container(
                              padding: const EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  color: BaseColor.backGroundColor,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: [
                                  Image.asset(
                                    ImageVariable.imageLuuTru,
                                    width: 50,
                                    package: 'chat_package',
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Flexible(
                                      child: Text(
                                          'Hình ảnh mới nhất của cuộc trò chuyện sẽ xuất hiện ở đây')),
                                ],
                              ),
                            ),
                          );
                      }),
                    ),
                  ),
                  const Divider(
                    color: BaseColor.backGroundColor,
                    thickness: 8,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(TinNhanDaGhimUI.ROUTER_NAME,
                          arguments: {'groupInfo': _editGroupController.group});
                    },
                    child: itemSetting(
                        iconData: Icons.push_pin_outlined,
                        label: 'Tin Nhắn đã ghim'),
                  ),
                  if (_editGroupController.group.isGroup &&
                      userIDChat != _editGroupController.group.createUserID)
                    const Divider(
                      color: BaseColor.backGroundColor,
                      thickness: 3,
                    ),
                  if (checkShowDelete())
                    const Divider(
                      color: BaseColor.backGroundColor,
                      thickness: 8,
                    ),
                  if (checkShowDelete())
                    GestureDetector(
                      onTap: () {
                        AwesomeDialog(
                            context: context,
                            dialogType: DialogType.QUESTION,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Xóa toàn bộ tin nhắn',
                            desc:
                                'Bạn có muốn xóa toàn bộ tin nhắn của cuộc trò chuyện này?',
                            btnCancelText: 'Không',
                            btnCancelOnPress: () {},
                            btnOkText: 'Có',
                            btnOkOnPress: () async {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (BuildContext context) =>
                                      const Center(
                                        child: LoaderWidget(),
                                      ));
                              await _editGroupController.deleteAllMessage();
                              Get.back();
                            }).show();
                      },
                      child: itemSetting(
                          iconData: Icons.delete_forever,
                          label: 'Xóa toàn bộ tin nhắn',
                          colorLabel: Colors.red),
                    ),
                  if (checkShowDelete())
                    const Divider(
                      color: BaseColor.backGroundColor,
                      thickness: 3,
                    ),
                  GestureDetector(
                    onTap: () {
                      AwesomeDialog(
                          context: context,
                          dialogType: DialogType.QUESTION,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Xóa lịch sử cuộc trò chuyện',
                          desc: 'Bạn có muốn xóa lịch sử cuộc trò chuyện này?',
                          btnCancelText: 'Không',
                          btnCancelOnPress: () {},
                          btnOkText: 'Có',
                          btnOkOnPress: () async {
                            LoadingWidget.instance.show();
                            final int result =
                                await _editGroupController.deleteHistoryChat();
                            LoadingWidget.instance.hide();
                            if (result == 1)
                              LoadingWidget.instance.snackBarThongBao(
                                  'Thông báo', 'Xóa lịch sử thành công');
                            else
                              LoadingWidget.instance.snackBarThongBao('Thông báo', 'Xóa lịch sử thất bại');
                          }).show();
                    },
                    child: itemSetting(
                        iconData: Icons.delete_forever,
                        label: 'Xóa lịch sử cuộc trò chuyện',
                        colorLabel: Colors.red),
                  ),
                  if (checkShowDelete())
                    GestureDetector(
                      onTap: () {
                        AwesomeDialog(
                            context: context,
                            dialogType: DialogType.QUESTION,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Giải tán nhóm',
                            desc: 'Bạn có chắc muốn giải tán nhóm này?',
                            btnCancelText: 'Không',
                            btnCancelOnPress: () {},
                            btnOkText: 'Có',
                            btnOkOnPress: () {
                              _editGroupController.deleteGroup();
                            }).show();
                      },
                      child: itemSetting(
                          iconData: Icons.exit_to_app,
                          label: 'Giải tán nhóm',
                          colorLabel: Colors.red),
                    ),
                  if (_editGroupController.group.isGroup &&
                      userIDChat != _editGroupController.group.createUserID)
                    GestureDetector(
                      onTap: () {
                        AwesomeDialog(
                            context: context,
                            dialogType: DialogType.QUESTION,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Rời nhóm',
                            desc: 'Bạn chắc muốn rời khỏi nhóm này?',
                            btnCancelText: 'Không',
                            btnCancelOnPress: () {},
                            btnOkText: 'Có',
                            btnOkOnPress: () async {
                              if (_editGroupController.listUserInGroup.length ==
                                  1) {
                                showSnackBar(
                                    'Không thể rời nhóm khi số thành viên ít hơn 3');
                                return;
                              }
                              final DataUsers? user = _editGroupController
                                  .listUserInGroup
                                  .firstWhereOrNull((element) =>
                                      element.userID == userIDChat);
                              if (user != null)
                                await ApiRequest.instance.deleteUserInGroup(
                                    _editGroupController.group, user);
                            }).show();
                      },
                      child: itemSetting(
                          iconData: Icons.logout,
                          label: 'Rời khỏi nhóm',
                          colorLabel: Colors.red),
                    ),
                ],
              ),
              physics: const ClampingScrollPhysics(),
            ),
          ),
        ),
      ),
    );
  }

  Widget circleButton({
    String? label,
    IconData? icon,
    Color? colorIcon,
    double? height,
    double? width,
    Color? backgroundColor,
    double? fontSizeLabel,
  }) {
    return Column(
      children: [
        Container(
          height: height ?? 40,
          width: width ?? 40,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: backgroundColor ?? BaseColor.accentsColor),
          child: Center(
              child: Icon(
            icon,
            color: colorIcon ?? Colors.white,
          )),
        ),
        Text(
          label!,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: fontSizeLabel ?? 16),
        )
      ],
    );
  }

  Container _bottomSheetChangeBackgroundImage() {
    return Container(
        height: Get.height * 0.4,
        decoration: BoxDecoration(
            color: BaseColor.backGroundColor,
            borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Obx(() {
            if (_editGroupController.listHinhNen.isNotEmpty) {
              final List<String> listString = _editGroupController.listHinhNen;
              return ScrollConfiguration(
                behavior: DisableGlowListViewWidget(),
                child: GridView.builder(
                  itemCount: listString.length + 2,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    mainAxisSpacing: 5,
                    crossAxisSpacing: 5,
                  ),
                  itemBuilder: (context, index) {
                    if (index == 0)
                      // select background from camera
                      return GestureDetector(
                        onTap: () async {
                          Get.back();
                          Get.to(CameraScreen(
                            speciesCamera: 1,
                            disableVideoRecord: false,
                            onResutl: (value) {
                              _editGroupController
                                  .updateHinhNenGroupWithImageCamera(value);
                            },
                          ));
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: BaseColor.accentsColor),
                            child: const Center(
                                child: Icon(
                              Icons.camera_alt,
                              color: Colors.white,
                              size: 32,
                            ))),
                      );
                    else if (index == 2)
                      // select background white color
                      return GestureDetector(
                        onTap: () async {
                          Get.back();
                          _editGroupController.pathHinhNen = 'default';
                          await _editGroupController.updateHinhNenGroup(null);
                        },
                        child: Container(
                          decoration: const BoxDecoration(
                            color: Colors.white,
                          ),
                        ),
                      );
                    else if (index == 1)
                      // select background from gallery
                      return GestureDetector(
                        onTap: () async {
                          Get.back();
                          _editGroupController.isPickGroupIcon = false;
                          await _editGroupController.galleryController.getMedia(galleryType: GalleryType.image);
                          _editGroupController.slidingUpPanelController.anchor();
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: BaseColor.accentsColor),
                            child: const Center(
                                child: Icon(
                              Icons.image,
                              color: Colors.white,
                              size: 32,
                            ))),
                      );
                    else
                      return GestureDetector(
                          onTap: () async {
                            await _editGroupController.updateHinhNenGroup(
                                listString[index - 2],
                                selectHinhNen: true);
                            _editGroupController.pathHinhNen =
                                listString[index - 2];
                          },
                          child: ImageNetworkWidget(
                              packageName: LocalVariable.namePackage,
                              urlImageNetwork:
                                  baseUrlApiChat + listString[index - 2],
                              imageAssetDefault: ImageVariable.noimg));
                  },
                ),
              );
            } else
              return const Center(
                child: CircularProgressIndicator(),
              );
          }),
        ));
  }

  Widget itemSetting(
      {IconData? iconData,
      String? label,
      Color? colorIcon,
      Color? colorLabel,
      double? sizeIcon,
      double? sizeLabel}) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 4.0, right: 8.0),
            child: Icon(
              iconData,
              color: colorIcon ?? Colors.black54,
              size: sizeIcon ?? 24,
            ),
          ),
          Expanded(
            child: Text(
              label!,
              style: TextStyle(color: colorLabel, fontSize: sizeLabel ?? 16),
            ),
          )
        ],
      ),
    );
  }

  bool checkShowDelete() {
    if (userIDChat == _editGroupController.group.createUserID &&
        _editGroupController.group.isGroup)
      return true;
    else
      return false;
  }
}

class FullPhotoUI extends StatefulWidget {
  final GroupInfo group;
  final int currentIndex;
  const FullPhotoUI({required this.group, required this.currentIndex});
  double get _sigmaX => 5.0;
  double get _sigmaY => 5.0;

  @override
  _FullPhotoUIState createState() => _FullPhotoUIState();
}

class _FullPhotoUIState extends State<FullPhotoUI> {
  late int tempCurrentIndex;
  @override
  void initState() {
    tempCurrentIndex = widget.currentIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(widget.group.groupName!),
      ),
      body: Stack(
        children: [
          Center(
            child: BackdropFilter(
              filter: ImageFilter.blur(
                  sigmaX: widget._sigmaX, sigmaY: widget._sigmaY),
              child: ScrollConfiguration(
                behavior: DisableGlowListViewWidget(),
                child: ExtendedImageGesturePageView.builder(
                  onPageChanged: (value)=> print(value),
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) {
                    final item = (widget.group.isGroup
                            ? baseUrlApiChat
                            : baseUrlUpload) +
                        (widget.group.groupIcon ?? '');
                    final Widget image = ExtendedImage.network(
                      item,
                      fit: BoxFit.scaleDown,
                      mode: ExtendedImageMode.gesture,
                      loadStateChanged: (ExtendedImageState state) {
                        switch (state.extendedImageLoadState) {
                          case LoadState.completed:
                            return ExtendedImage.network(
                              item,
                              fit: BoxFit.scaleDown,
                              mode: ExtendedImageMode.gesture,
                              initGestureConfigHandler:
                                  (ExtendedImageState state) {
                                return GestureConfig(
                                  minScale: 1,
                                  animationMinScale: 0.5,
                                  maxScale: 2,
                                  animationMaxScale: 2.5,
                                  speed: 1,
                                  inertialSpeed: 100.0,
                                  initialScale: 1.0,
                                  inPageView: true,
                                  initialAlignment: InitialAlignment.center,
                                );
                              },
                            );
                          case LoadState.failed:
                            return GestureDetector(
                              child: Image.asset(
                                ImageVariable.noimg,
                                package: 'chat_package',
                              ),
                            );
                          default:
                            return Image.asset(
                              ImageVariable.noimg,
                              package: 'chat_package',
                            );
                        }
                      },
                    );
                    return Padding(
                      padding: const EdgeInsets.all(2),
                      child: image,
                    );
                  },
                  controller: PageController(
                    initialPage: widget.currentIndex,
                  ),
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AvatarPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final shapeBounds = Rect.fromLTRB(0, 0, size.width, size.height);
    final paint = Paint()..color = BaseColor.accentsColor;

    final centerAvatar = Offset(shapeBounds.center.dx, shapeBounds.bottom);
    final avatarBounds =
        Rect.fromCircle(center: centerAvatar, radius: Get.width / 6);

    final backgroundPath = Path()
      ..moveTo(shapeBounds.left, shapeBounds.top)
      ..lineTo(shapeBounds.bottomLeft.dx, shapeBounds.bottomLeft.dy)
      ..arcTo(avatarBounds, -pi, pi, false) //5
      ..lineTo(shapeBounds.bottomRight.dx, shapeBounds.bottomRight.dy)
      ..lineTo(shapeBounds.topRight.dx, shapeBounds.topRight.dy)
      ..close();

    canvas.drawPath(backgroundPath, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
