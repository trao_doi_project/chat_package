import 'dart:async';
import 'dart:io';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_result.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class EditGroupController extends GetxController {
  RxList<DataUsers> listUserInGroup = <DataUsers>[].obs;
  RxList<String> listHinhNen = <String>[].obs;
  RxList<FileUpload> list5Image = <FileUpload>[].obs;
  Rx<String> groupName = ''.obs;
  Rx<String> groupIcon = ''.obs;
  Rx<bool> tatThongBao = false.obs;
  String? pathHinhNen;

  File? groupIconPick;
  bool isPickGroupIcon = true;

  late String tagCtrMessageDetail;

  StreamSubscription? _subDeleteUserInGroup;
  StreamSubscription? _subAddUserInGroup;

  late GroupInfo group;
  final TextEditingController groupNameGroupCtrl = TextEditingController();
  SlidingUpPanelController slidingUpPanelController = SlidingUpPanelController();
  final GalleryController galleryController = Get.find<GalleryController>();

  final List<BottomSheetItem> bottomItems = <BottomSheetItem>[
    BottomSheetItem(title: 'Máy ảnh', image: ImageVariable.cameraPick, id: 1),
    BottomSheetItem(title: 'Hình ảnh', image: ImageVariable.gallery, id: 2)
  ];

  Future<void> actionBottomSheet(int id) async{
    switch (id) {
      case 1:
        Get.back();
        Get.to(CameraScreen(
          speciesCamera: 1,
          disableVideoRecord: false,
          onResutl: (value) {
            if (value != null) {
              groupIconPick = value;
              updateGroupIcon(groupIconPick!);
            }
          },
        ));
        break;
      case 2:
        Get.back();
        await galleryController.getMedia(galleryType: GalleryType.image);
        slidingUpPanelController.anchor();
        break;
    }
  }

  Future<void> selectGalleryGroupIconAndUpload(List<AssetEntity> images) async {
    groupIconPick = await images.first.file;
    updateGroupIcon(groupIconPick!);
  }

  Future deleteGroup() async {
    await ApiRequest.instance.deleteGroup(group.groupID);
    Get.offAllNamed(GroupInfoUI.ROUTER_NAME);
  }

  Future deleteAllMessage() async {
    try {
      await ApiRequest.instance.deleteAllMessageByGroupId(group.groupID);
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<int> deleteHistoryChat() async {
    try {
      final Map<String, dynamic> param = {
        'userID' : userIDChat,
        'groupID' : group.groupID,
        'isGroup': group.isGroup
      };
      final int result = await ApiRequest.instance.deleteHistoryChat(param);
      if(result > 0){
        await SyncChat.instance.deleteHistoryMessageByGroupID(group.groupID);
        Get.find<MessageDetailController>(tag: tagCtrMessageDetail).listChatMessage.clear();
        return 1;
      }
      return 0;
    } on Exception catch (e) {
      print(e);
      return 0;
    }
  }

  Future updateGroupIcon(File image) async {
    final List<FileResults>? imageUpload =
        await ApiRequest.instance.uploadFile(files: [image]);
    if (imageUpload != null && imageUpload.isNotEmpty) {
      group.groupIcon = imageUpload.first.linkFile!;
      await ApiRequest.instance.updateGroupIconAndGroupName(group);
      groupIcon.value = imageUpload.first.linkFile!;
    }
  }

  Future updateGroupName() async {
    groupName.value = groupNameGroupCtrl.text.trim();
    group.groupName = groupNameGroupCtrl.text.trim();
    await ApiRequest.instance.updateGroupIconAndGroupName(group);
    Get.back();
  }

  Future<void> updateHinhNenGroupWithImageGallery(
      List<AssetEntity> images) async {
    final File? fileImage = await images.first.file;
    if (fileImage != null) {
      final List<FileResults>? images =
          await ApiRequest.instance.uploadFile(files: [fileImage]);
      if (images != null && images.isNotEmpty) {
        await updateHinhNenGroup(images.first.linkFile!);
        pathHinhNen = images.first.linkFile!;
        group.hinhNen = images.first.linkFile!;
        slidingUpPanelController.hide();
        LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật hình nền cuộc trò chuyện thành công!');
      }
    } else {
      slidingUpPanelController.hide();
      LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật không thành công!');
    }
  }

  Future<void> updateHinhNenGroupWithImageCamera(File image) async {
    final List<FileResults>? images =
        await ApiRequest.instance.uploadFile(files: [image]);
    if (images != null && images.isNotEmpty) {
      await updateHinhNenGroup(images.first.linkFile!);
      pathHinhNen = images.first.linkFile!;
      group.hinhNen = images.first.linkFile!;
      LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật hình nền cuộc trò chuyện thành công!');
    } else
      LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật không thành công!');
  }

  Future updateHinhNenGroup(String? urlImage,
      {bool selectHinhNen = false}) async {
    try {
      group.hinhNen = urlImage;
      await ApiRequest.instance.updateHinhNenGroup(group);
      if (selectHinhNen) Get.back();
      LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật hình nền cuộc trò chuyện thành công!');
    } on Exception catch (e) {
      LoadingWidget.instance.snackBarThongBao('Cập nhật', 'Cập nhật không thành công!');
      print(e);
    }
  }

  void get5Image() {
    SyncChat.instance.getImageByGroupIDWithNumber(group.groupID, 5).then((value) {
      if (value != null) list5Image.value = value;
    });
  }

  void getListUserInGroup() {
    SyncChat.instance.getUserByGroupIDHasMe(group.groupID).then((value) {
      if (value != null) {
        listUserInGroup.value = value;
      }
    });
  }

  void getListHinhNen() {
    ApiRequest.instance.getListHinhNenChat().then((value) {
      if (value != null) listHinhNen.value = value;
    });
  }

  void checkOffTinNhan() {
    SyncChat.instance.checkOffTinNhan(userIDChat, group.groupID).then((value) {
      tatThongBao(value);
    });
  }

  Future tatTinNhan(bool isOff) async {
    isOff = await ApiRequest.instance.appTraoDoiV2TatThongBao({
      'userID': userIDChat,
      'deviceID': deviceIDChat,
      'groupID': group.groupID,
      'token': tokenChat,
      'isOff': !isOff
    });
    if (isOff != null) {
      tatThongBao(isOff);
      await SyncChat.instance
          .updateOffTinNhan(userIDChat, group.groupID, isOff);
    }
  }

  void listenSignalR(){
    _subAddUserInGroup ??= insertUserInGroupCtrl.stream.listen((groupEvent) {
      if(groupEvent?.groupID == group.groupID)
        getListUserInGroup();
    });

    _subDeleteUserInGroup ??= deleteUserInGroupCtrl.stream.listen((userEvent) {
      if(userEvent?.groupID == group.groupID)
        getListUserInGroup();
    });
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    group = arg['groupInfo'];
    if (group != null) {
      groupName.value = group.groupName ?? '';
      groupIcon.value = group.groupIcon ?? '';
      groupNameGroupCtrl.text = group.groupName ?? '';
    }
    tagCtrMessageDetail = arg['tagCtrMessageDetail'];

    listenSignalR();
    get5Image();
    getListUserInGroup();
    getListHinhNen();
    checkOffTinNhan();

    super.onInit();
  }

  @override
  void onClose() {
    groupNameGroupCtrl.dispose();
    _subDeleteUserInGroup?.cancel();
    _subAddUserInGroup?.cancel();
    super.onClose();
  }
}
