import 'dart:async';

import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';
import 'package:collection/collection.dart';

class AddUserInGroupController extends GetxController {
  RxList<DataUsers> listUser = <DataUsers>[].obs;
  RxList<DataUsers> listDataUserAdd = <DataUsers>[].obs;

  late GroupInfo group;
  Timer? delay;
  List<DataUsers> _listAllDataUser = <DataUsers>[];
  List<DataUsers> _listUserInGroup = <DataUsers>[];
  final TextEditingController searchCtrl = TextEditingController();

  void getListDataUser(int groupID) {
    SyncChat.instance.getDataUser().then((values) async {
      if (values != null) {
        values.removeWhere((a) => a.userID == userIDChat);
        _listUserInGroup =
            await SyncChat.instance.getUserByGroupID(groupID, userIDChat);
        if (_listUserInGroup.isNotEmpty) {
          for (final DataUsers user in _listUserInGroup) {
            values
                .firstWhereOrNull((element) => element.userID == user.userID)
                ?.isThamGiaGroup = true;
          }
        }
        _listAllDataUser = values;
        listUser.value = values;
      }
    });
  }

  void delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (delay != null) delay!.cancel();
    delay = Timer(duration, callback);
  }

  void getListUserChatBySearch(String keySearch) {
    if (keySearch.isNotEmpty) {
      final String keySearchKoDau = keySearch.xoaDau().toLowerCase();
      final List<DataUsers> items = _listAllDataUser.where((element) {
        final String el = element.fullName!.xoaDau().toLowerCase();
        if (el.contains(keySearchKoDau)) return true;
        return false;
      }).toList();
      listUser.value = items;
    } else
      listUser.value = _listAllDataUser;
  }

  Future addUserInGroup() async {
    await ApiRequest.instance.insertUserInGroup(group, listDataUserAdd);
    Get.back(result: countUserInGroup());
  }

  void selectUser(DataUsers user) {
    if (listDataUserAdd.contains(user))
      listDataUserAdd.remove(user);
    else
      listDataUserAdd.add(user);
  }

  int countUserInGroup() {
    return _listUserInGroup.length + listDataUserAdd.length + 1;
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    group = arg['groupInfo'];
    if (group != null) getListDataUser(group.groupID);
    super.onInit();
  }

  @override
  void onClose() {
    searchCtrl.dispose();
    delay?.cancel();
    super.onClose();
  }
}
