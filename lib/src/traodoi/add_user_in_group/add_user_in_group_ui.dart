import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/traodoi/add_user_in_group/add_user_in_group_controller.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:get/get.dart';

import '../../../chat_package.dart';

class AddUserInGroupUI extends StatelessWidget {
  static const ROUTER_NAME = '/AddUserInGroupUI';

  final AddUserInGroupController _addUserInGroupController =
      Get.put(AddUserInGroupController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: BaseColor.accentsColor,
        flexibleSpace: SafeArea(
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.only(right: 16),
            child: Row(
              children: <Widget>[
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: BaseColor.primaryColor,
                  ),
                ),
                const SizedBox(
                  width: 2,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Text(
                        'Thêm thành viên',
                        style: TextStyle(
                            fontSize: 14,
                            // fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                            color: BaseColor.primaryColor),
                      ),
                      const SizedBox(
                        height: 2,
                      ),
                      Text(
                        _addUserInGroupController.group.groupName ?? '',
                        style: const TextStyle(
                          color: BaseColor.colorStaus,
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                textInputAction: TextInputAction.search,
                controller: _addUserInGroupController.searchCtrl,
                onChanged: (value) async => _addUserInGroupController
                    .delaySearch(() => _addUserInGroupController
                        .getListUserChatBySearch(value)),
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.search,
                      size: Get.width * 0.08,
                      color: Colors.grey,
                    ),
                    border: InputBorder.none,
                    hintText: 'Tìm kiếm'),
              ),
            ),
          ),
          Obx(() {
            if (_addUserInGroupController.listDataUserAdd.isNotEmpty)
              return SizedBox(
                height: Get.height * 0.075,
                width: Get.width,
                child: ScrollConfiguration(
                  behavior: DisableGlowListViewWidget(),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: _addUserInGroupController.listDataUserAdd.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Stack(
                          children: [
                            SizedBox(
                              height: Get.height * 0.06,
                              width: Get.height * 0.06,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: ImageNetworkWidget(
                                    packageName: LocalVariable.namePackage,
                                    urlImageNetwork: _addUserInGroupController
                                        .listDataUserAdd[index].avatar,
                                    imageAssetDefault: ImageVariable.person,
                                  )),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: GestureDetector(
                                onTap: () {
                                  _addUserInGroupController.listDataUserAdd
                                      .removeAt(index);
                                },
                                child: const Icon(
                                  Icons.cancel,
                                  color: Colors.black,
                                  size: 16,
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ),
              );
            else
              return const SizedBox.shrink();
          }),
          Expanded(
            child: ScrollConfiguration(
                behavior: DisableGlowListViewWidget(),
                child: Obx(() {
                  if (_addUserInGroupController.listUser.isNotEmpty)
                    return ListView.builder(
                        itemCount: _addUserInGroupController.listUser.length,
                        itemBuilder: (context, index) {
                          final DataUsers user =
                              _addUserInGroupController.listUser[index];
                          return GestureDetector(
                            onTap: () {
                              if (!user.isThamGiaGroup)
                                _addUserInGroupController.selectUser(user);
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                              child: Obx(() => Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: (_addUserInGroupController
                                                .listDataUserAdd
                                                .contains(user))
                                            ? BaseColor.accentsColor
                                            : Colors.transparent,
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8.0, 4, 8, 4),
                                          child: Stack(
                                            children: [
                                              SizedBox(
                                                height: Get.height * 0.06,
                                                width: Get.height * 0.06,
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            50),
                                                    child: ImageNetworkWidget(
                                                      packageName: LocalVariable.namePackage,
                                                      urlImageNetwork:
                                                          baseUrlUpload +
                                                              (user.avatar ??
                                                                  ''),
                                                      imageAssetDefault:
                                                          ImageVariable.person,
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          constraints: BoxConstraints(
                                              maxWidth: Get.width * 0.7),
                                          child: Text(
                                            user.fullName ?? '',
                                            maxLines: 3,
                                            style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              // fontFamily: 'Medium',
                                            ),
                                          ),
                                        ),
                                        const Spacer(),
                                        if (user.isThamGiaGroup)
                                          const Padding(
                                            padding: EdgeInsets.only(right: 8),
                                            child: Icon(
                                              Icons.check,
                                            ),
                                          )
                                        else
                                          _addUserInGroupController
                                                  .listDataUserAdd
                                                  .contains(user)
                                              ? const Padding(
                                                  padding:
                                                      EdgeInsets.only(right: 8),
                                                  child: Icon(
                                                    Icons.check,
                                                    color:
                                                        BaseColor.accentsColor,
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                      ],
                                    ),
                                  )),
                            ),
                          );
                        });
                  return const SizedBox.shrink();
                })),
          ),
          Container(
            height: Get.height * 0.07,
            decoration: const BoxDecoration(
              color: Color(0xfff2f2f2),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: const Text(
                    'Hủy',
                    style: TextStyle(
                      color: BaseColor.primaryColor,
                      fontSize: 16,
                      // fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if (_addUserInGroupController.listDataUserAdd.isNotEmpty) {
                      _addUserInGroupController.addUserInGroup();
                    } else
                      LoadingWidget.instance.snackBarThongBao('Thông báo', 'Vui lòng chọn thành viên');
                  },
                  child: const Text(
                    'Thêm',
                    style: TextStyle(
                      color: BaseColor.primaryColor,
                      fontSize: 16,
                      // fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
