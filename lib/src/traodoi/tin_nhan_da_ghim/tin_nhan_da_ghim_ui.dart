import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/src/traodoi/tin_nhan_da_ghim/tin_nhan_da_ghim_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:chat_package/src/model/trao_doi/ghim_mess_model.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/custom_widget_chat.dart';

class TinNhanDaGhimUI extends StatelessWidget {
  static const ROUTER_NAME = '/TinNhanDaGhimUI';

  final TinNhanDaGhimController _tinNhanDaGhimController = Get.put(TinNhanDaGhimController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: const Text('Tin nhắn đã ghim'),
        backgroundColor: BaseColor.primaryColor,
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.grey.shade100,
        ),
        child: Obx((){
          if(_tinNhanDaGhimController.listMessGhim.isNotEmpty)
            return ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: ListView.builder(
                  itemCount: _tinNhanDaGhimController.listMessGhim.length,
                  itemBuilder: (context, index) {
                    final GhimMessModel messageGhim = _tinNhanDaGhimController.listMessGhim[index];
                    final ReplyModel replyModel = ReplyModel.fromJson(messageGhim.ghimMessage!);
                    return GestureDetector(
                      onTap: ()=> _tinNhanDaGhimController.clickMessOrGroup(messageID: replyModel.messID),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        shadowColor: Colors.black,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: ShowGhimMess(
                            ghimMessData: replyModel,
                            optionMessaGhim: (value) {},
                          ),
                        ),
                      ),
                    );
                  }),
            );
          return const SizedBox.shrink();
        })
      ),
    );
  }
}

