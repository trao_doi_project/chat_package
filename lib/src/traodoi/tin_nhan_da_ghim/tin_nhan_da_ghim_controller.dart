import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/ghim_mess_model.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

import '../../../chat_package.dart';

class TinNhanDaGhimController extends GetxController {
  late GroupInfo group;

  RxList<GhimMessModel> listMessGhim = <GhimMessModel>[].obs;

  void getListMessageGhim(GroupInfo groupInfo) {
    ApiRequest.instance
        .getAllMessageGhimByGroupID(groupInfo.groupID)
        .then((value) {
      if (value != null && value.isNotEmpty) listMessGhim.value = value;
    });
  }

  Future clickMessOrGroup({int? messageID}) async {
    final Map<String, dynamic> _paramRoot = <String, dynamic>{};

    final List<DataUsers> listItemUser =
        await SyncChat.instance.getUserByGroupID(group.groupID, userIDChat);
    _paramRoot.putIfAbsent('itemUser', () => listItemUser);
    _paramRoot.putIfAbsent('groupInfo', () => group);
    _paramRoot.putIfAbsent('messageID', () => messageID);

    Get.back();
    Get.back(
        result: {'pageRouter': TinNhanDaGhimUI.ROUTER_NAME, 'data': messageID});

    // await Get.offNamedUntil(MessageDetailUI.ROUTER_NAME, (router) => true,
    //     arguments: _paramRoot);
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    group = arg['groupInfo'];
    getListMessageGhim(group);
    super.onInit();
  }
}
