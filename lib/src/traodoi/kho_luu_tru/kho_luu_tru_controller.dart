import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class KhoLuuTruController extends GetxController {
  RxMap<int, List<FileUpload>> khoLuuTru = <int, List<FileUpload>>{}.obs;
  Rx<int> isClick = 1.obs;
  Rx<String> avatar = ''.obs;
  bool isLoadMore = true;
  List<FileUpload> files = <FileUpload>[];

  List<FileUpload> listFileSearchFirstLoad = <FileUpload>[];
  bool isSearch = false;
  String keySearch = '';

  late GroupInfo group;
  int pageIndex = 0;
  int fileType = FileType.Image.index;
  final ScrollController listScrollController = ScrollController();
  final List<KhoLuuTru> listkhoLuuTru = <KhoLuuTru>[
    KhoLuuTru(1, 'Media', 0),
    KhoLuuTru(2, 'File', FileType.File.index),
    KhoLuuTru(3, 'Link', FileType.Link.index),
    KhoLuuTru(4, 'Audio', FileType.Audio.index)
  ];

  void checkClickKhoLuuTru(int id) {
    isClick.value = id;
  }

  void getKhoLuuTru(int groupID, int typeKhoLuuTru, int pageIndex) {
    isSearch = false;
    SyncChat.instance
        .getKhoLuuTruByGroupIDAndType(groupID, typeKhoLuuTru, pageIndex)
        .then((value) {
      if (value != null) {
        files = value;
        isLoadMore = true;
        khoLuuTru.value = {typeKhoLuuTru: files};
      } else {
        khoLuuTru.value = {typeKhoLuuTru: []};
        isLoadMore = false;
      }
    });
  }

  void getAvatar(int userID) {
    SyncChat.instance.getDataUserByID(userID).then((value) {
      if (value != null && value.avatar != null) avatar.value = value.avatar!;
    });
  }

  void loadMore(int groupID, int typeKhoLuuTru, int pageIndex) {
    if (files.isNotEmpty) {
      SyncChat.instance
          .getKhoLuuTruByGroupIDAndType(groupID, typeKhoLuuTru, pageIndex)
          .then((value) {
        if (value != null && value.isNotEmpty) {
          files.addAll(value);
          khoLuuTru.value = {typeKhoLuuTru: files};
        } else
          isLoadMore = false;
      });
    }
  }

  ///-------------------------------Begin search file in Group--------------------------------------
  Future<void> searchFile() async {
    isSearch = true;
    final List<FileUpload>? listFileTemp = await SyncChat.instance
        .searchFileByNameInGroup(
            groupId: group.groupID,
            limit: 15,
            pageIndex: 0,
            keySearch: keySearch);
    if (listFileTemp != null) {
      isLoadMore = true;
      listFileSearchFirstLoad = listFileTemp;
      khoLuuTru.value = {2: listFileSearchFirstLoad};
    } else
      isLoadMore = false;
  }

  Future<void> loadMoreSearchFile(
      {required String keySearch, required int pageIndex}) async {
    isSearch = true;
    final List<FileUpload>? listFileTemp = await SyncChat.instance
        .searchFileByNameInGroup(
            groupId: group.groupID,
            limit: 15,
            pageIndex: pageIndex,
            keySearch: keySearch);
    if (listFileTemp != null) {
      listFileSearchFirstLoad.addAll(listFileTemp);
      khoLuuTru.value = {2: listFileSearchFirstLoad};
    } else
      isLoadMore = false;
  }

  void resetDataSearch(){
    khoLuuTru.value = {2: files};
  }
  ///-------------------------------End search file in Group--------------------------------------

  void _scrollListener() {
    if (!isLoadMore) return;
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      pageIndex++;
      if (isSearch)
        loadMoreSearchFile(keySearch: keySearch, pageIndex: pageIndex);
      else
        loadMore(group.groupID, fileType, pageIndex);
    }
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    group = arg['groupInfo'];

    getKhoLuuTru(group.groupID, fileType, pageIndex);
    listScrollController.addListener(_scrollListener);

    super.onInit();
  }

  @override
  void onClose() {
    listScrollController.dispose();
    super.onClose();
  }
}

class KhoLuuTru {
  int id;
  String title;
  int typeKhoLuuTru;
  KhoLuuTru(this.id, this.title, this.typeKhoLuuTru);
}
