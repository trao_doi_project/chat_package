import 'dart:ui';

import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

import '../../../../chat_package.dart';

class FullPhotoUI extends StatefulWidget {
  final List<FileUpload> images;
  final int currentIndex;
  const FullPhotoUI({required this.images, required this.currentIndex});
  double get _sigmaX => 5.0;
  double get _sigmaY => 5.0;

  @override
  _FullPhotoUIState createState() => _FullPhotoUIState();
}

class _FullPhotoUIState extends State<FullPhotoUI> {
  late int tempCurrentIndex;
  @override
  void initState() {
    tempCurrentIndex = widget.currentIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(widget.images.first.fileName ?? ''),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.download_rounded,
              size: 28,
              color: Colors.white,
            ),
            onPressed: () async {
              await ApiRequest.instance
                  .downloadMediaInChat(widget.images[tempCurrentIndex]);
            },
          ).paddingOnly(right: 10)
        ],
      ),
      body: Stack(
        children: [
          Center(
            child: BackdropFilter(
              filter: ImageFilter.blur(
                  sigmaX: widget._sigmaX, sigmaY: widget._sigmaY),
              child: ScrollConfiguration(
                behavior: DisableGlowListViewWidget(),
                child: ExtendedImageGesturePageView.builder(
                  itemCount: widget.images.length,
                  itemBuilder: (BuildContext context, int index) {
                    final item =
                        baseUrlApiChat + (widget.images[index].fileUrl ?? '');
                    final Widget image = ExtendedImage.network(
                      item,
                      mode: ExtendedImageMode.gesture,
                      fit: BoxFit.scaleDown,
                      initGestureConfigHandler: (state) {
                        return GestureConfig(
                          minScale: 1,
                          animationMinScale: 0.5,
                          maxScale: 2,
                          animationMaxScale: 2.5,
                          speed: 1,
                          inertialSpeed: 100.0,
                          initialScale: 1.0,
                          inPageView: true,
                          initialAlignment: InitialAlignment.center,
                        );
                      },
                    );
                    return Padding(
                      padding: const EdgeInsets.all(2),
                      child: image,
                    );
                  },
                  controller: PageController(
                    initialPage: widget.currentIndex,
                  ),
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
