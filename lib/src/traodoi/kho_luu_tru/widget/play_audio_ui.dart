import 'dart:async';

import 'package:chat_package/base/image_variable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart' as cache;
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:intl/intl.dart';
import 'package:just_audio/just_audio.dart';

import '../../../../chat_package.dart';

class PlayAudioUI extends StatefulWidget {
  final FileUpload audio;

  const PlayAudioUI({required this.audio});

  @override
  State<StatefulWidget> createState() {
    return _MessAudioWidgetStateV2();
  }
}

class _MessAudioWidgetStateV2 extends State<PlayAudioUI> {
  late AudioPlayer _player;
  cache.FileInfo? file;
  Duration? _duration;
  String _durationText = '00:00';

  final StreamController<bool> _isPlaying = StreamController<bool>.broadcast();

  @override
  void initState() {
    _player = AudioPlayer();
    _init();
    super.initState();
  }

  @override
  void dispose() {
    _player.dispose();
    _isPlaying.close();
    super.dispose();
  }

  Future<void> _init() async {
    if (widget.audio.messageID > 0) {
      file = await CacheManagerCustom.instance
          .getFileInCache(baseUrlApiChat + widget.audio.fileUrl!);
      if (file == null)
        await _player.setUrl(baseUrlApiChat + widget.audio.fileUrl!);
      else
        await _player.setFilePath(file!.file.path);
    } else
      await _player.setFilePath(widget.audio.fileUrl!);

    _duration = await _player.load();
    _durationText = _getTextDuration(_duration);
    setState(() {});
  }

  String _getTextDuration(Duration? duration) {
    if (duration != null) {
      final date = DateTime.fromMillisecondsSinceEpoch(duration.inMilliseconds,
          isUtc: true);
      return DateFormat('mm:ss').format(date);
    }
    return _durationText;
  }

  Future<void> _play() async {
    _isPlaying.sink.add(true);
    await _player.play();
    await _player.stop();
    _isPlaying.sink.add(false);
  }

  Future<void> _stop() async {
    _isPlaying.sink.add(false);
    await _player.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Image.asset(
          ImageVariable.audio,
          width: 32,
          fit: BoxFit.cover,
          package: 'chat_package',
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              (widget.audio.createDate != null)
                  ? DateFormat('hh:mm yyyy-MM-dd')
                      .format(widget.audio.createDate!)
                  : '',
              style: const TextStyle(fontSize: 16),
            ),
            StreamBuilder<bool>(
                initialData: false,
                stream: _isPlaying.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData && !snapshot.data!)
                    return Text('00:00/' + _durationText);
                  return StreamBuilder<Duration>(
                      initialData: _duration,
                      stream: _player.positionStream,
                      builder: (context, snapshot) {
                        return RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: _getTextDuration(
                                      snapshot.data ?? _duration),
                                  style: const TextStyle(color: Colors.red)),
                              TextSpan(
                                  text: '/' + _durationText,
                                  style: const TextStyle(color: Colors.black)),
                            ],
                          ),
                        );
                      });
                }),
          ],
        ),
        GestureDetector(
          onTap: () async => _player.playing ? await _stop() : await _play(),
          child: StreamBuilder<bool>(
              initialData: false,
              stream: _isPlaying.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData && !snapshot.data!)
                  return const Icon(
                    Icons.play_arrow,
                    color: Colors.cyan,
                    size: 48,
                  );
                return const Icon(
                  Icons.stop,
                  color: Colors.red,
                  size: 48,
                );
              }),
        ),
      ],
    );
  }
}
