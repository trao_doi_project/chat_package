import 'dart:async';
import 'dart:ui';
import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/traodoi/kho_luu_tru/kho_luu_tru_controller.dart';
import 'package:chat_package/src/traodoi/kho_luu_tru/widget/full_photo_ui.dart';
import 'package:chat_package/src/traodoi/kho_luu_tru/widget/play_audio_ui.dart';
import 'package:flutter_core_getx_dev/app/services/cache_manager_custom.dart';
import 'package:flutter_core_getx_dev/app/utils/tools.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/download_file_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/traodoi/message_detail/custom_widget/video_play.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart' as cache;
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path/path.dart' as path;

import '../../../chat_package.dart';

class KhoLuuTruUI extends StatefulWidget {
  static const String ROUTER_NAME = '/KhoLuuTrUI';

  @override
  _KhoLuuTruUIState createState() => _KhoLuuTruUIState();
}

class _KhoLuuTruUIState extends State<KhoLuuTruUI>
    with SingleTickerProviderStateMixin {
  final KhoLuuTruController _khoLuuTruController =
      Get.put(KhoLuuTruController());
  final TextEditingController _textEditingSearchCtr = TextEditingController();

  Timer? _delay;

  void _delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (_delay != null) _delay!.cancel();
    _delay = Timer(duration, callback);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _delay?.cancel();
    _textEditingSearchCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: const Text('Kho lưu trữ'),
        backgroundColor: BaseColor.primaryColor,
      ),
      body: Container(
        color: Colors.white70,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: GridView.builder(
                  itemCount: _khoLuuTruController.listkhoLuuTru.length,
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 2,
                    crossAxisSpacing: 10,
                    crossAxisCount: 4,
                  ),
                  itemBuilder: (context, index) {
                    return Obx(() {
                      final KhoLuuTru klt =
                          _khoLuuTruController.listkhoLuuTru[index];
                      return MaterialButton(
                        color: (_khoLuuTruController.isClick.value == klt.id)
                            ? BaseColor.primaryColor
                            : null,
                        textColor:
                            (_khoLuuTruController.isClick.value == klt.id)
                                ? Colors.white
                                : Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: const EdgeInsets.all(8.0),
                        onPressed: () {
                          _textEditingSearchCtr.clear();
                          _khoLuuTruController.pageIndex = 0;
                          _khoLuuTruController.fileType = klt.typeKhoLuuTru;
                          _khoLuuTruController.isClick.value = klt.id;
                          _khoLuuTruController.getKhoLuuTru(
                              _khoLuuTruController.group.groupID,
                              klt.typeKhoLuuTru,
                              _khoLuuTruController.pageIndex);
                        },
                        child: Text(
                          klt.title,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      );
                    });
                  }),
            ),
            Expanded(child: Obx(() {
              if (_khoLuuTruController.khoLuuTru.values.isNotEmpty &&
                  _khoLuuTruController.khoLuuTru.values.first.isNotEmpty) {
                final int typeKhoLuuTru =
                    _khoLuuTruController.khoLuuTru.keys.first;
                final List<FileUpload> files =
                    _khoLuuTruController.khoLuuTru.values.first;
                switch (typeKhoLuuTru) {
                  case 0:
                  case 1:
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StaggeredGridView.countBuilder(
                        staggeredTileBuilder: (int index) =>
                            const StaggeredTile.fit(1),
                        controller: _khoLuuTruController.listScrollController,
                        crossAxisCount: 3,
                        itemCount: files.length,
                        mainAxisSpacing: 4,
                        crossAxisSpacing: 4.0,
                        itemBuilder: (BuildContext context, int index) => Card(
                          margin: EdgeInsets.zero,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Container(
                            margin: const EdgeInsets.all(4),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: (files[index].fileType == FileType.Video)
                                  ? GestureDetector(
                                      onTap: () async {
                                        final String _tempUrl =
                                            '$baseUrlApiChat/${files[index].fileUrl}';
                                        final cache.FileInfo? _tempFile =
                                            await CacheManagerCustom.instance
                                                .getFileInCache(
                                                    files[index].fileUrl!);
                                        if (_tempFile != null)
                                          Get.to(ChewieDemo(
                                            _tempFile.file.path,
                                            true,
                                            isDownLoad: true,
                                            file: files[index],
                                          ));
                                        else
                                          Get.to(ChewieDemo(
                                            _tempUrl,
                                            false,
                                            isDownLoad: true,
                                            file: files[index],
                                          ));
                                      },
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          ImageNetworkWidget(
                                              packageName: LocalVariable.namePackage,
                                              urlImageNetwork: baseUrlApiChat +
                                                  (files[index].thumbnail ??
                                                      ''),
                                              imageAssetDefault:
                                                  ImageVariable.noimg),
                                          Image.asset(
                                            ImageVariable.playVideo,
                                            width: 50,
                                            color: Colors.white70,
                                            package: 'chat_package',
                                          ),
                                        ],
                                      ),
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        Get.dialog(FullPhotoUI(
                                          images: [files[index]],
                                          currentIndex: 0,
                                        ));
                                      },
                                      child: ImageNetworkWidget(
                                        packageName: LocalVariable.namePackage,
                                        urlImageNetwork: baseUrlApiChat +
                                            (files[index].fileUrl ?? ''),
                                        imageAssetDefault: ImageVariable.noimg,
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ),
                    );
                  case 2:
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: Get.width,
                            height: 40,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38),
                                borderRadius: BorderRadius.circular(10)),
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              children: [
                                Expanded(
                                  child: TextField(
                                    controller: _textEditingSearchCtr,
                                    style:
                                        const TextStyle(color: Colors.black87),
                                    onChanged: (value) async =>
                                        _delaySearch(() {
                                      _khoLuuTruController.keySearch = value;
                                      if (value.isEmpty)
                                        _khoLuuTruController.resetDataSearch();
                                      else {
                                        _khoLuuTruController.searchFile();
                                      }
                                    }),
                                    cursorColor: Colors.black87,
                                    decoration: const InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Tìm kiếm',
                                        hintStyle:
                                            TextStyle(color: Colors.black38)),
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                GestureDetector(
                                    onTap: () {
                                      _textEditingSearchCtr.clear();
                                      _khoLuuTruController.keySearch = '';
                                      _khoLuuTruController.resetDataSearch();
                                    },
                                    child: const Icon(
                                      Icons.cancel,
                                      color: Colors.black87,
                                    )),
                                const SizedBox(
                                  width: 5,
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                              physics: const ClampingScrollPhysics(),
                              controller:
                                  _khoLuuTruController.listScrollController,
                              itemCount: files.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      border: Border.all(
                                          color: BaseColor.accentsColor)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          ImageVariable.fileExtensionIcon(
                                              path.extension(
                                                  files[index].fileName!)),
                                          height: 35,
                                          width: 35,
                                          package: 'chat_package',
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 5, right: 5),
                                            child: Column(
                                              children: [
                                                richTextSearch(
                                                    files[index].fileName ?? '',
                                                    _khoLuuTruController
                                                        .keySearch,
                                                    styleRichText:
                                                        const TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 16)),
                                                Text(
                                                  DateFormat(
                                                          'HH:mm, dd/MM/yyyy')
                                                      .format(files[index]
                                                              .createDate ??
                                                          DateTime.now()),
                                                  style: const TextStyle(
                                                      color: Colors.black54,
                                                      fontSize: 12),
                                                ),
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      path
                                                          .extension(
                                                              files[index]
                                                                  .fileName!)
                                                          .replaceAll('.', '')
                                                          .toUpperCase(),
                                                      style: const TextStyle(
                                                          color: Colors.black38,
                                                          fontSize: 12),
                                                    ),
                                                    const SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      files[index].fileSizeStr,
                                                      style: const TextStyle(
                                                          color: Colors.black38,
                                                          fontSize: 10),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                            ),
                                          ),
                                        ),
                                        DownLoadFileWidget(
                                          buttonSize: 30,
                                          buttonColor: BaseColor.primaryColor,
                                          iconSize: 20,
                                          fileName: path
                                              .basename(files[index].fileUrl!),
                                          linkFile:
                                              '$baseUrlApiChat${files[index].fileUrl!}',
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                    );
                  case 3:
                    return ScrollConfiguration(
                      behavior: DisableGlowListViewWidget(),
                      child: ListView.builder(
                          itemCount: files.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () async {
                                launch(files[index].fileUrl!,
                                    universalLinksOnly: true);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                        height: Get.width * 0.2,
                                        width: Get.width * 0.2,
                                        child: ImageNetworkWidget(
                                          packageName: LocalVariable.namePackage,
                                          urlImageNetwork:
                                              files[index].thumbnail ?? '',
                                          imageAssetDefault:
                                              ImageVariable.noimg,
                                        )),
                                    Flexible(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              files[index].fileUrl ?? '',
                                              style: const TextStyle(
                                                  decoration:
                                                      TextDecoration.underline,
                                                  color: Colors.blue),
                                            ),
                                            Text(
                                              files[index].fileName ?? '',
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                    );
                  case 5:
                    return ScrollConfiguration(
                      behavior: DisableGlowListViewWidget(),
                      child: ListView.builder(
                          itemCount: files.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: PlayAudioUI(audio: files[index]),
                            );
                          }),
                    );
                  default:
                    return const SizedBox.shrink();
                }
              } else
                return const SizedBox.shrink();
            }))
          ],
        ),
      ),
    );
  }
}
