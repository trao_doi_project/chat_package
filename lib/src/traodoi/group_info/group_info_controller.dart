import 'dart:async';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class GroupInfoController extends GetxController {
  RxList<DataUsers> listUserOnline = <DataUsers>[].obs;
  RxList<GroupInfo> listGroup = <GroupInfo>[].obs;

  final ScrollController scrollListGroupController = ScrollController();

  StreamSubscription? _subHasNewMess;
  StreamSubscription? _subCreateGroup;
  StreamSubscription? _subAddUserInGroup;
  StreamSubscription? _subUpdateGroupIconAndGroupName;
  StreamSubscription? _subDeleteGroup;
  StreamSubscription? _subDeleteUserInGroup;
  StreamSubscription? _subDeleteHistoryChat;
  StreamSubscription? _subThuHoiLastMessage;
  StreamSubscription? _subDeleteLastMessage;

  void getListUserOnline() {
    SignalR.instance.connection.send(
        methodName: LocalVariable.getUsersOnlineKey,
        args: [SignalR.instance.connection.connectionId]);

    SignalR.instance.connection.on(LocalVariable.getUsersOnlineKey,
        (arguments) {
      final List data = arguments![0] as List;
      final List<DataUsers> users =
          data.map((e) => DataUsers.fromJson(e)).toList();
      users.removeWhere((element) => element.userID == userIDChat);
      listUserOnline.value = users;
    });
  }

  Future<void> getListGroup() async {
    final List<GroupInfo> listGroupTemp =
        await SyncChat.instance.getDataGroupInfo();
    if (listGroupTemp.isNotEmpty) {
      for (final group in listGroupTemp)
        await setDataForGroup2User(group, userIDChat);
      listGroup.value = listGroupTemp;
    }
  }

  void listenSignR() {
    _subHasNewMess ??= chatMessageCtrl.stream.listen((_) {
      getListGroup();
    });

    _subCreateGroup ??= createGroupInfoCtrl.stream.listen((_) {
      getListGroup();
    });
    _subAddUserInGroup ??= insertUserInGroupCtrl.stream.listen((_) {
      getListGroup();
    });

    _subUpdateGroupIconAndGroupName ??=
        updateGroupIconAndGroupNameCtrl.stream.listen((_) {
      getListGroup();
    });
    _subDeleteGroup ??= deleteGroupCtrl.stream.listen((_) {
      getListGroup();
    });

    _subDeleteUserInGroup ??= deleteUserInGroupCtrl.stream.listen((event) {
      if (event?.userID == userIDChat) getListGroup();
    });

    _subDeleteHistoryChat ??= deleteHistoryMessageCtrl.stream.listen((event) {
      getListGroup();
    });

    _subThuHoiLastMessage ??=
        thuHoiMessageByMessageIDCtrl.stream.listen((event) {
      if (event?['newMessageID'] > 0) getListGroup();
    });

    _subDeleteLastMessage ??=
        deleteMessageByMessageIDCtrl.stream.listen((event) {
      if (event?['newMessageID'] > 0) getListGroup();
    });
  }

  Future checkAndGoChat(
      {List<DataUsers>? listItemUser,
      GroupInfo? groupInfo,
      bool isSearch = false,
      int? messageID,
      bool isOff = false,
      bool inXemThanhVienPage = false}) async {
    final Map<String, dynamic> _paramRoot = <String, dynamic>{};

    // click lich su
    if (listItemUser == null) {
      listItemUser = await SyncChat.instance
          .getUserByGroupID(groupInfo!.groupID, userIDChat);
      _paramRoot.putIfAbsent('itemUser', () => listItemUser);
      _paramRoot.putIfAbsent('groupInfo', () => groupInfo);
      if (isSearch) _paramRoot.putIfAbsent('messageID', () => messageID);
    } else {
      // click user online
      _paramRoot.putIfAbsent('itemUser', () => listItemUser);
      groupInfo =
          await SyncChat.instance.checkUserInGroup(listItemUser.first.userID);
      if (groupInfo != null) {
        _paramRoot.putIfAbsent('groupInfo', () => groupInfo);
      }
    }

    if (isOff) {
      await Get.offAndToNamed(MessageDetailUI.ROUTER_NAME,
          arguments: _paramRoot);
    } else {
      await Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: _paramRoot);
    }

    // update groupInfo
    getListGroup();
  }

  Future<void> setDataForGroup2User(GroupInfo group, int userId) async {
    if (!group.isGroup) {
      final List<DataUsers> listUserTemp =
          await SyncChat.instance.getUserByGroupID(group.groupID, userId);
      if (listUserTemp.isNotEmpty) {
        group.groupName = listUserTemp.first.fullName;
        group.groupIcon = listUserTemp.first.avatar;
      }
    }
  }

  @override
  void onInit() {
    // reset countNewMessInChatConfig
    // countNewMessInChatConfig = 0;
    // countNewMessInChatConfigCtr.sink.add(0);
    // isInChatInChatConfig = true;

    getListUserOnline();
    getListGroup();
    listenSignR();
    super.onInit();
  }

  @override
  void onClose() {
    _subHasNewMess?.cancel();
    _subCreateGroup?.cancel();
    _subAddUserInGroup?.cancel();
    _subUpdateGroupIconAndGroupName?.cancel();
    _subDeleteGroup?.cancel();
    _subDeleteUserInGroup?.cancel();
    _subDeleteHistoryChat?.cancel();
    _subThuHoiLastMessage?.cancel();
    _subDeleteLastMessage?.cancel();
    super.onClose();
  }
}
