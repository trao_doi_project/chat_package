import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/src/traodoi/group_info/group_info_controller.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/create_group/create_group_chat_ui.dart';
import 'package:chat_package/src/traodoi/search_chat/search_chat_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:chat_package/extention/custom_ex.dart';

import '../../../chat_package.dart';

class GroupInfoUI extends StatelessWidget {
  static const String ROUTER_NAME = '/GroupInfoUI';

  final GroupInfoController _groupInfoController =
      Get.put(GroupInfoController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: KeyboardDismisser(
        gestures: const [
          GestureType.onTap,
          GestureType.onPanUpdateDownDirection,
        ],
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBarWidget(
              backgroundColor: BaseColor.primaryColor,
              iconLeading: IconButton(
                icon: const Icon(Icons.add),
                iconSize: 36,
                onPressed: () {
                  Get.toNamed(CreateGroupChatUI.ROUTER_NAME);
                },
              ),
              title: const Text('Trao đổi'),
              iconButton1: IconButton(
                icon: const Icon(Icons.search),
                iconSize: 36,
                onPressed: () {
                  Get.toNamed(SearchChatUI.ROUTER_NAME);
                },
              ),
            ),
            body: Stack(
              children: [
                Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    ScrollConfiguration(
                      behavior: DisableGlowListViewWidget(),
                      child: Obx(() {
                        if (_groupInfoController.listUserOnline.isNotEmpty)
                          return SizedBox(
                            width: Get.width,
                            height: Get.height * 0.12,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    _groupInfoController.listUserOnline.length,
                                itemBuilder: (context, index) {
                                  final DataUsers _itemUser =
                                      _groupInfoController
                                          .listUserOnline[index];
                                  return GestureDetector(
                                    onTap: () => _groupInfoController
                                        .checkAndGoChat(
                                            listItemUser: [_itemUser]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 10, left: 10),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          AvatarCircleWidget(
                                            imageNetworkWidget:
                                                ImageNetworkWidget(
                                                    packageName: LocalVariable.namePackage,
                                                    urlImageNetwork:
                                                        baseUrlUpload +
                                                            (_itemUser.avatar ??
                                                                ''),
                                                    imageAssetDefault:
                                                        ImageVariable.person,
                                                    isOnTapReload: false),
                                            size: Get.height * 0.07,
                                            positionDotOnline: 0,
                                            sizeDotOnline: 10,
                                            online: true,
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            _itemUser.fullName!,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontFamily: 'Regular',
                                              color: Color(0xff9e9e9e),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          );
                        return const SizedBox.shrink();
                      }),
                    ),
                    Expanded(
                      child: Obx(() {
                        if (_groupInfoController.listGroup.isNotEmpty) {
                          return ScrollConfiguration(
                            behavior: DisableGlowListViewWidget(),
                            child: SingleChildScrollView(
                              child: Column(
                                children: _groupInfoController.listGroup
                                    .map((e) => _itemGroup(e))
                                    .toList(),
                              ),
                            ),
                          );
                        }
                        return const SizedBox.shrink();
                      }),
                    )
                  ],
                )
              ],
            )),
      ),
    );
  }

  Padding _itemGroup(GroupInfo groupInfo) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Row(
        children: <Widget>[
          AvatarCircleWidget(
              size: 50,
              imageNetworkWidget: ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork:
                      (groupInfo.isGroup ? baseUrlApiChat : baseUrlUpload) +
                          (groupInfo.groupIcon ?? ''),
                  imageAssetDefault: groupInfo.isGroup ? ImageVariable.groupAvatar : ImageVariable.person)),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: GestureDetector(
              onTap: () =>
                  _groupInfoController.checkAndGoChat(groupInfo: groupInfo),
              behavior: HitTestBehavior.translucent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    groupInfo.groupName ?? '',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontFamily: 'Medium',
                    ),
                  ),
                  if (groupInfo.lastMessage == null &&
                      !CheckMessType(groupInfo.lastMessageType!).check())
                    const SizedBox.shrink()
                  else
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            CheckMessType(groupInfo.lastMessageType!)
                                .text(groupInfo.lastMessage ?? ''),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: (groupInfo.countNewMessage > 0)
                                    ? Colors.black
                                    : const Color(0xff757575),
                                fontSize: 14,
                                fontWeight: (groupInfo.countNewMessage > 0)
                                    ? FontWeight.bold
                                    : FontWeight.normal),
                          ),
                        ),
                        const Spacer(),
                        if (groupInfo.countNewMessage > 0)
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: SizedBox(
                              height: 15,
                              width: 15,
                              child: DecoratedBox(
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle, color: Colors.red),
                                child: Center(
                                  child: Text(
                                      (groupInfo.countNewMessage > 99)
                                          ? '99+'
                                          : '${groupInfo.countNewMessage}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 8,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ),
                            ),
                          )
                      ],
                    ),
                  Text(
                    (groupInfo.lastMessageDate != null)
                        ? DateFormat('HH:mm dd/MM', 'vi')
                            .format(groupInfo.lastMessageDate ?? DateTime.now())
                        : '--không có tin nhắn--',
                    style: const TextStyle(
                      color: Color(0xff757575),
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return true;
  }
}
