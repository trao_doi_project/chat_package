import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:metadata_fetch/metadata_fetch.dart';
import 'package:path/path.dart' as path;
import 'package:youtube_explode_dart/youtube_explode_dart.dart';
import 'package:collection/collection.dart';

import '../../../chat_package.dart';

class ListGroupSendShareCtrl extends GetxController {
  RxList<GroupInfo> listGroup = <GroupInfo>[].obs;
  Map<String, dynamic> data = {};

  StreamSubscription? _subHasNewMess;

  Future<void> getListGroup() async {
    final List<GroupInfo> listGroupTemp =
        await SyncChat.instance.getDataGroupInfo();
    if (listGroupTemp.isNotEmpty) {
      for (final group in listGroupTemp)
        await setDataForGroup2User(group, userIDChat);
      listGroup.value = listGroupTemp;
    }
  }

  Future checkAndGoChat(
      {List<DataUsers>? listItemUser,
      GroupInfo? groupInfo,
      bool isSearch = false,
      int? messageID,
      bool isOff = false,
      bool inXemThanhVienPage = false}) async {
    final Map<String, dynamic> _paramRoot = <String, dynamic>{};

    // click lich su
    if (listItemUser == null) {
      listItemUser = await SyncChat.instance
          .getUserByGroupID(groupInfo!.groupID, userIDChat);
      _paramRoot.putIfAbsent('itemUser', () => listItemUser);
      _paramRoot.putIfAbsent('groupInfo', () => groupInfo);
      if (isSearch) _paramRoot.putIfAbsent('messageID', () => messageID);
    } else {
      // click user online
      _paramRoot.putIfAbsent('itemUser', () => listItemUser);
      groupInfo =
          await SyncChat.instance.checkUserInGroup(listItemUser.first.userID);
      if (groupInfo != null) {
        _paramRoot.putIfAbsent('groupInfo', () => groupInfo);
      }
    }

    if (isOff) {
      await Get.offAndToNamed(MessageDetailUI.ROUTER_NAME,
          arguments: _paramRoot);
    } else {
      await Get.toNamed(MessageDetailUI.ROUTER_NAME, arguments: _paramRoot);
    }

    // update groupInfo
    getListGroup();
  }

  Future<void> setDataForGroup2User(GroupInfo group, int userId) async {
    if (!group.isGroup) {
      final List<DataUsers> listUserTemp =
          await SyncChat.instance.getUserByGroupID(group.groupID, userId);
      if (listUserTemp.isNotEmpty) {
        group.groupName = listUserTemp.first.fullName;
        group.groupIcon = listUserTemp.first.avatar;
      }
    }
  }

  Future sendMessage(ChatMessage message, List<File>? files) async {
    switch (message.messageType) {
      case MessageType.Video:
        message.media = await Future.wait(files!.map((e) async {
          // final File thumb = await MediaPicker.instance.getVideoThumb(e.path);
          return FileUpload()..fileUrl = e.path;
          // ..thumbnail = thumb.path;
        }).toList());
        await _upLoad(message, files);
        return;
      case MessageType.Image:
        message.media = files!.map((e) => FileUpload(fileUrl: e.path)).toList();
        await _upLoad(message, files);
        return;
      case MessageType.File:
      case MessageType.Audio:
        message.media = files!
            .map((e) =>
                FileUpload(fileUrl: e.path, fileName: path.basename(e.path)))
            .toList();
        await _upLoad(message, files);
        return;
      case MessageType.Link:
      case MessageType.Youtube:
        _getMetadata(message);
        return;
      case MessageType.Emoticon:
      case MessageType.Reply:
      case MessageType.Text:
        break;
    }

    await SignalR.instance.connection
        .send(methodName: LocalVariable.sendMessage, args: [message.toJson()]);
  }

  Future _getMetadata(ChatMessage mess) async {
    try {
      String _url = mess.message!;
      if (mess.messageType == MessageType.Link) {
        if (mess.message!.substring(0, 4).toLowerCase() != 'http')
          _url = 'https://' + mess.message!;
        final Metadata? _tempData = await MetadataFetch.extract(_url);
        mess.message = jsonEncode([
          FileResults()
            ..fileName = _tempData?.title
            ..linkFile = _url
            ..thumbnail = _tempData?.image
            ..description = _tempData?.description
            ..fileType = FileType.Link.index
        ]);
      } else {
        final Video _tempData = await YoutubeExplode().videos.get(mess.message);
        mess.message = jsonEncode([
          FileResults()
            ..fileName = _tempData.title
            ..linkFile = _url
            ..thumbnail = _tempData.thumbnails.standardResUrl
            ..description = _tempData.description
            ..fileType = FileType.Youtube.index
        ]);
      }

      await SignalR.instance.connection
          .send(methodName: LocalVariable.sendMessage, args: [mess.toJson()]);
    } catch (e) {}
  }

  Future _upLoad(ChatMessage message, List<File> files) async {
    try {
      // if (message.messageType != MessageType.Text &&
      //     message.messageType != MessageType.Emoticon &&
      //     message.messageType != MessageType.Reply)
      //   SyncChat.instance.insertTempUpload(message);
      final dataUpload = await ApiRequest.instance.compressAndUpload(files);
      if (dataUpload != null) {
        final List data = jsonDecode(dataUpload.response!);
        final List<FileResults> imageUpload =
            data.map((e) => FileResults.fromJson(e)).toList();
        if (imageUpload != null) {
          switch (message.messageType) {
            case MessageType.Video:
              // final FileResults? _temp = imageUpload
              //     .firstWhereOrNull((element) => element.fileType == 1);
              // _temp!.thumbnail = imageUpload
              //     .firstWhereOrNull((element) => element.fileType == 0)
              //     ?.linkFile;

              final List<FileResults> _lstTemp = imageUpload
                  .where((element) => element.fileType == 1)
                  .toList();

              for (final i in _lstTemp) {
                i.thumbnail = imageUpload
                    .firstWhereOrNull((element) =>
                        element.fileType == 0 &&
                        path.basenameWithoutExtension(element.fileName!) ==
                            path.basenameWithoutExtension(i.fileName!))
                    ?.linkFile;
              }

              message.message = jsonEncode(_lstTemp);
              break;
            case MessageType.File:
              //do file sẽ bị lẫn lộn với image và video nên khi up xong gán type là 2
              imageUpload.map((e) => e.fileType = 2).toList();
              message.message = jsonEncode(imageUpload);
              break;
            case MessageType.Audio:
              imageUpload.map((e) => e.fileType = 5).toList();
              message.message = jsonEncode(imageUpload);
              break;
            case MessageType.Image:
              final List<FileResults> _temp = imageUpload
                  .where((element) =>
                      !element.fileName!.contains('thumbnailChat_'))
                  .toList();

              for (final FileResults i in _temp) {
                i.thumbnail = imageUpload
                        .firstWhereOrNull((element) =>
                            element.fileName
                                ?.contains('thumbnailChat_${i.fileName}') ??
                            false)!
                        .linkFile ??
                    '';
              }
              message.message = jsonEncode(_temp);
              break;
            default:
              message.message = jsonEncode(imageUpload);
              break;
          }
          await SignalR.instance.connection.send(
              methodName: LocalVariable.sendMessage, args: [message.toJson()]);
        } else {
          await SyncChat.instance.deleteMesageGhost(message.groupID!);
          LoadingWidget.instance.snackBarThongBao('Thông báo', 'Gửi file lỗi');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    data = Get.arguments;
    getListGroup();
    _subHasNewMess ??= chatMessageCtrl.stream.listen((value) {
      getListGroup();
      if (value!.userIDGui == userIDChat) {
        LoadingWidget.instance.hide();
        LoadingWidget.instance.snackBarThongBao('Thông báo', 'Đã gửi');
      }
    });
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    _subHasNewMess?.cancel();
    _subHasNewMess = null;
    super.onClose();
  }
}
