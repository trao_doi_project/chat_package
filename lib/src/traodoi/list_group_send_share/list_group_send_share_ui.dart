import 'dart:io';

import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:chat_package/extention/custom_ex.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';

import '../../../chat_package.dart';
import 'list_group_send_share_ctrl.dart';

class ListGroupSendShareUI extends StatelessWidget {
  static const String ROUTER_NAME = '/ListGroupSendShareUI';
  ListGroupSendShareUI({Key? key}) : super(key: key);

  final ListGroupSendShareCtrl _listGroupSendShareCtrl =
      Get.put(ListGroupSendShareCtrl());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Danh sách'),
      ),
      body: Obx(() {
        if (_listGroupSendShareCtrl.listGroup.isNotEmpty) {
          return ScrollConfiguration(
            behavior: DisableGlowListViewWidget(),
            child: SingleChildScrollView(
              child: Column(
                children: _listGroupSendShareCtrl.listGroup
                    .map((e) => _itemGroup(e))
                    .toList(),
              ),
            ),
          );
        }
        return const SizedBox.shrink();
      }),
    );
  }

  Padding _itemGroup(GroupInfo groupInfo) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Row(
        children: <Widget>[
          AvatarCircleWidget(
              size: 50,
              imageNetworkWidget: ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork:
                      (groupInfo.isGroup ? baseUrlApiChat : baseUrlUpload) +
                          (groupInfo.groupIcon ?? ''),
                  imageAssetDefault: ImageVariable.person)),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: GestureDetector(
              onTap: () =>
                  _listGroupSendShareCtrl.checkAndGoChat(groupInfo: groupInfo),
              behavior: HitTestBehavior.translucent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    groupInfo.groupName ?? '',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontFamily: 'Medium',
                    ),
                  ),
                  if (groupInfo.lastMessage == null &&
                      !CheckMessType(groupInfo.lastMessageType!).check())
                    const SizedBox.shrink()
                  else
                    Text(
                      CheckMessType(groupInfo.lastMessageType!)
                          .text(groupInfo.lastMessage ?? ''),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: (groupInfo.countNewMessage > 0)
                              ? Colors.black
                              : const Color(0xff757575),
                          fontSize: 14,
                          fontWeight: (groupInfo.countNewMessage > 0)
                              ? FontWeight.bold
                              : FontWeight.normal),
                    ),
                  Text(
                    (groupInfo.lastMessageDate != null)
                        ? DateFormat('HH:mm dd/MM')
                            .format(groupInfo.lastMessageDate ?? DateTime.now())
                        : '--Nhóm không có tin nhắn--',
                    style: const TextStyle(
                      color: Color(0xff757575),
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.lightBlue),
            onPressed: () {
              _sendMessage(
                  groupInfo: groupInfo,
                  text: _listGroupSendShareCtrl.data['text'],
                  files: _listGroupSendShareCtrl.data['files'],
                  type: _listGroupSendShareCtrl.data['type']);
            },
            child: const Text(
              'Gửi',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _sendMessage(
      {String? text,
      List<File>? files,
      MessageType type = MessageType.Text,
      required GroupInfo groupInfo}) async {
    LoadingWidget.instance.show();
    final ChatMessage _mess = ChatMessage(
        messageID: 0,
        messageKey: const Uuid().v4(),
        userNameGui: userNameChat,
        userIDGui: userIDChat,
        message: text,
        tinhTrangID: 0,
        tenTinhTrang: 'Đang gửi',
        createDate: DateTime.now(),
        groupID: groupInfo.groupID,
        groupKey: groupInfo.groupKey,
        messageType: type);

    if (type == MessageType.Text && text!.trim().isLink()) {
      if (text.trim().isYoutube())
        _mess.messageType = MessageType.Youtube;
      else
        _mess.messageType = MessageType.Link;
    }

    await _listGroupSendShareCtrl.sendMessage(_mess, files);
  }
}
