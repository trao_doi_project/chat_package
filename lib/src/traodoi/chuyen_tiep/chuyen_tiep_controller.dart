import 'dart:async';

import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:flutter_core_getx_dev/app/utils/extension.dart';

class ChuyenTiepController extends GetxController {
  RxList<ItemChuyenTiep> listChuyenTiep = <ItemChuyenTiep>[].obs;
  RxList<DataUsers> listUser = <DataUsers>[].obs;
  RxList<GroupInfo> listGroup = <GroupInfo>[].obs;

  late ChatMessage message;
  List<GroupInfo> groups = <GroupInfo>[];
  List<DataUsers> users = <DataUsers>[];
  final TextEditingController textEditingController = TextEditingController();
  Timer? _delay;

  Future<void> get5GroupInfo() async {
    final List<GroupInfo>? groupList =
        await SyncChat.instance.getDataGroupInfoWithNumber(number: 5);
    if (groupList != null && groupList.isNotEmpty) {
      groups = groupList;
      for (final group in groups)
        if (!group.isGroup) {
          // group 2 user no has group name and group icon => update
          final List<DataUsers> users = await SyncChat.instance
              .getUserByGroupID(group.groupID, userIDChat);
          if (users.isNotEmpty) {
            group.groupIcon = users.first.avatar;
            group.groupName = users.first.fullName;
          }
        }
      listGroup.value = groups;
    }
  }

  Future<void> getAllGroupInfo() async {
    groups = await SyncChat.instance.getDataGroupInfo();
    if (groups.isNotEmpty) {
      for (final group in groups)
        if (!group.isGroup) {
          // group 2 user no has group name and group icon => update
          final List<DataUsers> users = await SyncChat.instance
              .getUserByGroupID(group.groupID, userIDChat);
          if (users.isNotEmpty) {
            group.groupIcon = users.first.avatar;
            group.groupName = users.first.fullName;
          }
        }
      listGroup.value = groups;
    }
  }

  void get5DataUser() {
    SyncChat.instance.getDataUserNoGroupWithNumber(number: 5).then((value) {
      if (value != null && value.isNotEmpty) {
        users = value;
        listUser.value = value;
      }
    });
  }

  Future getAllUserNoGroup() async {
    SyncChat.instance.getDataUserNoGroup().then((value) {
      if (value != null && value.isNotEmpty) {
        users = value;
        listUser.value = value;
      }
    });
  }

  Future getListDataSearch(String keySearch) async {
    final String keySearchKoDau = keySearch.xoaDau().toLowerCase();

    // list user
    if (users.length < 6) users = await SyncChat.instance.getDataUserNoGroup();
    if (users.isNotEmpty) {
      final List<DataUsers> items = users.where((element) {
        final String el = element.fullName!.xoaDau().toLowerCase();
        if (el.contains(keySearchKoDau)) return true;
        return false;
      }).toList();
      listUser.value = items;
    } else
      listUser.clear();

    // list group
    if (groups.length < 6) groups = await SyncChat.instance.getDataGroupInfo();
    if (groups.isNotEmpty) {
      for (final group in groups)
        if (!group.isGroup) {
          // group 2 user no has group name and group icon => update
          final List<DataUsers> users = await SyncChat.instance
              .getUserByGroupID(group.groupID, userIDChat);
          if (users.isNotEmpty) {
            group.groupIcon = users.first.avatar;
            group.groupName = users.first.fullName;
          }
        }
      listGroup.value = groups.where((group) {
        final String key = group.groupName!.xoaDau().toLowerCase();
        if (key.contains(keySearchKoDau)) return true;
        return false;
      }).toList();
    } else
      listGroup.clear();
  }

  Future<void> updateListChuyenTiep({required ItemChuyenTiep item}) async {
    if (checHasChuyenTiep(item)) if (item.type == 'group')
      listChuyenTiep.removeWhere(
          (element) => element.group?.groupID == item.group!.groupID);
    else
      listChuyenTiep
          .removeWhere((element) => element.user?.userID == item.user!.userID);
    else {
      if (item.type == 'group' && !item.group!.isGroup) {
        // group 2 user no has group name and group icon => update
        final List<DataUsers> users = await SyncChat.instance
            .getUserByGroupID(item.group!.groupID, userIDChat);
        if (users.isNotEmpty) {
          item.group!.groupIcon = users.first.avatar;
          item.group!.groupName = users.first.fullName;
        }
      }
      listChuyenTiep.add(item);
    }
  }

  bool checHasChuyenTiep(ItemChuyenTiep item) {
    if (item.type == 'user') {
      for (final e in listChuyenTiep)
        if (e.type == 'user' && e.user!.userID == item.user!.userID)
          return true;
      return false;
    } else {
      for (final e in listChuyenTiep)
        if (e.type == 'group' && e.group!.groupID == item.group!.groupID)
          return true;
      return false;
    }
  }

  void resetDataSearch() {
    listUser.value = users;
    listGroup.value = groups;
  }

  void delaySearch(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (_delay != null) _delay!.cancel();
    _delay = Timer(duration, callback);
  }

  @override
  void onInit() {
    final Map<String, dynamic> arg = Get.arguments;
    message = arg['chatMessage'];
    get5GroupInfo();
    get5DataUser();

    super.onInit();
  }

  @override
  void onClose() {
    textEditingController.dispose();
    _delay?.cancel();
    super.onClose();
  }
}

class ItemChuyenTiep {
  String type;
  GroupInfo? group;
  DataUsers? user;
  ItemChuyenTiep({required this.type, this.group, this.user});
}
