import 'package:chat_package/base/base_color.dart';
import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/src/traodoi/chuyen_tiep/chuyen_tiep_controller.dart';
import 'package:flutter_core_getx_dev/app/widget/app_bar_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/disable_glow_listview_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';
import 'package:flutter_core_getx_dev/app/widget/loader_widget.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as path;

import '../../../chat_package.dart';

class ChuyenTiepUI extends StatelessWidget {
  static const ROUTER_NAME = '/ChuyenTiepUI';
  final ChuyenTiepController _chuyenTiepController =
      Get.put(ChuyenTiepController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: const Text('Chuyển tiếp'),
        backgroundColor: BaseColor.primaryColor,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: Get.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                  )
                ],
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      textInputAction: TextInputAction.search,
                      controller: _chuyenTiepController.textEditingController,
                      onChanged: (value) =>
                          _chuyenTiepController.delaySearch(() {
                        if (value.trim().isNotEmpty)
                          _chuyenTiepController.getListDataSearch(value);
                        else
                          _chuyenTiepController.resetDataSearch();
                      }),
                      decoration: InputDecoration(
                          icon: Icon(
                            Icons.search,
                            size: Get.width * 0.08,
                            color: Colors.grey,
                          ),
                          border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),
                  ),
                  GestureDetector(
                      onTap: () {
                        _chuyenTiepController.textEditingController.clear();
                        _chuyenTiepController.resetDataSearch();
                      },
                      child: const Icon(
                        Icons.cancel,
                        color: Colors.black54,
                      )),
                  const SizedBox(
                    width: 5,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: ScrollConfiguration(
              behavior: DisableGlowListViewWidget(),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Obx(() {
                      if (_chuyenTiepController.listGroup.isNotEmpty)
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Trò chuyện gần đây',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            customListGroup(),
                          ],
                        );
                      return const SizedBox.shrink();
                    }),
                    Obx(() {
                      if (_chuyenTiepController.listUser.isNotEmpty)
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Danh bạ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            customListDataUser(),
                          ],
                        );
                      return const SizedBox.shrink();
                    })
                  ],
                ),
              ),
            ),
          ),
          Obx(() {
            if (_chuyenTiepController.listChuyenTiep.isNotEmpty)
              return Container(
                decoration: const BoxDecoration(color: Colors.white),
                width: Get.width,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: SizedBox(
                        height: Get.height * 0.07,
                        child: ScrollConfiguration(
                          behavior: DisableGlowListViewWidget(),
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount:
                                _chuyenTiepController.listChuyenTiep.length,
                            itemBuilder: (context, index) {
                              return _imageItem(
                                  _chuyenTiepController.listChuyenTiep[index]);
                            },
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: BaseColor.backGroundColor),
                            constraints:
                                BoxConstraints(maxWidth: Get.width * 0.85),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: _messageChuyenTiep(),
                            ),
                          ),
                          const Spacer(),
                          GestureDetector(
                            onTap: () async {
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (BuildContext context) =>
                                      const Center(
                                        child: LoaderWidget(),
                                      ));
                              await ApiRequest.instance.chuyenTiepMessage(
                                  _chuyenTiepController.message,
                                  _chuyenTiepController.listChuyenTiep);
                              Get.back();
                              LoadingWidget.instance.snackBarThongBao('Thông báo',
                                  'Đã chuyển tiếp thành công');
                            },
                            child: Icon(
                              Icons.send_sharp,
                              size: Get.width * 0.1,
                              color: Colors.blue,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            return const SizedBox.shrink();
          })
        ],
      ),
    );
  }

  Column customListDataUser() {
    final List<Widget> children = <Widget>[];
    for (final element in _chuyenTiepController.listUser)
      children.add(_dataUserItem(element));
    if (_chuyenTiepController.listUser.length == 5)
      children.add(GestureDetector(
        onTap: () {
          _chuyenTiepController.getAllUserNoGroup();
        },
        child: Column(
          children: [
            const Divider(
              thickness: 2,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 16),
              child: SizedBox(
                height: Get.height * 0.03,
                width: Get.width,
                child: const Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'xem thêm',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    return Column(
      children: children,
    );
  }

  Column customListGroup() {
    final List<Widget> children = <Widget>[];
    for (final element in _chuyenTiepController.listGroup)
      children.add(_groupItem(element));

    if (_chuyenTiepController.listGroup.length == 5)
      children.add(GestureDetector(
        onTap: () {
          _chuyenTiepController.getAllGroupInfo();
        },
        child: Column(
          children: [
            const Divider(
              thickness: 3,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: SizedBox(
                height: Get.height * 0.03,
                width: Get.width,
                child: const Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'xem thêm',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    return Column(
      children: children,
    );
  }

  Row _dataUserItem(DataUsers user) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
          child: SizedBox(
            height: Get.height * 0.06,
            width: Get.height * 0.06,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                imageAssetDefault: ImageVariable.person,
                urlImageNetwork: baseUrlUpload + (user.avatar ?? ''),
              ),
            ),
          ),
        ),
        Container(
          constraints: BoxConstraints(maxWidth: Get.width * 0.7),
          child: Text(
            user.fullName ?? '',
            maxLines: 3,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontFamily: 'Medium',
            ),
          ),
        ),
        const Spacer(),
        GestureDetector(
          onTap: () {
            _chuyenTiepController.updateListChuyenTiep(
                item: ItemChuyenTiep(type: 'user', user: user));
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Obx(() => Icon(
                  (_chuyenTiepController.checHasChuyenTiep(
                          ItemChuyenTiep(type: 'user', user: user)))
                      ? Icons.check_circle
                      : Icons.radio_button_unchecked,
                  color: BaseColor.primaryColor,
                  size: 26,
                )),
          ),
        )
      ],
    );
  }

  Row _groupItem(GroupInfo group) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
          child: SizedBox(
            height: Get.height * 0.06,
            width: Get.height * 0.06,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                imageAssetDefault: ImageVariable.person,
                urlImageNetwork: baseUrlApiChat + (group.groupIcon ?? ''),
              ),
            ),
          ),
        ),
        Container(
          constraints: BoxConstraints(maxWidth: Get.width * 0.7),
          child: Text(
            group.groupName ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontFamily: 'Medium',
            ),
          ),
        ),
        const Spacer(),
        Obx(() {
          return GestureDetector(
            onTap: () {
              _chuyenTiepController.updateListChuyenTiep(
                  item: ItemChuyenTiep(type: 'group', group: group));
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                (_chuyenTiepController.checHasChuyenTiep(
                        ItemChuyenTiep(type: 'group', group: group)))
                    ? Icons.check_circle
                    : Icons.radio_button_unchecked,
                color: BaseColor.primaryColor,
              ),
            ),
          );
        })
      ],
    );
  }

  Widget _imageItem(ItemChuyenTiep item) {
    if (item.type == 'group') {
      final GroupInfo groupInfo = item.group!;
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Stack(
          children: [
            SizedBox(
              height: Get.height * 0.06,
              width: Get.height * 0.06,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: ImageNetworkWidget(
                    packageName: LocalVariable.namePackage,
                    imageAssetDefault: ImageVariable.person,
                    urlImageNetwork:
                        baseUrlApiChat + (groupInfo.groupIcon ?? ''),
                  )),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: GestureDetector(
                onTap: () {
                  _chuyenTiepController.listChuyenTiep.remove(item);
                },
                child: const Icon(
                  Icons.cancel,
                  color: Colors.black,
                  size: 16,
                ),
              ),
            )
          ],
        ),
      );
    } else {
      final DataUsers user = item.user!;
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Stack(
          children: [
            SizedBox(
              height: Get.height * 0.06,
              width: Get.height * 0.06,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  imageAssetDefault: ImageVariable.person,
                  urlImageNetwork: baseUrlUpload + (user.avatar ?? ''),
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: GestureDetector(
                onTap: () {
                  _chuyenTiepController.listChuyenTiep.remove(item);
                },
                child: const Icon(
                  Icons.cancel,
                  color: Colors.black,
                  size: 16,
                ),
              ),
            )
          ],
        ),
      );
    }
  }

  Widget _messageChuyenTiep() {
    switch (_chuyenTiepController.message.messageType) {
      case MessageType.Reply:
      case MessageType.Text:
        return Text(_chuyenTiepController.message.message ?? '');
      case MessageType.Image:
        return Row(
          children: [
            ImageNetworkWidget(
                packageName: LocalVariable.namePackage,
                urlImageNetwork: baseUrlApiChat +
                    (_chuyenTiepController.message.media!.first.fileUrl ?? ''),
                imageWidth: 35,
                imageAssetDefault: ImageVariable.noimg),
            const SizedBox(
              width: 5,
            ),
            const Text('[Hình Ảnh]'),
          ],
        );
      case MessageType.Video:
        return Row(
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                ImageNetworkWidget(
                  packageName: LocalVariable.namePackage,
                  urlImageNetwork: baseUrlApiChat +
                      (_chuyenTiepController.message.media!.first.thumbnail ??
                          ''),
                  imageAssetDefault: ImageVariable.noimg,
                  imageWidth: 35,
                ),
                Image.asset(
                  ImageVariable.playVideo,
                  width: 15,
                  package: 'chat_package',
                )
              ],
            ),
            const SizedBox(
              width: 5,
            ),
            const Text('[Video]'),
          ],
        );
      case MessageType.File:
        return Row(
          children: [
            Image.asset(
              ImageVariable.fileExtensionIcon(path.extension(
                _chuyenTiepController.message.media!.first.fileName!,
              )),
              width: 35,
              fit: BoxFit.cover,
              package: 'chat_package',
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Text(
                _chuyenTiepController.message.media!.first.fileName!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        );
      case MessageType.Audio:
        return Row(
          children: [
            Image.asset(
              ImageVariable.audio,
              width: 32,
              fit: BoxFit.cover,
              package: 'chat_package',
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Text(
                _chuyenTiepController.message.media!.first.fileName ?? 'Audio',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        );
      case MessageType.Link:
      case MessageType.Youtube:
        return Row(
          children: [
            ImageNetworkWidget(
              packageName: LocalVariable.namePackage,
              imageAssetDefault: ImageVariable.noimg,
              urlImageNetwork:
                  _chuyenTiepController.message.media!.first.thumbnail,
              imageWidth: 35,
              imageHeight: 35,
            ),
            const SizedBox(
              width: 5,
            ),
            Flexible(
              child: Text(
                  _chuyenTiepController.message.media!.first.fileUrl ?? '',
                  style: const TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline)),
            ),
          ],
        );
      case MessageType.Emoticon:
        return Row(
          children: [
            ImageNetworkWidget(
              packageName: LocalVariable.namePackage,
              imageWidth: 35,
              imageHeight: 35,
              imageAssetDefault: ImageVariable.noimg,
              urlImageNetwork: _chuyenTiepController.message.message,
            ),
            const SizedBox(
              width: 5,
            ),
            const Text('[Sticker]'),
          ],
        );
      default:
        return const SizedBox.shrink();
    }
  }
}
