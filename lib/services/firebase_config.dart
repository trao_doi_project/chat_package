import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/data/core_http.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

enum OnFirebaseAction { onLoad, onMessage, onBackground }

class FireBaseConfig {
  FireBaseConfig._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  static late FireBaseConfig instance = FireBaseConfig._();

  final AndroidNotificationChannel _channel = const AndroidNotificationChannel(
    '02862582324', // id
    'VietInfo', // title
    'VietInfo', // description
    importance: Importance.max,
    enableLights: true,
    enableVibration: true,
    playSound: true,
  );
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<void> _initializeLocalNotifications() async {
    const initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher');
    final initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
    final initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _selectNotificationLocal);
  }

  Future<dynamic> _onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    print('dao 2');
  }

  Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    print('Handling a background message: ${message.messageId}');
  }

  Future<FireBaseConfig> resetInstance() async {
    await _initializeLocalNotifications();
    if (Platform.isIOS)
      await _iOSConfig();
    else
      await _androidConfig();

    await _firebaseConfig();
    instance = FireBaseConfig._();
    return instance;
  }

  Future _iOSConfig() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
  }

  Future _androidConfig() async {
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(_channel);
  }

  Future _firebaseConfig() async {
    final NotificationSettings settings =
        await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    FirebaseMessaging.onMessage.listen((event) {
      _actionFirebase(OnFirebaseAction.onMessage, event);
      print('${event.messageId} onMessage');
    });

    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      _actionFirebase(OnFirebaseAction.onBackground, event);
      print('${event.messageId} onMessageOpenedApp');
    });

    _firebaseMessaging.subscribeToTopic('allDevice');
  }

  Future onTerminated() async {
    final RemoteMessage? _initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (_initialMessage == null) return;
    notifyClean();
    _actionFirebase(OnFirebaseAction.onLoad, _initialMessage);
    print('_initialMessage');
  }

  Future<String> getDeviceId() async {
    final deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      final IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      final AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  Future<String> getFireBaseToken() async {
    final String? token = await _firebaseMessaging.getToken();
    print(token);
    return token ?? '';
  }

  void _showHeadUp(RemoteMessage message) {
    final RemoteNotification? notification = message.notification;
    final AndroidNotification? android = message.notification?.android;

    _flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification?.title,
        notification?.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            _channel.id,
            _channel.name,
            _channel.description,
            importance: _channel.importance,
            icon: android?.smallIcon,
            // other properties...
          ),
          iOS: const IOSNotificationDetails(
              presentBadge: true, presentAlert: true, presentSound: true),
        ),
        payload: message.data['arguments']);
  }

  void upAndDowHeadUp(String message) {
    _flutterLocalNotificationsPlugin.show(
      1,
      appNameChat,
      message,
      NotificationDetails(
        android: AndroidNotificationDetails(
          _channel.id,
          _channel.name,
          _channel.description,
          setAsGroupSummary: true,
          importance: _channel.importance,
          icon: 'ic_launcher',
          // other properties...
        ),
        iOS: const IOSNotificationDetails(
            presentBadge: true, presentAlert: true, presentSound: true),
      ),
    );
  }

  Future _actionFirebase(OnFirebaseAction action, RemoteMessage message) async {
    switch (action) {
      case OnFirebaseAction.onLoad:
        fireBaseOnLoad = message.data;
        break;
      case OnFirebaseAction.onMessage:
        if (Get.currentRoute != MessageDetailUI.ROUTER_NAME)
          // _showBigPictureNotification(message);
          showSnackbar(message);
        break;
      case OnFirebaseAction.onBackground:
        notifyClean();
        _pushPage(message.data);
        break;
    }
  }

  Future<dynamic> _selectNotificationLocal(String? payload) async {
    print(payload);
    notifyClean();
    _pushPage(jsonDecode(payload ?? ''));
  }

  Future<void> _showBigPictureNotification(RemoteMessage message) async {
    final Map<String, dynamic> arg = jsonDecode(message.data['arguments']);
    _initializeLocalNotifications();
    if (Platform.isAndroid) {
      if (message.data['image'] != null && message.data['image'] != '') {
        final largeIconPath =
            await _downloadAndSaveFile(message.data['image'], 'largeIcon');
        final bigPicturePath =
            await _downloadAndSaveFile(message.data['image'], 'bigPicture');
        final bigPictureStyleInformation =
            BigPictureStyleInformation(FilePathAndroidBitmap(bigPicturePath));
        final androidPlatformChannelSpecifics = AndroidNotificationDetails(
            _channel.id, _channel.name, _channel.description,
            importance: _channel.importance,
            enableLights: true,
            enableVibration: true,
            playSound: true,
            setAsGroupSummary: true,
            groupKey: arg['groupID'].toString(),
            largeIcon: FilePathAndroidBitmap(largeIconPath),
            styleInformation: bigPictureStyleInformation);
        final platformChannelSpecifics =
            NotificationDetails(android: androidPlatformChannelSpecifics);
        await _flutterLocalNotificationsPlugin.show(
            int.parse(message.data['id']),
            message.notification!.title,
            message.notification!.body,
            platformChannelSpecifics,
            payload: jsonEncode(message.data));

        // await _deleteFile(bigPicturePath);
        // await _deleteFile(largeIconPath);
      } else
        await _showNotification(
          int.parse(message.data['id']),
          message.notification?.title,
          message.notification?.body,
          jsonEncode(message.data),
          isGroup: true,
          groupKey: arg['groupID'].toString(),
        );
    } else {
      await _showNotification(
        int.parse(message.data['id']),
        message.notification!.title,
        message.notification!.body,
        jsonEncode(message.data),
        isGroup: true,
        groupKey: arg['groupID'].toString(),
      );
    }
  }

  Future<String> _downloadAndSaveFile(String url, String fileName) async {
    try {
      final directory = await getTemporaryDirectory();
      final filePath = '${directory.path}/$fileName';
      await CoreHttp.instance.download(url, filePath);
      return filePath;
    } catch (e) {
      print(e);
      return 'https://github.com/vietinfo/library/blob/main/noimg.png';
    }
  }

  Future<void> _showNotification(
      int id, String? title, String? body, String? data,
      {bool isGroup = false, String groupKey = ''}) async {
    await _flutterLocalNotificationsPlugin.show(
        id,
        title,
        body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            _channel.id,
            _channel.name,
            _channel.description,
            importance: _channel.importance,
            setAsGroupSummary: isGroup,
            groupKey: groupKey,
            icon: 'ic_launcher',
            // other properties...
          ),
          iOS: const IOSNotificationDetails(
              presentBadge: true, presentAlert: true, presentSound: true),
        ),
        payload: data);
  }

  Future _pushPage(Map<String, dynamic> message) async {
    try {
      final Map<String, dynamic> arg = jsonDecode(message['arguments']);
      List<String> pageName;
      final String routerName = message['page'];
      if (routerName.isEmpty) return;
      pageName = routerName.split(',');

      for (final String i in pageName) {
        if (Get.currentRoute == i) {
          if (Get.currentRoute == MessageDetailUI.ROUTER_NAME) {
            return;
          }

          Get.back();
          await Get.toNamed(i, arguments: arg);
        } else {
          if (i == MessageDetailUI.ROUTER_NAME) {
            final List<DataUsers> _itemUser = await SyncChat.instance
                .getUserByGroupID(arg['groupID'], userIDChat);
            final GroupInfo? _groupInfo =
                await SyncChat.instance.getGroupByGroupID(arg['groupID']);

            await Get.offNamedUntil(i, ModalRoute.withName(rootPageRouter),
                arguments: {'itemUser': _itemUser, 'groupInfo': _groupInfo});
          } else
            await Get.toNamed(i, arguments: arg);
        }
      }
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  void notifyClean() {
    _flutterLocalNotificationsPlugin.cancelAll();
  }

  void showSnackbar(RemoteMessage message) {
    final Map<String, dynamic> arg = jsonDecode(message.data['arguments']);
    Get.rawSnackbar(
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.transparent,
        snackStyle: SnackStyle.GROUNDED,
        barBlur: 10,
        messageText: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    message.notification?.title ?? '',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    message.notification?.body ?? '',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            if (message.data['image'] != null && message.data['image'] != '')
              Image.network(
                message.data['image'],
                width: 40,
                height: 40,
              )
          ],
        ),
        duration: const Duration(seconds: 2));
    // SnackBar(
    //   backgroundColor: Colors.transparent,
    //   content: SizedBox(
    //     height: 30,
    //     child: BackdropFilter(
    //       filter: ImageFilter.blur(
    //         sigmaX: 10.0,
    //         sigmaY: 10.0,
    //       ),
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Text(
    //             message.notification?.title ?? '',
    //             style: const TextStyle(
    //               fontWeight: FontWeight.bold,
    //             ),
    //           ),
    //           Expanded(
    //             child: Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               children: [
    //                 Text(message.notification?.body ?? ''),
    //                 if (message.data['image'] != null &&
    //                     message.data['image'] != '')
    //                   Image.network(
    //                     message.data['image'],
    //                     width: 25,
    //                   )
    //               ],
    //             ),
    //           )
    //         ],
    //       ),
    //     ),
    //   ),
    //   action: SnackBarAction(
    //     label: 'Undo',
    //     onPressed: () {
    //       // Some code to undo the change.
    //     },
    //   ),
    // );
  }
}
