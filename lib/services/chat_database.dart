import 'dart:async';
import 'dart:io';

import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite_sqlcipher/sqflite.dart';

class ChatDatabase extends GetxService {
  ChatDatabase._();
  static late ChatDatabase instance;

  static String _localDbPath = '';

  static late Database database;

  static Future<ChatDatabase> getInstance() async {
    database = await _openDatabase();
    instance = ChatDatabase._();
    return instance;
  }

  static Future<Database> _openDatabase() async {
    if (Platform.isIOS) {
      final Directory tempDir = await getLibraryDirectory();
      _localDbPath = tempDir.path;
    } else {
      _localDbPath = await getDatabasesPath();
    }
    //construct path
    final dbPath = join(_localDbPath, 'modulechat', 'module_chat_sql.db');
    //open database
    return openDatabase(dbPath,
        version: 1, onCreate: _createTable, password: 'Vietinfo@!@#');
  }

  Future delDatabase() async {
    final dbPath = join(_localDbPath, 'modulechat', 'module_chat_sql.db');
    //del database
    await deleteDatabase(dbPath);
  }

  static Future _createTable(Database database, int version) async {
    await database.execute('''
      CREATE TABLE UserInGroup (
        id INTEGER PRIMARY KEY,
        groupID INTEGER,
        groupKey TEXT,
        userID INTEGER,
        userName TEXT,
        createDate TEXT,
        lastUpdate TEXT,
        isOff TEXT,
        isDelete TEXT,
        deactivated TEXT)''');

    await database.execute('''
       CREATE TABLE ChatMessage (
        messageID INTEGER,
        messageKey TEXT PRIMARY KEY,
        groupID INTEGER,
        groupKey TEXT,
        userIDGui INTEGER,
        userNameGui TEXT,
        message TEXT,
        messageType INTEGER,
        replyData TEXT,
        reaction TEXT,
        tinhTrangID INTEGER,
        tenTinhTrang TEXT,
        userIDDaXem TEXT,
        fullNameDaXem TEXT,
        createDate TEXT,
        updateDate TEXT,
        isThuHoi TEXT,
        ghiChu TEXT,
        userDelMess TEXT)''');

    await database.execute('''
        CREATE TABLE GroupInfo (
        groupID INTEGER PRIMARY KEY,
        groupKey TEXT,
        groupName TEXT,
        groupIcon TEXT,
        hinhNen TEXT,
        isGroup TEXT,
        isDonVi TEXT,
        createUserID INTEGER,
        createUserName TEXT,
        lastMessage TEXT,
        lastMessageDate TEXT,
        lastMessageType INTEGER,
        ghimMessage TEXT,
        createDate TEXT,
        lastUpdate TEXT,
        lastUpdateStr TEXT,
        countNewMessage INTEGER DEFAULT 0,
        isDelete TEXT)''');

    await database.execute('''
        CREATE TABLE GroupGhimMess (
        id INTEGER PRIMARY KEY,
        groupID INTEGER,
        groupKey TEXT,
        userGhimID INTEGER,
        ghimMessage TEXT,
        Note TEXT,
        loaiGhim INTEGER,
        createDate TEXT,
        lastUpdate TEXT,
        isDelete TEXT)''');

    await database.execute('''
    CREATE TABLE DataUsers (
        userID INTEGER PRIMARY KEY,
        userName TEXT,
        fullName TEXT,
        avatar TEXT,
        phongBanID INTEGER,
        donViID INTEGER,
        tenPhongBan TEXT,
        chucVuID INTEGER,
        tenChucVu TEXT,
        soDT TEXT,
        email TEXT,
        nameSort TEXT)''');

    await database.execute('''
      CREATE TABLE FileManager (
        fileID INTEGER PRIMARY KEY,
        groupID INTEGER,
        groupKey TEXT,
        userIDGui INTEGER,
        messageID INTEGER,
        messageKey TEXT,
        fileName TEXT,
        thumbnail TEXT,
        fileUrl TEXT,
        fileType INTEGER,
        fileSizeStr TEXT DEFAULT "",
        fileSize FLOAT DEFAULT 0.0,
        moTa TEXT,
        createDate TEXT,
        lastUpdate TEXT,
        isDelete TEXT)''');
  }

  Future<int> insertData(String tblName, Map<String, dynamic> data) async {
    final int result = await database.insert(
      tblName,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return result;
  }

  Future<int> updateData(
      String tblName, Map<String, dynamic> data, String key, dynamic value) {
    return database
        .update(tblName, data, where: '$key = ?', whereArgs: [value]);
  }

  Future<List<dynamic>?> getData(String tblName,
      {String? key,
      dynamic value,
      String? keyOrder,
      String? orderValue}) async {
    final results = await database.rawQuery(
        'SELECT * FROM $tblName${(key != null) ? ' WHERE $key = $value ${keyOrder != null ? 'order by $keyOrder $orderValue' : ''}' : ''}');
    if (results.isNotEmpty) {
      return results;
    }
    return null;
  }
}
