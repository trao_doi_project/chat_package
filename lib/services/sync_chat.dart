import 'dart:convert';
import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/chat_package.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_sqlcipher/sqflite.dart';

import 'api_resquest.dart';
import 'chat_database.dart';

class SyncChat {
  static const String tblDataUsers = 'DataUsers';
  static const String tblGroupInfo = 'GroupInfo';
  static const String tblUserInGroup = 'UserInGroup';
  static const String tblChatMessage = 'ChatMessage';
  static const String tblFileManager = 'FileManager';
  static const String tblGroupGhimMess = 'GroupGhimMess';

  SyncChat._();
  static late SyncChat instance = SyncChat._();

  static SyncChat resetInstance() {
    instance = SyncChat._();
    return instance;
  }

  Future synDataUser() async {
    final List<dynamic>? data =
        await ApiRequest.instance.appTraoDoiV2GetDataUsers();
    if (data != null) {
      ChatDatabase.database.transaction((txn) async {
        final Batch batch = txn.batch();
        data
            .map((e) => batch.insert(tblDataUsers, e,
                conflictAlgorithm: ConflictAlgorithm.replace))
            .toList();
        await batch.commit(noResult: true);
      });
    }
  }

  Future<List<DataUsers>> getUserByGroupID(int groupID, int userID) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblDataUsers du inner join $tblUserInGroup iu on du.userID = iu.userID where iu.groupID = $groupID and du.userID <> $userID');
      if (data.isNotEmpty)
        return data.map((e) => DataUsers.fromJson(e)).toList();
      return [];
    } catch (e) {
      return [];
    }
  }

  Future<List<DataUsers>?> getUserByGroupIDHasMe(int groupID) async {
    final List<
        Map<String,
            Object?>> data = await ChatDatabase.database.rawQuery(
        'select * from $tblDataUsers du inner join $tblUserInGroup iu on du.userID = iu.userID where iu.groupID = $groupID');
    if (data.isNotEmpty) return data.map((e) => DataUsers.fromJson(e)).toList();
    return null;
  }

  Future<GroupInfo?> getGroupByGroupID(int groupID) async {
    try {
      final List<Map<String, Object?>> data = await ChatDatabase.database
          .rawQuery('select * from $tblGroupInfo where groupID = $groupID');
      if (data.isNotEmpty) return GroupInfo.fromMapObject(data.first);
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<List<FileUpload>?> getKhoLuuTruByGroupIDAndType(
      int groupID, int typeFile, int pageIndex) async {
    String sql = '';
    pageIndex = pageIndex * 15;

    sql =
        'select * from $tblFileManager where groupID = $groupID and fileType = $typeFile order by fileID desc limit 20 offset $pageIndex';

    // get image and video
    if (typeFile == FileType.Image.index)
      sql =
          'select * from $tblFileManager where groupID = $groupID and (fileType = 0 or fileType = 1) order by fileID desc limit 20 offset $pageIndex';

    // get Link and Youtube
    if (typeFile == FileType.Link.index)
      sql =
          'select * from $tblFileManager where groupID = $groupID and (fileType = 3 or fileType = 4) order by fileID desc limit 20 offset $pageIndex';
    final List<dynamic> files = await ChatDatabase.database.rawQuery(sql);
    if (files.isNotEmpty)
      return files.map((e) => FileUpload.fromJson(e)).toList();
    return null;
  }

  Future<List<FileUpload>?> getImageByGroupIDWithNumber(
      int groupID, int number) async {
    final String sql =
        'select * from $tblFileManager where groupID = $groupID and fileType = 0 order by fileID desc limit $number';
    final List<Map<String, Object?>> files =
        await ChatDatabase.database.rawQuery(sql);
    if (files.isNotEmpty)
      return files.map((e) => FileUpload.fromJson(e)).toList();
    return null;
  }

  Future<List<ChatMessage>?> getMessageTextInGroupWithNumber(
      {required int number, required int groupId}) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'SELECT * FROM $tblChatMessage WHERE messageType = 0 AND isThuHoi = "false" AND groupID = $groupId order by messageID desc limit $number');
      if (data.isNotEmpty)
        return data.map((e) => ChatMessage.fromJson(e)).toList();
      else
        return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<FileUpload>?> getFileAndVideoByGroupIDWithNumber(
      int groupID, int number) async {
    final String sql =
        'select * from $tblFileManager where groupID = $groupID AND (fileType = 1 OR fileType = 2) order by fileID desc limit $number';
    final List<Map<String, Object?>> files =
        await ChatDatabase.database.rawQuery(sql);
    if (files.isNotEmpty)
      return files.map((e) => FileUpload.fromJson(e)).toList();
    return null;
  }

  Future<List<FileUpload>?> searchFileAndVideoByNameInGroup(
      {required int groupId,
      required int limit,
      required int pageIndex,
      required String keySearch}) async {
    final int offset = pageIndex * limit;
    final String sql =
        'select * from $tblFileManager where groupID = $groupId AND fileName LIKE "%$keySearch%" AND (fileType = 1 OR fileType = 2) order by fileID desc limit $limit offset $offset';
    final List<Map<String, Object?>> files =
        await ChatDatabase.database.rawQuery(sql);
    if (files.isNotEmpty)
      return files.map((e) => FileUpload.fromJson(e)).toList();
    return null;
  }

  Future<List<FileUpload>?> searchFileByNameInGroup(
      {required int groupId,
      required int limit,
      required int pageIndex,
      required String keySearch}) async {
    final int offset = pageIndex * limit;
    final String sql =
        'select * from $tblFileManager where groupID = $groupId AND fileName LIKE "%$keySearch%" AND fileType = 2 order by fileID desc limit $limit offset $offset';
    final List<Map<String, Object?>> files =
        await ChatDatabase.database.rawQuery(sql);
    if (files.isNotEmpty)
      return files.map((e) => FileUpload.fromJson(e)).toList();
    return null;
  }

  Future<List<DataUsers>?> getDataUser() async {
    try {
      final List<Map<String, Object?>> data = await ChatDatabase.database
          .rawQuery('select * from $tblDataUsers order by fullName');
      return data.map((e) => DataUsers.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<List<DataUsers>?> getDataUserWithNumber({required int number}) async {
    try {
      final List<Map<String, Object?>> data = await ChatDatabase.database
          .rawQuery(
              'select * from $tblDataUsers order by fullName limit $number');
      return data.map((e) => DataUsers.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<List<GroupInfo>?> getDataGroupInfoWithNumber(
      {required int number}) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblGroupInfo order by lastUpdate desc limit $number');
      if (data.isNotEmpty)
        return data.map((e) => GroupInfo.fromMapObject(e)).toList();
      return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<GroupInfo>?> getDataGroupInfoIsGroupWithNumber(
      {required int number}) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblGroupInfo WHERE isGroup = "true" order by lastUpdate desc limit $number');
      if (data.isNotEmpty)
        return data.map((e) => GroupInfo.fromMapObject(e)).toList();
      return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<ChatMessage>?> getMessageTextWithNumber(
      {required int number}) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'SELECT * FROM $tblChatMessage WHERE messageType = 0 AND isThuHoi = "false" order by messageID desc limit $number');
      if (data.isNotEmpty)
        return data.map((e) => ChatMessage.fromJson(e)).toList();
      else
        return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<DataUsers?> getDataUserByID(int userID) async {
    try {
      final List<Map<String, Object?>> data = await ChatDatabase.database
          .rawQuery('select * from $tblDataUsers where userID = $userID');
      return DataUsers.fromJson(data.first);
    } catch (e) {
      return null;
    }
  }

  Future<List<ChatMessage>> getDataMess(int groupID,
      {int offset = 0, int limit = 1000}) async {
    try {
      offset = offset * limit;
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblChatMessage where groupID = $groupID order by createDate desc limit $limit offset $offset');
      if (data.isNotEmpty)
        return Future.wait(data
            .map((e) async {
              final ChatMessage _temChat = ChatMessage.fromJson(e);
              if (_temChat.messageType != MessageType.Text &&
                  _temChat.messageType != MessageType.Emoticon &&
                  _temChat.messageType != MessageType.Reply) {
                if (_temChat.messageID > 0) {
                  final List<FileUpload> _tempMedia = await getFiles(
                      groupID,
                      int.parse(e['messageID'].toString()),
                      _getTypeFile(_temChat.messageType));
                  _temChat.media = _tempMedia;
                } else {
                  final List _item = jsonDecode(_temChat.message!);
                  _temChat.media =
                      _item.map((e) => FileUpload.fromJson(e)).toList();
                }
              }
              return _temChat;
            })
            .toList()
            .reversed);
      return [];
    } catch (e) {
      printError(info: e.toString() + '_getDataMess');
      return [];
    }
  }

  Future<List<ChatMessage>> getDataMessFindPosition(
      int groupID, int messID, int limit,
      {int selectType = 1}) async {
    try {
      ///1. là lấy ở giữa
      ///2. là lấy cũ
      ///3. là lấy mới
      String rawSql = '';
      if (selectType == 3)
        rawSql = 'messageID > $messID';
      else if (selectType == 2)
        rawSql = 'messageID < $messID';
      else
        rawSql =
            '(messageID BETWEEN ${messID - limit / 2} and ${messID + limit / 2})';

      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblChatMessage where groupID = $groupID and $rawSql order by createDate desc limit $limit');
      if (data.isNotEmpty)
        return Future.wait(data
            .map((e) async {
              final ChatMessage _temChat = ChatMessage.fromJson(e);
              if (_temChat.messageType != MessageType.Text &&
                  _temChat.messageType != MessageType.Emoticon &&
                  _temChat.messageType != MessageType.Reply) {
                if (_temChat.messageID > 0) {
                  final List<FileUpload> _tempMedia = await getFiles(
                      groupID,
                      int.parse(e['messageID'].toString()),
                      _getTypeFile(_temChat.messageType));
                  _temChat.media = _tempMedia;
                } else {
                  final List _item = jsonDecode(_temChat.message!);
                  _temChat.media =
                      _item.map((e) => FileUpload.fromJson(e)).toList();
                }
              }
              return _temChat;
            })
            .toList()
            .reversed);
      return [];
    } catch (e) {
      printError(info: e.toString() + '_getDataMess');
      return [];
    }
  }

  FileType _getTypeFile(MessageType type) {
    switch (type) {
      // case MessageType.Image:
      //   return FileType.Image;
      case MessageType.Video:
        return FileType.Video;
      case MessageType.File:
        return FileType.File;
      case MessageType.Link:
        return FileType.Link;
      case MessageType.Youtube:
        return FileType.Youtube;
      case MessageType.Audio:
        return FileType.Audio;
      default:
        return FileType.Image;
    }
  }

  Future<List<GroupInfo>> getDataGroupInfo() async {
    try {
      final data = await ChatDatabase.database
          .rawQuery('select * from $tblGroupInfo order by lastUpdate desc');
      return data.map((e) => GroupInfo.fromMapObject(e)).toList();
    } catch (e) {
      return [];
    }
  }

  Future<List<GroupInfo>> getDataGroupInfoLoadMore(int pageIndex) async {
    try {
      final data = await ChatDatabase.database.rawQuery(
          'select * from $tblGroupInfo order by lastUpdate desc limit 20 offset $pageIndex');
      return data.map((e) => GroupInfo.fromMapObject(e)).toList();
    } catch (e) {
      return [];
    }
  }

  Future<GroupInfo?> checkUserInGroup(int userID) async {
    final String _query = 'select gif.* from $tblGroupInfo gif left join '
        '$tblUserInGroup uig on gif.groupID = uig.groupID where uig.userID = $userID and gif.isGroup = "false"';
    final result = await ChatDatabase.database.rawQuery(_query);
    if (result.isNotEmpty) return GroupInfo.fromMapObject(result[0]);
    return null;
  }

  Future insertNewMessageAndUpdateGroup(Map<String, dynamic> mess) async {
    final Batch batch = ChatDatabase.database.batch();
    batch.execute(
        'update $tblGroupInfo set lastMessage = "${mess['message']}",lastMessageType = ${mess['messageType']},lastMessageDate = "${mess['createDate']}",lastUpdate = "${mess['createDate']}" where groupID = ${mess['groupID']}');

    mess['isThuHoi'] = mess['isThuHoi'].toString();
    batch.insert(tblChatMessage, mess,
        conflictAlgorithm: ConflictAlgorithm.replace);
    await batch.commit();
  }

  Future insertTempUpload(ChatMessage message) async {
    message.message = jsonEncode(message.media);
    await ChatDatabase.database.transaction((txn) async {
      await txn.execute('insert into $tblChatMessage (messageID, messageKey, '
          'groupID, groupKey, userIDGui, message, messageType, createDate) values '
          "(${message.messageID}, '${message.messageKey}', ${message.groupID}, '${message.groupKey}', "
          "${message.userIDGui}, '${message.message}', ${message.messageType.index}, '${message.createDate?.toIso8601String()}')");
    });
  }

  Future insertGroupInfo({GroupInfo? info, Map<String, dynamic>? data}) async {
    try {
      if (data != null) {
        data['groupDetail']['isGroup'] =
            data['groupDetail']['isGroup'].toString();
        data['groupDetail']['isDelete'] =
            data['groupDetail']['isDelete'].toString();
        data['groupDetail']['isDonVi'] =
            data['groupDetail']['isDonVi'].toString();
        ChatDatabase.database.insert('GroupInfo', data['groupDetail']);
        final Batch batch = ChatDatabase.database.batch();
        final List list = data['userInGroupDetail'] as List;

        for (final element in list) {
          element['isDelete'] = element['isDelete'].toString();
          element['isOff'] = element['isOff'].toString();
          element['deactivated'] = element['deactivated'].toString();
          batch.insert(tblUserInGroup, element,
              conflictAlgorithm: ConflictAlgorithm.replace);
        }

        await batch.commit();
      } else {
        final List? data =
            await ApiRequest.instance.getUserInGroup(info!.groupID);
        if (data != null) {
          final Batch batch = ChatDatabase.database.batch();
          batch.insert('GroupInfo', info.toJson(false, true));
          for (final element in data) {
            element['isDelete'] = element['isDelete'].toString();
            batch.insert(tblUserInGroup, element,
                conflictAlgorithm: ConflictAlgorithm.replace);
          }
          await batch.commit();
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future insertUserInGroup(Map<String, dynamic> insertUserInGroup) async {
    final Batch batch = ChatDatabase.database.batch();
    insertUserInGroup['data'].forEach((element) {
      element['isDelete'] = element['isDelete'].toString();
      element['isOff'] = element['isOff'].toString();
      element['deactivated'] = element['deactivated'].toString();
      batch.insert(tblUserInGroup, element,
          conflictAlgorithm: ConflictAlgorithm.replace);
    });
    insertUserInGroup['group']['isGroup'] =
        insertUserInGroup['group']['isGroup'].toString();
    insertUserInGroup['group']['isDelete'] =
        insertUserInGroup['group']['isDelete'].toString();
    ChatDatabase.database.insert(tblGroupInfo, insertUserInGroup['group'],
        conflictAlgorithm: ConflictAlgorithm.replace);
    await batch.commit();
  }

  Future updateGroupIconAndGroupName(Map<String, dynamic> data) async {
    final String _rawQuery =
        'update $tblGroupInfo set groupName = "${data['groupName']}" , groupIcon = "${data['groupIcon']}" where groupID = ${data['groupID']}';
    await ChatDatabase.database.execute(_rawQuery);
  }

  Future updateDaXemGroup(int groupID) async {
    final String _rawQuery =
        'UPDATE $tblGroupInfo SET countNewMessage = countNewMessage + 1 WHERE groupID = $groupID';
    await ChatDatabase.database.transaction((txn) async {
      await txn.execute(_rawQuery);
    });
  }

  Future<int> getTotalMessNotSeen() async {
    try {
      final data = await ChatDatabase.database
          .rawQuery('select SUM(countNewMessage) as total from $tblGroupInfo');
      print(data.first['total'].toString() +
          '==========================================');
      return int.parse(data.first['total']?.toString() ?? '0');
    } catch (e) {
      return 0;
    }
  }

  Future resetDaXemGroup(int groupID) async {
    final String _rawQuery =
        'update $tblGroupInfo SET countNewMessage = 0 where groupID = $groupID';
    await ChatDatabase.database.transaction((txn) async {
      await txn.execute(_rawQuery);
    });
  }

  Future insertFileMessage(List<dynamic> files) async {
    try {
      final Batch batch = ChatDatabase.database.batch();

      for (final element in files) {
        element['isDelete'] = element['isDelete'].toString();
        batch.insert(tblFileManager, element,
            conflictAlgorithm: ConflictAlgorithm.replace);
      }
      await batch.commit();
    } catch (e) {
      print(e);
    }
  }

  Future updateTinhTrangTin(Map<String, dynamic> data) async {
    String _rawQuery = '';
    if (data['tinhTrangID'] == 2) {
      _rawQuery =
          'update $tblChatMessage set tinhTrangID = CASE tinhTrangID WHEN tinhTrangID <> 3 '
          'THEN ${data['tinhTrangID']} ELSE tinhTrangID END, '
          ' tenTinhTrang = CASE tinhTrangID WHEN tinhTrangID <> 3 '
          'THEN "${data['tenTinhTrang']}" ELSE tenTinhTrang END '
          'where messageID = ${data['messageID']}';
    }
    if (data['tinhTrangID'] == 3) {
      _rawQuery =
          'update $tblChatMessage set tinhTrangID = ${data['tinhTrangID']} ,'
          ' tenTinhTrang = "${data['tenTinhTrang']}", userIDDaXem = "${data['userIDDaXem']}" '
          'where messageID = ${data['messageID']}';
    }
    await ChatDatabase.database.transaction((txn) async {
      await txn.execute(_rawQuery);
    });

    ///isolate
    if (data['userIDGui'] == userIDChat)
      return;
    else
      ApiRequest.instance.updateTinhTrangMessage(data);

    ///
  }

  Future updateHinhNenGroup(Map<String, dynamic> groupInfo) async {
    await ChatDatabase.database.transaction((txn) async {
      final String _rawQuery =
          'update $tblGroupInfo set hinhNen = "${groupInfo['hinhNen']}" where groupID = ${groupInfo['groupID']}';
      await txn.rawUpdate(_rawQuery);
    });
  }

  Future<List<FileUpload>> getFiles(
      int groupID, int messageID, FileType fileType) async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblFileManager where groupID = $groupID and messageID = $messageID and fileType = ${fileType.index}');

      return data.map((e) => FileUpload.fromJson(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future syncOnLoad() async {
    try {
      ///isolate
      final Map<String, dynamic>? data =
          await ApiRequest.instance.appSyncOnLoad();

      ///----------
      if (data != null) {
        final Batch batch = ChatDatabase.database.batch();
        final List? _group = data['listGroupDetail'];

        if (_group != null) {
          for (final element in _group) {
            if (!element['isDelete']) {
              element['isGroup'] = element['isGroup'].toString();
              element['isDelete'] = element['isDelete'].toString();
              element['isDonVi'] = element['isDonVi'].toString();
              batch.insert(tblGroupInfo, element,
                  conflictAlgorithm: ConflictAlgorithm.replace);
            } else {
              //   batch.execute(
              //       'DELETE FROM $tblUserInGroup WHERE groupID = ${data['groupID']} and userID = ${data['userID']}');
              batch.rawDelete('DELETE FROM $tblGroupInfo WHERE groupID = (?)',
                  [element['groupID']]);
            }
          }
        }

        final List? _userInGroup = data['listUserInGroupDetail'];

        if (_userInGroup != null) {
          for (final element in _userInGroup) {
            if (!element['isDelete']) {
              element['isDelete'] = element['isDelete'].toString();
              element['isOff'] = element['isOff'].toString();
              element['deactivated'] = element['deactivated'].toString();
              batch.insert(tblUserInGroup, element,
                  conflictAlgorithm: ConflictAlgorithm.replace);
            } else {
              if (element['userID'] == userIDChat) {
                batch.rawDelete(
                    'DELETE FROM $tblUserInGroup WHERE groupID = (?)',
                    [element['groupID']]);

                batch.rawDelete('DELETE FROM $tblGroupInfo WHERE groupID = (?)',
                    [element['groupID']]);

                batch.rawDelete(
                    'DELETE FROM $tblChatMessage WHERE groupID = (?)',
                    [element['groupID']]);

                batch.rawDelete(
                    'DELETE FROM $tblFileManager WHERE groupID = (?)',
                    [element['groupID']]);

                batch.rawDelete(
                    'DELETE FROM $tblGroupGhimMess WHERE groupID = (?)',
                    [element['groupID']]);
              } else
                batch.rawDelete(
                    'DELETE FROM $tblUserInGroup WHERE groupID = (?) and userID = (?)',
                    [element['groupID'], element['userID']]);
            }
          }
        }

        final List? _mess = data['listMessagesDetail'];

        if (_mess != null) {
          for (final element in _mess) {
            if (!element['isThuHoi']) {
              element['isThuHoi'] = element['isThuHoi'].toString();
              batch.insert(tblChatMessage, element,
                  conflictAlgorithm: ConflictAlgorithm.replace);
            } else {
              batch.rawDelete(
                  'UPDATE $tblChatMessage SET isThuHoi = "true" WHERE groupID = (?) and messageID = (?)',
                  [element['groupID'], element['messageID']]);

              if (element['messageType'] != 0)
                batch.rawDelete(
                    'DELETE FROM $tblFileManager WHERE MessageID = (?)',
                    [element['messageID']]);
            }
          }
        }

        final List? _files = data['listFileManager'];

        if (_files != null) {
          for (final element in _files) {
            if (!element['isDelete']) {
              element['isDelete'] = element['isDelete'].toString();
              batch.insert(tblFileManager, element,
                  conflictAlgorithm: ConflictAlgorithm.replace);
            } else {
              batch.rawDelete(
                  'DELETE FROM $tblFileManager WHERE groupID = (?) and messageID = (?)',
                  [element['groupID'], element['messageID']]);
            }
          }
        }

        final List? _ghimMess = data['listGroupGhimMess'];

        if (_ghimMess != null) {
          for (final element in _ghimMess) {
            if (!element['isDelete']) {
              element['isDelete'] = element['isDelete'].toString();
              batch.insert(tblGroupGhimMess, element,
                  conflictAlgorithm: ConflictAlgorithm.replace);
            } else {
              batch.rawDelete(
                  'DELETE FROM $tblGroupGhimMess WHERE groupID = (?) and id = (?)',
                  [element['groupID'], element['id']]);
            }
          }
        }

        // final List? _tinhTrangMess = data['listTinhTrangMess'];
        //
        // if (_tinhTrangMess != null) {
        //   for (final element in _tinhTrangMess) {
        //     // if (!element['isDelete']) {
        //     //   element['isDelete'] = element['isDelete'].toString();
        //     batch.insert(tblTinhTrang, element,
        //         conflictAlgorithm: ConflictAlgorithm.replace);
        //     // } else {
        //     //   batch.rawDelete(
        //     //       'DELETE FROM $tblGroupGhimMess WHERE groupID = (?) and id = (?)',
        //     //       [element['groupID'], element['id']]);
        //     // }
        //   }
        // }

        await batch.commit(noResult: true);
      }
    } catch (e) {
      print(e);
    }
  }

  Future thuHoiMessageByMessageID(Map<String, dynamic> data) async {
    // oldMessage la mess bi thu hoi
    if (data['oldMessageID'] == null) return;
    final List<int> _listMessageID = (data['oldMessageID'] as String)
        .split(',')
        .map((e) => int.parse(e))
        .toList();

    if (_listMessageID.isNotEmpty) {
      final Batch batch = ChatDatabase.database.batch();

      for (final element in _listMessageID) {
        batch.rawDelete(
            'UPDATE $tblChatMessage SET isThuHoi = "true" WHERE MessageID = (?)',
            [element]);
        if (data['messageType'] != 0)
          batch.rawDelete(
              'DELETE FROM $tblFileManager WHERE MessageID = (?)', [element]);
      }

      // newMessage is mess lien truoc => update last mess in screen group info
      if (data['newMessageID'] > 0) {
        batch.execute(
            'UPDATE $tblGroupInfo SET lastMessage = "${data['message']}", lastMessageType = 0, lastMessageDate = "${data['lastMessageDate']}" Where groupID = ${data['groupID']}');
      }

      await batch.commit();
    }
  }

  Future deleteMessageByMessageID(Map<String, dynamic> data) async {
    // oldMessage la mess bi xoa
    if (data['oldMessageID'] == null) return;
    final List<int> _listMessageID = (data['oldMessageID'] as String)
        .split(',')
        .map((e) => int.parse(e))
        .toList();

    if (_listMessageID.isNotEmpty) {
      final Batch batch = ChatDatabase.database.batch();

      for (final element in _listMessageID) {
        batch.rawDelete(
            'DELETE FROM $tblChatMessage WHERE MessageID = (?)', [element]);
        if (data['messageType'] != 0)
          batch.rawDelete(
              'DELETE FROM $tblFileManager WHERE MessageID = (?)', [element]);
      }

      // newMessage is mess lien truoc => update last mess in screen group info
      if (data['newMessageID'] > 0) {
        batch.execute(
            'UPDATE $tblGroupInfo SET lastMessage = "${data['message']}", lastMessageType = ${data['messageType']}, lastMessageDate = "${data['lastMessageDate']}" Where groupID = ${data['groupID']}');
      }

      await batch.commit();
    }
  }

  Future deleteUserInGroup(Map<String, dynamic> data) async {
    final Batch batch = ChatDatabase.database.batch();
    if (data['userID'] == userIDChat) {
      batch.execute(
          'DELETE FROM $tblGroupInfo WHERE groupID = ${data['groupID']}');
    }

    batch.execute(
        'DELETE FROM $tblUserInGroup WHERE groupID = ${data['groupID']} and userID = ${data['userID']}');
    await batch.commit();
  }

  Future deleteMesageGhost(int groupID) async {
    final Batch batch = ChatDatabase.database.batch();
    batch.execute(
        'DELETE FROM $tblChatMessage WHERE groupID = $groupID and messageID = 0');
    await batch.commit();
  }

  Future deleteGroupByGroupID(int groupID) async {
    await ChatDatabase.database.transaction((txn) async {
      final Batch batch = txn.batch();
      batch.execute('DELETE FROM $tblUserInGroup WHERE groupID = $groupID');
      batch.execute('DELETE FROM $tblGroupInfo WHERE groupID = $groupID');
      await batch.commit();
    });

    await deleteHistoryMessageByGroupID(groupID);
  }

  Future deleteHistoryMessageByGroupID(int groupID) async {
    await ChatDatabase.database.transaction((txn) async {
      final Batch batch = txn.batch();
      batch.execute('DELETE FROM $tblChatMessage WHERE groupID = $groupID');
      batch.execute('DELETE FROM $tblFileManager WHERE groupID = $groupID');
      batch.execute('DELETE FROM $tblGroupGhimMess WHERE groupID = $groupID');
      batch.execute(
          '''UPDATE $tblGroupInfo SET lastMessage = "", lastMessageType = 0, lastMessageDate = null, countNewMessage = 0 WHERE groupID = $groupID''');
      await batch.commit();
    });
  }

  Future deleteAllTable() async {
    final Batch batch = ChatDatabase.database.batch();
    batch.execute('DELETE FROM $tblUserInGroup');
    batch.execute('DELETE FROM $tblGroupInfo');
    batch.execute('DELETE FROM $tblDataUsers');
    batch.execute('DELETE FROM $tblChatMessage');
    batch.execute('DELETE FROM $tblFileManager');
    batch.execute('DELETE FROM $tblGroupGhimMess');
    await batch.commit();
  }

  Future<GroupInfo?> checkHasGroup2User(int userID) async {
    final List<
        Map<String,
            Object?>> data = await ChatDatabase.database.rawQuery(
        'SELECT * FROM $tblGroupInfo WHERE isGroup = "false" AND createUserID = $userID');
    if (data.isNotEmpty) return GroupInfo.fromMapObject(data.first);
    return null;
  }

  Future<List<ChatMessage>?> getSearchMessageTextInGroup(
      {required int groupID,
      required int limit,
      required int pageIndex,
      required String query}) async {
    try {
      if (query.isEmpty) return null;
      pageIndex *= limit;
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'SELECT * FROM $tblChatMessage WHERE groupID = $groupID AND messageType = 0 AND isThuHoi = "false" AND message LIKE "%$query%" order by messageID desc limit $limit offset $pageIndex');
      if (data.isNotEmpty)
        return data.map((e) => ChatMessage.fromJson(e)).toList();
      else
        return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<DataUsers>?> getDanhBa() async {
    final List<Map<String, Object?>> data = await ChatDatabase.database
        .rawQuery('SELECT * FROM $tblDataUsers order by fullName asc');
    if (data.isNotEmpty)
      return data.map((e) => DataUsers.fromJson(e)).toList();
    else
      return null;
  }

  Future<List<DataUsers>?> getDataUserNoGroupWithNumber(
      {required int number}) async {
    final String _query = 'select dtuser.* from $tblDataUsers dtuser '
        'where dtuser.userID not in '
        '(select usog.userID from $tblUserInGroup usog inner join $tblGroupInfo ginfo '
        'on usog.groupID = ginfo.groupID where ginfo.isGroup = "false") order by fullName asc limit $number';
    final result = await ChatDatabase.database.rawQuery(_query);
    if (result != null && result.isNotEmpty)
      return result.map((e) => DataUsers.fromJson(e)).toList();
    return null;
  }

  Future<List<DataUsers>> getDataUserNoGroup() async {
    const String _query = 'select dtuser.* from $tblDataUsers dtuser '
        'where dtuser.userID not in '
        '(select usog.userID from $tblUserInGroup usog inner join $tblGroupInfo ginfo '
        'on usog.groupID = ginfo.groupID where ginfo.isGroup = "false") order by fullName asc';
    final List<Map<String, Object?>> result =
        await ChatDatabase.database.rawQuery(_query);
    if (result.isNotEmpty)
      return result.map((e) => DataUsers.fromJson(e)).toList();
    return [];
  }

  Future<GroupInfo?> checkGroupChatDonVi(String groupKey) async {
    final List<Map<String, Object?>> data = await ChatDatabase.database
        .rawQuery('SELECT * FROM $tblGroupInfo WHERE groupKey = "$groupKey"');
    if (data.isNotEmpty) return GroupInfo.fromMapObject(data.first);
    return null;
  }

  Future<Map<String, dynamic>> updataGhimMessage(
      Map<String, dynamic> dataGhim) async {
    await ChatDatabase.database.transaction((txn) async {
      final Batch _batch = txn.batch();
      const String rawUpdate =
          'update $tblGroupInfo set ghimMessage = ?, lastUpdate = ? where groupID = ?';
      _batch.rawUpdate(rawUpdate, [
        dataGhim['ghimMessage'],
        dataGhim['lastUpdate'],
        dataGhim['groupID']
      ]);

      if (dataGhim['id'] > 0) {
        dataGhim['isDelete'] = dataGhim['isDelete'].toString();
        _batch.insert(tblGroupGhimMess, dataGhim);
      }

      await _batch.commit(noResult: false);
    });

    return {
      'groupID': dataGhim['groupID'],
      'ghimMessage': dataGhim['ghimMessage']
    };
  }

  Future updateMessagReaction(Map<String, dynamic> data) async {
    await ChatDatabase.database.transaction((txn) async {
      await txn.rawUpdate(
          'update $tblChatMessage set reaction = (?) where groupID = (?) and messageID = (?)',
          [
            data['reaction'],
            data['groupID'],
            data['messageID'],
          ]);
    });
  }

  Future<bool> checkOffTinNhan(int userID, int groupID) async {
    final List<
        Map<String,
            Object?>> data = await ChatDatabase.database.rawQuery(
        'SELECT * FROM $tblUserInGroup WHERE groupID = $groupID and userID = $userID');
    if (data.isNotEmpty)
      return data
          .map((e) => e['isOff'].toString().toLowerCase() == 'true')
          .first;
    return false;
  }

  Future updateOffTinNhan(int userID, int groupID, bool isOff) async {
    await ChatDatabase.database.transaction((txn) async {
      await txn.rawUpdate(
          'update $tblUserInGroup set isOff = "${isOff.toString()}" where groupID = $groupID and userID = $userID');
    });
  }

  Future<List<UserInGroup>?> getListUserInGroupByGroupID(int groupID) async {
    final List<Map<String, Object?>> data = await ChatDatabase.database
        .rawQuery('select * from $tblUserInGroup where groupID = $groupID');
    if (data.isNotEmpty)
      return data.map((e) => UserInGroup.fromJson(e)).toList();
    return null;
  }

  Future<List<GroupInfo>> getDataOnlyGroup() async {
    try {
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblGroupInfo WHERE isGroup = "true" order by lastUpdate desc');
      if (data.isNotEmpty)
        return data.map((e) => GroupInfo.fromMapObject(e)).toList();
      return [];
    } catch (e) {
      return [];
    }
  }

  ///++++++++++++++++++++++++++++++++++++
  /// begin search chat
  Future<List<GroupInfo>?> searchGroupName({required String query, required int pageIndex}) async {
    try {
      final int offset = 20*pageIndex;
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'select * from $tblGroupInfo WHERE isGroup = "true" AND groupName LIKE "%$query%" order by lastUpdate desc limit 20 offset $offset');
      if (data != null)
        return data.map((e) => GroupInfo.fromMapObject(e)).toList();
      return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<ChatMessage>?> searchMessageText({required String query, required int pageIndex}) async {
    try {
      final int offset = 20*pageIndex;
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'SELECT * FROM $tblChatMessage WHERE messageType = 0 AND isThuHoi = "false" AND message LIKE "%$query%" order by messageID desc limit 20 offset $offset');
      if (data.isNotEmpty)
        return data.map((e) => ChatMessage.fromJson(e)).toList();
      else
        return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<DataUsers>?> searchNameDataUser({required String query, required int pageIndex}) async {
    try {
      final int offset = 20*pageIndex;
      final List<
          Map<String,
              Object?>> data = await ChatDatabase.database.rawQuery(
          'SELECT * FROM $tblDataUsers WHERE fullName LIKE "%$query%" AND userID != $userIDChat order by fullName desc limit 20 offset $offset');
      if (data.isNotEmpty)
        return data.map((e) => DataUsers.fromJson(e)).toList();
      else
        return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }
  /// end search chat
  ///+++++++++++++++++++++++++++++++++++++++++++

  Future updateTinhTrangMess(Map<String, dynamic> data) async {
    try {
      // final String rawQuery =
      //     'update $tblChatMessage set fullNameDaXem = "${data['fullNameDaXem']}" where messageID = ${data['messageID']}';
      data['isThuHoi'] = data['isThuHoi'].toString();
      ChatDatabase.database.insert(tblChatMessage, data,
          conflictAlgorithm: ConflictAlgorithm.replace);
    } catch (e) {
      print(e);
    }
  }

  Future<String> getTinhTrangByMessID(int messID) async {
    final String rawQuery =
        'select fullNameDaXem from $tblChatMessage where messageID = $messID';
    final List<Map<String, Object?>> data =
        await ChatDatabase.database.rawQuery(rawQuery);
    if (data.isNotEmpty) {
      return data.first.values.first != null
          ? data.first.values.first! as String
          : '';
    }
    return '';
  }
}
