import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/customlayout/loading_widget.dart';
import 'package:chat_package/services/signal_r.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/file_result.dart';
import 'package:chat_package/src/model/trao_doi/file_upload.dart';
import 'package:chat_package/src/model/trao_doi/ghim_mess_model.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/traodoi/chuyen_tiep/chuyen_tiep_controller.dart';
import 'package:flutter_core_getx_dev/app/data/core_http.dart';
import 'package:flutter_core_getx_dev/app/data/storage.dart';
import 'package:flutter_core_getx_dev/app/services/core_up_download_service/up_download_package.dart';
import 'package:flutter_core_getx_dev/app/services/media_picker.dart';
import 'package:flutter_core_getx_dev/app/utils/strings.dart';
import 'package:flutter_uploader/flutter_uploader.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

import '../chat_package.dart';
import '../src/model/trao_doi/message.dart';

class ApiRequest {
  ApiRequest._();
  static late ApiRequest instance = ApiRequest._();
  static ApiRequest resetInstance() {
    instance = ApiRequest._();
    return instance;
  }

  Future downloadMediaInChat(FileUpload file) async {
    LoadingWidget.instance.show();
    final CreateFolderModel _data = await DownloadFile.instance
        .checkAndCreateCacheFolder(file.fileName!, appFolder: appNameChat);
    if (_data != null) {
      try {
        if (!_data.isExists) {
          await DownloadFile.instance
              .downLoadFile('$baseUrlApiChat${file.fileUrl}', _data.filePath)
              .then((value) async {
            if (value != null) await ImageGallerySaver.saveFile(value);
            LoadingWidget.instance.hide();
            LoadingWidget.instance
                .showSnackbar(noiDung: 'Tải xuống thành công');
          });
        } else
          LoadingWidget.instance.hide();
        LoadingWidget.instance.showSnackbar(noiDung: 'Tải xuống thành công');
      } catch (e) {
        print(e);
      }
    }
  }

  Future<List<FileResults>?> uploadFile({required List<File> files}) async {
    try {
      final String _url = baseUrlApiChat + '/api/TraoDoi/FileUpload';
      final data = await CoreHttp.instance.postUploadFile(_url, files: files);
      if (data != null) {
        final images =
            List<FileResults>.from(data.map((e) => FileResults.fromJson(e)));
        return images;
      }
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<String?> uploadBackgroud({required List<File> files}) async {
    final String _url = baseUrlApiChat + '/api/TraoDoi/FileUpload';
    // final String _auth = 'Basic ' +
    //     base64Encode(
    //         utf8.encode('${LocalVariable.apiUser}:${LocalVariable.apiPass}'));
    try {
      final String taskID =
          await CoreHttp.instance.uploadBackgroud(_url, files);
      return taskID;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future deleteToken(int userID, String userName, String token) async {
    final String _url = '$baseUrlApiChat/api/TraoDoi/DeleteToken';
    await CoreHttp.instance.postAsync(
        _url, {'userID': userID, 'userName': userName, 'token': token});
  }

  // trao doi v2

  Future<List<dynamic>?> appTraoDoiV2GetDataUsers() async {
    try {
      final String _url =
          baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_GetDataUsers';
      final List? _data = await CoreHttp.instance.getAsync(_url);
      if (_data != null) {
        return _data;
      }
      return null;
    } on Exception catch (e) {
      print(e);
      return null;
    }
  }

  Future<GroupInfo?> createGroupMessage(GroupInfo groupI) async {
    final String _url =
        '$baseUrlApiChat/api/TraoDoi/App_TraoDoi_V2_CreateGroup';
    final Map<String, dynamic>? _data =
        await CoreHttp.instance.postAsync(_url, groupI.toJson(true, false));
    if (_data != null) {
      return GroupInfo.fromMapObject(_data);
    }
    return null;
  }

  Future<List<dynamic>?> getUserInGroup(int groupID) async {
    final String _url = baseUrlApiChat +
        '/api/TraoDoi/App_TraoDoi_V2_GetUserInGroupDetail/$groupID';
    final List? _data = await CoreHttp.instance.getAsync(_url);
    return _data;
  }

  Future<List<String>?> getListHinhNenChat() async {
    final String _url = baseUrlApiChat + '/api/TraoDoi/GetHinhNen';
    final List? _data = await CoreHttp.instance.getAsync(_url);
    if (_data != null) return List<String>.from(_data);
    return null;
  }

  Future<Map<String, dynamic>?> appSyncOnLoad() async {
    String _lastSync = Storage.instance.readString(LocalVariable.lastUpdate);
    if (_lastSync.isEmpty)
      _lastSync =
          DateTime.now().add(const Duration(days: -180)).toIso8601String();
    final String _url = baseUrlApiChat + '/api/TraoDoi/App_SyncOnLoad';

    final Map<String, dynamic> prams = {
      'UserID': userIDChat,
      'UserName': userNameChat,
      'LastUpdate': _lastSync
    };
    final Map<String, dynamic>? _data =
        await CoreHttp.instance.postAsync(_url, prams);
    if (_data != null) {
      Storage.instance.writeString(LocalVariable.lastUpdate,
          DateTime.now().add(const Duration(seconds: -10)).toIso8601String());
      return _data;
    }
    return null;
  }

  Future updateGroupIconAndGroupName(GroupInfo groupInfo) async {
    try {
      final String _url = baseUrlApiChat +
          '/api/TraoDoi/App_TraoDoi_V2_UpdateGroupIconAndGroupName';
      await CoreHttp.instance.postAsync(_url, groupInfo.toJson(false, false));
    } on Exception catch (e) {
      print(e);
    }
  }

  Future insertUserInGroup(GroupInfo groupInfo, List<DataUsers> users) async {
    final List<UserInGroup>? userInGroups =
        await SyncChat.instance.getListUserInGroupByGroupID(groupInfo.groupID);
    groupInfo.groupIcon = groupInfo.groupIcon?.replaceAll(baseUrlApiChat, '');
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_InsertUserInGroup';
    if (userInGroups != null) {
      userInGroups.addAll(users.map((e) {
        return UserInGroup(
            groupID: groupInfo.groupID,
            groupKey: groupInfo.groupKey,
            userID: e.userID,
            userName: e.userName);
      }).toList());
    }

    final Map<String, dynamic> _prams = {
      'data': userInGroups?.map((e) => e.toJson()).toList(),
      'group': groupInfo.toJson(false, false)
    };
    await CoreHttp.instance.postAsync(_url, _prams);
  }

  Future deleteAllMessageByGroupId(int groupID) async {
    final String _url = baseUrlApiChat +
        '/api/TraoDoi/App_TraoDoi_V2_DeleteHistoryMessage/$groupID';
    await CoreHttp.instance.getAsync(_url);
  }

  Future deleteGroup(int groupID) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_DeleteGroup/$groupID';
    await CoreHttp.instance.getAsync(_url);
  }

  Future<int> deleteHistoryChat(Map<String, dynamic> param) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_CleanMessageByUserID';
    final int result = (await CoreHttp.instance.postAsync(_url, param)) ?? -1;
    return result;
  }

  Future updateTinhTrangMessage(Map<String, dynamic> data) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_UpdateTinhTrangMessage';
    CoreHttp.instance.postAsync(_url, data);
  }

  Future deleteMessageByMessageID(
      List<ChatMessage> messages, int index, int userId, int lastIndex) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_DeleteMessageByMessageID';
    final Map<String, dynamic> data = {
      'messages': messages.map((e) => e.toJson()).toList(),
      'index': index,
      'userID': userId,
      'lastIndex': lastIndex
    };
    await CoreHttp.instance.postAsync(_url, data);
  }

  Future thuHoiMessageByMessageID(
      List<ChatMessage> messages, int index, int lastIndex) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_ThuHoiMessageByMessageID';
    final Map<String, dynamic> data = {
      'messages': messages.map((e) => e.toJson()).toList(),
      'index': index,
      'lastIndex': lastIndex
    };
    await CoreHttp.instance.postAsync(_url, data);
  }

  Future deleteUserInGroup(GroupInfo groupInfo, DataUsers user) async {
    final String _url =
        baseUrlApiChat + '/api/TraoDoi/App_TraoDoi_V2_DeleteUserInGroup';
    final UserInGroup _userInGroup = UserInGroup(
        userID: user.userID,
        userName: user.userName,
        groupID: groupInfo.groupID,
        groupKey: groupInfo.groupKey);
    await CoreHttp.instance.postAsync(_url, _userInGroup.toJson());
  }

  Future updateHinhNenGroup(GroupInfo groupInfo) async {
    try {
      final String _url = baseUrlApiChat +
          '/api/TraoDoi/App_TraoDoi_V2_UpdateHinhNenGroupByGroupID';
      await CoreHttp.instance.postAsync(_url, groupInfo.toJson(false, false));
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<List<DataUsers>?> getDataUserInQT(
      int vanBanID, String loaiVanBan) async {
    final Map<String, dynamic> params = {
      'vanBanID': vanBanID,
      'loaiVanBan': loaiVanBan
    };
    final String url = baseUrlApiChat + '/api/vanbanchung/VB_GetUserInQuaTrinh';
    final List? _data = await CoreHttp.instance.postAsync(url, params);
    if (_data != null) {
      return _data.map((e) => DataUsers.fromJson(e)).toList();
    }
    return null;
  }

  Future<bool> deleteConnection(
      {required int userID,
      required String token,
      required String deviceID}) async {
    final Map<String, dynamic> _params = {
      'userID': userID,
      'token': token,
      'deviceID': deviceID,
      'connectionID': SignalR.instance.connection.connectionId,
    };
    final String _url =
        baseUrlApiChat + '/api/traodoi/App_TraoDoi_V2_DeleteConnection';
    final bool? _data = await CoreHttp.instance.postAsync(_url, _params);
    if (_data == null || !_data)
      return false;
    else {
      await SignalR.instance.connection.stop();
      await SyncChat.instance.deleteAllTable();
      Storage.instance.removeKey(apiAuthBearerOrBasic);
      Storage.instance.removeKey('isLoginApp');
      return true;
    }
  }

  Future appTraoDoiV2GhimMessage(GroupInfo params) async {
    final String url =
        baseUrlApiChat + '/api/traodoi/App_TraoDoi_V2_GhimMessage';
    await CoreHttp.instance.postAsync(url, params.toJson(false, false));
  }

  Future appTraoDoiV2Reaction(Map<String, dynamic> params) async {
    params.putIfAbsent('userIDGui', () => userIDChat);
    final String url =
        baseUrlApiChat + '/api/traodoi/App_TraoDoi_V2_UpdateMessReaction';
    await CoreHttp.instance.postAsync(url, params);
  }

  Future<bool> appTraoDoiV2TatThongBao(Map<String, dynamic> params) async {
    final String _url =
        baseUrlApiChat + '/api/traodoi/App_TraoDoi_V2_TatThongBao';
    final bool? _data = await CoreHttp.instance.postAsync(_url, params);
    if (_data != null) {
      final bool isOff = _data;
      return isOff;
    }
    return false;
  }

  Future<List<GhimMessModel>?> getAllMessageGhimByGroupID(int groupID) async {
    final String url = baseUrlApiChat +
        '/api/traodoi/App_TraoDoi_V2_GetAllMessageGhimByGroupID/$groupID';
    final List? _data = await CoreHttp.instance.getAsync(url);
    if (_data != null) {
      return _data.map((e) => GhimMessModel.fromJson(e)).toList();
    }
    return null;
  }

  Future chuyenTiepMessage(
      ChatMessage message, List<ItemChuyenTiep> data) async {
    final String url =
        baseUrlApiChat + '/api/traodoi/App_TraoDoi_V2_ChuyenTiepMessage';
    final DataUsers? myDataUser =
        await SyncChat.instance.getDataUserByID(userIDChat);
    final List<GroupInfo> groups = <GroupInfo>[];
    final List<DataUsers> users = <DataUsers>[];
    for (final e in data) {
      if (e.type == 'user')
        users.add(e.user!);
      else
        groups.add(e.group!);
    }
    final params = {
      'ListGroup': groups.map((e) => e.toJson(false, false)).toList(),
      'Message': message.toJson(isMedia: true),
      'ListUser': users.map((e) => e.toJson()).toList(),
      'MyDataUser': myDataUser!.toJson()
    };
    await CoreHttp.instance.postAsync(url, params);
    Get.back();
  }

  Future<UploadTaskResponse> compressAndUpload(List<File> files) async {
    final String _url = baseUrlApiChat + '/api/TraoDoi/FileUpload';
    // final String _auth = 'Basic ' +
    //     base64Encode(
    //         utf8.encode('${LocalVariable.apiUser}:${LocalVariable.apiPass}'));
    final List<File> _filesCompress = await MediaPicker.instance
        .compressMedia(files, quality: 80, isThumb: true);
    final String _taskID =
        await CoreHttp.instance.uploadBackgroud(_url, _filesCompress);
    final UploadTaskResponse dataUpload = await UploadFile.instance.uploadResult
        .firstWhere(
            (element) => element.taskId == _taskID && element.response != null);
    return dataUpload;
  }

  Future<void> updateCountNewMess(Map<String, dynamic> param) async {
    try {
      final String _url =
          baseUrlApiChat + '/api/TraoDoi/InsertCountMessengerNew';
      await CoreHttp.instance.postAsync(_url, param);
    } on Exception catch (e) {
      print(e);
    }
  }
}
