import 'dart:async';

import 'package:chat_package/base/base_local_rx.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:chat_package/services/api_resquest.dart';
import 'package:chat_package/services/sync_chat.dart';
import 'package:chat_package/src/model/trao_doi/data_users.dart';
import 'package:chat_package/src/model/trao_doi/group_info.dart';
import 'package:chat_package/src/model/trao_doi/message.dart';
import 'package:chat_package/src/traodoi/message_detail/message_detail_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/data/storage.dart';
import 'package:get/get.dart';
import 'package:signalr_core/signalr_core.dart';

import '../chat_package.dart';

class SignalR {
  static late SignalR instance = SignalR._();
  static SignalR resetInstance() {
    instance = SignalR._();
    return instance;
  }

  Map<String, dynamic> param = {
    'UserID': userIDChat,
    'UserName': userNameChat,
    'DeviceID': deviceIDChat
  };

  late HubConnection connection;
  SignalR._() {
    connection = HubConnectionBuilder()
        .withUrl(
          '$baseUrlApiChat/chatHub',
          HttpConnectionOptions(
            logging: (level, message) => print(message),
          ),
        )
        .withAutomaticReconnect()
        .build();

    connection.keepAliveIntervalInMilliseconds = 3600000;
    connection.serverTimeoutInMilliseconds = 86400000;
    connection.onreconnected((exception) async {
      print('testConnect : on  reconnected');
      SyncChat.instance.syncOnLoad();
      // param.putIfAbsent('ConnectionID', () => connection.connectionId);
      await connection.send(methodName: 'UserConnect', args: [param]);
    });

    connection.onclose((exception) {
      print(exception);
      print('testConnect : on close');
    });

    _onSignalR();
  }

  Future startServer({isConnect = false, isReload = false}) async {
    //isConnect = true khi check internet
    //isReload = true khi AppLifecycleState.resumed

    try {
      switch (connection.state) {
        case HubConnectionState.disconnected:
          print('testConnect : disconnected');
          if (isReload) SyncChat.instance.syncOnLoad();
          await connection.start();
          await connection.send(methodName: 'UserConnect', args: [param]);
          break;
        case HubConnectionState.connecting:
          print('testConnect : connecting');
          break;
        case HubConnectionState.connected:
          print('testConnect : connected');

          if (isConnect) {
            param.putIfAbsent('ConnectionID', () => connection.connectionId);
            await connection.send(methodName: 'UserConnect', args: [param]);
          }
          break;
        case HubConnectionState.disconnecting:
          print('testConnect : disconnecting');
          break;
        case HubConnectionState.reconnecting:
          print('testConnect : reconnecting');
          break;
        default:
          break;
      }
    } catch (e) {
      print(e);
    }
  }

  void _onSignalR() {
    connection.on(LocalVariable.receiveMessage, (arguments) async {
      Storage.instance.writeString(LocalVariable.lastUpdate,
          DateTime.now().add(const Duration(seconds: -10)).toIso8601String());

      await SyncChat.instance.insertNewMessageAndUpdateGroup(arguments?[0]);

      if (Get.currentRoute != MessageDetailUI.ROUTER_NAME &&
          arguments?[0]['userIDGui'] != userIDChat &&
          currentGroupId.value != arguments?[0]['groupID']) {
        // update new mess in group screen
        SyncChat.instance.updateDaXemGroup(arguments?[0]['groupID']);

        // count new mess in bottom bar
        countNewMessInChatConfig += 1;
        final Map<String, dynamic> _param = {
          'groupID': arguments?[0]['groupID'],
          'userID': arguments?[0]['userIDGui'],
          'count': countNewMessInChatConfig
        };
        ApiRequest.instance.updateCountNewMess(_param);
        countNewMessInChatConfigCtr.sink.add(countNewMessInChatConfig);
      }

      chatMessageCtrl.value =
          ChatMessage.fromJson(arguments?[0], files: arguments?[1]);

      if (arguments?[0]['userIDGui'] != userIDChat &&
          (currentGroupObj.value.groupID == 0 ||
              currentGroupObj.value.groupID != arguments?[0]['groupID'])) {
        final Map<String, dynamic> pram = {
          'groupID': arguments?[0]['groupID'],
          'groupKey': arguments?[0]['groupKey'],
          'messageID': arguments?[0]['messageID'],
          'userID': userIDChat,
          'userIDGui': arguments?[0]['userIDGui'],
          'tinhTrangID': 2,
          'tenTinhTrang': 'Đã nhận',
        };
        connection
            .send(methodName: LocalVariable.updateTinhTrangTin, args: [pram]);
      }
    });

    connection.on(LocalVariable.getGroupCreated, (arguments) async {
      await SyncChat.instance.insertGroupInfo(data: arguments?[0]);
      final GroupInfo _temGroup =
          GroupInfo.fromMapObject(arguments?[0]['groupDetail']);
      createGroupInfoCtrl.value = _temGroup;
      if (_temGroup.createUserID == userIDChat && _temGroup.isGroup) {
        // create group success, go screen mess detail of group
        final List list = arguments?[0]['userInGroupDetail'] as List;
        Get.offAndToNamed(MessageDetailUI.ROUTER_NAME, arguments: {
          'itemUser': list.map((e) => DataUsers.fromJson(e)).toList(),
          'groupInfo': _temGroup
        });
      }
    });

    connection.on(LocalVariable.getFileMessage, (arguments) async {
      await SyncChat.instance.insertFileMessage(arguments?[0]);
    });

    connection.on(LocalVariable.updateTinhTrangTin, (arguments) async {
      updateTinhTrangCtrl.value = arguments?[0];
      await SyncChat.instance.updateTinhTrangTin(arguments?[0]);
    });

    connection.on(LocalVariable.UpdateTinhTrangMess, (arguments) async {
      SyncChat.instance.updateTinhTrangMess(arguments?[0]);
    });

    connection.on(LocalVariable.getInsertUserInGroup, (arguments) async {
      await SyncChat.instance.insertUserInGroup(arguments?[0]);
      insertUserInGroupCtrl.value =
          GroupInfo.fromMapObject(arguments?[0]['group']);
    });

    connection.on(LocalVariable.deleteGroupByGroupID, (arguments) async {
      await SyncChat.instance.deleteGroupByGroupID(arguments?[0]);
      deleteGroupCtrl.value = arguments?[0];
    });

    connection.on(LocalVariable.deleteHistoryMessageByGroupID,
        (arguments) async {
      await SyncChat.instance.deleteHistoryMessageByGroupID(arguments?[0]);
      deleteHistoryMessageCtrl.value = arguments?[0];
    });

    connection.on(LocalVariable.updateGroupIconAndGroupName, (arguments) async {
      await SyncChat.instance.updateGroupIconAndGroupName(arguments?[0]);
      updateGroupIconAndGroupNameCtrl.value = arguments?[0];
    });

    connection.on(LocalVariable.thuHoiMessageByMessageID, (arguments) async {
      await SyncChat.instance.thuHoiMessageByMessageID(arguments?[0]);
      thuHoiMessageByMessageIDCtrl.value = arguments?[0];
    });

    // listen to update for other user with account same
    connection.on(LocalVariable.deleteMessageByMessageID, (arguments) async {
      await SyncChat.instance.deleteMessageByMessageID(arguments?[0]);
      deleteMessageByMessageIDCtrl.value = arguments?[0];
    });

    connection.on(LocalVariable.deleteUserInGroup, (arguments) async {
      await SyncChat.instance.deleteUserInGroup(arguments?[0]);
      deleteUserInGroupCtrl.value = UserInGroup.fromJson(arguments?[0]);

      if (arguments?[0]['userID'] == userIDChat &&
          currentGroupObj.value.groupID > 0) {
        Get.until(ModalRoute.withName(rootPageRouter));
      }
    });

    connection.on(LocalVariable.updateHinhNenGroup, (arguments) async {
      await SyncChat.instance.updateHinhNenGroup(arguments?[0]);
    });

    connection.on(LocalVariable.updateGhimMessager, (arguments) async {
      final Map<String, dynamic> _temGroup =
          await SyncChat.instance.updataGhimMessage(arguments?[0]);
      updateGhimMess.value = _temGroup;
    });

    connection.on(LocalVariable.updateMessageReaction, (arguments) async {
      updateReaction.value = arguments?[0];
      await SyncChat.instance.updateMessagReaction(arguments?[0]);
    });
  }

  // void updateTinhTrangReConnect() {
  //   if (fireBaseOnLoad != null) {
  //     final Map<String, dynamic> arg = jsonDecode(fireBaseOnLoad!['arguments']);
  //     SyncChat.instance
  //         .getTinhTrangByCreateDate(groupID: arg['groupID'])
  //         .then((value) {
  //       value.map((e) {
  //         e.putIfAbsent('userID', () => userIDChat);
  //         e.putIfAbsent('tinhTrangID', () => 2);
  //         e.putIfAbsent('tenTinhTrang', () => 'Đã nhận');
  //
  //         // connection
  //         //     .send(methodName: LocalVariable.updateTinhTrangTin, args: [e]);
  //       });
  //     });
  //   } else
  //     SyncChat.instance.getTinhTrangByCreateDate().then((value) {
  //       value.map((e) {
  //         e.putIfAbsent('userID', () => userIDChat);
  //         e.putIfAbsent('tinhTrangID', () => 2);
  //         e.putIfAbsent('tenTinhTrang', () => 'Đã nhận');
  //
  //         // connection
  //         //     .send(methodName: LocalVariable.updateTinhTrangTin, args: [e]);
  //       });
  //     });
  //
  //   lastOnline = DateTime.now().add(const Duration(seconds: -5));
  // }
}
