import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/loader_widget.dart';
import 'package:flutter_core_getx_dev/flutter_core_getx_dev.dart';

class LoadingWidget {
  LoadingWidget._();
  static final LoadingWidget instance = LoadingWidget._();

  void show() {
    Get.dialog(
      const LoaderWidget(),
      barrierDismissible: false,
    );
  }

  void hide() {
    Get.back();
  }

  void showSnackbar(
      {String title = 'Thông báo', required String noiDung, int showTime = 2}) {
    Get.snackbar(title, noiDung, duration: Duration(seconds: showTime));
  }

  void snackBarThongBao(String title, String message) {
    Get.snackbar(title, message,
        backgroundColor: Colors.green, colorText: Colors.white);
  }
}
