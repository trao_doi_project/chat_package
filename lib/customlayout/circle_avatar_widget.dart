import 'package:chat_package/base/image_variable.dart';
import 'package:chat_package/base/local_variable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core_getx_dev/app/widget/image_network_widget.dart';

class CircleAvatarWidget extends StatelessWidget {
  final urlImage;
  final double size;
  final Color backgroundColor;
  final bool isGroup;
  const CircleAvatarWidget(this.urlImage,
      {this.size = 50, this.backgroundColor = Colors.transparent, this.isGroup = false});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: backgroundColor,
      child: SizedBox(
        height: size,
        width: size,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(75),
            child: ImageNetworkWidget(
              packageName: LocalVariable.namePackage,
                urlImageNetwork: urlImage,
                imageAssetDefault: isGroup ? ImageVariable.groupAvatar : ImageVariable.person)),
      ),
    );
  }
}
